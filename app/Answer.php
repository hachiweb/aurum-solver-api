<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    //fetch data 
    protected $fillable = ['questioner_id','answerer_id','subjects','questions','question_attach','ans_attach','title','ans_title','answers','desired_price','post','duedate','delivery_accepted'];

    //user detail
    public function userDetail()
    {
        return $this->belongsTo('App\AppUsers','answerer_id','id');
    }
    public function answererDetail()
    {
        return $this->belongsTo('App\AppUsers','answerer_id','id');
    }
    public function questionDetail()
    {
        return $this->belongsTo('App\Question','question_id','id');
    }
    public function questionerDetail()
    {
        return $this->belongsTo('App\AppUsers','questioner_id','id');
    }

  	//category details
 	public function categoryDetail()
    {
        return $this->belongsTo('App\Category','category_id','id');
    }

  	//is reviewed
    public function isReviewed()
    {
        return $this->belongsTo('App\Feedback','id','rated_for_id');
    }
    // Answer

}
