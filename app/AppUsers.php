<?php

namespace App;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class AppUsers extends Model
{
    //App users 
    protected $fillable = ['user_email','first_name','whoami','last_name','dob','profile_pic','role','user_type','paypalaccount','stripe_account'];
    protected $table = 'apps_user';
    
    public function category()
    {
        return $this->belongsTo('App\Category','category_id','id');
    }
    
    public function userFeedback()
    {
        return $this->hasMany('App\Feedback','rated_to','id');
    }

    //user answer
    public function userAnswer()
    {
        return $this->hasMany('App\Answer','answerer_id','id');
    }

    //user purchased answer
    public function userAnswerPurchased()
    {
        return $this->hasMany('App\Answer','answerer_id','id');
    }

    //user questions
    public function userQuestion()
    {
        return $this->hasMany('App\Question','user_id','id');
    }

    //user earnings
    public function userEarning()
    {
        return $this->hasMany('App\Transaction','to','id');//->select('amount',DB::raw('sum(amount) as user_earning'))->groupBy('id');
    }
   
}
