<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //fetch all data from backend
    protected $fillable = ['subjects'];
    protected $table = 'category';

    public function quizQuestions()
    {
        return $this->hasMany('App\Quizze','category_id','id');
    }

    /***
    	@Get category quiz count
    **/
 	public function quizCount()
    {	
        return $this->quizQuestions()->count();
    }

 	/***
    	@categoryQuiz quiz answer for user
    **/
    public function attemptHistory()
    {
        return $this->hasMany('App\Quiz_answer','category_id','id');
    }

    public function categoryQuiz()
    {
        return $this->hasMany('App\Quizze','category_id','id');
    }
    public function totalQuiz()
    {
        return $this->hasMany('App\Quizze','category_id','id');
    }
}
