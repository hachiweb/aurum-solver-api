<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    //Quizzes questions
    protected $fillable = ['rating','rated_to','rated_by','rated_for','rated_for_id'];
    protected $table = 'feedbacks';

    public function userDetails()
    {
        return $this->belongsTo('App\AppUsers','rated_by','id');
    }

    public function reviewerDetails()
    {
        return $this->belongsTo('App\AppUsers','rated_by','id');
    }
   
}
