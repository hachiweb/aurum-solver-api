<?php

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 

class Get_User extends Model
{
    //App users 
    protected $fillable = ['user_email','first_name','whoami','last_name','dob','profile_pic','role','user_type','paypalaccount'];
    protected $table = 'apps_user';

}
