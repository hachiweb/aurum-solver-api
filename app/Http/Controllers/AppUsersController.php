<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Category;
use App\PublicAsset;
use App\Zone;
use App\AppUsers;
use App\Transaction;
use App\Quizze;
use App\User;
use App\OnDemand;
use Auth;
use Flasher\Prime\FlasherInterface;
use Illuminate\Support\Facades\Log;

class AppUsersController extends Controller
{
    // create new user
    public function createNewUser(Request $request, FlasherInterface $flasher)
    {
        $user = AppUsers::where('user_email', $request->user_email)->first();
        if(empty($user)){
            $crateUser = new AppUsers;
            $user_id = AppUsers::orderBy('id', 'desc')->first();
            $userId = ($user_id->id + 1);
            $file = $request->file('profile_pic');
            
            if (!empty($file))
            {
                try {
                    $filename = $file->getClientOriginalName();
                    if (!empty($filename)) 
                    {
                        $directory = public_path('user-profile-pic/id-' . $userId);
                        $file->move($directory, $filename);
                        $pic = env('APP_URL') . '/user-profile-pic/id-' . $userId . '/' . $filename;
                    }
                } 
                catch (\Exception $e) 
                {
                    return $e->getMessage();
                }
            } 
            else 
            {
                $pic = '';
            }
            $crateUser->user_email = $request->user_email;
            $crateUser->first_name = $request->first_name;
            $crateUser->last_name = $request->last_name;
            $crateUser->whoami = $request->about;
            $crateUser->dob = $request->date_of_birth;
            $crateUser->timezone = $request->timezone;
            $crateUser->profile_pic = $pic;
            $crateUser->subscribe_category = $request->subscribe_category;
            $crateUser->subscribe_notification_interval = $request->subscribe_notification_interval;
            $crateUser->user_type = $request->user_type;
            $crateUser->phone = $request->phone_number;
            $crateUser->password = Hash::make($request->password); 
            $result = $crateUser->save();
        
            if($result)
            {
                $flasher->addSuccess('New User added successfully!');
                return redirect()->back();  
            }
        } 
        else 
        {
            $flasher->addWarning('Email already exists!');
            return redirect()->back();
        } 
        // if($result)
        // {
        //     flash()->success('New User add Successfully!');
        //     return redirect()->back();  
        // }
        // } else{
        //     echo "warning";
        //     $notification = array(
        //         'message' => 'Email already exist!',
        //         'alert-type' => 'warning'
        //     );
        //     return back()->with($notification);
        // }   
    }

   public function createUser()
   {
       $data = Category::get();
       $zone = Zone::get();
       return view('user.adduser',compact('data','zone'));
   }
   
    public function usersList(){
        $data = AppUsers::orderBy('id','desc')->get();
        return view('user.app-user',compact('data'));
    }

    //block user list show
    public function blockUser(){
        $data = AppUsers::where('is_active',0)->get();
        return view('user.block-user',compact('data'));
    }


    // block user list
    // public function updateBlock($id, FlasherInterface $flasher){
    //     $update = AppUsers::where('id',$id)->update(array('is_active'=>0));       
    //     if($update){
    //         $flasher->addsuccess('User have Blocked!');
    //         return redirect()->back();               
    //     } 
    // }
    
    public function updateBlock($id){
       $update = AppUsers::where('id',$id)->update(array('is_active'=>0));
        if($update){
            return response()->json(['success' => true]);
        }else{
            return response()->json(['Something went wrong']);
        }
    }

    //unblock  
    // public function unblock($id, FlasherInterface $flasher){
    //     $update = AppUsers::where('id',$id)->update(array('is_active'=>1));
    //     $flasher->addsuccess('User have activate!');
    //     return redirect()->back();
         
    // }

    public function unblock($id){
       $update = AppUsers::where('id',$id)->update(array('is_active'=>1));
        if($update){
            return response()->json(['success' => true]);
        }else{
            return response()->json(['Something went wrong']);
        }
    }

    // delete user form dashboard list
    
    // public function deleteUser($id , FlasherInterface $flasher){
    //     $res=AppUsers::find($id)->delete();
    //     if($res){
    //         $flasher->addsuccess('User deleted');
    //         return redirect()->back(); 
    //     }else{
    //         $flasher->adderror('Something went wrong');
    //     return redirect()->back();
    //     } 
    // }
   
    public function deleteUser($id){
        $res=AppUsers::find($id)->delete();
        if($res){
            return response()->json(['success' => true]);
        }else{
            return response()->json(['Something went wrong']);
        }
    }

    // user Profile
    public function myProfile(){
        $data = AppUsers::find(Auth::user()->id);
        return view('user.my-profile',compact('data'));
    }

    public function editProfile(){
        return view('user.edit-profile');
    }
    
    public function myTrasactionHistory(){
        $id = Auth::user()->id;
        $data=Transaction::with('fromUserTransaction')->with('toUserTransaction')->where('id',$id)->get();
        return view('user.my-transaction',compact('data'));
    
    }
    public function savePaypal(Request $request){
        //  print_r($request->all());
        //  exit;
        $validator =Validator::make($request->all(),[
            'email_paypal'=>'required',           
        ]);
        if($validator->fails()){
            flash()->error('Paypal email feild is required!');
            return redirect()->back();
        }
        flash()->success('Paypal email has been set successfully.');
        return redirect()->back();
        // print_r($request->all());
    }

    // Both
    public function createBothUser(Request $request , FlasherInterface $flasher)
    {
        $user=AppUsers::where('user_email',$request->buser_email)->first();
        $user1=User::where('email',$request->buser_email)->first();
        if(empty($user) || empty($user1)){
            $crateUser=new AppUsers;
            $user_id=AppUsers::orderBy('id', 'desc')->first();
            $userId=($user_id->id +1);
            $file = $request->file('bprofile_pic');
            if(!empty($file)){
                try{
                    $filename=$file->getClientOriginalName();
                    if(!empty($filename))
                    {
                        $directory = public_path('user-profile-pic/id-' . $userId);
                        $file->move($directory, $filename);
                        $pic = env('APP_URL') . '/user-profile-pic/id-' . $userId . '/' . $filename;
                    } 
                }
                catch (\Exception $e) 
                {
                    return $e->getMessage();
                }
            }
            else
            {
                $pic = ''; 
            } 
            $user = new User;
            $user->email = $request->buser_email;
            $user->name = $request->bfirst_name;
            $user->password = Hash::make($request->bpassword);
            
            $crateUser->user_email = $request->buser_email;
            $crateUser->first_name = $request->bfirst_name;
            $crateUser->last_name = $request->blast_name;
            $crateUser->whoami = $request->babout;
            $crateUser->dob = $request->bdate_of_birth;
            $crateUser->timezone = $request->btimezone;
            $crateUser->profile_pic = $pic;
            $crateUser->subscribe_category = $request->bsubscribe_category;
            $crateUser->subscribe_notification_interval = $request->bsubscribe_notification_interval;
            $crateUser->user_type = $request->buser_type	;
            $crateUser->phone = $request->bphone_number;
            $crateUser->password = Hash::make($request->bpassword); 
            $result = $crateUser->save();
            $user->save();
            if($result)
            {
                $flasher->addsuccess('New User add Successfully!');
                return redirect()->back();    
            }
            else
            {
                $flasher->addwarning('Something went wrong.');
                return redirect()->back();
            }
        }
        else
        {
            $flasher->addwarning('Email id already exist.');
            return redirect()->back();
        }  
    }

    // public function createDasboardUser(Request $request,FlasherInterface $flasher)
    // {
    //     $user1=User::where('email',$request->buser_email)->first();
    //     if( empty($user1))
    //     {    
    //         $user = new User;
    //         $user->email = $request->demail;
    //         $user->name = $request->dfirst_name;
    //         $user->password = Hash::make($request->dpassword);    
    //         $result = $user->save(); 
    //         if($result)
    //         {
    //             $flasher->addsuccess('New User add Successfully!');
    //             return redirect()->back();    
    //         }
    //         else
    //         {
    //            $flasher->addwarning('Something went wrong.');
    //             return redirect()->back();
    //         }
    //     }
    //     else
    //     {
    //         $flasher->addwarning('Email id already exist.');
    //         return redirect()->back();
    //     }  
    // }

    public function createDasboardUser(Request $request, FlasherInterface $flasher)
    {
        $existingUser = User::where('email', $request->demail)->first();

        if (!$existingUser) 
        {
            $user = new User;
            $user->email = $request->demail;
            $user->name = $request->dfirst_name;
            $user->password = Hash::make($request->dpassword);

            try 
            {
                $user->save();
                $flasher->addSuccess('New User added successfully!');
            } 
            catch (\Exception $e) 
            {
                $flasher->addWarning('Something went wrong. Please try again.');
            }
            
            return redirect()->back();
        } 
        else 
        {
            $flasher->addWarning('Email ID already exists.');
            return redirect()->back();
        }
    }

    public function onDemandConnect(Request $request){
        $valid = Validator::make($request->all(),[
            'user_id' => 'required',
            'tutor_id' => 'required',
            'select_duration' => 'required',
        ]);
        if($valid->fails()){
            return response()->json($valid->errors()->toJson(), 400);
        }

        $user = AppUsers::find($request->user_id);
        $tutor = AppUsers::find($request->tutor_id);

        if($user->count()<1){
            return response()->json(['status'=>'error','message'=> 'User not found.'], 404);
        }
        if($tutor->count()<1){
            return response()->json(['status'=>'error','message'=> 'Tutor not found.'], 404);
        }
        else{
            $ondemandinit = new OnDemand();
            $ondemandinit->user_id = $request->user_id;
            $ondemandinit->tutor_id = $request->tutor_id;
            $ondemandinit->select_duration = $request->select_duration;
            $ondemandinit->total_cost = number_format((float)0.75*(($request->select_duration*$tutor->hourly_rate)/60),2,'.','');
            $ondemandinit->save();
            // Add transaction
            $transaction = new Transaction();
            $transaction->from = $request->user_id;
            $transaction->to = $request->tutor_id;
            $transaction->currency = 'USD';
            $transaction->amount = $ondemandinit->total_cost;
            $transaction->transaction_id = 'AS_TXN_'.rand('111111111','999999999');
            $transaction->description = 'On-demand tutoring';
            $transaction->status = 'paid';
            $transaction->save();
            return response()->json(['status'=>'success','message'=> 'ODT session has been created.','odtid'=>$ondemandinit->id], 200);

        }
    }

    public function onDemandDisconnect(Request $request){
        $valid = Validator::make($request->all(),[
            'odtid' => 'required',
            'effective_duration' => 'required',
        ]);
        if($valid->fails()){
            return response()->json($valid->errors()->toJson(), 400);
        }
        else{
            $ondemandinit = OnDemand::find($request->odtid);
            if($ondemandinit->count()<1){
                return response()->json(['status'=>'error','message'=> 'ODT record not found.'], 404);
            }
            else{
                $user = AppUsers::find($ondemandinit->user_id);
                $tutor = AppUsers::find($ondemandinit->tutor_id);
                $ondemandinit->effective_duration = $request->effective_duration;
                $ondemandinit->effective_cost = number_format((float)0.75*(($request->effective_duration*$tutor->hourly_rate)/60),2,'.','');
                $ondemandinit->save();
                // Add transaction
                $transaction = new Transaction();
                $transaction->from = $ondemandinit->tutor_id;
                $transaction->to = $ondemandinit->user_id;
                $transaction->currency = 'USD';
                $transaction->amount = $ondemandinit->total_cost-$ondemandinit->effective_cost;
                $transaction->transaction_id = 'AS_TXN_'.rand('111111111','999999999');
                $transaction->description = 'On-demand tutoring refund';
                $transaction->status = 'paid';
                $transaction->save();
                return response()->json(['status'=>'success','message'=> 'ODT session has been updated.','odtid'=>$ondemandinit->id], 200);
            }   
        }
    }

}