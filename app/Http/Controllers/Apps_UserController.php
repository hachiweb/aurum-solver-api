<?php
namespace App\Http\Controllers;
use App\AppUsers;
use App\Get_User;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
// use Laravel\Socialite\Facades\Socialite;
// use JWTAuth;
// use Socialite;
// use Tymon\JWTAuth\Exceptions\JWTException;
class Apps_UserController extends Controller
{
    
    public function appsUser(Request $request){
        $random_id = str_random(70);
        $valid = Validator::make($request->all(),[
            'user_email' =>'required|string|email|max:255|',
            'password' =>'required|string|min:8',
            'type'=>'required',
        ]);
        if($valid->fails()){
            return response()->json($valid->errors()->toJson(), 400);
        }
        $user = Get_User::select('*')->where('user_email', $request->user_email)->first();
        // return response()->json($user, 200);
        
        if(!$user){
            $apps_user = new AppUsers;           
            $apps_user->verification_token = $random_id;
            $apps_user->user_email = $request->user_email;
            $apps_user->registration_type = $request->type;
            $apps_user->password = Hash::make($request->password);
            $apps_user->is_verified = 1;
            $apps_user->is_active = 1;
            $apps_user->registration_token = isset($request->token)?$request->token:'NA';   
            
            $result = $apps_user->save();
            
            return response()->json(['status'=>'200','status'=>'success','message'=> 'Successfully registered', 'data'=>AppUsers::orderBy('id','desc')->first()], 200);  
        }
        else{
            return response()->json(['status'=>'error','message'=> 'Email already exits'], 400);
        }     
    }  
    
     
    function mailVerify(Request $r) {
        $token_id = $r->token;
        // $id = Get_User::find($token_id);
        $res = DB::table('apps_user')->where('verification_token', '=', $token_id)->update(array('verification_token' => 'NULL','is_verified'=>1,'is_active'=>1));
        if($res==true){
            return response()->json(['status'=>'success','message'=> 'Verified '], 200);
        }
        else{
            return response()->json(['status'=>'error','message'=> 'Token does not exits'], 400);
        }
    }

    public function getUser($registration_token){
        if(!isset($registration_token)){
            return response()->json(['status'=>'error', 'message'=>'Missing token id'], 200);
          }
      
       $user = Get_User::where('registration_token',$registration_token)->first();
       if(empty($user)){
        return response()->json(['status'=>'error', 'message'=>'Invalid registration token'], 400);
        }
        return response()->json(['status'=>'success', 'message'=>'User profile','data'=>$user], 200);
    }
}