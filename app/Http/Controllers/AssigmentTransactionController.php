<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Withdrawal;
use App\Transaction;
use App\Answer;
use Auth;
class AssigmentTransactionController extends Controller
{
    public function cancellAssignmentList(){
        $assignment = Question::with('userDetail')->with('tutorDetail')->with('assignmentAnswer')->where('is_cancelled',1)->where('delivery_accept',0)->get();
        return view('user.refund-amount',compact('assignment'));
    }

    public function withdrawalRequest(){
        $amount = Question::where('delivery_accept',1)->where('is_paid','1')->sum('admin_fee');
        $tamount = Answer::where('delivery_accepted',1)->where('purchase','1')->sum('admin_fee');
        $withdarawal = withdrawal::where('is_approved',1)->where('user_id',Auth::user()->id)->sum('amount');
        $total_balance = (($tamount + $amount) - $withdarawal);
        $withdrawal = Withdrawal::with('requesterUser')->get(); 
        
        return view('user.withdrawal-request',compact('total_balance','withdrawal'));
    }

    public function refundAccept($id){
        $update = Question::where('id',$id)->update(array('is_approved' =>1));
       
            if($update){
                toastr()->success('Refund request has been approved!');
                return redirect()->back();
            }else{
                toastr()->error('Somthing went wrong!');
                return redirect()->back();
            }
        
    }
    public function refundReject($id){
        $update = Question::where('id',$id)->update(array('is_approved' =>2));
            if($update){
                toastr()->warning('Refund request has been approved!');
                return redirect()->back();
            }else{
                toastr()->error('Somthing went wrong!');
                return redirect()->back();
            }
        
    }

}