<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Answer;
use App\Category;
use App\AppUsers;
use Flasher\Prime\FlasherInterface;

class AssignmentQuestionAnswerController extends Controller
{
    public function assignment(){
        $data = Question::with('tutorDetail')->with('userDetail')->with('assignmentAnswer')->withCount('totalAnswers')->where('is_assigned',1)->get();
        // print_r($data);exit;
        return view('question.assignment',compact('data'));
    }

    public function allQuestionAnswer(){
        $data = Question::with('tutorDetail')->with('userDetail')->withCount('totalAnswers')->get();
        return view('question.all-question-answer',compact('data'));
    }

    public function cancelledAssignment(){
        $data = Question::get();
        return view('user.refund-amount',compact('data'));
    }

    public function editAssignment($id){
        $question = Question::with('assignerDetail')->with('categoryDetail')->where('id',$id)->first();
        $answer = Answer::with('userDetail')->where('question_id',$id)->first();  
        return view('question.edit-assignment',compact('answer','question'));
    }
    public function editAllQuetion($id){
        $question = Question::with('assignerDetail')->with('categoryDetail')->where('id',$id)->first();
        $category = Category::find($question->category_id)->first()->subjects;   
        $answers = Answer::with('userDetail')->where('question_id',$id)->get();
        return view('question.edit-all-question',compact('question','answers'));
    }

    // delete postQuestion
    // public function deletePostQuestion($id ,FlasherInterface $flasher){
    //     $res = Question::find($id)->delete();
    //     if($res){
    //         $flasher->addsuccess('Post question has been removed!');
    //         return redirect()->back();
    //     }else{
    //         $flasher->adderror('Something went wrong!');
    //         return redirect()->back();
    //     }
    // }
    public function deletePostQuestion($id){
        $res = Question::find($id)->delete();
        if($res){
            return response()->json(['success' => true]);
        }else{
            return response()->json(['Something went wrong']);
        }
    }

    public function removeAnswer($id ,FlasherInterface $flasher){
        $res = Answer::find($id)->delete();
        if($res){
            $flasher->addsuccess('Answer has been removed!');
            return redirect()->back();
        }else{
            $flasher->adderror('Something went wrong!');
            return redirect()->back();
        }
    }

    public function answerUpdate(Request $request ,FlasherInterface $flasher){
        $update = Answer::where('id',$request->answer_id)->update(array('title' =>$request->tittle_answer,'answer' =>$request->answer));
        if($update){
            $flasher->addsuccess('Answer has been updated!');
            return redirect()->back();
        }else{
            $flasher->adderror('Something went wrong!');
            return redirect()->back();
        }
         
    }
    public function quetionUpdate(Request $request ,FlasherInterface $flasher){
        $update = Question::where('id',$request->queston_id)->update(array('title' =>$request->title,'question' =>$request->question));
        if($update){
            $flasher->addsuccess('Question has been updated!');
            return redirect()->back();
        }else{
            $flasher->adderror('Something went wrong!');
            return redirect()->back();
        }
         
    }

    // remove question
    // public function removeQuestion($id ,FlasherInterface $flasher){
    //     $res = Question::find($id)->delete();
    //     if($res){
    //         $flasher->addsuccess('Assignment has been removed!');
    //         return redirect()->back();
    //     }else{
    //         $flasher->adderror('Something went wrong!');
    //         return redirect()->back();
    //     }
    // }
    public function removeQuestion($id){
        $res = Question::find($id)->delete();
        if($res){
            return response()->json(['success' => true]);
        }else{
            return response()->json(['Something went wrong']);
        }
    }

    public function cancelAssignment($id ,FlasherInterface $flasher){
        // Question::where('id',$id)->update(array('is_assigned' =>$request->title,'private_question' =>$request->question));
        // $result = $res->is_assigned =
        $res = 0; 
        if($res){
            $flasher->addsuccess('Assignment has been removed!');
            return redirect()->back();
        }else{
            $flasher->adderror('Something went wrong!');
            return redirect()->back();
        }
    }
}