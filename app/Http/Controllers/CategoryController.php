<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Quizze;
use App\Quiz_answer;
use App\Get_User;
use App\Category;
use App\Feedback;
use App\AppUsers;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
   
    // check quiz attempt
    public function categoryQuizzes(Request $request){
        $validator =Validator::make($request->all(),[
            'category_id'=>'required|numeric', 
            'user_id'=>'required|numeric',    
        ]);
        
       if($validator->fails()){
        return response()->json($validator->errors()->toJson(), 400);
       }
       $user = AppUsers::find($request->user_id);
       $category = Category::find($request->category_id);
       if(empty($user)){
        return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
        }
        if(empty($category)){
            return response()->json(['status'=>'error', 'message'=>'Invalid category id'], 404);
        }
        $check = Quiz_answer::where('category_id',$request->category_id)->where('user_id',$request->user_id)->first();
        if(!empty($check)){
            $data = Quiz_answer::with('quizQuestions')->withCount('categoryQuiz')->where('category_id',$request->category_id)->where('user_id',$request->user_id)->get();
            return response()->json(['status'=>'success','message'=>'Quiz category data with user attempt history','data'=>$data ],200);   
        }
         else{
            $data = Category::with('quizQuestions')->withCount('categoryQuiz')->where('id',$request->category_id)->get();
            return response()->json(['status'=>'success','message'=>'Quiz category','data'=>$data ],200);    
        } 
    }
// User list by subscription category
    public function usersByCategory(Request $request){
        if(!isset($request->category_id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 400);
        }
        $category = Category::find($request->category_id);
        if(empty($category)){
        return response()->json(['status'=>'error', 'message'=>'Invalid category id'], 404);
        }else{ 
            $user=AppUsers::with('category')->where('category_id',$request->category_id)->get();
            if($user->isNotEmpty()){
                foreach($user as $key=>$val){
                    $ans[$key]['id'] = $val->id;
                    $ans[$key]['user'] = !empty(AppUsers::find($val->id))?AppUsers::withCount('userQuestion')->withCount('userAnswer')->withCount(['userAnswerPurchased' => function($q) {
                $q->where('purchase', '1');}])->withCount(['userEarning AS user_earning' => function($q) {
                         $q->select(DB::raw('sum(amount)'))->where('withdrawal', '0');
                        // return $q->where('to', $user_id)->where('withdrawal', '0');
                    }])->where('is_verified', '1')->where('is_active', '1')->where('id',$val->id)->first():'';
                    $rating = Feedback::where('rated_to',$val->id)->get();
                    $rating_count = $rating->count()!=0?$rating->count():-1;
                    $average_rating = round($rating->sum("rating")/$rating_count, 1);
                   
                    if(!empty($ans[$key]['user'])){
                        $ans[$key]['user']['average_rating'] = $average_rating;   
                    }
                }
                $data['user_list'] = $ans;
                return response()->json(['status'=>'success', 'data'=>$data], 200);
            }
            else{
                return response()->json(['status'=>'error', 'message'=>'User have not subscribed'], 404);
            }
           
        }
        
    }


// Submit answer quiz 
    public function answerQuiz(Request $request){
        $validator =Validator::make($request->all(),[
            'category_id'=>'required|numeric', 
            'user_id'=>'required|numeric', 
            'correct_answer'=>'required|numeric',   
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = AppUsers::find($request->user_id);
        $category = Category::find($request->category_id);
        if(empty($user)){
         return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
         }
         if(empty($category)){
             return response()->json(['status'=>'error', 'message'=>'Invalid category id'], 404);
         }
        $check = Quiz_answer::where('category_id',$request->category_id)->where('user_id',$request->user_id)->first();
        $total_quiz=!empty(Quizze::where('category_id',$request->category_id))||Quizze::where('category_id',$request->category_id)->count()==='0'?Quizze::where('category_id',$request->category_id)->count():-1 ;
        $total_quiz=$total_quiz==0?-1:$total_quiz;
          
        if(empty($check)){
            $percent=round((($request->correct_answer*100)/$total_quiz),2); 
            $quiz = new Quiz_answer;
            $quiz->user_id = $request->user_id;
            $quiz->correct_answer =  $request->correct_answer;
            $quiz->category_id = $request->category_id;
            $quiz->percent =$percent;
            $quiz->total_attempt = 1;
            if($percent>=$category->min_percent){
                $quiz->is_passed = 1;    
            }
            $result = $quiz->save();
            $data =Quiz_answer::latest()->first();
            return response()->json(['status'=>'success','message'=>'Quizz attempt record','data'=>$data],200);   
        }else{  
            
            if($check->percent>=$category->min_percent){
                return response()->json(['status'=>'success','message'=>'You have already passed the quizz','data'=> $check],200);
            }
            else{  
                if($check->total_attempt>2){
                return response()->json(['status'=>'success','message'=>'Attempt limit exceeded','data'=> $check],200);
            }
      
            $total_attempt = ($check->total_attempt + 1);
                $percent=round((($request->correct_answer*100)/$total_quiz),2);  
                    
                if($percent>=$category->min_percent){
                    $pass = 1;    
                }else{
                    $pass = 0; 
                }
                $update = Quiz_answer::where('category_id',$request->category_id)->where('user_id',$request->user_id)->update(array('category_id' => $request->category_id,'user_id' =>$request->user_id, 'correct_answer' => $request->correct_answer,'total_attempt' => $total_attempt,'percent' => $percent,'is_passed' => $pass));
                if($update):
                    $data =Quiz_answer::latest()->first();
                    return response()->json(['status'=>'success','message'=>'Record saved','data'=>$data ],200);
                endif;
            }
        }      
    }
    public function category(Request $request){
        if(!isset($request->category_id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 400);
        }
        $category = Category::find($request->category_id);
        if(empty($category)){
            return response()->json(['status'=>'error', 'message'=>'Invalid category id'], 404);
        }else{ 
            $data =Quizze::where('category_id',$request->category_id)->count();
            return response()->json(['status'=>'success','Category'=>$category, 'total_questions'=>$data], 200);
        }
    }


    //  //Select all category
    //  public function categoryAll(){
    //     $result = Category::all();
    //     foreach($result as $key=>$val){
    //         $ans[$key]['id'] =$val->id;
    //         $ans[$key]['subjects'] =$val->subjects;
    //         $ans[$key]['duration'] =$val->duration;
    //         $ans[$key]['min_percent'] =$val->min_percent;
    //         $question = Quizze::where('category_id',$val->id)->get();
    //         $question_count = $question->count()!=0?$question->count():'';
    //         $ans[$key]['total_questions'] = $question_count;  
             
    //     }
    //     $data['category'] = $ans;
    //     return response()->json(['status'=>'success', 'data'=>$data], 200);
    // }
    public function categoryAll(){
        $result = Category::withCount('quizQuestions')->get();
        return response()->json(['status'=>'success','data'=>$result], 200);
    }

    //User category all
    public function usercategoryAll(Request $request){
        $user_id = $request->user_id;
        $result = Category::withCount('quizQuestions')->with(['attemptHistory' => function($q) use ($user_id){
        $q->where('user_id', $user_id);}])->get();
      
        return response()->json(['status'=>'success','data'=>$result], 200);
    }
    // Create Category
    public function categoryCreate(Request $request){
        $validator =Validator::make($request->all(),[
            'category_name'=>'required|string',
            'duration'=>'required|numeric',
            'min_percent'=>'required|numeric',
          ]);
              
        if($validator->fails()){
        return response()->json($validator->errors()->toJson(), 400);
        }
        $result = Category::where('subjects',$request->category_name)->first();
       
        if(!empty($result)){
            return response()->json(['status'=>'error','message'=>'Category already exist.'], 403);
        }
        else{
            $cat = new Category;
            $cat->subjects = $request->category_name;
            $cat->duration =  $request->duration;
            $cat->min_percent = $request->min_percent;
            $result=$cat->save();
            if($result){
                $data=Category::where('subjects',$request->category_name)->first();
                return response()->json(['status'=>'success','message'=>'Successfully create Category.','data'=>$data], 200);
            }
        }
    }
}