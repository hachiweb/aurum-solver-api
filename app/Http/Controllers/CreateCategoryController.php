<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Category;
use App\Quizze;
class CreateCategoryController extends Controller
{
    //Category sections
    public function createCategory(Request $request){
        $subjects = $request->category_name;
        $duration =  $request->duration_time;
        $min_percent = $request->pass_percent;
        if(empty($subjects) || empty($duration) || empty($min_percent)){
            echo "warning";
            $notification = array(
                'message' => 'Fields is required!',
                'alert-type' => 'warning'
            );
            return back()->with($notification);  
        }    
        else{
            $cat = Category::where('subjects', $request->category_name)->first();
            if(empty($cat)){
                $category = new Category;
                $category->subjects = $request->category_name;
                $category->duration =  $request->duration_time;
                $category->min_percent = $request->pass_percent;
                $status = $category->save();
                if($status){
                    echo "success";
                    $notification = array(
                        'message' => 'Category created!',
                        'alert-type' => 'success'
                    );
                    return back()->with($notification);
                }
            }else{
                echo "warning";
                $notification = array(
                    'message' => 'Category already exist!',
                    'alert-type' => 'warning'
                );
                return back()->with($notification);
            }
        } 
       
    }

    // retrieve all category
    public function allCategory(){
        $cat = Category::all();
        return view('category.manage-category',['data'=>$cat]);
    }

    // edit category
    public function edit(Request $request){
        $data = Category::find($request->id);
        return view('category.update')->with('data',$data); 
    }

    // update category
    public function updateCategory(Request $req){
        $subjects = $req->name_category;
        $duration =  $req->duration_time;
        $min_percent = $req->passing_marks;
        if(empty($subjects) || empty($duration) || empty($min_percent)){
            echo "warning";
            $notification = array(
                'message' => 'Fields is required!',
                'alert-type' => 'warning'
            );
            return back()->with($notification);  
        }else{
            $update = Category::where('id',$req->id)
            ->update(array('subjects' => $req->name_category,'duration' => $req->duration_time,'min_percent' => $req->passing_marks)); 
            echo "success";
            $notification = array(
                'message' => 'Category updated!',
                'alert-type' => 'success'
            );
            return back()->with($notification);  
        } 
    }

    // delete category
    // public function delete($id){
    //     $res=Category::where('id',$id)->delete();
    //     if($res){
    //         toastr()->success('Successfully deleted!');
    //         return redirect()->back();
    //     }else{
    //         toastr()->error('Something went wrong');
    //         return redirect()->back();
    //     }  
    // }  
    
    public function delete($id){
        $res=Category::where('id',$id)->delete();
        if($res){
            return response()->json(['success' => true]);
        }else{
            return response()->json(['Something went wrong']);
        }
    }
}