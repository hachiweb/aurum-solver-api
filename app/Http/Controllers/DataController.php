<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Tymon\JWTAuth\Facades\JWTAuth;
// use Tymon\JWTAuth\Facades\JWTFactory;
use App\User;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{       
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token,$expTTL,$response)
    {   
        // return ['HTTP_Authorization' => "Bearer {$token}"];
        return response()->json([
            'message' => $response,
            'access_token' => $token,
            'token_type'   => 'bearer',
            // 'exp'   => $expTTL,
        ]);
    }

    public function generateToken(Request $r) 
    {   
        $response = array();
        if(isset($r->request_token) && $r->request_token == 'hachi#123'){
            $response["success"] = TRUE;
            $response["status"] = 200;
            $factory = JWTFactory::customClaims([
                'sub'   => env('API_ID'),
            ]);
            $expTTL = 60*60; //minutes
            $payload = $factory->make();
            // JWTAuth::factory()->setTTL($expTTL);
            $token = JWTAuth::encode($payload);

            User::updateOrCreate(
                ['id' => 1],
                ['access_token' => $token->get()]
            ); 
            
            return $this->respondWithToken($token->get(), $expTTL, $response);
        }
        else{
            $response["success"] = FALSE;
            $response["status"] = 401;
            $response["reason"] = 'Invalid request token';
            return response()->json($response,401);
        }

    }

    // Destroy Token
    public function destroyToken(Request $r) 
    {   
        $response = array();
        if(isset($r->request_token) && $r->request_token == 'Vw4&2tw4'){
            $response["success"] = TRUE;
            $response["status"] = 200;
            $user = User::find(1);
            $token = $user->access_token; //JWTAuth::parseToken()->getToken();
            \JWTAuth::manager()->invalidate(new \Tymon\JWTAuth\Token($token), $forceForever = false);
            return response()->json($response,200);
        }
        else{
            $response["success"] = FALSE;
            $response["status"] = 401;
            $response["reason"] = 'Invalid request token';
            return response()->json($response,401);
        }

    }
}