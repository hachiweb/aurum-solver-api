<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Auth;
use Illuminate\Support\Facades\Hash;
class FirebaseController extends Controller
{
    //Reset password
    public function resetPassword(Request $request){
        return view('password.resetpassword')->with('oobCode',$request->input('oobCode'));
    }
    public function savePassword(Request $request){
        $oobCode = $request->oobCode;
        $newPassword =Hash::make($request->password);
        $invalidatePreviousSessions = false; 
        //         echo $oobCode;
        //         echo "<br>";
        //         echo $newPassword;
        // exit;
        // toastr()->success('Data has been saved successfully!');
        // return redirect()->back();
        try {
            $auth->confirmPasswordReset($oobCode, $newPassword, $invalidatePreviousSessions);
        } catch (\Kreait\Firebase\Exception\Auth\ExpiredOobCode $e) {
            // Handle the case of an expired reset code
        } catch (\Kreait\Firebase\Exception\Auth\InvalidOobCode $e) {
            // Handle the case of an invalid reset code
        } catch (\Kreait\Firebase\Exception\AuthException $e) {
            // Another error has occurred
        }
       

    }
}