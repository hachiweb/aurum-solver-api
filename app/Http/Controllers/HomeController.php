<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Question;
use App\Answer;
use App\AppUsers;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $answer = Question::where('is_assigned',1)->get()->count();
        $question = Question::get()->count();
        $users = AppUsers::get()->count();
        return view('home',compact('users','question','answer'));
    }
    // logout
    public function logout()
    { 
        Auth::logout();
        return view('comming-soon');
    }
}