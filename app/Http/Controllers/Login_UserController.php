<?php
namespace App\Http\Controllers;
use App\AppUsers;
use App\Answer;
use App\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
Use \Carbon\Carbon;
// use JWTAuth;
// use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
// use PHPMailer\PHPMailer\PHPMailer;
// use Socialite;
class Login_UserController extends Controller
{
    // check login type and login 
    public function loginUser(Request $request){
        $validator = Validator::make($request->all(), [
            'user_email' => 'required|string|email|max:255',
            'type'=>'required'
        ]);

        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }
      
         $email = AppUsers::select('*')
                ->where('user_email', '=', $request->user_email)
                ->where('registration_type', '=', $request->type)
                ->count();
        if($email > 0){
             $isverify = AppUsers::select('*')
                ->where('user_email', '=', $request->user_email)
                ->where('registration_type', '=', $request->type)
                ->first();
                
            if($isverify->registration_type=='email'){
                $is_verified = AppUsers::select('*')
                    ->where('user_email', '=', $request->user_email)
                    ->where('registration_type', '=', $request->type)
                    ->where('is_verified', '=', '1')
                    ->count();
                if(!$is_verified > 0){
                    return response()->json(['status'=>'error','message'=> 'Email have not varified!'], 404);
                }else{
                    $is_active = AppUsers::where('user_email', '=', $request->user_email)
                    ->where('registration_type', $request->type)
                    ->where('is_active', '1');
                    if($is_active->count() >0){
                        $validator = Validator::make($request->all(), [
                            'password' =>'required|string|min:8',
                        ]);
                
                        if($validator->fails()){
                                return response()->json($validator->errors()->toJson(), 400);
                        }
                        $user_id = $is_active->first()->id;
                        $rating = Feedback::where('rated_to',$user_id)->get();
                            $rating_count = $rating->count()!=0?$rating->count():-1;
                            $average_rating = round($rating->sum("rating")/$rating_count, 1);
                        $user = AppUsers::withCount('userQuestion')->withCount('userAnswer')->withCount(['userAnswerPurchased' => function($q){
                                $q->where('purchase', '1');}])->withCount(['userEarning AS user_earning' => function($q){
                                    $q->select(DB::raw('sum(amount)'))->where('withdrawal', '0');
                                    // return $q->where('to', $user_id)->where('withdrawal', '0');
                                }])->withCount(['userFeedback AS average_rating' => function($q) {
                                    $q->select(DB::raw('avg(rating)'));
                                }])
                        ->where('user_email', $request->user_email)
                        ->where('registration_type', $request->type)
                        ->where('is_verified', '1')
                        ->where('is_active', '1')->first();
                             
                        $password = $request->password;
                        if((!$user) || (!Hash::check($password, $user->password))){
                            return response()->json(['status'=>'error','message'=> $user], 400);
                        }   

                        else
                        {  
                            // return $user->password;
                            $mytime = Carbon::now();
                            $curr_time = $mytime->toDateTimeString();
                            $user->last_login = $curr_time;
                            $user->save();
                            return response()->json(['status'=>'success','message'=> 'Logged in','data'=>$user], 200);
                        }   
                    }
                    else{
                        return response()->json(['status'=>'success','message'=>'Not Active'],200);
                    }
                }
            }
            else{
                $is_verified = AppUsers::where('user_email', '=', $request->user_email)
                    ->where('registration_type', $request->type)
                    ->where('is_verified', '1')
                    ->count();
                // print_r("you have select social");
                if($is_verified > 0){
                $is_active = AppUsers::where('user_email', $request->user_email)
                        ->where('registration_type', $request->type)
                        ->where('is_active', '1')
                        ->where('is_verified', '1');
                        if($is_active->count() > 0){
                            $user_id = $is_active->first()->id;
                            $rating = Feedback::where('rated_to',$user_id)->get();
                            $rating_count = $rating->count()!=0?$rating->count():-1;
                            $average_rating = round($rating->sum("rating")/$rating_count, 1);
                            $user = AppUsers::withCount('userQuestion')->withCount('userAnswer')->withCount(['userAnswerPurchased' => function($q){
                                $q->where('purchase', '1');}])->withCount(['userEarning AS user_earning' => function($q){
                                     $q->select(DB::raw('sum(amount)'))->where('withdrawal', '0');
                                    // return $q->where('to', $user_id)->where('withdrawal', '0');
                            }])->withCount(['userFeedback AS average_rating' => function($q) {
                        $q->select(DB::raw('avg(rating)'));
                    }])->where('user_email', $request->user_email)
                                ->where('registration_type', $request->type)
                                ->where('is_verified', '1')
                                ->where('is_active', '1')->first();
                                
                                $mytime = Carbon::now();
                                $curr_time = $mytime->toDateTimeString();
                                $user->last_login = $curr_time;
                                $user->save();
                                return response()->json(['status'=>'success','message'=> 'Logged in','data'=>$user], 200);
                            }
                    else{
                        return response()->json(['status'=>'success','message'=>'Not Active'],404);
                        }
                    }
                    else{
                        return response()->json(['status'=>'success','message'=>'Email is not verified!'],404);
                    }
                }
             
            }
            else{
                return response()->json(['status'=>'error','message'=> 'User not found!'], 404);
            }      
         
    }
    // select all users 
    public function users(){
        $user = AppUsers::withCount('userQuestion')->withCount('userAnswer')->withCount(['userAnswerPurchased' => function($q){
                $q->where('purchase', '1');}])->withCount(['userEarning AS user_earning' => function($q){
                        $q->select(DB::raw('sum(amount)'))->where('withdrawal', '0');
                        // return $q->where('to', $user_id)->where('withdrawal', '0');
                    }])->where('is_verified', '1')->where('is_active', '1')->get();
        return response()->json(['status'=>'success','user'=>$user], 200);

    }
    
}

 