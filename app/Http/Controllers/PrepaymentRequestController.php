<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Question;
use App\Prepayments;
use App\Get_User;
use App\Answer;
use App\AppUsers;
use App\Transaction;
use App\Feedback;
use Illuminate\Support\Facades\DB;

class PrepaymentRequestController extends Controller
{
    //
    /***
    `Approve to prepayments
    **/
    public function updateRequest(Request $request){
        $validator =Validator::make($request->all(),[
            'prepayment_id'=>'required|numeric',  
            'status'=>'required|numeric|max:1',      
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
       
        $pre_request = Prepayments::find($request->prepayment_id);
        
        if(empty($pre_request)){
            return response()->json(['status'=>'error', 'message'=>'Invalid id'], 404);
        }else{
            $approve_status = $request->status;  
          
            $total_balance = Transaction::where('to',$pre_request->to_request)->where('status','paid')->where('withdrawal',0)->sum('amount');
            $total_expand = Transaction::where('from',$pre_request->to_request)->where('to', '!=',$pre_request->to_request)->where('status','paid')->where('withdrawal',0)->sum('amount');
            $total = number_format(($total_balance -  $total_expand),2);
            $pay_amount = number_format($pre_request->requested_amount,2); 
            //    print_r($pay_amount);
            //    exit;
            if($approve_status==1){
                if($total >= $pay_amount){  
                    $pre_request->approve_status = $approve_status;  
                    $pay = new Transaction;      
                    $pay->from = $pre_request->to_request;
                    $pay->to =  $pre_request->from_request;
                    $pay->amount = $pay_amount;
                    $pay->transaction_id = 'T_ACTION_AS_EJKJAGJKLQ';
                    $pay->question_id = $pre_request->question_id;
                    $pay->status = 'paid';
                    $result = $pay->save();
                    $pre_request->save();
                if($result == 1):
                    Prepayments::where('id',$request->prepayment_id)
                    ->update(array('approve_status' => 1));
                    $data =Transaction::with('prepaymentRequesterDetail')->where('to',$pre_request->from_request)->first();
                    return response()->json(['status'=>'success','message'=> 'Prepayment request accepted','Transaction_details'=>$data], 200);
                endif;
                    
                } else{
                    return response()->json(['status'=>'error','message'=> 'You do not have enough balance','pay_amount '=>$pay_amount,'available_balance'=> $total], 404); 
                } 
            } else{
                Prepayments::where('id',$request->prepayment_id)->update(array('approve_status'=>$request->status));
                    return response()->json(['status'=>'success','message'=> 'Prepayment request rejected'],200);
            }
        }
   
    }
    //******************** */Prepayments request by tutor ***************/    
    public function MyRequest(Request $request){
        if(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 400);
        }
          $user = AppUsers::find($request->user_id);
          
        if(empty($user)){
            return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
        }
        else{
            $assignment = Question::where('is_assigned',1)->where('tutor_id',$request->user_id)->get();
            if($assignment->isNotEmpty()){
                $pre=Prepayments::with('RequesterDetails')->with('payerDetails')->with('QuestionDetails')->where('from_request',$request->user_id)->get();
              //  $rating = Feedback::where('rated_to',$request->user_id)->get();
              //  $rating_count = $rating->count()!=0?$rating->count():-1;
              //  $average_rating = round($rating->sum("rating")/$rating_count, 1);

            
                if($pre->isNotEmpty())
                {
                    $rating = Feedback::where('rated_to',$request->user_id)->get();
                    $rating_count = $rating->count()!=0?$rating->count():-1;
                    $average_rating = round($rating->sum("rating")/$rating_count, 1);
                    return response()->json(['status'=>'success', 'data'=>$pre], 200);
                }
               else
               {
                return response()->json(['status'=>'error', 'message'=>'No create prepayment request.'], 404);
               }     
            }else{
                return response()->json(['status'=>'error', 'message'=>'No Assignment.'], 404);
            }
        }
    }
        
//See Requested prepayment
public function PreRequested(Request $request){
    if(!isset($request->user_id)){
        return response()->json(['status'=>'error', 'message'=>'Missing id'], 400);
    }
    $user = AppUsers::find($request->user_id);
    if(empty($user)){
        return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
    }
    else{
        $prerequest = Prepayments::where('to_request',$request->user_id)->get();
        if(empty($prerequest)){
            return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
        }else{
            $myrequested=Prepayments::with('RequesterDetails')->with('payerDetails')->with('QuestionDetails')->where('to_request',$request->user_id)->get();
            if($myrequested->isNotEmpty())
           {
                $rating = Feedback::where('rated_to',$request->user_id)->get();
                $rating_count = $rating->count()!=0?$rating->count():-1;
                $average_rating = round($rating->sum("rating")/$rating_count, 1);
                 return response()->json(['status'=>'success', 'data'=>$myrequested,'answerer_reviews'=>$average_rating ], 200);
           }
           else
           {
                return response()->json(['status'=>'error', 'message'=>'No prepayment requested.'], 404);
           }       
        }
    }
}


   


}