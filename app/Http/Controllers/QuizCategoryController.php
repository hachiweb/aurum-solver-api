<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Category;
use App\Quizze;
use Illuminate\Support\Facades\DB;

class QuizCategoryController extends Controller
{
    // All category quiz
    public function allQuizzes(){  
        $data = Category::withCount('totalQuiz')->get();
        $cat = Category::withCount('totalQuiz')->get();
        return view('quiz.manage-quiz',compact('data','cat'));
    }

     //Quiz sections
     public function quizzAllCreate(){
         
        $data = Category::withCount('totalQuiz')->get();
        return view('quiz.create-quiz',compact('data'));
    }
    
    public function selectQuizById($id){
        $data = Quizze::where('category_id',$id)->count();
        return response()->json(['data'=>$data]);
         
    }
  
     public function insertQuiz(Request $request){  
        foreach ($request->questions as $key=>$row)
        {   
            $category = new Quizze;
            $category->category_id = $request->category_id[$key];
            $category->questions = $request->questions[$key]; 
            $category->opt1 = $request->answer_a[$key]; 
            $category->opt2 = $request->answer_b[$key]; 
            $category->opt3 = $request->answer_c[$key]; 
            $category->opt4 = $request->answer_d[$key]; 
            $category->correct = $request->correct_answer[$key]; 
            $result = $category->save();      
        } 
        if($result == 1){
            echo "success";
            $notification = array(
                'message' => 'success! Quiz create successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        }else{
            echo "warning";
            $notification = array(
              'message' => 'Unable to create quiz!',
              'alert-type' => 'warning'
          );
          return back()->with($notification);
        } 
    }

    public function category(){
        $cat = Category::all();
        return view('category.create-quiz',['data'=>$cat]);
    }

    public function quizEdit($id){
        $data = Quizze::withCount('categoryQuiz')->where('category_id',$id)->get();
        $cat = Category::find($id);
        return view('quiz.edit-quiz',['data'=>$data,'category'=>$cat]); 

    }

    public function quizDelete($id){
        $res=Quizze::where('id',$id)->delete();
      if($res){
        echo "success";
        $notification = array(
            'message' => 'Quiz  deleted!',
            'alert-type' => 'success'
        );
      }
        return back()->with($notification);
    }

    public function createQuiz(){
        $cat=Category::get();
        return view('quiz.quiz',['data'=>$cat]);    
    }

    public function updateQuiz(Request $request){
        foreach ($request->questions as $key=>$row)
        {
            $quiz = Quizze::find($request->id[$key]);
            $quiz->questions = $request->questions[$key]; 
            $quiz->opt1 = $request->opt1[$key]; 
            $quiz->opt2 = $request->opt2[$key]; 
            $quiz->opt3 = $request->opt3[$key]; 
            $quiz->opt4 = $request->opt4[$key]; 
            $quiz->correct = $request->correct[$key]; 
            $resul=$quiz->save();    
        }   
        echo "success";
        $notification = array(
            'message' => 'Quizzes updated!',
            'alert-type' => 'success'
        );
        return back()->with($notification);  
    }

    public function allQuiz($id){

        $data = Quizze::with('categoryQuiz')->where('category_id',$id)->get();
       
        return view('quiz.quiz-category',compact('data'));
    }

    //delete quiz 
    // public function delQuiz($id){
    //     $res=Quizze::where('id',$id)->delete();
    //     if($res){
    //         toastr()->warning('Quiz delete successfully');
    //         return redirect()->back();
    //     }
    // }

    public function delQuiz($id){
        $res=Quizze::where('id',$id)->delete();
        if($res){
           return response()->json(['success'=>true]);
        }
        else{
            return response()->json(['Something went wrong']);
        }
    }
}
