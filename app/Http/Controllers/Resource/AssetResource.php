<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AppUsers;
use App\PublicAsset;
use Exception;

class AssetResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{

            $asset = PublicAsset::all();

            return response()->json($asset, 200, [], JSON_UNESCAPED_SLASHES); 

        } catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'asset_name' => 'required',
            'asset_path' => 'required',
        ]);
        

        try{

            $create_asset = new PublicAsset;
            $create_asset->asset_name = $request->asset_name;
            $create_asset->asset_path = $request->asset_name;
            $create_asset->asset_description = isset($request->asset_name) && !empty($request->asset_name)?$request->asset_name:NULL;
            $create_asset->save();

        } catch(Exception $e){
            if(!$request->ajax()){
                return response()->json(['error' => $e->getMessage()], 500);
            }else{
                return back()->with('flash_error',$e->getMessage());
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        try{
             // delete
            $asset = PublicAsset::find($id);
            return $asset;
        } catch(Exception $e){

            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $this->validate($request,[
                'id' => 'required',
        ]);

        try{
             // delete
            $asset = PublicAsset::find($id);
            $asset->delete();
            return response()->json(['message' => 'Successfully'], 200);
        } catch(Exception $e){
            if($request->ajax()){
                return back()->with('flash_error',$e->getMessage());
            }else{
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }
    } 
    public function viewLoader(Request $request)
    {
        return view('assets.loader');
    }

}
