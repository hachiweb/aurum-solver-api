<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AppUsers;
use App\Card;
use Exception;

class CardResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{

            $cards = Card::where('user_id',$request->user_id)->orderBy('created_at','desc')->get();

            return $cards; 

        } catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'stripe_token' => 'required',
            'user_id' => 'required|exists:apps_user,'.$request->user_id,
        ]);
        

        try{
            $customer_id = $this->customer_id($request->user_id);
            $this->set_stripe();
            $customer = \Stripe\Customer::retrieve($customer_id);
            $card = $customer->sources->create(["source" => $request->stripe_token]);

            $exist = Card::where('user_id',$request->user_id
            ->where('last_four',$card['last4'])
            ->where('brand',$card['brand'])->count();

            if($exist == 0){

                $create_card = new Card;
                $create_card->user_id = Auth::user()->id;
                $create_card->card_id = $card['id'];
                $create_card->last_four = $card['last4'];
                $create_card->brand = $card['brand'];
                $create_card->save();

            }else{
                return response()->json(['message' => 'Card has already been added']); 
            }

            if(!$request->ajax()){
                return response()->json(['message' => 'Card added']); 
            }else{
                return back()->with('flash_success','Card added');
            }
            
        } catch(Exception $e){
            if(!$request->ajax()){
                return response()->json(['error' => $e->getMessage()], 500);
            }else{
                return back()->with('flash_error',$e->getMessage());
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $this->validate($request,[
                'card_id' => 'required|exists:cards,card_id,user_id,'.$request->id,
            ]);

        try{


            $this->set_stripe();
            $user = AppUser::where('id',$request->id)->first();
            $customer = \Stripe\Customer::retrieve($user->stripe_cust_id);
            $customer->sources->retrieve($request->card_id)->delete();

            Card::where('card_id',$request->card_id)->delete();

            if($request->ajax()){
               return back()->with('flash_success','Card Deleted');
            }else{
                return response()->json(['message' => 'Card Deleted']);
            }

        } catch(Exception $e){
            if($request->ajax()){
                return back()->with('flash_error',$e->getMessage());
            }else{
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }
    }

    /**
     * setting stripe.
     *
     * @return \Illuminate\Http\Response
     */
    public function set_stripe(){
        return \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
    }

    /**
     * Get a stripe customer id.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer_id($id)
    {   
        $user = AppUser::where('id',$id)->first();
        if($user->stripe_cust_id != null){
            return $user->stripe_cust_id;
        }else{

            try{

                $stripe = $this->set_stripe();

                $customer = \Stripe\Customer::create([
                    'email' => $user->email,
                ]);

                $user->update(['stripe_account' => $customer['id']]);
                return $customer['id'];

            } catch(Exception $e){
                return $e;
            }
        }
    }

}
