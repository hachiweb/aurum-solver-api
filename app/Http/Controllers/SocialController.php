<?php

namespace App\Http\Controllers;
use App\AppUsers;
use App\Get_User;
// use App\Question;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// Laravel\Socialite\Two\User;
class SocialController extends Controller
{
    //  Sign with facebook and twitter
    public function socialSignup(Request $request)
    { 
        $validator = Validator::make($request->all(), [
            // 'name'/ => 'required|string|max:255',
            'user_email' => 'required|string|email|max:255',
            'type' => 'required|string',
            'token' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
   
        $user = Get_User::where('user_email', $request->user_email)->first();
        if(!$user){
            // return $user;
            $insert = new AppUsers;
            $insert->user_email = $request->user_email;
            $insert->registration_type = $request->type;
            $insert->registration_token = $request->token;
            $result=$insert->save();
          if ($result){
            return response()->json(['status'=>'200','status'=>'success','message'=> 'Account created successfully'], 200);
          }
        }
        else{
          return response()->json(['status'=>'error','message'=> 'Email allready exits'], 400);
        }
    }   
}
