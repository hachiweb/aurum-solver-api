<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AboutUs;
use App\Support;
use Illuminate\Support\Facades\Validator;
use Flasher\Prime\FlasherInterface;

class SupportAboutFaqController extends Controller
{  
    public function createFaq(Request $request ,FlasherInterface $flasher){
        $validator = Validator::make($request->all(),[
            'question' => 'required',
            // 'answer' => 'required',
            ]); 
            if($validator->fails()){
                $flasher->addwarning('Field is required');
                return redirect()->back();
            }

        foreach ($request->question as $key=>$row)
        {   
            $support = new Support;
            $support->question = $request->question[$key]; 
            $support->answer = $request->answer[$key]; 
            $result = $support->save();      
        } 
        if($result){
            $flasher->addsuccess('Successfully created');
            return redirect()->back();
           
        }else{
            $flasher->adderror('Something went wrong');
            return redirect()->back();
        }
    }

    public function updateFaqSupport(Request $request, FlasherInterface $flasher){
        if ($request->status == 0) {
            $res = Support::where('id', $request->id)->delete();
            if ($res) {
                $flasher->addSuccess('Successfully deleted!');
                return redirect()->back();
            } else {
                $flasher->addError('Something went wrong');
                return redirect()->back();
            }
        } else {
            $faq = Support::find($request->id);
            if (!$faq) {
                $flasher->addError('FAQ not found');
                return redirect()->back();
            }
            return view('support_faq.edit', compact('faq'));
        }
    }

    public function manageFaqSupport(){
        $data = Support::get();
        return view('support_faq.manage_faq',compact('data'));
    }

    public function removeSupportsQuestion($id){
        $res = Support::where('id',$id)->delete();
        if($res){
            // toastr()->success('Successfully deleted!');
            return response()->json(['success' => true]);
        }else{
            // toastr()->error('Something went wrong');
            return response()->json(['Something went wrong!']);
        }  
    }

    public function updateSupportsQuestion(Request $request ,FlasherInterface $flasher){
        $validator = Validator::make($request->all(),[
            'question' => 'required',
            'answer' => 'required',
            ]); 
            if($validator->fails()){
                $flahser->addwarning('Field is required');
                return redirect()->back();
            }
        $update = Support::find($request->id);
        $update->question = $request->question;
        $update->answer = $request->answer;
        $result = $update->save();
        if($result){
            $flasher->addsuccess('Successfully upadated');
            return redirect()->back();
           
        }else{
            $flasher->adderror('Something went wrong');
            return redirect()->back();
        }
    }

    public function createAbout(Request $request ,FlasherInterface $flasher){
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'about' => 'required',
            ]); 
            if($validator->fails()){
                $flasher->addwarning('Field is required');
                return redirect()->back();
            }
        $about = new AboutUs;
        $about->about_us = $request->about; 
        $about->tittle = $request->title; 
        $result = $about->save();
        if($result){
            $flasher->addsuccess('Successfully created');
            return redirect()->back();
           
        }else{
            $flasher->adderror('Something went wrong');
            return redirect()->back();
        }
    }
    
   public function manageAbout(){
       $data = AboutUs::get();
    return view('about_us.manage_about',compact('data'));
 
   }
   public function displayFaq(){
    $data = Support::get();
    return view('support_faq.support',compact('data'));
   }
   public function aboutUs(){
    $data = AboutUs::get();
    return view('support_faq.about',compact('data'));
   }
   
    public function delAbout($id){
        $res = AboutUs::where('id',$id)->delete();
        if($res){
            // toastr()->success('Successfully deleted!');
            // return redirect()->back();
            return response()->json(['success'=> true]);
        }else{
            // toastr()->error('Something went wrong');
            // return redirect()->back();
            return response()->json(['Something went wrong!']);
        }  
    } 
    public function editAbout($id){
        $data = AboutUs::where('id',$id)->first();
        return view('about_us.edit',compact('data'));
    }
    
    public function updateAbout(Request $request ,FlasherInterface $flasher){
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'about' => 'required',
            ]); 
            if($validator->fails()){
                $flasher->addwarning('Field is required');
                return redirect()->back();
            }
        $about = AboutUs::find($request->id);
        $about->about_us = $request->about; 
        $about->tittle = $request->title; 
        $result = $about->save();
        if($result){
            $flasher->addsuccess('Successfully updated');
            return redirect()->back();
           
        }else{
            $flasher->adderror('Something went wrong');
            return redirect()->back();
        }
    }
}