<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Quizze;
use App\Quiz_answer;
use App\Get_User;
use App\Category;
use App\Feedback;
use App\AppUsers;
use Illuminate\Support\Facades\DB;

class UsersProfilesController extends Controller
{
    //User profiles
    public function userProfile(Request $request){
        if(!isset($request->id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 400);
        }
        $user = AppUsers::find($request->id);
        if(empty($user)){
        return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
        }else{ 
            $data = AppUsers::withCount('userQuestion')->withCount('userAnswer')->withCount(['userAnswerPurchased' => function($q) {
                $q->where('purchase', '1');}])->withCount(['userEarning AS user_earning' => function($q) {
                         $q->select(DB::raw('sum(amount)'))->where('withdrawal', '0');
                    }])->where('is_verified', '1')->where('is_active', '1')->with('userFeedback')->where('id',$request->id)->get();
            return response()->json(['status'=>'success','data'=>$data], 200);      
        } 
    }
}
