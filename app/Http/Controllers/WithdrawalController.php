<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\AdaptivePayments;
use App\Question;
use App\Withdrawal;
use App\Prepayments;
use App\Transaction;
use App\Answer;
use App\AppUsers;
use Auth;
use Session;
use PayPal;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\ExecutePayment;
use PayPal\Exception\PayPalConnectionException;
class WithdrawalController extends Controller
{
    public function withdrawalApprove(Request $request){   
        $withdrawal_status = $request->status;
        $withdrawal_id = $request->id;
        $withdrawal_request = Withdrawal::find($withdrawal_id);
        if($withdrawal_status == 1){
           
            $total_expand = Transaction::where('from',$withdrawal_request->user_id)->where('to', '!=',$withdrawal_request->user_id)->where('status','paid')->sum('amount');
            $total_balance = (Transaction::where('to',$withdrawal_request->user_id)->where('from',$withdrawal_request->user_id)->where('status','paid')->where('is_gateway',1)->where('withdrawal',0)->sum('amount')) + (Transaction::where('to',$withdrawal_request->user_id)->where('status','paid')->where('is_gateway',0)->where('withdrawal',0)->sum('amount'));
            $amount = round(($total_balance -  $total_expand),2);
    
            if($amount < $withdrawal_request->amount){
                Withdrawal::where('id',$withdrawal_id)->update(array('is_approved' =>0,'description'=>'Insufficient balance!',));
                toastr()->success('Withdrawl could not be processed due to insufficient balance');
                return redirect()->back();
            }
            else{
                 \Session::put('withdrawal_id', $withdrawal_id);    
                // $provider = new ExpressCheckout;      // To use express checkout.
                $provider = new AdaptivePayments();     // To use adaptive payments.

                // Through facade. No need to import namespaces
                // $provider = PayPal::setProvider('express_checkout');      // To use express checkout(used by default).
                // $provider = PayPal::setProvider('adaptive_payments');     // To use adaptive payments.
                // Change the values accordingly for your application
                $provider->setCurrency('USD');
                $options = [
                    'BRANDNAME' => 'Aurum Solver',
                    'LOGOIMG' => 'https://aurumsolver.com/images/icons/favicon.ico',
                    'CHANNELTYPE' => 'Merchant'
                ];

                $provider->addOptions($options);
                // $provider->setApiCredentials($config);
                $data = [
                    'receivers'  => [
                        [
                            'email' => 'sb-kvhyv1939756@personal.example.com',
                            'amount' => $withdrawal_request->amount,
                            'primary' => false,
                        ]
                    ],
                    'payer' => 'EACHRECEIVER', // (Optional) Describes who pays PayPal fees. Allowed values are: 'SENDER', 'PRIMARYRECEIVER', 'EACHRECEIVER' (Default), 'SECONDARYONLY'
                    'return_url' => route('dashboard.withdrawal.handle',1), 
                    'cancel_url' => route('dashboard.withdrawal.handle',2),
                ];
                
                $response = $provider->createPayRequest($data);
                // The above API call will return the following values if successful:
                // 'responseEnvelope.ack', 'payKey', 'paymentExecStatus'

                // Next, you need to redirect the user to PayPal to authorize the payment
                
                if(!isset($response['payKey']) || empty($response['payKey'])){
                    // echo '<pre>';
                    // print_r($response);
                    // print_r($email);
                    // exit;
                    toastr()->error('Something went wrong');
                    return redirect()->back();
                }      
                \Session::put('payKey', $response['payKey']); 
                $redirect_url = $provider->getRedirectUrl('approved', $response['payKey']);

                return redirect($redirect_url);
            }
             
        }
        else{         
            Withdrawal::where('id',$withdrawal_id)->update(array('is_approved' =>0,'description'=>'Request rejected',));
            toastr()->error('Withdrawl request rejected');
            return redirect()->back();
        }
        
    }
     
    public function withdrawalHandle(Request $request){
        $status = $request->status; 
        /** Get the payment ID before session clear **/
        $withdrawal_id = \Session::get('withdrawal_id');
        $payKey = \Session::get('payKey');
        /** clear the session payment ID **/
        Session::forget('withdrawal_id');
        Session::forget('payKey');

        if ($status == '1') {
            $currency = 'USD';
             
            $withdrawalRequest = Withdrawal::find($withdrawal_id);
            $withdrawalRequest->is_approved = 1;
						$withdrawalRequest->description = 'Withdrawal processed approved';
            $withdrawalRequest->save();
            
            $transaction = new Transaction;
            $transaction->transaction_id = $payKey;
            $transaction->amount  = $withdrawalRequest->amount;
            $transaction->description  = "Withdrawal processed approved";
            $transaction->currency  = 'USD';
            $transaction->status  = 'paid';
            $transaction->from =  $withdrawalRequest->user_id;
            $transaction->to  = $withdrawalRequest->user_id;
            $transaction->withdrawal = 1;
            $transaction->is_gateway = 1;
            $transaction->save();
            toastr()->success('Withdrawl request has successfully been accepted');
            \Session::put('success', 'Payment success');
        }
        else{
            toastr()->error('Unable to process payment.');
            \Session::put('error', 'Payment failed');
        }
        return redirect()->route('dashboard');   
    }

    public function refundRequestUpdate(Request $request){   
        $refund_status = $request->status;
        $refund_id = $request->id;
        $refund_request = Question::find($refund_id);
        
        if($refund_status == 1){            
            $total_refund_amount = ( $refund_request->willing_to_pay) - (Prepayments::where('to_request',$refund_request->user_id)->where('question_id',$refund_id)->where('approve_status',1)->sum('requested_amount'));
          
      
        
            $wallet_balance = Transaction::where('question_id',$refund_id)->where('from',$refund_request->user_id)->where('to',$refund_request->user_id)->where('status','paid')->sum('amount');
         
            if($wallet_balance < $total_refund_amount){
                
                Question::where('id',$refund_id)->update(array('is_approved' =>2));
                toastr()->success('Refund could not be processed due to insufficient balance');
                return redirect()->back();
            }
            else{
                 \Session::put('refund_id', $refund_id); 
                 \Session::put('refund_amount', $total_refund_amount);    
                // $provider = new ExpressCheckout;      // To use express checkout.
                $provider = new AdaptivePayments();     // To use adaptive payments.

                // Through facade. No need to import namespaces
                // $provider = PayPal::setProvider('express_checkout');      // To use express checkout(used by default).
                // $provider = PayPal::setProvider('adaptive_payments');     // To use adaptive payments.
                // Change the values accordingly for your application
                $provider->setCurrency('USD');
                $options = [
                    'BRANDNAME' => 'Aurum Solver',
                    'LOGOIMG' => 'https://aurumsolver.com/images/icons/favicon.ico',
                    'CHANNELTYPE' => 'Merchant'
                ];

                $provider->addOptions($options);
                // $provider->setApiCredentials($config);
                $data = [
                    'receivers'  => [
                        [
                            'email' => 'sb-kvhyv1939756@personal.example.com',
                            'amount' => $total_refund_amount,
                            'primary' => false,
                        ]
                    ],
                    'payer' => 'EACHRECEIVER', // (Optional) Describes who pays PayPal fees. Allowed values are: 'SENDER', 'PRIMARYRECEIVER', 'EACHRECEIVER' (Default), 'SECONDARYONLY'
                    'return_url' => route('dashboard.refund.handle',1), 
                    'cancel_url' => route('dashboard.refund.handle',2),
                ];
                
                $response = $provider->createPayRequest($data);
                // The above API call will return the following values if successful:
                // 'responseEnvelope.ack', 'payKey', 'paymentExecStatus'

                // Next, you need to redirect the user to PayPal to authorize the payment
              
                if(!isset($response['payKey']) || empty($response['payKey'])){
                    // echo '<pre>';
                    // print_r($response);
                    // print_r($email);
                    // exit;
                    toastr()->error('Something went wrong');
                    return redirect()->back();
                }      
                \Session::put('payKey', $response['payKey']); 
                $redirect_url = $provider->getRedirectUrl('approved', $response['payKey']);

                return redirect($redirect_url);
            }
             
                      
        }
        else{         
            Question::where('id',$refund_id)->update(array('is_approved' =>0));
            toastr()->error('Refund request rejected');
            return redirect()->back();
        }
        
     
    }


    public function refundHandle(Request $request){
        $status = $request->status; 
        /** Get the payment ID before session clear **/
        $refund_id = \Session::get('refund_id');
        $payKey = \Session::get('payKey');
        $refund_amount = \Session::get('refund_amount');
        /** clear the session payment ID **/
        Session::forget('refund_id');
        Session::forget('payKey');
        Session::forget('refund_amount');

        if ($status == '1') {
            $currency = 'USD';
             
            $refundRequest = Question::find($refund_id);
           
            $refundRequest->is_approved = 1;			 
            $refundRequest->save();
            
            $refundtransaction = new Transaction;
            $refundtransaction->question_id = $refund_id;
            $refundtransaction->transaction_id = $payKey;
            $refundtransaction->amount  = $refund_amount;
            $refundtransaction->description  = "Amount Refunded";
            $refundtransaction->currency  = 'USD';
            $refundtransaction->status  = 'paid';
            $refundtransaction->from =  $refundRequest->user_id;
            $refundtransaction->to  = $refundRequest->user_id;
            $refundtransaction->withdrawal = 0;
            $refundtransaction->is_gateway = 1;
            $refundtransaction->save();
            toastr()->success('Refund request has successfully been accepted');
            \Session::put('success', 'Payment success');
        }
        else{
            toastr()->error('Unable to process payment.');
            \Session::put('error', 'Payment failed');
        }
        return redirect()->route('dashboard');   
    }

}