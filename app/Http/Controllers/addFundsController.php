<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Question;
use App\Get_User;
use App\Answer;
use App\Transaction;
use Illuminate\Support\Facades\DB;

class addFundsController extends Controller
{
    //Add money in funds
    public function addFunds(Request $request){
            $validator =Validator::make($request->all(),[
                'user_id'=>'required',   
                'transaction_id'=>'required',   
                'status'=>'required',   
                'amount'=>'required',
        
            ]);
            
           if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
           }
           $user = Get_User::where('id', $request->user_id)->count();
           if($user >0){
            $funds= new Transaction;
            $funds->transaction_id = $request->transaction_id;
            $funds->amount = $request->amount;
            $funds->from = $request->user_id;
            $funds->to = $request->user_id;
            $funds->status = $request->status;
            $funds->is_gateway = 1;
            $result = $funds->save();
            if($result == 1){
                $data = Transaction::latest()->first();
                return response()->json(['status'=>'success','message'=> 'Add balance successfully','data'=>$data], 200);
            }
        }
        else{
            return response()->json(['status'=>'error','message'=> 'User id is Invalid'], 404);
        }
           
    }
}
