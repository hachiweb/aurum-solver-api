<?php

namespace App\Http\Controllers;
use App\Question;
use App\Get_User;
use App\Answer;
use App\Quiz_answer;
use App\AppUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ansQuestionsController extends Controller
{
     // Complete answers submit 
    public function assignSubmit(Request $request){
        $valid = Validator::make($request->all(),[
            'question_id' => 'required',
            'answer_title' => 'nullable',
            'answer' => 'required',
            'desired_price' => 'nullable',
        ]);
        if($valid->fails()){
            return response()->json($valid->errors()->toJson(), 400);
        }
        $quest_detail = Question::with('categoryDetail')->where('id', $request->question_id)->first();
        
        if(isset($quest_detail->tutor_id)){
            $answererId = $quest_detail->tutor_id;
            $subject = $quest_detail->categoryDetail->subjects;
            $questionId = $request->question_id ;
            $questionerId = $quest_detail->user_id;
            $isuser = AppUsers::find($answererId);

            if($isuser->count()<1){
                return response()->json(['status'=>'error','message'=> 'Tutor does not exist!'], 404);
            }
            if($isuser->subscribe_category!=$subject){
                return response()->json(['status'=>'error','message'=> 'Tutor subscribed category does not match!'], 400);
            }
            
            $id = $answererId;
            $files = $request->file('answer_attach'); 
            if(!empty($files)){
                try{
                    foreach($files as $file){
                        if($file){
                            $ansfile = $file->getClientOriginalName();
                            if(!empty($ansfile)){
                                $afile  = 'answer-' . time() . '.' . $ansfile;
                                $file->storeAs('/public/assignment_attachments/anwers/user-id-'. $id, $afile);
                                $ansAttach[]=url('/').'/storage/assignment_attachments/anwers/user-id-'.$id.'/'. $afile;
                            }
                        }
                        else{
                            $ansAttach[] = '';  
                        }
                    }
                }catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
            else{
                $ansAttach[] = ''; 
            }
            
            // questions attachment 
            $question = $request->file('questions_attach');
            if(!empty($question)){
                try{
                    foreach($question as $file){
                        if($file){
                            $filename=$file->getClientOriginalName();
                            if(!empty($filename)){
                                $qfile  = 'questions-' . time() . '.' . $filename;
                                $file->storeAs('/public/assignment_attachments/questions/user-id-'. $id, $qfile);
                                $pic[]=url('/').'/storage/assignment_attachments/questions/user-id-'.$id.'/'. $qfile;
                            }
                        }
                        else{
                            $pic[] = '';  
                        }
                    }
                }catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
            else{
                $pic[] = ''; 
            }    
               
            $existing_submission = Answer::where('question_id', $request->question_id) ->where('answerer_id', $answererId) ->first() ;
            if(isset($existing_submission->id)) {
             $existing_submission->answerer_id = $id; 
            $existing_submission->question_id = $request->question_id; $existing_submission->questioner_id = $questionerId; $existing_submission->title = $request->answer_title; $existing_submission->category = $subject; 
            $existing_submission->answer = $request->answer; $existing_submission->ans_attach = implode(',',$ansAttach); $existing_submission->post = date('Y-m-d m:i:s'); 
            $result = $existing_submission->save();
            $quest_detail->delivery_accept = 3; //for re submission
            $quest_detail->save() ;
             if($result==1): 
                $answer = Answer::orderBy('id','desc')->first(); return response()->json(['status'=>'success','message'=> 'Assignment submitted successfully','data'=>$answer], 200); 
                endif; 
            }
            else{
                $ans = new Answer;
                $ans->answerer_id = $id; 
                $ans->question_id = $request->question_id; 
                $ans->questioner_id = $questionerId; 
                $ans->title = $request->answer_title; 
                $ans->category = $subject; 
                $ans->answer = $request->answer; 
                $quest_detail->delivery_accept = 3; //for first submission
                $quest_detail->save() ;
                $ans->ans_attach = implode(',',$ansAttach); $ans->post = date('Y-m-d m:i:s'); $result = $ans->save(); if($result==1): $answer = Answer::orderBy('id','desc')->first(); return response()->json(['status'=>'success','message'=> 'Assignment submitted successfully','data'=>$answer], 200); endif; 
            }

            
        }else{
            return response()->json(['status'=>'error','message'=> 'Tutor does not exist!'], 404);
        }
    }
   
    // submit assigned question
    public function replyAns(Request $request){
        $valid = Validator::make($request->all(),[
            'answerer_id'=>'required|numeric',
            'question_id'=>'required|numeric',
        ]);
        if($valid->fails()){
            return response()->json($valid->errors()->toJson(), 400);
        }
        $user = Get_User::find($request->answerer_id);
        if(!empty($user)){
            $question = Question::with('categoryDetail')->where('id', $request->question_id)->first();
            if(!empty($question)){
                $quiz_pass = Quiz_answer::where('category_id', $question->category_id)->where('user_id', $request->answerer_id)->first();
                $id =$request->answerer_id;         
                if(!empty($quiz_pass)){   
                    if($quiz_pass->percent>=80) { 
                        $answer = Answer::where('answerer_id',$id)->where('category', $question->categoryDetail->subjects)->where('question_id', $request->question_id)->first();
                        $validator =Validator::make($request->all(),[ 
                        'answer'=>'required',
                        'title' => 'required',
                        'desired_price' => 'required',
                        'answer_attachment' => 'nullable |mimes:jpeg,bmp,png,jpg,svg,pdf,docx'
                        ]);
                        if($valid->fails()){
                            return response()->json($valid->errors()->toJson(), 400);
                        } 
                        $files = $request->file('answer_attachment');
                            if(!empty($files)){
                                try{
                                    foreach($files as $file){
                                        $ansfile=$file->getClientOriginalName();
                                        if(!empty($ansfile)){
                                            $afile  = 'answer-' . time() . '.' . $ansfile;
                                            $file->storeAs('/public/answer_attachments/anwers/answerer-id-'. $id, $afile);
                                            $ansAttach[]=url('/').'/storage/answer_attachments/anwers/answerer-id-'.$id.'/'. $afile;
                                        }
                                        else{
                                            $ansAttach[] = '';  
                                        }
                                    }
                                }catch (\Exception $e) {
                                    return $e->getMessage();
                                }
                            }
                            else{
                                $ansAttach[] = ''; 
                            }
                            if(empty($answer)){
                                $ans = new Answer;
                                $ans->answerer_id = $id;
                                $ans->question_id = $request->question_id;
                                $ans->questioner_id =  $question->user_id;
                                $ans->title = $request->title;
                                $ans->category = $question->categoryDetail->subjects;
                                $ans->question = $question->question;
                                $ans->answer = $request->answer;
                                $ans->ans_attach =  implode(',',$ansAttach);
                                $ans->post = date('Y-m-d m:i:s');
                                $ans->desired_price = $request->desired_price;
                                $ans->duedate = $question->due_date;
                                $result = $ans->save();
                                if($result==1):
                                    $answer = Answer::orderBy('id','desc')->first();
                                    return response()->json(['status'=>'success','message'=> 'Answer submitted!','data'=>$ans], 200);
                                endif; 
                            }else{
                                $ansfiles = implode(',',$ansAttach);
                                if(isset($request->title) && (!empty($request->title) || $request->title!='')){
                                    Answer::where('id',$answer->id)->update(array('title'=>$request->title,'post'=> date('Y-m-d')));
                                }
                                if(isset($request->answer) && (!empty($request->answer) || $request->answer!='')){
                                    Answer::where('id',$answer->id)->update(array('answer'=>$request->answer,'post'=> date('Y-m-d')));
                                }
                                if(isset($ansfiles) && (!empty($ansfiles) || $ansfiles!='')){
                                    Answer::where('id',$answer->id)->update(array('ans_attach'=>$ansfiles,'post'=> date('Y-m-d')));
                                }
                                if(isset($request->desired_price) && (!empty($request->desired_price) || $request->desired_price!='')){
                                    Answer::where('id',$answer->id)->update(array('desired_price'=>$request->desired_price,'post'=> date('Y-m-d')));
                                }
                                $answer = Answer::where('id',$answer->id)->first();
                                return response()->json(['status'=>'success','message'=> 'Answer submitted!','data'=>$answer], 200);

                            }
                    }else{
                        return response()->json(['status'=>'error','message'=> 'You can\'t answer. You have not passed the quiz.','category_eligibility'=>$question->categoryDetail->subjects], 403); 
                        }                     
                    }
                else{
                    return response()->json(['status'=>'error','message'=> 'You can\'t answer. You have not attempted the quiz.','category_eligibility'=>$question->categoryDetail->subjects], 403);                      
                }
            }
            else{
                return response()->json(['status'=>'error','message'=> 'Question id does not exit'], 400);
            }  
        }else{
            return response()->json(['status'=>'error','message'=> 'Answerer id does not exist.'], 404);
        }
    }
    // total my answers reply by me
    public function myAnswers(Request $request){
        if(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 200);
          }
          $user = AppUsers::find($request->user_id);
       
          if(empty($user)){
              return response()->json(['status'=>'error', 'message'=>'Invalid  id'], 200);
          }
          else{
            // $user = Apps_user::find($request->user_id);
                 $result= Answer::where('answerer_id',$request->user_id)->count();
                if($result >0){
                    $answer= Answer::withCount('isReviewed as is_reviewed')->with('answererDetail')->with('questionDetail')->with('questionerDetail')->where('answerer_id',$request->user_id)->get();
                    
                    return response()->json(['status'=>'success','data'=>$answer], 200);      
                }
                else{
                    return response()->json(['status'=>'error','message'=>'There are no answers'],404);
                }
          }
    }
 
}