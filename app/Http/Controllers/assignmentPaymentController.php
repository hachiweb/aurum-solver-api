<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Question;
use App\Prepayments;
use App\Get_User;
use App\Answer;
use App\AppUsers;
use App\Transaction;
use App\Feedback;
use Illuminate\Support\Facades\DB;

class assignmentPaymentController extends Controller
{
    //Pay for the assignment
    public function assignmentPayment(Request $request){
        $validator =Validator::make($request->all(),[
            'assignment_id' =>'required', 
            'transaction_id' =>'required', 
            'status' =>'required', 
        ]); 
        if(!isset($request->assignment_id)){
          return response()->json(['status'=>'error', 'message'=>'Missing id'], 400);
        }
        $assignment = Question::where('is_assigned',1)->find($request->assignment_id);
        if(empty($assignment)){
            return response()->json(['status'=>'error', 'message'=>'Invalid assignment id'], 404);
        }else{
            $total_balance = Transaction::where('to',$assignment->user_id)->where('status','paid')->where('withdrawal',0)->sum('amount');
            $total_expand = Transaction::where('from',$assignment->user_id)->where('to', '!=',$assignment->user_id)->where('status','paid')->where('withdrawal',0)->sum('amount');
            $total = number_format(($total_balance -  $total_expand),2);    
            $total_pay = $assignment->willing_to_pay+(10*$assignment->willing_to_pay/100);
            $answer_amount = round($total_pay , 2);            
            // print_r($total_pay);
            // echo "<br>";
            // print_r($assignment->willing_to_pay);
            // exit;
        // if($answer_amount <= $total){
        //     $from_questioner = round(($answer_amount - $assignment->willing_to_pay),2);
        //     $from_answerer = round((30*$assignment->willing_to_pay/100),2);
        //     $answerer_fee = $answer_amount -($from_answerer + $from_questioner);
        //     $admin_fee = round(($from_answerer + $from_questioner),2);

        // }
              
            $pay = new Transaction;
            $pay->question_id = $assignment->id;
            $pay->from = $assignment->user_id;
            $pay->amount = $answer_amount;
            $pay->to = $assignment->user_id;
            $pay->status = $request->status;
            $pay->transaction_id = $request->transaction_id;
            $result = $pay->save();
            if($result==1){
                 Question::where('is_assigned',1)->where('id',$request->assignment_id)->update(array('is_paid'=>1));
                $transaction= Transaction::latest()->first();
                return response()->json(['status'=>'success','message'=> 'Paid successfully','transaction_details'=>$transaction], 200);
            }
        }
    }

    // cancel assignment
    public function cancelAssignment(Request $request){
        $validator =Validator::make($request->all(),[
            'assignment_id' =>'required', 
            'reason' =>'required', 
        ]); 
        if(!isset($request->assignment_id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 400);
        }
        //   $questionerId = $request->user_id;
        $assignment = Question::find($request->assignment_id); 
        if(!empty($assignment)){
            $pay = new Transaction; 
            $pay->question_id = $assignment->id;
            $pay->from =$assignment->tutor_id;
            $pay->amount =  $assignment->willing_to_pay;
            $pay->to =  $assignment->user_id;
            $pay->status =  'paid';
            $pay->transaction_id = 'AS_TXN_'.rand('111111111','999999999');
            $result = $pay->save();
            if($result==1){
                Question::where('is_assigned',1)->where('id',$request->assignment_id)->update(array('is_paid'=>0,'is_cancelled'=>1,'delivery_accept'=>0,'cancellation_reason'=>$request->reason,'is_active'=>0));
                $transaction= Transaction::latest()->first();
                return response()->json(['status'=>'success','message'=> 'Assingnment cancelled successfully. Refund is being processed','transaction_details'=>$transaction], 200);
            }  
        }
        else{
            return response()->json(['status'=>'error','message'=> 'Invalid assignment id'], 404);
        } 
    }



     // Complete pay for assignment 
     public function deliveryUpdate(Request $request){
        $validator =Validator::make($request->all(),[
            'assignment_id' =>'required',
            'user_id' =>'required',
            'delivery_status' =>'nullable', 
        ]); 
        if($validator->fails()){
          return response()->json($validator->errors()->toJson(), 400);
        }
        $assignment = Question::where('user_id',$request->user_id)->find($request->assignment_id);
        if(empty($assignment)){
            return response()->json(['status'=>'error','message'=> 'Invalid assignment id'], 403);
        } 
        $request_status = isset($request->delivery_status)?$request->delivery_status:0;
        if($request->delivery_status==1) 
        {   
           
            $total_balance = Transaction::where('from',$request->user_id)->where('to',$request->user_id)->where('question_id',$request->assignment_id)->sum('amount');     
            $prepayment_balance = Prepayments::where('from_request',$assignment->user_id)->where('to_request',$assignment->tutor_id)->where('question_id',$request->assignment_id)->sum('requested_amount');
            // $pay_amount=round(($total_balance-$prepayment_balance),2);
            $delivery=Question::where('id',$request->assignment_id)->first();
            $total_pay = $assignment->willing_to_pay+(10*$assignment->willing_to_pay/100);
            $answer_amount = round($total_pay , 2);  
            $from_questioner = round(($answer_amount - $delivery->willing_to_pay),2);
            $from_answerer = round((30*$delivery->willing_to_pay/100),2);
            $answerer_fee =(($answer_amount -($from_answerer + $from_questioner)) -$prepayment_balance);
            $admin_fee = round(($from_answerer + $from_questioner),2);
            Question::where('id',$request->assignment_id)->update(array('delivery_accept'=>1,'questioner_fee'=>$answer_amount,'answerer_fee'=>$answerer_fee,'admin_fee'=>$admin_fee));
            $pay = new Transaction;
            $pay->to = $assignment->tutor_id;
            $pay->from = $request->user_id;
            $pay->amount = $answerer_fee;
            $pay->transaction_id = 'AS_TXN_'.rand(1111111111,9999999999);
            $pay->question_id = $request->assignment_id;
            $pay->status = 'paid';
            $result = $pay->save();
            if($result){
                $data = Transaction::orderBy('id','desc')->first();
                return response()->json(['status'=>'success','message'=> 'Delivery accepted','data'=>$delivery,'transction_details'=>$data], 200);
            } 
        }
      else{
         Question::where('id',$request->assignment_id)->update(array('delivery_accept'=>2));
          return response()->json(['status'=>'success','message'=> 'Delivery rejected'], 200);
      }
    }

}