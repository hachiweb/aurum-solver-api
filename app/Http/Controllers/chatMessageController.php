<?php

namespace App\Http\Controllers;
use App\Chat;
use App\AppUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class chatMessageController extends Controller
{
    //Chats messages
    public function chats(Request $request){
      
        $valid =Validator::make($request->all(),[
            'from_message'=>'required',
            'to_message'=>'required',
            'message'=>'required',
             
            ]);
            if($valid->fails()){
                return response()->json($valid->errors()->toJson(), 400);
            }
        $users2=AppUsers::find($request->to_user_id);
        if(empty($users2)){
            $msg=Chat::latest()->first();
            return response()->json(['status'=>'success','message'=>'User not exit.','to_user_id'=>$request->to_user_id],200);
        }
        $chat = new Chat;
        $chat->from_message = $request->from_message;
        $chat->to_message = $request->to_message;
        $chat->message = $request->message;
        $result = $chat->save();
        if($result):
            return response()->json(['status'=>'success','message'=>'chat message saved'],200);
        endif;
    }
}
