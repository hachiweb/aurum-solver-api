<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Feedback;
use App\Answer;
use App\AppUsers;
use App\Question;
use App\Transaction;
use App\Category;
use Illuminate\Support\Facades\DB;

class feedbacksController extends Controller
{
    //Feedback review
    public function feedback(Request $request){
        $validator =Validator::make($request->all(),[
            'rated_by'=>'nullable',
            'rating'=>'required|numeric|max:10',   
            'review'=>'nullable',   
            'rated_to'=>'nullable',   
            'rated_for'=>'required',
            'answer_id'=>'required',              
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $rating_id = Answer::find($request->answer_id);
        if(empty($rating_id)){
            return response()->json(['status'=>'error', 'message'=>'Invalid answer id'], 200);
        }
        $rating_id->questioner_id;
        $feed = new Feedback;
        $feed->rated_by = $rating_id->questioner_id;
        $feed->rated_to = $rating_id->answerer_id;
        $feed->rated_for = $request->rated_for;
        $feed->rated_for_id = $request->answer_id;
        $feed->rating = $request->rating;
        $feed->review = $request->review;
        $result = $feed->save();
        if($result == 1):
            return response()->json(['status'=>'success','message'=> 'Review submitted.'], 200);
        endif;
       
    }
    // My feedback
     public function myFeedback(Request $request){
        if(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 200);
          }
          $user = AppUsers::find($request->user_id);
       
          if(empty($user)){
              return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
          }
          else{ 
            
            $feed = Feedback::with('userDetails')->where('rated_to',$request->user_id)->orderBy('id','desc')->limit(10)->get();
            if($feed->isNotEmpty()){
                return response()->json(['status'=>'success','feedback'=>$feed], 200);
            }else{
                return response()->json(['status'=>'error','message'=>'No feedback.'], 404);
            }
          }
     }

    //user feed back
    public function myInformation(Request $request){
        if(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 200);
          }
          $user = AppUsers::find($request->user_id);
          if(empty($user)){
              return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
          }
          else{ 
           $user_id= $request->user_id;
            $result = Question::where('user_id',$request->user_id)->count();
            $answer = Answer::where('questioner_id',$request->user_id)->count();
            $money = Transaction::where('to',$request->user_id)->where('from','!=',$request->user_id)->sum('amount');
            $total_amount=round($money,2);
            return response()->json(['status'=>'success','total_earn'=>$total_amount,'total_post_questions'=>$result,'total_buy_answer'=>$answer], 200);
             
          }
     }
}
