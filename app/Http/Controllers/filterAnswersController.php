<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Question;
use App\Category;
use App\Answer;
use App\AppUsers;
use App\Transaction;
use Illuminate\Support\Facades\DB;

class filterAnswersController extends Controller
{
    //apply filters
    public function filter(Request $request){
         
            $category =Category::select('subjects')->get();
            // return $category;
            $valid =Validator::make($request->all(),[
            'category'=>'required',
            'min_amount_to_pay'=>'required',
            'max_amount_to_pay'=>'required',
            'postedafter'=>'required',
            'beforeduedate'=>'required',              
            ]);
            if($valid->fails()){
                return response()->json($valid->errors()->toJson(), 400);
            }
            $category = $request->category;
            $min_amount = $request->min_amount_to_pay;
            $max_amount = $request->max_amount_to_pay;
            $posted = $request->postedafter;
            $duedate = $request->beforeduedate;
            //  echo $max_amount;
            //  echo $min_amount;
            $searche = Question::where('category', $category)->where('willing_to_pay','<=', $max_amount)->where('willing_to_pay','>=', $min_amount)->whereDate('post_date','>=', $posted)->whereDate('due_date','<=', $duedate)->count();
            if(!empty($searche)){
                $search = Question::where('category', $category)->where('willing_to_pay','<=', $max_amount)->where('willing_to_pay','>=', $min_amount)->whereDate('post_date','>=', $posted)->whereDate('due_date','<=', $duedate)->get();

                $answer = Question::with('userDetail')->where('category', $category)->where('willing_to_pay','<=', $max_amount)->where('willing_to_pay','>=', $min_amount)->whereDate('post_date','>=', $posted)->whereDate('due_date','<=', $duedate)->get();
              
                 return response()->json(['Status'=>'success','Questions Details'=>$search,'data'=>$answer], 200); 
            }else{
                return response()->json(['Status'=>'success','message'=>'Result not found'], 404); 
            }
          
    }
    // search the key
    public function search(Request $request){
      // echo strlen($request->search_key);
      // exit;
        if(!isset($request->search_key)){
            return response()->json(['status'=>'error', 'message'=>'Missing search_key'], 200);
          }
          if(strlen($request->search_key)<3){
            return response()->json(['status'=>'error', 'message'=>'Search string must be atleast three letters long'], 400);
          }
          $question = Question::with('categoryDetail')->withCount('totalAnswers')->where('question', 'like','%'.$request->search_key.'%')->orWhere('title', 'like','%'.$request->search_key.'%')->count();
          $user = AppUsers::where('first_name', 'like','%'.$request->search_key.'%')->count();
          // return($user);
          //   exit;
          if(!empty($question) || !empty($user)){
            $allQuestions = Question::with('categoryDetail')->withCount('totalAnswers')->with('userDetail')->where('question', 'like','%'.$request->search_key.'%')->orWhere('title', 'like','%'.$request->search_key.'%')->get();
            // $question = Question::where('category', 'like','%'.$request->search_key. '%')->orWhere('question', 'like',$request->search_key. '%')->orWhere('title', 'like',$request->search_key. '%')->get();

            $users = AppUsers::withCount('userQuestion')->withCount('userAnswer')->withCount(['userAnswerPurchased' => function($q){
                $q->where('purchase', '1');}])->withCount(['userEarning AS user_earning' => function($q) {
                        $q->select(DB::raw('sum(amount)'))->where('withdrawal', '0');
                    }])->withCount(['userFeedback AS average_rating' => function($q) {
                        $q->select(DB::raw('avg(rating)'));
                    }])->where('is_verified', '1')->where('is_active', '1')->where('first_name', 'like','%'.$request->search_key.'%')->orWhere('last_name', 'like','%'.$request->search_key.'%')->get();

            return response()->json(['status'=>'success', 'questions'=>$allQuestions,'users'=>$users], 200);
             
          }
          else{
            return response()->json(['status'=>'error', 'message'=>'Result not found'], 404);
          }
    }
}
