<?php

namespace App\Http\Controllers;

use App\Get_User;
use App\AppUsers;
use App\Feedback;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
// use JWTAuth;
// use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class profileUpdateController extends Controller
{
    //  Profile updates
    public function userUpdate(Request $request){
        $user_email = $request->email;
        $valid = Validator::make($request->all(),[
            'email'=>'required|string|email|max:255',
            'type'=>'required',
        ]);
        if($valid->fails()){
            return response()->json($valid->errors()->toJson(), 400);
        }
        $email = $request->user_email;
        $users = Get_User::where('user_email', $user_email)->first();
        if(!empty($users)){
            if($request->type!='email'){
                $valid = Validator::make($request->all(),[
                    'profile_pic'=>'nullable|mimes:jpeg,bmp,jpg,png',
                    'first_name'=>'nullable',
                    'last_name'=>'nullable',
                    'dob'=>'nullable',
                    'role'=> 'nullable',
                    'paypalaccount'=> 'nullable',
                    'subscribe_category'=> 'nullable',
                    'user_type'=> 'nullable',
                    'category_id'=> 'nullable|numeric',
                    'hourly_rate'=> 'nullable|numeric',
                    'timezone'=> 'nullable',
                    'subscribe_notification_interval'=> 'nullable',
                    
                ]);

                if($valid->fails()){
                    return response()->json($valid->errors()->toJson(), 400);
                }

                $first_name = $request->first_name;
                $bio = $request->bio;
                $dob = $request->dob;
                $last_name = $request->last_name;       
                $role = $request->role;
                $category_id = $request->category_id;  
                $phone = $request->phone;          
                $paypalaccoun = $request->paypalaccount;
                $user_type = $request->user_type;
                
                if($user_type == "Tutor" || $user_type == "Both"){
                    $is_active = 1;    
                }
                else{
                    $is_active = 0;    
                }

                $hourly_rate = $request->hourly_rate;
                $subscribe_category = $request->subscribe_category;
                $timezone = $request->timezone;
                $subscribe_notification_interval = $request->subscribe_notification_interval;
                $file = $request->file('profile_pic');
                
                if(!empty($file)){
                    try{
                        $filename = $file->getClientOriginalName();
                        if(!empty($filename)){
                            $file->storeAs('/public/user-profile-pic/id-'. $users->id, $filename);
                            $pic[]=url('/').'/storage/user-profile-pic/id-'.$users->id.'/'. $filename;
                        } 
                    }catch (\Exception $e) {
                        return $e->getMessage();
                    }
                }
                else{
                    $pic[] = ''; 
                } 

                if(isset($first_name) && (!empty($first_name) || $first_name!='')){ 
                    $update = Get_User::where('user_email',$request->email)->update(array('first_name' => $first_name));
                }
                    
                if(isset($last_name) && (!empty($last_name) || $last_name!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('last_name' => $last_name));
                }

                if(isset($bio) && (!empty($bio) || $bio != '')){
                    $update = Get_User::where('user_email',$request->email)->update(array('whoami' => $bio));
                }

                if(isset($phone) && (!empty($phone) || $phone != '')){
                    $update = Get_User::where('user_email',$request->email)->update(array('phone' => $phone));
                }

                if(isset($paypalaccount) && (!empty($paypalaccount) || $paypalaccount != '')){
                    $update = Get_User::where('user_email',$request->email)->update(array('paypalaccount' => $paypalaccount));
                }

                if(isset($user_type) && (!empty($user_type) || $user_type != '')){
                    $update = Get_User::where('user_email',$request->email)->update(array('user_type' => $user_type));
                }

                if(isset($hourly_rate) && (!empty($hourly_rate) || $hourly_rate != '')){
                    $update = Get_User::where('user_email',$request->email)->update(array('hourly_rate' => $hourly_rate));
                }

                if(isset($category_id) && (!empty($category_id) || $category_id != '')){
                    $update = Get_User::where('user_email', $request->email)
                    ->where('registration_type', $request->type)
                    ->update(array('category_id' => $category_id));
                }

                if(isset($subscribe_category) && (!empty($subscribe_category) || $subscribe_category!='')){
                    $update = Get_User::where('user_email',$request->email)
                    ->where('registration_type', $request->type)
                    ->update(array('subscribe_category' => $subscribe_category));
                }

                if(isset($timezone) && (!empty($timezone) || $timezone!='')){
                    $update = Get_User::where('user_email', $request->email)
                    ->where('registration_type', $request->type)
                    ->update(array('timezone' => $timezone));
                }

                if(isset($subscribe_notification_interval) && (!empty($subscribe_notification_interval) || $subscribe_notification_interval!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('subscribe_notification_interval' => $subscribe_notification_interval));
                }

                if(isset($pic) && (!empty($pic[0]) || $pic[0]!='')){
                    $update = Get_User::where('user_email', $request->email)->update(array('profile_pic' => implode(',',$pic)));
                }

                if(isset($dob) && (!empty($dob) || $dob!='')){
                    $update = Get_User::where('user_email', $request->email)
                    ->where('registration_type', $request->type)
                    ->update(array('dob' => $dob));
                }

                if(isset($role) && (!empty($role) || $role!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('role' => $role));
                }
                
                if(isset($is_active) && (!empty($is_active) || $is_active != '')){ 
                    $update = Get_User::where('user_email',$request->email)->update(array('is_active' => $is_active));
                }

                $data = Get_User::where('user_email', $request->email)->first();

                return response()->json(['status'=>'success','message'=> 'Profile updated successfully','data'=>$data], 200);                                                    
            }    
            else{
                $valid = Validator::make($request->all(),[
                    'profile_pic'=>'nullable |mimes:jpeg,bmp,,jpg,png,gif',
                    'first_name'=>'nullable',
                    'last_name'=>'nullable',
                    'dob'=>'nullable',
                    'password' =>'nullable|string|min:8',
                    'paypalaccount'=> 'nullable',
                    'subscribe_category'=> 'nullable',
                    'category_id'=> 'nullable|numeric',
                    'user_type'=> 'nullable',
                    'hourly_rate'=> 'nullable|numeric',
                    'timezone'=> 'nullable',
                    'category_id'=> 'nullable',
                    'subscribe_notification_interval'=> 'nullable',    
                ]);

                if($valid->fails()){
                    return response()->json($valid->errors()->toJson(), 400);
                }

                $first_name = $request->first_name;
                $password = $request->password; 
                $bio = $request->bio;
                $dob = $request->dob;
                $last_name = $request->last_name;       
                $role = $request->role;
                $phone = $request->phone; 
                $category_id = $request->category_id;         
                $paypalaccount=$request->paypalaccount;
                $user_type = $request->user_type;
                if($user_type == "Tutor" || $user_type == "Both"){
                    $is_active = 1;    
                }
                else{
                    $is_active = 0;    
                }
                $hourly_rate = $request->hourly_rate;
                $subscribe_category = $request->subscribe_category;
                $timezone = $request->timezone;
                $subscribe_notification_interval = $request->subscribe_notification_interval;
                $file = $request->file('profile_pic');
                //profile pic
                if(!empty($file)){
                    try{
                        $filename=$file->getClientOriginalName();
                        if(!empty($filename)){
                            $file->storeAs('/public/user-profile-pic/id-'. $users->id, $filename);
                            $pic[]=url('/').'/storage/user-profile-pic/id-'.$users->id.'/'. $filename;
                        } 
                    }catch (\Exception $e) {
                        return $e->getMessage();
                    }
                }
                else{
                    $pic[] = ''; 
                }               
                if(isset($first_name) && (!empty($first_name) || $first_name!='')){ 
                    $update = Get_User::where('user_email',$request->email)
                    ->update(array('first_name' => $first_name));
                }
                    
                if(isset($last_name) && (!empty($last_name) || $last_name!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('last_name' => $last_name));
                }

                if(isset($bio) && (!empty($bio) || $bio!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('whoami' => $bio));
                }

                if(isset($phone) && (!empty($phone) || $phone!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('phone' => $phone));
                }

                if(isset($paypalaccount) && (!empty($paypalaccount) || $paypalaccount!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('paypalaccount' => $paypalaccount));
                }

                if(isset($user_type) && (!empty($user_type) || $user_type!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('user_type' => $user_type));
                }
                if(isset($category_id) && (!empty($category_id) || $category_id!='')){
                    $update = Get_User::where('user_email', $request->email)
                    ->update(array('category_id' => $category_id));
                }

                if(isset($subscribe_category) && (!empty($subscribe_category) || $subscribe_category!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('subscribe_category' => $subscribe_category));
                }

                if(isset($hourly_rate) && (!empty($hourly_rate) || $hourly_rate!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('hourly_rate' => $hourly_rate));
                }

                if(isset($timezone) && (!empty($timezone) || $timezone!='')){
                    $update = Get_User::where('user_email', $request->email)
                    ->update(array('timezone' => $timezone));
                }

                if(isset($subscribe_notification_interval) && (!empty($subscribe_notification_interval) || $subscribe_notification_interval!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('subscribe_notification_interval' => $subscribe_notification_interval));
                }

                if(isset($pic) && (!empty($pic[0]) || $pic[0]!='')){
                    $update = Get_User::where('user_email', $request->email)->update(array('profile_pic' => implode(',',$pic)));
                }

                if(isset($dob) && (!empty($dob) || $dob!='')){
                    $update = Get_User::where('user_email', $request->email)
                    ->update(array('dob' => $dob));
                }

                if(isset($role) && (!empty($role) || $role!='')){
                    $update = Get_User::where('user_email',$request->email)->update(array('role' => $role));
                }
                if(isset($password) && (!empty($password) || $password!='')){
                    $password =Hash::make($request->password);
                    $update = Get_User::where('user_email',$request->email)
                    ->update(array('password' => $password));
                }
                if(isset($is_active) && (!empty($is_active) || $is_active != '')){ 
                    $update = Get_User::where('user_email',$request->email)->update(array('is_active' => $is_active));
                }
                $data = AppUsers::withCount('userQuestion')->withCount('userAnswer')->withCount(['userAnswerPurchased' => function($q) {
                $q->where('purchase', '1');}])->withCount(['userEarning AS user_earning' => function($q) {
                         $q->select(DB::raw('sum(amount)'))->where('withdrawal', '0');
                    }])->where('is_verified', '1')->where('is_active', '1')->where('user_email', $request->email)->first();
                return response()->json(['status'=>'success','message'=> 'Profile updated successfully','data'=>$data], 200);
          
            }
        }
        else{
            return response()->json(['status'=>'error','message'=> 'Sorry, user does not exist!'], 404);
        }
                
    }

    // profile picture upadate
    function picUpdate(Request $request)
    { 
        $userId = $request->userId;
        $valid =Validator::make($request->all(),[
            'userId'=>'required',
            'profile_pic'=>'required',
            
        ]);
        if($valid->fails()){
            return response()->json($valid->errors()->toJson(), 400);
        }

        $files = $request->file('profile_pic');

        if($files){
            $filename=$files->getClientOriginalName();
             $profile_pic =  $files->storeAs('/public/user-profile-pic/id-'. $userId, $filename);
            $pic=env('APP_URL').'/storage/user-profile-pic/id-'.$userId.'/'. $filename;
        }
        else{
            $pic='';  
        }

        $profile = Get_User::where('id', '=',$userId)
        ->where('is_verified', '=', '1')
        ->where('is_active', '=', '1')
        ->update(array('profile_pic' => $pic));

        if($profile==1){
            // print_r('upadte success');
            return response()->json(['status'=>'success','message'=> 'Profile updated successfully'], 200);
        } 
        else{
            return response()->json(['status'=>'0','message'=> 'Unable to update'], 400);
        }  
    }
    // user information by their id
    public function userInfo(Request $request){
        if(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 400);
          }
          $user = AppUsers::withCount('userQuestion')->withCount('userAnswer')->withCount(['userAnswerPurchased' => function($q) {
                $q->where('purchase', '1');}])->withCount(['userEarning AS user_earning' => function($q) {
                         $q->select(DB::raw('sum(amount)'))->where('withdrawal', '0');
                        // return $q->where('to', $user_id)->where('withdrawal', '0');
                    }])->where('is_verified', '1')->where('is_active', '1')->where('id',$request->user_id)->first();
          $rating = Feedback::where('rated_to',$request->user_id)->get();
          $rating_count = $rating->count()!=0?$rating->count():-1;
          $average_rating = round($rating->sum("rating")/$rating_count, 1);
          if(empty($user)){
            return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
          }else{
            return response()->json(['status'=>'success', 'User Details'=> $user,'Rating'=>$average_rating], 200);
          }
    }
}
