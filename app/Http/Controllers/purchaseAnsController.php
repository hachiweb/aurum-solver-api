<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Question;
use App\Get_User;
use App\Answer;
use App\AppUsers;
use App\Transaction;
class purchaseAnsController extends Controller
{
    //purchase answers
    
    public function purchaseAns(Request $request){
        $valid =Validator::make($request->all(),[
            'answer_id'=>'required',
            'user_id'=>'required',
            'amount'=>'nullable',
            'status'=>'required',
            'transaction_id'=>'required',     
            ]);
            if($valid->fails()){
                return response()->json($valid->errors()->toJson(), 400);
            }
            $user = Get_User::find($request->user_id);
            if(empty($user)){
                return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
            }
            else{
                $total_balance = Transaction::where('to',$request->user_id)->where('status','paid')->where('withdrawal',0)->sum('amount');
                $total_expand = Transaction::where('from',$request->user_id)->where('to', '!=',$request->user_id)->where('status','paid')->where('withdrawal',0)->sum('amount');
                $total = number_format(($total_balance -  $total_expand),2);
              
                $answers = Answer::find($request->answer_id); 
              
                if(!empty($answers)){
                    $total_pay = $answers->desired_price+(10*$answers->desired_price/100);
                    $answer_amount = round($total_pay , 2);            
                if($answer_amount <= $total){
                    $from_questioner = round(($answer_amount - $answers->desired_price),2);
                    $from_answerer = round((30*$answers->desired_price/100),2);
                    $answerer_fee = $answer_amount -($from_answerer + $from_questioner);
                    $admin_fee = round(($from_answerer + $from_questioner),2);
                    $buy = new Transaction;
                    $buy->answer_id = $request->answer_id;
                    $buy->question_id = $answers->question_id;
                    $buy->from = $request->user_id;
                    $buy->amount = $answerer_fee;
                    $buy->to = $answers->answerer_id;
                    $buy->status = $request->status;
                    $buy->transaction_id = $request->transaction_id;
                    $result = $buy->save();
                    if($result==1){
                        Answer::where('id', $request->answer_id)->update(array('purchase' =>1,'questioner_fee'=>$answer_amount,'answerer_fee'=>$answerer_fee,'admin_fee'=>$admin_fee));
                        $answer= Answer::with('userDetail')->where('id', $request->answer_id)->first();
                        return response()->json(['status'=>'success','message'=> 'Answer purchase successfully','transaction_details'=>$buy,'answers'=>$answer], 200);
                    }
                } else{
                    return response()->json(['status'=>'error','message'=> 'You have no enough balace.'], 403);
                }  
                } else{
                    return response()->json(['status'=>'error','message'=> 'Answer id does not exist'], 404);
                }    
                
            }
        }

    //Answers
    public function myansQuestion(Request $request){
        $valid =Validator::make($request->all(),[
            'answer_id'=>'required',
            'user_id'=>'required',
            ]);
            if($valid->fails()){
                return response()->json($valid->errors()->toJson(), 400);
            }
           $chechId = Get_User::find($request->user_id);
        if(empty($chechId)){
            return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
        }else{
        $buyans = Transaction::where('from',$request->user_id)->where('answer_id',$request->answer_id)->count(); 
        if($buyans >0){
            $buyans = Answer::where('id',$request->answer_id)->where('purchase', 1)->get(); 
            return response()->json(['status'=>'success','message'=> 'Answer details','data'=>$buyans], 200);
            
        }else{
            return response()->json(['status'=>'error','message'=> 'You have no purchase details'], 200);
        }
        
        }
    }
    // Paid answer details
    public function MyBuyAnswer(Request $request){
        if(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 400);
          }
          $user = AppUsers::find($request->user_id);
          
        if(empty($user)){
        return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 404);
        }else{
            $answer = Transaction::with('AnswerDetails')->where('from',$request->user_id)->where('to','!=',$request->user_id)->count();
            if($answer > 0){
                $answer = Transaction::with('AnswerDetails')->with('AnswererDetail')->where('from',$request->user_id)->where('to','!=',$request->user_id)->get();
                return response()->json(['status'=>'success', 'Purchase details'=> $answer], 200);
              
            }else{         
                return response()->json(['status'=>'error', 'message'=>'There are no answer purchase records'], 404);    
            }
                 
            
        }
    }
}