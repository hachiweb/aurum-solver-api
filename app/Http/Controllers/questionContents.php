<?php

namespace App\Http\Controllers;
use App\Question;
use App\Get_User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class questionContents extends Controller
{ 
    

    //Ask question 
    public function askQuestion(Request $request ){
      $validator =Validator::make($request->all(),[
        // 'attachment'=>'nullable |mimes:jpeg,bmp,png,jpg,gif,svg,pdf,docx',
        'question_id'=>'nullable',
        'user_id'=>'required|numeric',
        'title'=>'required',
        'question'=>'required',
        'budget_amount'=>'nullable',
        'tutor_id' =>'nullable',
        'private_question'=>'nullable',
        'assignment'=>'nullable',
        'due_date'=>'required',
        'subject'=>'nullable',
        'category_id'=>'required|numeric'
      ]);
          
      if($validator->fails()){
        return response()->json($validator->errors()->toJson(), 400);
      }
      $questionerId = $request->user_id;
      $userId = Get_User::find($questionerId);  
      if(!empty($userId)){
          $addquestions = new Question;
          $files = $request->file('attachment');
          if(!empty($files) && is_array($files)){
            try{
              foreach($files as $file){
                if($file){
                    $filename=$file->getClientOriginalName();
                    if(!empty($filename)){
                      $qfile  = 'questions-' . time() . '.' . $filename;
                      $file->storeAs('/public/attachments/questions/user-id-'. $questionerId, $qfile);
                      $pic[]=url('/').'/storage/attachments/questions/user-id-'.$questionerId.'/'. $qfile;
                    }
                }
                else{
                  $pic[]='';  
                } 
              }
            }
            catch (\Exception $e) {
              return $e->getMessage();
            }
        }else{
          $pic[]='';  
        }
        $tutor_id = isset($request->tutor_id)?trim($request->tutor_id):'';
        $private = !empty($request->private_question)?$request->private_question:0;
        $is_assigned = !empty($request->assignment)?$request->assignment:0;
        $tutorId = Get_User::find($tutor_id);          
        if(isset($request->tutor_id) && (empty($tutorId))){ 
          return response()->json(['status'=>'error','message'=> 'Tutor id '.$tutor_id .' does not exist'], 404);
        }
        if(($is_assigned==1) && !isset($request->tutor_id) ){ 
          return response()->json(['status'=>'error','message'=> 'Please select tutor id.'], 404);
        }
       
       
        $pay = number_format(round($request->budget_amount , 2),2);
        $pay2 = round($request->budget_amount , 2);
        $questioner_fee = number_format(round($pay2+(5*$pay2/100),2),2);
        $questioner_fee2 = round($pay2+(5*$pay2/100),2);
        $admin_from = round(($questioner_fee2 - $pay2),2);
        $final = round((20*$pay2/100),2);
        $admin_fee = number_format($final + $admin_from,2);
        $admin_fee2 = $final + $admin_from;
        $answerer_fee = number_format(($questioner_fee2-$admin_fee2), 2);
        if(!isset($request->question_id)){    
          if($questionerId != $tutor_id):  
            $post_date = date('Y-m-d');   
            $addquestions->user_id = $questionerId ;
            $addquestions->tutor_id = $tutor_id;
            $addquestions->category_id = $request->category_id;
            $addquestions->title = $request->title;
            $addquestions->attachment = implode(',',$pic);
            $addquestions->question = $request->question;
            $addquestions->post_date =$post_date;
            $addquestions->due_date =$request->due_date;
            $addquestions->private_question = $private;
            $addquestions->is_assigned = $is_assigned;
            $addquestions->willing_to_pay = $pay;
            $addquestions->questioner_fee = $questioner_fee;
            $addquestions->answerer_fee = $answerer_fee;
            $addquestions->admin_fee = $admin_fee; 
            $result = $addquestions->save();
            if($result==true):
              $data = Question::orderBy('id', 'desc')->first();
              return response()->json(['status'=>'success','message'=> 'Question has been submitted.', 'data'=>$data], 200);
            endif;
            else:
            return response()->json(['status'=>'error','message'=> 'This '.$tutor_id .' is your own tutor id.'], 400);
          endif;
        }
        else{
          $checkqQuestion = Question::find($request->question_id);
          $update = Question::where('id',$request->checkqQuestion)->update(array('user_id'=>$questionerId,'tutor_id'=>$tutor_id,'category_id'=>$request->category_id,'title'=>$request->title,'attachment'=>implode(',',$pic),'question'=>$request->question,'post_date'=>date('Y-m-d'),'due_date'=>$request->due_date,'private_question'=>$private,'is_assigned'=>$is_assigned,'willing_to_pay'=>$pay,'questioner_fee'=>$questioner_fee,'answerer_fee'=>$answerer_fee,'admin_fee'=>$admin_fee));
          $data = Question::orderBy('id', 'desc')->first();
          return response()->json(['status'=>'success','message'=> 'Question has been updated.', 'data'=>$data], 200);
        }   
          // }
          // else {
          //   return response()->json(['status'=>'error','message'=> 'Tutor id '.$tutor_id .' does not exist'], 404);
          // }
        // }
        // else{                    
        //   $pay = number_format(round($request->budget_amount , 2),2);
        //   $pay2 = round($request->budget_amount , 2);
        //   $questioner_fee = number_format(round($pay2+(5*$pay2/100),2),2);
        //   $questioner_fee2 = round($pay2+(5*$pay2/100),2);
        //   $admin_from = round(($questioner_fee2 - $pay2),2);
        //   $final = round((30*$pay2/100),2);
        //   $admin_fee = number_format($final + $admin_from,2);
        //   $admin_fee2 = $final + $admin_from;
        //   $answerer_fee = number_format(($questioner_fee2-$admin_fee2), 2); 
        //   $addquestions->user_id =  $questionerId ;
        //   $addquestions->category_id = $request->category_id;
        //   $addquestions->title = $request->title;
        //   $addquestions->attachment = implode(',',$pic);
        //   $addquestions->title = $request->title;
        //   $addquestions->question = $request->question;
        //   $addquestions->post_date = date('Y-m-d');
        //   $addquestions->due_date =$request->due_date;
        //   $addquestions->private_question = $private;
        //   $addquestions->is_assigned = $is_assigned;
        //   $addquestions->willing_to_pay = $pay;
        //   $addquestions->questioner_fee = $questioner_fee;
        //   $addquestions->answerer_fee = $answerer_fee;
        //   $addquestions->admin_fee = $admin_fee; 
        //   $result = $addquestions->save();
        //   $data = Question::orderBy('id', 'desc')->first();
        //   if($result==true){
        //     return response()->json(['status'=>'success','message'=> 'Question has been submitted.', 'data'=>$data], 200);
        //   }
        // }  
      }else{
        return response()->json(['status'=>'error','message'=> 'User id does not exist'], 404);
      }         
  }
    
    
}