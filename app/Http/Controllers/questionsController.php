<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Question;
use App\Answer;
use App\AppUsers;
use App\Feedback;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
class questionsController extends Controller
{     
    //assignment request update
    public function assignmentUpdate(Request $request){
        $validator =Validator::make($request->all(),[
            'assignment_id' => 'required',
            'user_id' =>'required',      
        ]); 
        if($validator->fails()){
          return response()->json($validator->errors()->toJson(), 400);
        }
       
        $result = Question::where('id',$request->assignment_id)->where('tutor_id',$request->user_id)->where('is_assigned',1)
        ->count();
        if($result==0){
            return response()->json(['status'=>'error', 'message'=>'No assignment'], 404);
        }else{
            $validator = Validator::make($request->all(),[
                'status_request'=>'required|numeric|max:1',    
            ]);
            
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }
            $request_status = $request->status_request;
            if($request_status == 1){ 
                $result = Question::where('id',$request->assignment_id)->where('tutor_id',$request->user_id)->where('is_assigned',1)->update(array('request_accept' =>1));
                if($result==1){
                    return response()->json(['status'=>'success','message'=> 'Assignment request accepted'],200);
                }
            }else{
                $result = Question::where('id',$request->assignment_id)->where('tutor_id',$request->user_id)->where('is_assigned',1)->update(array('request_accept' =>0));
                if($result==1){
                    return response()->json(['status'=>'success','message'=> 'Assignment request rejected'],200);
                }
            }
        } 
    }
    //  asked  questions
    public function askedQuestions(Request $request){
      
        if(!isset($request->user_id)){
          return response()->json($validator->errors()->toJson(), 400);
        }
        $userId = $request->user_id;
        $users = AppUsers::where('id',$userId)->where('is_verified', '1')->where('is_active','1')->count();
        if($users > 0){
            $result= Question::with('categoryDetail')->where('user_id',$userId)->count();
            if($result > 0){
                $question = Question::with('categoryDetail')->withCount('totalAnswers')->where('user_id',$userId)->get();
                return response()->json(['Status'=>'success','data'=>$question], 200);      
            }
            else{
                return response()->json(['status'=>'error','message'=>'No question details.'],404);
            }
        }
        else{
            return response()->json(['status'=>'error','message'=>'User id does not exist'],404);
        }
        
    }
      
    // Get question by id
    public function getQuestion(Request $request){
        if(!isset($request->id)){
          return response()->json(['status'=>'error', 'message'=>'Missing id'], 200);
        }
        $question_detail = Question::find($request->id);
     
        if(empty($question_detail)){
            return response()->json(['status'=>'error', 'message'=>'Invalid id'], 200);
        }
        else{
            $question_detail['questioner'] = !empty(AppUsers::find($question_detail->user_id))?AppUsers::find($question_detail->user_id)->getOriginal():'';
            $answers = Answer::where('question_id',$request->id)->get();

            if($answers->isNotEmpty()){
                foreach($answers as $key=>$val){
                    $ans[$key]['id'] = $val->id;
                    $ans[$key]['answerer_details'] = !empty(AppUsers::find($val->answerer_id))?AppUsers::find($val->answerer_id)->getOriginal():'';
                    $rating = Feedback::where('rated_to',$val->answerer_id)->get();
                    $feedback = Feedback::select('rating', 'review', 'rated_by', 'created_at')->with('reviewerDetails')->where('rated_for_id',(int)$val->id)->orderBy('id', 'DESC')->first();
                    $rating_count = $rating->count()!=0?$rating->count():-1;
                    $average_rating = round($rating->sum("rating")/$rating_count, 1);
                    $buyers = Answer::where('answerer_id',$request->id)->where('purchase','1')->count();
                    $is_reviewed = Answer::withCount('isReviewed as is_reviewed')->where('id',$val->id)->first()->is_reviewed;
                    if(!empty($ans[$key]['answerer_details'])){
                        $ans[$key]['answerer_details']['average_rating'] = $average_rating;
                        $ans[$key]['answerer_details']['buyers'] = $buyers;
                    }
                    $ans[$key]['attachments'] = $val->ans_attach;
                    $ans[$key]['category'] = $val->category;
                    $ans[$key]['title'] = $val->title;
                    $ans[$key]['answer'] = $val->answer;
                    $ans[$key]['feedback'] = $feedback;
                    $ans[$key]['desired_price'] = $val->desired_price;
                    $ans[$key]['is_reviewed'] = $is_reviewed;
                    $ans[$key]['purchase'] = $val->purchase;
                    $ans[$key]['created_at'] = $val->created_at;
                    if(is_array($ans[$key]['answerer_details'])){
                        ksort($ans[$key]['answerer_details']);
                    }
                }
                $question_detail['answerers'] = $ans;
            }
            else{
                $ans = array("");
            }
            return response()->json(['status'=>'success', 'data'=>$question_detail], 200);
        } 
    }
    // Assigned questions
    public function assignedQuestion(Request $request){
        if(!isset($request->user_id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 400);
          }
          $user = AppUsers::find($request->user_id);
       
          if(empty($user)){
              return response()->json(['status'=>'error', 'message'=>'Invalid  id'], 200);
          }
          else{
            // $user = Apps_user::find($request->user_id);
                $is_assigned_flag = 1;
                $result = Question::where('tutor_id',$request->user_id)->with(['tutorDetail' => function($q) use ($is_assigned_flag){
        $q->where('is_assigned', $is_assigned_flag);}])->count();
                if($result > 0){
                    $question = Question::with('categoryDetail')->with('assignerDetail')->where('tutor_id',$request->user_id)->where('is_assigned',1)->get();
                    
                    return response()->json(['status'=>'success','data'=>$question], 200);      
                }
                else{
                    return response()->json(['status'=>'error','message'=>'There are no questions'],404);
                }
          }        
        
    }
    // show publish questions
    public function allQuestions(){
       
             $result= Question::select('*')->count();
            //  Por
            if($result >0){
                $allQuestions= Question::with('categoryDetail')->withCount('totalAnswers')->with('userDetail')->get();
                return response()->json(['Status'=>'success','data'=>$allQuestions], 200);      
            }
            else{
                return response()->json(['status'=>'error','message'=>'Data not available'],404);
            }
       
    }   
    public function cmp($a, $b) {
        return strcmp($a->name, $b->name);
    }
}