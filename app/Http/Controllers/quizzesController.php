<?php

namespace App\Http\Controllers;
use App\Quizze;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class quizzesController extends Controller
{
  //quizzes questions
  public function quizAttempts(Request $request){
    $validator = Validator::make($request->all(),[
        'questions' => 'required',
        'optB' => 'required',
        'optC' => 'required',
        'optD' => 'required',
        'optA' => 'required',
        'correct_option' => 'required',
    ]); 
    if($validator->fails()){
        return response()->json($validator->errors()->toJson(), 400);
    }
    $question = new Quizze;
    // $check = Quizze::select('*')->get();
    // print_r($question);
    $question->questions = $request->questions;
    $question->opt1 = $request->optA;
    $question->opt2 = $request->optB;
    $question->opt3 = $request->optC;
    $question->opt4 = $request->optD;
    $question->correct = $request->correct_option;
    $result = $question->save();
    if($result){
      return response()->json(['status'=>'success','message'=> 'Created Quizz'], 200);
    }
    else{
      return response()->json(['status'=>'success','message'=> 'unable to quizzes questions'], 400);
    }
  }

  public function ansAtempts(){
    $check = Quizze::select('*')->get();
    return response()->json(['status'=>'success','message'=> 'Answers attempted','data'=>$check], 200);
  }
  // user quiz available

//   public function ansAtempts(){
//     $check = Quizze::select('*')->get();
//     return response()->json(['status'=>'success','message'=> 'Answers attempted','data'=>$check], 200);

//     if(!isset($request->id)){
//       return response()->json(['status'=>'error', 'message'=>'Missing id'], 200);
//     }
//     $question_detail = Question::find($request->id);

//     if(empty($question_detail)){
//         return response()->json(['status'=>'error', 'message'=>'Invalid id'], 200);
//     }
//     else{
//     }
// }
}