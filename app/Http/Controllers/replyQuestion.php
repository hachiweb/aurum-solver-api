<?php

namespace App\Http\Controllers;
use\App\Question;
use\App\Get_User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class replyQuestion extends Controller
{
    //show and reply answers
    public function showQuestion(Request $request){
        $validator =Validator::make($request->all(),[
            'user_email' => 'required|string|email|max:255',
            'type' => 'required|string',
            // 'token' => 'required
            ]); 
            if($validator->fails()){
              return response()->json($validator->errors()->toJson(), 400);
            }
          
          $user = Question::all();
          
        return response()->json($user, 200);
    }
    public function replyAns(Request $req){
        $validator =Validator::make($req->all(),[
            'user_email' => 'required|string|email|max:255',
            'type' => 'required|string',
            // 'token' => 'required
            ]); 
            if($validator->fails()){
              return response()->json($validator->errors()->toJson(), 400);
            }
          
          $user = Get_User::select('*')
                    ->where('user_email', '=', $req->user_email)
                    ->where('registration_type', '=', $req->type)
                    ->where('is_active', '=', '1')
                    ->where('is_verified', '=', '1')
                    ->count();
            // return $user;
            if($user >0){
               
                $valid =Validator::make($req->all(),[
                    'attachment'=>'nullable |mimes:jpeg,bmp,png,gif,svg,pdf,docx',
                    'answers'=>'nullable',
                    'user_id'=>'required'
                ]);
                if($valid->fails()){
                    return response()->json($valid->errors()->toJson(), 400);
                  }
                //   print_r("Ask questions");
                  $users = Question::find($req->user_id);
                  $files = $req->file('attachment');
                  foreach($files as $file){
                    if($file){
                        $filename=$file->getClientOriginalName();
                        if(!empty($filename)){
                            print_r($filename);
                            $file->storeAs('/public/user-profile-pic/id-'. $user->id, $filename);
                            $pic[]=url('/').'/storage/user-profile-pic/id-'.$user->id.'/'. $filename;
                        }
                    }
                    else{
                        $pic[]='';  
                    }
                }
                  $extension = $files->getClientOriginalExtension();
                  Storage::disk('public')->put($files->getFilename().'.'.$extension,  File::get($files));
                  $attach = $files->getFilename().'.'.$extension;
                  $ans=Question::where('user_id','=',$req->user_id)
                            ->update(array('answers'=>$req->answers,'answers_attach'=>$attach));
                            
                // return response()->json($users, 200);
                    if($ans==true){
                        return response()->json(['status'=>'success','message'=>'Replied.' ], 200);
                    }
                    else{
                        echo 'unable to answers.';
                    }
                // $answers = $req->answers;
                // $users->answers = $answers;
                // $result= $users->save();
                return response()->json($users, 200);
                return $users;
            }else{
               
            return response()->json(['status'=>'error','message'=> 'invalid id'], 200);
            }
        
    }
     
}
