<?php

namespace App\Http\Controllers;
use App\Quizze;
use App\Quiz_answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class replyQuizzeeController extends Controller
{
    //
    public function quizAns(Request $r)

    {
      $validator =Validator::make($r->all(),[
        'user_id'=>'required',   
        'question_attempt'=>'required',   
        'is_right'=>'required',   
        'marks'=>'required',
        'category'=>'required',   
        'total_questions'=>'required',   
        'question_attempt'=>'required',   
        'duration'=>'required',
        'per'=>'required'   
      ]);
      $questions = Quizze::select('*')->first();
      if($validator->fails()){
        return response()->json($validator->errors()->toJson(), 400);
      }
      $quiz = new Quiz_answer;
      $category = $r->category;
      $user_id = $r->user_id;
      $total_questions = $r->total_questions;
      $question_attempt = $r->question_attempt;
      $is_right = $r->is_right;
      $marks = $r->marks;
      $duration = $r->duration;
      $quiz->user_id = $user_id;
      $quiz->total_questions = $total_questions;
      $quiz->is_right = $is_right;
      $quiz->marks = $marks;
      $quiz->category = $category;
      $quiz->question_attempt = $question_attempt;
      $quiz->per = $r->per;
      $quiz->Duration = $duration;
      $result = $quiz->save();
      if($result):
          return response()->json(['status'=>'success','message'=>'Record saved'],200);
      endif;
    }
}
