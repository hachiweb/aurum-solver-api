<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Question;
use App\Prepayments;
use App\Get_User;
use App\Answer;
use App\AppUsers;
use App\Transaction;
use App\Feedback;
use Illuminate\Support\Facades\DB;

class transactionsController extends Controller
{


//******************** */Prepayments request to user/questioner***************/ 
    public function prePayments(Request $request){
        $validator =Validator::make($request->all(),[
            'user_id'=>'required|numeric',           
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $userId = $request->user_id;
        $user = Get_User::where('id', $userId)->count();
        if($user > 0){
            $validator =Validator::make($request->all(),[  
                'question_id'=>'required',   
                'requested_amount' => 'required'  
            ]);
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }
            $question_id = $request->question_id;
            $assign_question = Question::where('tutor_id', $userId)->where('id',$question_id)->count();
            if($assign_question > 0){
                $pre = Question::where('tutor_id', $userId)->where('id',$question_id)->first();
                $prepaid = Transaction::where('from', $userId)->where('to',$pre->user_id)->where('question_id',$question_id)->sum('amount');
              
                $requested_amount = $request->requested_amount; 
                $total_pay = $pre->willing_to_pay+(10*$pre->willing_to_pay/100);
                $answer_amount = round($total_pay , 2);  
                $from_questioner = round(($answer_amount - $pre->willing_to_pay),2);
                $from_answerer = round((30*$pre->willing_to_pay/100),2);
                $answerer_fee =($answer_amount -($from_answerer + $from_questioner));
                $admin_fee = round(($from_answerer + $from_questioner),2); 
                // if($prepaid >= 0){
                //     $paymentrequest = (($answerer_fee-$prepaid)*90)/100; 
                // }else{
                //     $paymentrequest = ($answerer_fee*90)/100; 
                // }
                $paymentrequest = ($answerer_fee*90)/100;

                // // $total_available_balance = $answerer_fee-$prepaid;
                
                // // // if($total_available_balance <= $answerer_fee){
                // //     if($total_available_balance <= $paymentrequest){
                // //         echo 'you can create';
                // //         print_r($total_available_balance);
                // //         print_r($paymentrequest);
                // //     }
                // //     else{
                // //         echo 'you have already  created';
                // //     }
                 
                // // echo "Payment Request by Answerer Fee:".$paymentrequest.'<br>';
                // // echo "Questioner Fee:".$total_pay.'<br>';
                // // echo "answerer_fee Fee:".$answerer_fee.'<br>';
                // // echo "admin_fee Fee:".$admin_fee.'<br>';
                // // print_r($prepayment_fee);
                // // print_r($prepaid);
                // print_r($paymentrequest);
                // exit;
               
                // $previous_requests = Prepayments::where('question_id',$question_id)->where('from_request', $userId)->sum('requested_amount');
                // if(($requested_amount+$previous_requests) > ($budget_amount*90)/100){
                //     return response()->json(['status'=>'error','message'=> 'Total prepayment amount cannot be more than 90% of the budget']);
                // }
                // $to_request = $pre->user_id;        
                // $prepayment = new Prepayments;
                // $prepayment->question_id = $question_id;
                // $prepayment->from_request = $userId;
                // $prepayment->to_request = $to_request;
                // $prepayment->budget_amount = $pre->willing_to_pay;
                // $prepayment->requested_amount = $paymentrequest;  
                $pre =Question::where('tutor_id', $userId)->where('id',$question_id)->first();
                $budget_amount = $pre->willing_to_pay;
                $requested_amount = $request->requested_amount; 
                $previous_requests = Prepayments::where('question_id',$question_id)->where('from_request', $userId)->sum('requested_amount');
                if(($requested_amount+$previous_requests) > ($budget_amount*90)/100){
                    return response()->json(['status'=>'error','message'=> 'Total prepayment amount cannot be more than 90% of the budget']);
                }
                $to_request = $pre->user_id;
                
                $prepayment = new Prepayments;
                $prepayment->question_id= $question_id;
                $prepayment->from_request=$userId;
                $prepayment->to_request=$to_request;
                $prepayment->budget_amount=$budget_amount;
                $prepayment->requested_amount=$requested_amount;  
                $result = $prepayment->save();
                if($result==1){
                    $requested = Prepayments::orderBy('id','desc')->first();
                    return response()->json(['status'=>'success','message'=> 'Prepayment successfully requested','data'=>$requested], 200);
                }else{
                    return response()->json(['status'=>'error',], 400);
                }
               
            } 
            else{
                return response()->json(['status'=>'error','message'=> 'No entries'], 404);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'User id does not exist'], 404);     
        }

    }
    
//******************** */ Check balance details by user id***************
    public function balance(Request $request){
        $userId =$request->user_id;
        if(!isset($userId)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 200);
          }
      
       $user = Get_User::find($userId);
       if(empty($user)){
        return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 200);
        }
 
    else{
        $users = Transaction::where('to', $userId)->orWhere('from', $userId)->count();
            if($users > 0){
                $total_balance =Transaction::where('to', $userId)->where('status','paid')->where('withdrawal',0)->sum('amount');
                $total_expand =Transaction::where('from', $userId)->where('to', '!=', $userId)->where('status','paid')->where('withdrawal',0)->sum('amount');
                $total = number_format((float)($total_balance -  $total_expand),2); 
                $userAmount = Get_User::where('id', $userId)->first();
                $userAmount->total_balance =  $total;
               
                return response()->json(['status'=>'success','data'=> $userAmount], 200); 
            }else{
                return response()->json(['status'=>'error','message'=> 'Balance not available'], 404);  
            }
       }
    }

    // particular user transaction by their id
    public function myTransaction($id){
        
        if(!isset($id)){
            return response()->json(['status'=>'error', 'message'=>'Missing id'], 200);
        }
      
        $user = Get_User::find($id);
        if(empty($user)){
            return response()->json(['status'=>'error', 'message'=>'Invalid user id'], 200);
        }
        $transaction = Transaction::with('fromUsers')->with('toUsers')->with('questionDetail')->where('to', $id)->orWhere('from', $id)->get();
        if($transaction->isNotEmpty()){
            return response()->json(['status'=>'success','message'=>'My transaction details','data'=> $transaction], 200); 
             
        }else{
            return response()->json(['status'=>'error','message'=>'No transaction history','data'=> $transaction], 200); 
        }
    
    }
     // Users Transaction details
     public function getTransactions(){
        $data=Transaction::with('fromUserTransaction')->with('toUserTransaction')->get();
        return view('user.payment',compact('data'));
    }



}
