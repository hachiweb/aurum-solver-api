<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Question;
use App\Get_User;
use App\Answer;
use App\Transaction;
use App\Withdrawal;
use Auth;
use Illuminate\Support\Facades\DB;

class withdrawalAmountController extends Controller
{
    //withdrawal amount
    public function withdrawal(Request $request){
        $valid =Validator::make($request->all(),[
            'user_id'=>'required',
        ]);

        if($valid->fails()){
            return response()->json($valid->errors()->toJson(), 400);
        }
        $user_id = $request->user_id;
        $user = Get_user::find($user_id);
        $total_expand = Transaction::where('from',$request->user_id)->where('to', '!=',$request->user_id)->where('status','paid')->sum('amount');
        $total_balance = (Transaction::where('to',$request->user_id)->where('from',$request->user_id)->where('status','paid')->where('is_gateway',1)->where('withdrawal',0)->sum('amount')) + (Transaction::where('to',$request->user_id)->where('status','paid')->where('is_gateway',0)->where('withdrawal',0)->sum('amount'));
        $amount = $total_balance - $total_expand;
         
        if($amount <= $request->amount){
            return response()->json(['status'=>'error','message'=>'Sorry you do not have sufficient balance.','totalbalance'=>$amount],400);
        }
       if($user->paypalaccount != null):
        $validator =Validator::make($request->all(),[
            'amount' => 'required',
            'transaction_id' => 'required',
            'status' => 'required',
            
        ]); 
            if($validator->fails()){
              return response()->json($validator->errors()->toJson(), 400);
            }   
             
            $user_id =  $request->user_id;;
            // $with = new Transaction;
            // $with->amount = $request->amount;
            // $with->from = $user_id;
            // $with->to = $user_id;
            // $with->transaction_id  = $request->transaction_id;
            // $with->status = $request->status;
            // $result = $with->save();

            $wrequest = new Withdrawal;
            $wrequest->amount = $request->amount;
            $wrequest->user_id = $user_id;
            $result = $wrequest->save();
            if($result):
                $data = Withdrawal::latest()->first();
            return response()->json(['status'=>'success','message'=>'Your request has been created successfully','data'=>$data],200);
        endif;

        else:
            return response()->json(['message'=>'You need to have a paypal account saved in your account profile to request a withdrawl.','data'=>$user]);
        endif;
    }
    public function myAccount(){
        // $id = Auth::user()->id;
        $amount = Question::where('delivery_accept',1)->where('is_paid','1')->sum('admin_fee');
        $tamount = Answer::where('delivery_accepted',1)->where('purchase','1')->sum('admin_fee');
        $withdarawal = withdrawal::where('is_approved',1)->where('user_id',Auth::user()->id)->sum('amount');
        $total_balance = (($tamount + $amount) - $withdarawal);
        return view('user.myaccount',compact('total_balance'));

    }
}