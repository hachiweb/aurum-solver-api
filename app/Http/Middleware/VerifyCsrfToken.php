<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
      '/assignment/request/update', 'assignment/pay', '/get-token','/destroy-token','/user/register','/user/login','/users/{user_id}','/apps/users/','/questions/ask/','/questions/answers/','/questions/add/content/reply/','/verify/email/{token}','/user/social/signup/','/profile/update','/quizzes/questions','/quizzes/attempt','/quizzes/ans','/questions/my/{user_id}','/answer/my/{user_id}','/questions/assigned/{user_id}','/submit/assignment','/answer/assign/update','/questions/del/{id}','/questions/answer/pay/','/answer/buy/','/account/withdrawal','/questions/filter','/prepayments/to','/prepayments/my/{user_id}','/account/funds/add','/fetch/transaction/','/account/preference','/chat/message','/profile/pic/','/questions/all','/submit/answer','/get/balance/{user_id}','/feedback/review','/prepayments/request/update','/search/{search_key}','/prepayments/requested/{user_id}','/delivery/update','/category/quizzes','/categories/all','/users/category/{category_id}','/answer/quiz','/user-profile/{id}','/category/{category_id}','/cancel/assignment/','/assignment/pay/{assignment_id}','/paid/answer/{user_id}','/my/feedbacck/{user_id}','/user/details/{user_id}','/create/category','/my/transaction/{id}','/app/user-profile/{registration_token}','/ondemand/connect','/ondemand/disconnect'
    ];
}
