<?php

namespace App;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class OnDemand extends Model
{
    //On Demand
    protected $fillable = ['user_id','tutor_id','select_duration'];
    protected $table = 'ondemand_tutoring';
    
}
