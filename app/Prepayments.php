<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prepayments extends Model
{
    //prepayment requested
    protected $fillable = ['question_id','from_request','to_request','budget_amount','requested_amount','approve_status'];
    protected $table = 'prepayments';

    public function QuestionDetails()
    {
        return $this->belongsTo('App\Question','question_id','id');
    }
    
    public function RequesterDetails()
    {
        return $this->belongsTo('App\AppUsers','from_request','id');
    }
    public function payerDetails()
    {
        return $this->belongsTo('App\AppUsers','to_request','id');
    }
    public function answererReviews(){
        return $this->belongsTo('App\Feedback','rated_to','id');
        // $rating = Feedback::where('rated_to',$answerer_id)->get();
        // $rating_count = $rating->count()!=0?$rating->count():-1;
        // $average_rating = round($rating->sum("rating")/$rating_count, 1);
        // return $average_rating;
    }
}
