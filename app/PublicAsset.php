<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicAsset extends Model
{
    /**
    * Public Asset for app
    */
    protected $table = 'public_assets';

    public function sampleFunction(){
        return $this->belongsTo('App\Feedback','rated_to','id');
        // $rating = Feedback::where('rated_to',$answerer_id)->get();
        // $rating_count = $rating->count()!=0?$rating->count():-1;
        // $average_rating = round($rating->sum("rating")/$rating_count, 1);
        // return $average_rating;
    }
}
