<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //ask questions
    protected $fillable = ['category','title','question','answer','attachment','answers_attach'];
    // protected $table = 'questions';
    public function userDetail()
    {
        return $this->belongsTo('App\AppUsers','user_id','id');
    }

    //tutor details
    public function tutorDetail()
    {
        return $this->belongsTo('App\AppUsers','tutor_id','id');
    }

    //assigner details
    public function assignerDetail()
    {
        return $this->belongsTo('App\AppUsers','user_id','id');
    }

    //category details
 	public function categoryDetail()
    {
        return $this->belongsTo('App\Category','category_id','id');
    }

    // answer counts
    public function totalAnswers()
    {
        return $this->hasMany('App\Answer','question_id','id');
    }
    public function assignmentAnswer()
    {
        return $this->belongsTo('App\Answer','question_id','id');
    }
    public function answer()
    {
        return $this->belongsTo('App\Answer','id','question_id');
    }

    
}
