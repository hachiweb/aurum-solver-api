<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz_answer extends Model
{
    
    protected $table = 'quiz_answer';

    public function categoryQuiz()
    {
        return $this->hasMany('App\Quizze','category_id','category_id');
    }

    public function categoryDetail()
    {
        return $this->hasMany('App\Category','category_id','id');
    }

    public function quizQuestions()
    {
        return $this->hasMany('App\Quizze','category_id','category_id');
    }

}
