<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quizze extends Model
{
    //Quizzes questions
    protected $fillable = ['questions','opt1','opt2','opt3','opt4','correct'];
    public function categoryQuiz()
    {
        return $this->belongsTo('App\Category','category_id','id');
    }
}
