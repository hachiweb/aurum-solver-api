<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    //
    protected $table = 'faq_support';
    public $timestamps = false;
    
}
