<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    public function AnswerDetails()
    {
        return $this->belongsTo('App\Answer','question_id','question_id');
    }

    public function questionDetail()
    {
        return $this->belongsTo('App\Question','question_id','id');
    }

    public function AnswererDetail()
    {
        return $this->belongsTo('App\AppUsers','to','id');
    }
    
    public function fromUsers()
    {
        return $this->belongsTo('App\AppUsers','to','id');
    }

    public function toUsers()
    {
        return $this->belongsTo('App\AppUsers','from','id');
    }
 
    public function prepaymentRequesterDetail()
    {
        return $this->belongsTo('App\AppUsers','to','id');
    }

    public function fromUserTransaction()
    {
        return $this->belongsTo('App\AppUsers','from','id');
    }
    
    public function toUserTransaction()
    {
        return $this->belongsTo('App\AppUsers','to','id');
    }

    public function userBalance()
    {
        return $this->belongsTo('App\Withdrawal','from','user_id');
    }
}
