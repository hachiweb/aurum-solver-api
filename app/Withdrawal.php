<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    protected $table = 'withdrawals';
    public function requesterUser()
    {
        return $this->belongsTo('App\AppUsers','user_id','id');
    }
    // public function userBalance()
    // {
    //     return $this->hasMany('App\Transaction','from','from');
    // }
}
