-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 11, 2020 at 02:27 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aurum-solver`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `questioner_id` varchar(255) DEFAULT NULL,
  `answerer_id` varchar(255) DEFAULT NULL,
  `subjects` text DEFAULT NULL,
  `questions` text NOT NULL,
  `question_attach` text DEFAULT NULL,
  `ans_attach` text DEFAULT NULL,
  `title` text DEFAULT NULL,
  `ans_title` text DEFAULT NULL,
  `answers` text NOT NULL,
  `desired_price` varchar(200) DEFAULT NULL,
  `post` date DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `questioner_id`, `answerer_id`, `subjects`, `questions`, `question_attach`, `ans_attach`, `title`, `ans_title`, `answers`, `desired_price`, `post`, `duedate`, `created_at`, `updated_at`) VALUES
(1, '74', '75', NULL, 'wahts is management Information?', NULL, NULL, NULL, 'Computer related', 'Managemen t is in information system.', NULL, '2020-04-10', NULL, '2020-04-10 05:18:12', '2020-04-10 05:47:45'),
(3, '75', '70', NULL, 'wahts is management Information?', NULL, NULL, NULL, NULL, 'Information Management System', NULL, '2020-04-11', NULL, '2020-04-11 00:09:02', '2020-04-11 00:09:02'),
(4, '75', '70', NULL, 'wahts is management Information?', NULL, NULL, NULL, NULL, 'Information Management System', NULL, '2020-04-11', NULL, '2020-04-11 00:10:10', '2020-04-11 00:10:10'),
(5, '75', '70', NULL, 'wahts is management Information?', NULL, NULL, NULL, NULL, 'The answer', NULL, '2020-04-11', NULL, '2020-04-11 00:18:22', '2020-04-11 00:18:22'),
(6, '70', '75', NULL, 'wahts is management Information?', NULL, NULL, NULL, NULL, 'Information Management System', NULL, '2020-04-11', NULL, '2020-04-11 01:24:01', '2020-04-11 01:24:01'),
(7, '74', '75', NULL, 'wahts is management Information?', NULL, NULL, NULL, NULL, 'Information Management System', NULL, '2020-04-11', NULL, '2020-04-11 04:33:53', '2020-04-11 04:33:53');

-- --------------------------------------------------------

--
-- Table structure for table `apps_user`
--

CREATE TABLE `apps_user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `whoami` text DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `user_email` varchar(100) NOT NULL,
  `password` varchar(200) DEFAULT NULL,
  `profile_pic` text DEFAULT NULL,
  `role` text DEFAULT NULL,
  `user_type` text DEFAULT NULL,
  `registration_type` varchar(100) NOT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `registration_token` text DEFAULT NULL,
  `verification_token` text DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `apps_user`
--

INSERT INTO `apps_user` (`id`, `first_name`, `last_name`, `whoami`, `dob`, `user_email`, `password`, `profile_pic`, `role`, `user_type`, `registration_type`, `is_verified`, `is_active`, `registration_token`, `verification_token`, `updated_at`, `created_at`) VALUES
(59, NULL, NULL, NULL, NULL, 'rajendra.kumar@hachiweb.coma', '$2y$10$9Sx8Yx2/TI9/6G7z5uXfxOWKSoQVfCVOXJNuM2KFRLTmHEzl7V372', NULL, NULL, NULL, 'email', 0, 0, NULL, 'YL9YGG6X8ADwco0mod5NiulfGnJNXDWq9cT1T9MZBupOVsagRHN2s6amZgXUl8lN7bjLmL', '2020-04-04 06:54:09', '2020-04-04 06:54:09'),
(60, NULL, NULL, NULL, NULL, 'rajendra.kumar@hachiweb.comx', '$2y$10$rV3BKpL1tM/FV2xvZB.NOO.8WfLCo6lZyMwf9F0iwzcFvLUICP1EC', NULL, NULL, NULL, 'email', 0, 0, NULL, 'NULL', '2020-04-04 06:54:38', '2020-04-04 06:54:38'),
(61, 'Rajendra', NULL, NULL, NULL, 'rajendra.kumar@hachiweb.comx', '$2y$10$9Sx8Yx2/TI9/6G7z5uXfxOWKSoQVfCVOXJNuM2KFRLTmHEzl7V372', 'phpA428.tmp.docx', NULL, NULL, 'email', 0, 0, NULL, 'NULL', '2020-04-05 23:55:15', '2020-04-05 22:45:49'),
(62, NULL, NULL, NULL, NULL, 'rajendraakmr@gmail.comx', NULL, NULL, NULL, NULL, 'twitter', 0, 0, NULL, NULL, '2020-04-07 03:32:01', '2020-04-07 03:32:01'),
(63, NULL, NULL, NULL, NULL, 'rajendraakmr@gmail.com', NULL, NULL, NULL, NULL, 'twitter', 0, 0, NULL, NULL, '2020-04-07 03:33:52', '2020-04-07 03:33:52'),
(64, NULL, NULL, NULL, NULL, 'rajendraakmr@gmail.com', NULL, NULL, NULL, NULL, 'twitter', 0, 0, NULL, NULL, '2020-04-07 05:54:46', '2020-04-07 05:54:46'),
(65, NULL, NULL, NULL, NULL, 'rajendraakmr@gmail.com', NULL, NULL, NULL, NULL, 'facebook', 0, 0, NULL, NULL, '2020-04-07 05:55:22', '2020-04-07 05:55:22'),
(66, NULL, NULL, NULL, NULL, 'rajendraakmr@gmail.com', NULL, NULL, NULL, NULL, 'facebook', 0, 0, NULL, NULL, '2020-04-07 05:57:26', '2020-04-07 05:57:26'),
(67, NULL, NULL, NULL, NULL, 'rajendraakmr@gmail.com', NULL, NULL, NULL, NULL, 'facebook', 0, 0, NULL, NULL, '2020-04-07 05:58:30', '2020-04-07 05:58:30'),
(68, NULL, NULL, NULL, NULL, 'rajendraakmr@gmail.com', NULL, NULL, NULL, NULL, 'facebook', 0, 0, NULL, NULL, '2020-04-07 06:43:17', '2020-04-07 06:43:17'),
(69, NULL, NULL, NULL, NULL, 'rajendraakmr@gmail.com', NULL, NULL, NULL, NULL, 'facebook', 0, 0, NULL, NULL, '2020-04-07 06:47:17', '2020-04-07 06:47:17'),
(70, 'Rajendra', 'Kumar', NULL, '2010-05-01', 'rajendra.kumar@hachiweb.coma', '$2y$10$zorD3TuA.Uygv5Z4DT9/VeX9o3IRtKuCfBJ1UFYW/sIXivDzcFiQS', 'php3FEF.tmp.PNG', 'Student', NULL, 'facebook', 0, 0, NULL, NULL, '2020-04-08 22:24:29', '2020-04-07 07:04:04'),
(71, NULL, NULL, NULL, NULL, 'rajendraakmr@gmail.com', NULL, NULL, NULL, NULL, 'twi', 1, 0, NULL, NULL, '2020-04-07 23:38:26', '2020-04-07 23:38:26'),
(72, NULL, NULL, NULL, NULL, 'rajendraakmr@gmail.com', NULL, NULL, NULL, NULL, 'twi', 0, 0, NULL, NULL, '2020-04-07 23:39:39', '2020-04-07 23:39:39'),
(73, NULL, NULL, NULL, NULL, 'Arvindkumar@gmail.com', NULL, NULL, NULL, NULL, 'twitter', 0, 0, NULL, NULL, '2020-04-07 23:41:34', '2020-04-07 23:41:34'),
(74, NULL, NULL, NULL, NULL, 'Arvindkumar@gmail.com', NULL, NULL, NULL, NULL, 'twitter', 1, 0, NULL, 'adadgj;alkl\n455adfadfadaa', '2020-04-08 01:52:25', '2020-04-08 01:52:25'),
(75, 'Rahul ', 'Prashad', NULL, NULL, 'Arvindkumar@gmail.com', NULL, NULL, 'Tutor', NULL, 'twitter', 1, 1, 'adadgj;alkl\n455adfadfadaa', NULL, '2020-04-08 01:53:19', '2020-04-08 01:53:19'),
(76, NULL, NULL, NULL, NULL, 'anilprashad@gmail.com', NULL, NULL, NULL, NULL, 'twitter', 1, 0, '0UJd3Eo809JLghjQWGn4WVvrKdyIQvKmzoIP5NQtJyetyRNhm7gXVyby6hbnOLTQA1h4Ec', NULL, '2020-04-08 01:54:04', '2020-04-08 01:54:04'),
(77, NULL, NULL, NULL, NULL, 'anilprashad@gmail.com', NULL, NULL, NULL, NULL, 'fb', 1, 1, '0UJd3Eo809JLghjQWGn4WVvrKdyIQvKmzoIP5NQtJyetyRNhm7gXVyby6hbnOLTQA1h4Ec', NULL, '2020-04-08 05:19:05', '2020-04-08 05:19:05'),
(78, NULL, NULL, NULL, NULL, 'anilprashad@gmail.com', NULL, NULL, NULL, NULL, 'fb', 1, 1, '0UJd3Eo809JLghjQWGn4WVvrKdyIQvKmzoIP5NQtJyetyRNhm7gXVyby6hbnOLTQA1h4Ec', NULL, '2020-04-08 05:22:35', '2020-04-08 05:22:35'),
(79, NULL, NULL, NULL, NULL, 'sanjaysharama@gmail.com', '$2y$10$1GGx7.moU.VuF.mSbMpwOeX2xmj378coA0PPHrLgZaZ2Uj9DUgiqW', NULL, NULL, NULL, 'email', 0, 0, NULL, 'vhaCVPBO2NSoTO8Lvd7n3yN2HGhZoJ8rBmuwCCpVkwPuSEK98qdqcMp9uLiYpG6eFxcInA', '2020-04-10 22:45:10', '2020-04-10 22:45:10'),
(80, NULL, NULL, NULL, NULL, 'sanjaysharama@gmail.com', NULL, NULL, NULL, NULL, 'facebook', 0, 1, 'adfaj5445a54ga1dgadg12eGAGAGFG54E5AGA', NULL, '2020-04-10 22:50:00', '2020-04-10 22:50:00'),
(81, NULL, NULL, NULL, NULL, 'rajendra.kumar@hachiweb.com', '$2y$10$e1xmGPCr.oEK6X7CoZAUCOQtsl/EwsUakGcxOiiWY8Qow1jy2GSAe', NULL, NULL, NULL, 'email', 1, 1, NULL, 'esUvh3v8SwV58GbYF0ik6aEb5rQouKwP6fBvYh4FvN0phsnoEuY23PKWWpicYciWmnfUsA', '2020-04-11 00:41:41', '2020-04-11 00:41:41'),
(82, NULL, NULL, NULL, NULL, 'rajendra.kumar@hachiweb.com', NULL, NULL, NULL, NULL, 'facebook', 0, 1, 'JDKFDIEDALGFG232D3AG3THBS32SG', NULL, '2020-04-11 00:46:05', '2020-04-11 00:46:05'),
(83, NULL, NULL, NULL, NULL, 'rajedraakmr@gmail.com', '$2y$10$XNE6P0bxQ0UnSuwYNp3Ch.58d6NUdvDETytytzoqHTOEG2OPFvkLC', NULL, NULL, NULL, 'email', 1, 1, NULL, 'xOtbMgZfkckku38D8eeUoMXtXdjJ57SkRPnEVyImTMSu4gZ5yzGQLF4vy21YZ29V8ZRfOM', '2020-04-11 03:38:29', '2020-04-11 03:38:29'),
(84, NULL, NULL, NULL, NULL, 'rajendra.kumar@hachiweb.com', NULL, NULL, NULL, NULL, 'facebook', 1, 1, 'JDKFDIEDALGFG232D3AG3THBS32SG', NULL, '2020-04-11 03:49:58', '2020-04-11 03:49:58');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `subjects` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `subjects`) VALUES
(1, 'Accounting'),
(2, 'Architecture'),
(3, 'Art'),
(4, 'Asian Studies'),
(5, 'Astronmy'),
(6, 'Biiology'),
(7, 'Bussiness'),
(8, 'Chemistry'),
(9, 'Communications'),
(10, 'Computer Science'),
(11, 'Economics'),
(12, 'Education'),
(13, 'Engineering'),
(14, 'English'),
(15, 'Finance'),
(16, 'Foreign Languages'),
(17, 'Gendr Studies'),
(18, 'General Studies'),
(19, 'Gendr Questions'),
(20, 'Geography'),
(21, 'Geology'),
(22, 'Health Care'),
(23, 'History'),
(24, 'Law'),
(25, 'Linguistics'),
(26, 'Literary'),
(27, 'Mathematics'),
(28, 'Literary Studies'),
(29, 'Music'),
(30, 'Other'),
(31, 'Philosophy'),
(32, 'Physics'),
(33, 'Political Science'),
(34, 'Religious Studies'),
(35, 'Sociology'),
(36, 'Statistics'),
(37, 'Urban Planning and Policy');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `from_message` text NOT NULL,
  `to_message` text NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tutor_id` int(11) DEFAULT NULL,
  `category` text DEFAULT NULL,
  `title` text DEFAULT NULL,
  `questions` text DEFAULT NULL,
  `payments` text DEFAULT NULL,
  `attachment` text DEFAULT NULL,
  `ans_title` text DEFAULT NULL,
  `willing_to_pay` varchar(255) DEFAULT NULL,
  `bought` text DEFAULT NULL,
  `paid` varchar(255) DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `user_id`, `tutor_id`, `category`, `title`, `questions`, `payments`, `attachment`, `ans_title`, `willing_to_pay`, `bought`, `paid`, `post_date`, `due_date`, `created_at`, `updated_at`) VALUES
(29, 74, 75, 'IT', 'Information Management System', 'wahts is management Information?', NULL, 'phpA84A.tmp.docx', NULL, NULL, NULL, NULL, NULL, NULL, '2020-04-09 22:14:43', '2020-04-09 22:14:43'),
(36, 75, 70, 'IT', 'Information Management System', 'wahts is management Information?', NULL, 'php1BA2.tmp.docx', NULL, '35.5', NULL, NULL, '2020-04-10', NULL, '2020-04-10 06:15:48', '2020-04-10 06:15:48'),
(40, 84, 75, 'Computer', 'What is Monitor mode?', 'How to modify?', NULL, 'phpD545.tmp.jpg', NULL, '5', NULL, NULL, '2020-04-11', NULL, '2020-04-11 04:22:36', '2020-04-11 04:22:36');

-- --------------------------------------------------------

--
-- Table structure for table `quizzes`
--

CREATE TABLE `quizzes` (
  `id` int(11) NOT NULL,
  `user_id` text DEFAULT NULL,
  `questions` text NOT NULL,
  `opt1` text NOT NULL,
  `opt2` text NOT NULL,
  `opt3` text NOT NULL,
  `opt4` text NOT NULL,
  `correct` varchar(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quizzes`
--

INSERT INTO `quizzes` (`id`, `user_id`, `questions`, `opt1`, `opt2`, `opt3`, `opt4`, `correct`, `created_at`, `updated_at`) VALUES
(1, NULL, 'what is latest version of php?', '3.4', '6.8', '8', '7.4', '4', '2020-04-09 01:38:12', '2020-04-09 01:38:12'),
(2, NULL, 'What is microsoft additions', '5', '5.5', '8', '9', '4', '2020-04-11 04:10:12', '2020-04-11 04:10:12'),
(3, NULL, 'What is latest bootsrtap versions.', '4.3', '5.5', '3', '3.5', 'A', '2020-04-11 04:14:41', '2020-04-11 04:14:41');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answer`
--

CREATE TABLE `quiz_answer` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `question_id` text NOT NULL,
  `choice` text NOT NULL,
  `is_right` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sgt_contact`
--

CREATE TABLE `sgt_contact` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `contact` int(10) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stw_contact`
--

CREATE TABLE `stw_contact` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `contact` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stw_contact`
--

INSERT INTO `stw_contact` (`id`, `name`, `email`, `contact`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Holmes Lowery', 'xoheb@mailinator.net', 9865321475, 'Aut est quis doloribus libero optio ut illum impedit soluta id nostrud voluptates occaecat maiores', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(2, 'Daquan Harmon', 'guqu@mailinator.net', 9632512436, 'Incidunt aliquid in qui at ducimus soluta sed', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(3, 'Daquan Harmon', 'guqu@mailinator.net', 9632512436, 'Incidunt aliquid in qui at ducimus soluta sed', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(4, 'name', 'sfytgfd@gmail.com', 9852369874, 'site', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(5, 'ContactForm', 'raphaesiftfriest@gmail.com', 375627528, 'Ciao!  shivatubewells.com \r\n \r\nWe suggesting \r\n \r\nSending your commercial proposal through the feedback form which can be found on the sites in the Communication partition. Feedback forms are filled in by our software and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This technique improve the chances that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nWhatsApp - +44 7598 509161 \r\nEmail - FeedbackForm@make-success.com', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(6, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'Look at a bonzer  wanting to thrive approach as a ripen to winning. shivatubewells.com \r\nhttp://bit.ly/2NJ0P2k', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(7, 'mohan', 'mohan@gmail.com', 7814016006, 'test message\n', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(8, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'There is a tangibles  value purchase from as a medication seeking your team. shivatubewells.com \r\nhttp://bit.ly/2NJwS2d', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(9, 'JimmieJax', 'spv529163@gmail.com', 354476655, 'Opportunism in business expansion FINANCIAL capital for NGOs entrepreneurship.FDI seeks to invest through equity, loans or guarantees, and through allocations of public funds to specific projects.', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(10, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'Take notice is  an important  visuals as a recondition to winning. shivatubewells.com \r\nhttp://bit.ly/2NJ0vAE', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(11, 'EddieAmock', 'support@monkeydigital.co', 117343455, 'What will we do to increase your DA? \r\n \r\nOur Plan is very simple yet very effective. We have researched all available sources and gained access to thousands of High DA domains that are directly indexed and re-crawled by Moz Robots on each update. So, we will build and index: \r\n \r\n- 5000 DA30+ backlinks \r\n- 2500 dofollow from high PA pages \r\n- 1000 unique domains links with high DA scores \r\n \r\nstart boosting ranks and SEO metrics with our plan today: \r\nhttp://monkeydigital.co/product/moz-da-seo-plan/ \r\n \r\nBest regards \r\nMike \r\nmonkeydigital.co@gmail.com', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(12, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'Pacify note an astounding  value pass on instead of you. shivatubewells.com \r\nhttp://bit.ly/2NUV7L4', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(13, 'Williamdet', 'raphaesiftfriest@gmail.com', 277241778, 'Hi!  shivatubewells.com \r\n \r\nWe suggesting \r\n \r\nSending your commercial offer through the feedback form which can be found on the sites in the Communication partition. Contact form are filled in by our application and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This method improve the chances that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(14, 'YourBusinessFundingNow', 'noreply@your-business-funding-now.info', 89146411334, 'Hi, letting you know that http://Your-Business-Funding-Now.info can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://Your-Business-Funding-Now.info \r\n \r\nMinimum requirements include your company being established for at least a year and with current gross revenue of at least 120K. Eligibility and funding can be completed in as fast as 48hrs. Terms are personalized for each business so I suggest applying to find out exactly how much you can get on various terms. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. These funds are Non-Restrictive, allowing you to spend the full amount in any way you require including business debt consolidation, hiring, marketing, or Absolutely Any Other expense. \r\n \r\nIf you need fast and easy business funding take a look at these programs now as there is limited availability: \r\n \r\nhttp://Your-Business-Funding-Now.info \r\n \r\nHave a great day, \r\nThe Your Business Funding Now Team \r\n \r\nunsubscribe/remove - http://your-business-funding-now.info/r.php?url=shivatubewells.com&id=e164', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(15, 'XaPEjVbDwGzv', 'ab2077230@gmail.com', 3943799398, '', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(16, 'Michaelwen', 'mircgyhaelBus@gmail.com', 84638323512, 'Look at unblended  inspiriting in rations of victory. shivatubewells.com  http://anuphepma.gq/gez6', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(17, 'HarryNUATE', 'edwardm@lioncourtcrypto.net', 81735995832, 'We buy all crypto currencies at good rate, with reasonable commission between sellers and the mandates. \r\n \r\nContact us with the informations below if you are or have a potential seller. \r\n \r\nTel: +353 1 4378345 \r\nFax: +353 1 6865354 \r\nEmail: edwardm@lioncourtcrypto.com \r\n \r\nThank you, \r\nCrypto currencies Purchase Department, \r\nEdward Molina.', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(18, 'Holliscessy', 'contacts@businesswebconnection.com', 85759784251, 'Are you using the internet to its greatest potential?  We are professional web developers that can help you get the most benefit from the web and make your site interactive with your users and clients, making your business more efficient and powerful than your competition. \r\nIf you are just looking to upgrade your web site to current technology or give your business a boost with data integration, we can guide you to success. \r\nhttps://www.businesswebconnection.com/ \r\ncontacts@businesswebconnection.com', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(19, 'http://v.ht/GaZiZA', 'forever_ever212@hotmail.com', 84488829599, 'http://go-4.net/fn5u', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(20, 'Thomasdat', 'noreplymonkeydigital@gmail.com', 89821934961, 'Having a better Alexa for your website will increase sales and visibility \r\n \r\nOur service is intended to improve the Global Alexa traffic rank of a website. It usually takes seven days to see the primary change and one month to achieve your desired three-month average Alexa Rank. The three-month average Alexa traffic rank is the one that Alexa.com shows on the Alexa’s toolbar. \r\n \r\nFor more information visit our website \r\nhttps://monkeydigital.co/product/alexa-rank-service/ \r\n \r\nthanks and regards \r\nMike \r\nmonkeydigital.co@gmail.com', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(21, 'Steviesnoma', 'guqing@chinagiftware.com', 89295984671, 'Wholesale Christmas Decorative Hanging  Ornaments. Online store: http://chinagiftware.com \r\nReindeer Shatterproof Ornament gold or silver Foil Finish + Glitter Finish, \r\nUnicorn Shatterproof Ornament gold or silver Foil Finish + Glitter Finish \r\n \r\nHandmade oil paintings reproductions, Museum quality oil painting reproductions for artist Pino Daeni, Henry Asencio and all Top Artists. A Custom Portrait painting From Photo Online store  http://www.canvaspainting.net/ \r\nWhatsApp: +86-13328758688', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(22, 'Sandrapsype', '', 85578192798, ' Hy there,  Look what we acquire in place of you! a kinddonation  http://esindiser.tk/3v2f', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(23, 'Prateek', 'prateek.asthana@hachiweb.com', 8837842284, 'Test', '2019-11-28 17:58:23', '2019-11-28 17:58:23'),
(24, 'Prateek', 'prateek.asthana@hachiweb.com', 8837842284, 'Test', '2019-11-29 04:02:16', '2019-11-29 04:02:16'),
(25, 'Prateek', 'prateek.asthana@hachiweb.com', 8837842284, 'Test', '2019-11-29 04:07:49', '2019-11-29 04:07:49'),
(26, 'Mohan', 'p.harichandra2407@gmail.com', 6434163461, 'Testing', '2019-11-30 07:15:26', '2019-11-30 07:15:26'),
(27, 'Mohan', 'p.harichandra2407@gmail.com', 6434163461, 'Testing', '2019-11-30 07:15:43', '2019-11-30 07:15:43'),
(28, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Hey, this is just for testing..!', '2019-11-30 07:16:51', '2019-11-30 07:16:51'),
(29, 'Hari', 'p.harichandra2407@gmail.com', 6464646646, 'Testing', '2019-11-30 07:34:44', '2019-11-30 07:34:44'),
(30, 'Prateek', 'infohachiweb@gmail.com', 8837842284, 'Test', '2019-11-30 09:34:18', '2019-11-30 09:34:18'),
(31, 'Mohan', 'p.harichandra2401@gmail.com', 6466494634, 'shus', '2019-11-30 13:00:37', '2019-11-30 13:00:37'),
(32, 'Mohan', 'p.harichandra2401@gmail.com', 6466494646, 'hshhsjs', '2019-11-30 13:02:32', '2019-11-30 13:02:32'),
(33, 'Pratesk', 'test@hachiweb.com', 8795747898, 'Test', '2019-12-04 16:07:49', '2019-12-04 16:07:49'),
(34, 'Test', 'infohachiweb@gmail.com', 8837842284, 'Hello', '2019-12-04 16:24:22', '2019-12-04 16:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `stw_content`
--

CREATE TABLE `stw_content` (
  `id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `value` text NOT NULL,
  `source` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stw_content`
--

INSERT INTO `stw_content` (`id`, `type`, `value`, `source`, `created_at`, `updated_at`) VALUES
(1, 0, 'We are renowned bore-well developer in the state. We deal with 5\", 7\" & 8\" Odex drilling. We employ ODEX Percussion Down-The-Hole Hammer drilling method. It is an adaptation of the air-operated down-the-hole hammer. It uses a swing-out eccentric bit to ream the bottom of the casing. The percussion bit is a two-piece bit consisting of a concentric pilot bit behind which is an eccentric second bit that swings out to enlarge the hole diameter. Immediately above the eccentric bit is a drive sub that engages a special internal shouldered drive shoe on the bottom of the ODEX casing. The ODEX is thus pulled down by the drill stem as the hole is advanced. Cuttings blow up through the drive sub and casing annuls to a swivel conducting them to a sample collector or onto the ground.\r\n\r\nBesides drilling, we are also authorized distributor of renowned submersible pumps in the state.We have been providing drilling services in the state for last 11 year. Services offered to commercial/non commercial and government projects. We have constructed at least one borewell in every part of the state.\r\nOur team of highly experienced drillers and skilled workers discharge their duties with the aim to deliver satisfactory result. For any assistance or query regarding the bore-well drilling feel free to contact us any time.\r\nTo contact us please navigate to our contact page\r\n\r\nshiva tubewells dehradun\r\nWe own two DTH hydraulic drill with accessories truck mounted for large dia LG compressor with capacity 1000/275 cfm/psi & 900/200 cfm/psi respectively.\r\n\r\n\r\nPlease note that we also hire other rig machines in case of over booking.', 'Home', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(2, 1, 'https://shivatubewells.com/images/slide3.jpg', 'Home', '2019-11-30 09:56:34', '0000-00-00 00:00:00'),
(3, 1, 'https://shivatubewells.com/images/slide6.jpg', 'Home', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(4, 1, 'https://shivatubewells.com/images/slide4.jpg', 'Home', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(5, 1, 'https://shivatubewells.com/images/slide1.jpg', 'Home', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(6, 1, 'https://shivatubewells.com/images/slide5.jpg', 'Home', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(7, 1, 'https://shivatubewells.com/images/slide2.jpg', 'Home', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(10, 0, 'We are Dehradun based govt. regd. leading water drilling firm. We are announced officially in the year 2004 but we\'ve been working much earlier.\r\nWe hold vast drilling experience. We have expertise in 5, 7, & 8\" Odex drilling.\r\nOur targeted customer belongs to the following categories:-\r\nFarmers-Agriculture\r\nIndustrialists-Plants and Machinery industry\r\nBuilders-Real estate (building construction)\r\nGovernment- Civil construction (like bridge, flyover, road, drainage construction or hand pump installations)\r\nCultivation- Nursery, farm irrigation\r\nCommon People- For domestic use etc.\r\nWe provide complete boring/drilling service by involving our own associated materials like submersible motor pumps, reqd. pipes (like MS, GI, PVC or column), panel (starter), wires etc.\r\n\r\nBeing a responsible and trusted firm we offer free maintenance service for 1 yr after the project and 3 yrs of warranty on products.\r\n*T&C Apply.\r\nFor more detail contact us.\r\n\r\n\r\nIntroduction To ODEX Casing System\r\n\r\nThe diagrams below show a ODEX Casing system in both the drilling mode as well as the retract mode. Also shown is a ODEX Sampling system that would be used for Placer Sampling of overburden. We have in stock 5\", 7\", and 8\" ODEX systems. Other sizes are available on request. In our water exploration drilling we always set ODEX casing at the top of the hole until bedrock is encountered. This helps reduce caving of the hole and helps direct the sample into the center of the drill pipe where it belongs by the use of a Sealed Diverter between the casing and the drill rod.', 'About', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(11, 1, 'https://shivatubewells.com/images/slide1.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(12, 1, 'https://shivatubewells.com/images/slide2.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(13, 1, 'https://shivatubewells.com/images/slide3.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(14, 1, 'https://shivatubewells.com/images/slide4.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(15, 1, 'https://shivatubewells.com/images/slide5.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(16, 1, 'https://shivatubewells.com/images/slide6.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(17, 1, 'https://shivatubewells.com/images/img1.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(18, 1, 'https://shivatubewells.com/images/odex1.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(19, 1, 'https://shivatubewells.com/images/odex2.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(20, 1, 'https://shivatubewells.com/images/img2.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(21, 1, 'https://shivatubewells.com/images/img4.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(22, 0, 'Our long term vision and commited service makes us stand out of the crowd. Being a responsible firm we understand your requirement and thats why there is a maintenance department in our firm who assures your after salses service.', 'Services', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(23, 1, 'https://shivatubewells.com/images/slides/1.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(24, 1, 'https://shivatubewells.com/images/slides/2.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(25, 1, 'https://shivatubewells.com/images/slides/3.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(26, 1, 'https://shivatubewells.com/images/slides/4.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(27, 1, 'https://shivatubewells.com/images/slides/5.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(28, 1, 'https://shivatubewells.com/images/slides/6.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(30, 0, 'WELCOME TO OUR PORTFOLIO PAGE. THE PAGE INCLUDES SOME OF OUR CLIENTS AND RECENT PROJECT SLIDES\r\n\r\n   \r\n    \r\nLARGEST BOREWELL DEVELOPER IN UTTARAKHAND\r\nMore than 1000 commercial,private/individual,government tubewells delivered since 2004\r\n\r\nOur recent borewell projects inlude Doiwala flyover bridge, multiple factories at Langha, multiple aashrams and hotels at Rishikesh, Hill View Apartment Sahastradhara, Mars Catering Mussoorie, Kalandi Medco, Poly houses at Herbertpur, Sahaspur and Badowala, Kukreja Inst, Corel Lab, Amber Interprises etc\r\n\r\nOUR CLIENTS\r\nAREA SERVED\r\n \r\nEvery region lying at the Himalayan belt are served. Instances below\r\n	       \r\nAjabpur         Koti		\r\n\r\nMahebawalla	Barautha\r\n\r\nHerbetpur 	Bariowganj\r\n\r\nVikasnagar 	Bhaniawala\r\n\r\nSelaqui 	Bhogpur	\r\n\r\nPrem Nagar 	Bhidoli\r\n\r\nBhauwala 	Chakrata \r\n\r\nRishikesh 	Chibroo\r\n\r\nRanipur 	Clement Town\r\n\r\nDoiwala 	Dakpathar\r\n\r\nKangri          Dehradun Cantt \r\n\r\nNepali Farm 	Dhakrani \r\n\r\nJolly grant 	Dehradun 		\r\n\r\nHathiBarkla         Balawala \r\n\r\nGhanghora	and many more...\r\n\r\nHarrawalla	\r\n\r\nHerbertpur	\r\n\r\nJharipani	\r\n\r\nKalsi	\r\n\r\nKolagarh	\r\n\r\n\r\n\r\n \r\nDEMONSTRATION\r\n \r\n\r\n\r\n\r\n \r\nGive us a call at\r\n+91 8869081529, +91 9412938365', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(31, 1, 'http://shivatubewells.com/css/images/1.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(32, 1, 'http://shivatubewells.com/css/images/2.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(33, 1, 'http://shivatubewells.com/css/images/2.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(34, 1, 'http://shivatubewells.com/css/images/3.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(35, 1, 'http://shivatubewells.com/css/images/4.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(36, 1, 'http://shivatubewells.com/css/images/5.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(37, 1, 'http://shivatubewells.com/css/images/6.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(39, 1, 'http://shivatubewells.com/css/images/7.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(40, 1, 'http://shivatubewells.com/css/images/8.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(41, 1, 'http://shivatubewells.com/css/images/9.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(42, 1, 'http://shivatubewells.com/css/images/10.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(43, 1, 'http://shivatubewells.com/css/images/11.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(44, 1, 'http://shivatubewells.com/css/images/12.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(45, 1, 'http://shivatubewells.com/css/images/13.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(46, 1, 'http://shivatubewells.com/css/images/14.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(47, 1, 'http://shivatubewells.com/css/images/15.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(48, 1, 'http://shivatubewells.com/css/images/16.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(49, 1, 'http://shivatubewells.com/css/images/17.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(50, 1, 'http://shivatubewells.com/css/images/18.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(51, 1, 'http://shivatubewells.com/css/images/19.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(52, 1, 'http://shivatubewells.com/css/images/20.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(53, 1, 'http://shivatubewells.com/css/images/21.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(54, 1, 'http://shivatubewells.com/css/images/22.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(55, 1, 'http://shivatubewells.com/css/images/23.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(56, 1, 'http://shivatubewells.com/css/images/24.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(57, 1, 'https://shivatubewells.com/images/slide1.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(58, 1, 'https://shivatubewells.com/images/slide2.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(59, 1, 'https://shivatubewells.com/images/slide3.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(60, 1, 'https://shivatubewells.com/images/slide4.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(61, 1, 'https://shivatubewells.com/images/slide5.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(62, 1, 'https://shivatubewells.com/images/slide6.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(63, 1, 'https://shivatubewells.com/images/slide6.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(64, 0, '\r\nHead Office : Prem Nagar\r\nBranch Office\r\n\r\nJhajra, Near Balaji Temple\r\nZip Code:248197\r\nCountry:India\r\nCity:Dehradun\r\nCell no:+91 8869081529\r\nCell no:+91 9412938365\r\nEmail:info@shivatubewells.com\r\n\r\nContact Us\r\nCall us or leave us a mail. We\'d be glad to assist', 'Contact', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(65, 0, 'Contact Form\r\nFields marked with * are mandatory.\r\nFull Name *	\r\nEmail Address *	\r\nContact Number	\r\nYour Message *	\r\nTo help prevent automated spam, please answer this question\r\n\r\n* Using only numbers, what is 10 + 15?   \r\nEnter your answer here', 'Contact', '2019-11-25 11:03:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stw_item`
--

CREATE TABLE `stw_item` (
  `id` int(11) NOT NULL,
  `serial_number` varchar(25) DEFAULT NULL,
  `item_name` text CHARACTER SET latin1 NOT NULL,
  `unit_rate` int(11) NOT NULL,
  `particulars` text DEFAULT NULL,
  `unit` varchar(10) NOT NULL,
  `quantity` int(7) NOT NULL,
  `description` text NOT NULL,
  `cgst` tinyint(2) NOT NULL,
  `sgst` tinyint(2) NOT NULL,
  `igst` tinyint(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stw_item`
--

INSERT INTO `stw_item` (`id`, `serial_number`, `item_name`, `unit_rate`, `particulars`, `unit`, `quantity`, `description`, `cgst`, `sgst`, `igst`, `created_at`, `updated_at`) VALUES
(1, '1', 'name', 951, '1', '1', 1, 'description', 1, 1, 1, '2019-07-02 10:19:22', '0000-00-00 00:00:00'),
(2, NULL, 'name', 951, NULL, '1', 1, 'description', 0, 0, 0, '2019-07-02 10:31:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stw_quote`
--

CREATE TABLE `stw_quote` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `contact` bigint(20) NOT NULL,
  `site` text NOT NULL,
  `bore_dia` text NOT NULL,
  `yield` float DEFAULT NULL,
  `purpose` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stw_quote`
--

INSERT INTO `stw_quote` (`id`, `name`, `email`, `contact`, `site`, `bore_dia`, `yield`, `purpose`, `created_at`, `updated_at`) VALUES
(1, 'name', 'emewtgfergail@gmail.com', 9852369874, 'site', '5cm', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(2, 'name', 'emewtgfergail@gmail.com', 9852369874, '', '', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(3, 'name', 'sfytgfd@gmail.com', 9852369874, '', '', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(4, 'mohan', 'mohan@gmail.com', 7814016006, 'www.google.com', '53', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(5, '\"Mohan\"', 'mohan@gmail.com', 7814016006, '\"122\"', '\"422\"', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(6, '\"Mohan\"', 'mohan@gmail.com', 7814016006, '\"122\"', '\"422\"', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(7, '\"Mohan\"', 'mohan@gmail.com', 7814016006, '\"122\"', '\"422\"', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(8, 'name', 'email@email.com', 9475862134, 'Dehradun', '5', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(9, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 06:51:37', '2019-11-30 06:51:37'),
(10, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 07:10:57', '2019-11-30 07:10:57'),
(11, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 07:12:56', '2019-11-30 07:12:56'),
(12, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 07:13:21', '2019-11-30 07:13:21'),
(13, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 07:18:39', '2019-11-30 07:18:39'),
(14, 'Hari', 'p.harichandra2407@gmail.com', 7816643191, 'School Ground', '36', 54, NULL, '2019-11-30 07:19:23', '2019-11-30 07:19:23'),
(15, 'Hari', 'p.harichandra2407@gmail.com', 7816643191, 'School Ground', '36', 54, NULL, '2019-11-30 07:19:29', '2019-11-30 07:19:29'),
(16, 'Prateek', 'prateek.asthana@hachiweb.com', 8837842284, 'Jhakra', '7', 3, NULL, '2019-11-30 09:33:40', '2019-11-30 09:33:40'),
(17, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 11:46:45', '2019-11-30 11:46:45'),
(18, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:01', '2019-11-30 12:58:01'),
(19, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:02', '2019-11-30 12:58:02'),
(20, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:03', '2019-11-30 12:58:03'),
(21, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:04', '2019-11-30 12:58:04'),
(22, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:04', '2019-11-30 12:58:04'),
(23, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:05', '2019-11-30 12:58:05'),
(24, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:06', '2019-11-30 12:58:06'),
(25, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:07', '2019-11-30 12:58:07'),
(26, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:08', '2019-11-30 12:58:08'),
(27, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:09', '2019-11-30 12:58:09'),
(28, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:10', '2019-11-30 12:58:10'),
(29, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:10', '2019-11-30 12:58:10'),
(30, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:11', '2019-11-30 12:58:11'),
(31, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:12', '2019-11-30 12:58:12'),
(32, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:13', '2019-11-30 12:58:13'),
(33, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:14', '2019-11-30 12:58:14'),
(34, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:15', '2019-11-30 12:58:15'),
(35, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:15', '2019-11-30 12:58:15'),
(36, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:16', '2019-11-30 12:58:16'),
(37, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:17', '2019-11-30 12:58:17'),
(38, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:18', '2019-11-30 12:58:18'),
(39, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:19', '2019-11-30 12:58:19'),
(40, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:20', '2019-11-30 12:58:20'),
(41, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:20', '2019-11-30 12:58:20'),
(42, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:21', '2019-11-30 12:58:21'),
(43, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:22', '2019-11-30 12:58:22'),
(44, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:23', '2019-11-30 12:58:23'),
(45, 'Mohan', 'p.harichandra2407@gmail.com', 6434943469, 'hzhsns', '76464', 6464, NULL, '2019-11-30 12:58:23', '2019-11-30 12:58:23'),
(46, 'Mohan', 'p.harichandra2407@gmail.com', 6764534644, 'zgsgsghsjs', '464', 546, NULL, '2019-11-30 13:00:06', '2019-11-30 13:00:06'),
(47, 'Test', 'p.harichandra2407@gmail.com', 9467987676, 'bzhzh', '646', 76, NULL, '2019-11-30 13:03:44', '2019-11-30 13:03:44'),
(48, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 13:05:21', '2019-11-30 13:05:21'),
(49, 'Test', 'test@hachiweb.com', 7895632912, 'Jhajra', '7', 6, NULL, '2019-11-30 13:31:23', '2019-11-30 13:31:23'),
(50, 'Prateek', 'test@hachiweb.com', 8996958688, 'Ddhradun', '5', 3, NULL, '2019-12-04 16:07:15', '2019-12-04 16:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `stw_zonal_data`
--

CREATE TABLE `stw_zonal_data` (
  `id` int(11) NOT NULL,
  `area` text NOT NULL,
  `bore_depth` int(6) NOT NULL,
  `rate` float NOT NULL,
  `unit` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `from` text NOT NULL,
  `to` text NOT NULL,
  `amount` text NOT NULL,
  `status` text NOT NULL,
  `withdrawal` text NOT NULL,
  `transaction_id` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `access_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@hachiweb.com', NULL, '$2y$10$gKdMZlfd1bFJUEWA0PpIG.9sSdLFnL6S1/DhSNCd1tO2v/RcGrNz.', 'BpA90FzFGgFpLW2GE00qCH52XJlkSxua3S1tx8tpWFBsc4valhMH4drZpppc', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvZ2V0LXRva2VuIiwiaWF0IjoxNTg2NjA3Nzk2LCJleHAiOjE1ODY2MTEzOTYsIm5iZiI6MTU4NjYwNzc5NiwianRpIjoidTVueHRaSE8yakh4VHI1YSIsInN1YiI6bnVsbH0.FwO9dCFWDCAS7QxOngtelhlInoQp_BOGoL55qyyw0VA', NULL, '2020-04-11 06:53:17');

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `zone_id` int(10) NOT NULL,
  `country_code` char(2) COLLATE utf8_bin NOT NULL,
  `zone_name` varchar(35) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`zone_id`, `country_code`, `zone_name`) VALUES
(1, 'AD', 'Europe/Andorra'),
(2, 'AE', 'Asia/Dubai'),
(3, 'AF', 'Asia/Kabul'),
(4, 'AG', 'America/Antigua'),
(5, 'AI', 'America/Anguilla'),
(6, 'AL', 'Europe/Tirane'),
(7, 'AM', 'Asia/Yerevan'),
(8, 'AO', 'Africa/Luanda'),
(9, 'AQ', 'Antarctica/McMurdo'),
(10, 'AQ', 'Antarctica/Casey'),
(11, 'AQ', 'Antarctica/Davis'),
(12, 'AQ', 'Antarctica/DumontDUrville'),
(13, 'AQ', 'Antarctica/Mawson'),
(14, 'AQ', 'Antarctica/Palmer'),
(15, 'AQ', 'Antarctica/Rothera'),
(16, 'AQ', 'Antarctica/Syowa'),
(17, 'AQ', 'Antarctica/Troll'),
(18, 'AQ', 'Antarctica/Vostok'),
(19, 'AR', 'America/Argentina/Buenos_Aires'),
(20, 'AR', 'America/Argentina/Cordoba'),
(21, 'AR', 'America/Argentina/Salta'),
(22, 'AR', 'America/Argentina/Jujuy'),
(23, 'AR', 'America/Argentina/Tucuman'),
(24, 'AR', 'America/Argentina/Catamarca'),
(25, 'AR', 'America/Argentina/La_Rioja'),
(26, 'AR', 'America/Argentina/San_Juan'),
(27, 'AR', 'America/Argentina/Mendoza'),
(28, 'AR', 'America/Argentina/San_Luis'),
(29, 'AR', 'America/Argentina/Rio_Gallegos'),
(30, 'AR', 'America/Argentina/Ushuaia'),
(31, 'AS', 'Pacific/Pago_Pago'),
(32, 'AT', 'Europe/Vienna'),
(33, 'AU', 'Australia/Lord_Howe'),
(34, 'AU', 'Antarctica/Macquarie'),
(35, 'AU', 'Australia/Hobart'),
(36, 'AU', 'Australia/Currie'),
(37, 'AU', 'Australia/Melbourne'),
(38, 'AU', 'Australia/Sydney'),
(39, 'AU', 'Australia/Broken_Hill'),
(40, 'AU', 'Australia/Brisbane'),
(41, 'AU', 'Australia/Lindeman'),
(42, 'AU', 'Australia/Adelaide'),
(43, 'AU', 'Australia/Darwin'),
(44, 'AU', 'Australia/Perth'),
(45, 'AU', 'Australia/Eucla'),
(46, 'AW', 'America/Aruba'),
(47, 'AX', 'Europe/Mariehamn'),
(48, 'AZ', 'Asia/Baku'),
(49, 'BA', 'Europe/Sarajevo'),
(50, 'BB', 'America/Barbados'),
(51, 'BD', 'Asia/Dhaka'),
(52, 'BE', 'Europe/Brussels'),
(53, 'BF', 'Africa/Ouagadougou'),
(54, 'BG', 'Europe/Sofia'),
(55, 'BH', 'Asia/Bahrain'),
(56, 'BI', 'Africa/Bujumbura'),
(57, 'BJ', 'Africa/Porto-Novo'),
(58, 'BL', 'America/St_Barthelemy'),
(59, 'BM', 'Atlantic/Bermuda'),
(60, 'BN', 'Asia/Brunei'),
(61, 'BO', 'America/La_Paz'),
(62, 'BQ', 'America/Kralendijk'),
(63, 'BR', 'America/Noronha'),
(64, 'BR', 'America/Belem'),
(65, 'BR', 'America/Fortaleza'),
(66, 'BR', 'America/Recife'),
(67, 'BR', 'America/Araguaina'),
(68, 'BR', 'America/Maceio'),
(69, 'BR', 'America/Bahia'),
(70, 'BR', 'America/Sao_Paulo'),
(71, 'BR', 'America/Campo_Grande'),
(72, 'BR', 'America/Cuiaba'),
(73, 'BR', 'America/Santarem'),
(74, 'BR', 'America/Porto_Velho'),
(75, 'BR', 'America/Boa_Vista'),
(76, 'BR', 'America/Manaus'),
(77, 'BR', 'America/Eirunepe'),
(78, 'BR', 'America/Rio_Branco'),
(79, 'BS', 'America/Nassau'),
(80, 'BT', 'Asia/Thimphu'),
(81, 'BW', 'Africa/Gaborone'),
(82, 'BY', 'Europe/Minsk'),
(83, 'BZ', 'America/Belize'),
(84, 'CA', 'America/St_Johns'),
(85, 'CA', 'America/Halifax'),
(86, 'CA', 'America/Glace_Bay'),
(87, 'CA', 'America/Moncton'),
(88, 'CA', 'America/Goose_Bay'),
(89, 'CA', 'America/Blanc-Sablon'),
(90, 'CA', 'America/Toronto'),
(91, 'CA', 'America/Nipigon'),
(92, 'CA', 'America/Thunder_Bay'),
(93, 'CA', 'America/Iqaluit'),
(94, 'CA', 'America/Pangnirtung'),
(95, 'CA', 'America/Atikokan'),
(96, 'CA', 'America/Winnipeg'),
(97, 'CA', 'America/Rainy_River'),
(98, 'CA', 'America/Resolute'),
(99, 'CA', 'America/Rankin_Inlet'),
(100, 'CA', 'America/Regina'),
(101, 'CA', 'America/Swift_Current'),
(102, 'CA', 'America/Edmonton'),
(103, 'CA', 'America/Cambridge_Bay'),
(104, 'CA', 'America/Yellowknife'),
(105, 'CA', 'America/Inuvik'),
(106, 'CA', 'America/Creston'),
(107, 'CA', 'America/Dawson_Creek'),
(108, 'CA', 'America/Fort_Nelson'),
(109, 'CA', 'America/Vancouver'),
(110, 'CA', 'America/Whitehorse'),
(111, 'CA', 'America/Dawson'),
(112, 'CC', 'Indian/Cocos'),
(113, 'CD', 'Africa/Kinshasa'),
(114, 'CD', 'Africa/Lubumbashi'),
(115, 'CF', 'Africa/Bangui'),
(116, 'CG', 'Africa/Brazzaville'),
(117, 'CH', 'Europe/Zurich'),
(118, 'CI', 'Africa/Abidjan'),
(119, 'CK', 'Pacific/Rarotonga'),
(120, 'CL', 'America/Santiago'),
(121, 'CL', 'America/Punta_Arenas'),
(122, 'CL', 'Pacific/Easter'),
(123, 'CM', 'Africa/Douala'),
(124, 'CN', 'Asia/Shanghai'),
(125, 'CN', 'Asia/Urumqi'),
(126, 'CO', 'America/Bogota'),
(127, 'CR', 'America/Costa_Rica'),
(128, 'CU', 'America/Havana'),
(129, 'CV', 'Atlantic/Cape_Verde'),
(130, 'CW', 'America/Curacao'),
(131, 'CX', 'Indian/Christmas'),
(132, 'CY', 'Asia/Nicosia'),
(133, 'CY', 'Asia/Famagusta'),
(134, 'CZ', 'Europe/Prague'),
(135, 'DE', 'Europe/Berlin'),
(136, 'DE', 'Europe/Busingen'),
(137, 'DJ', 'Africa/Djibouti'),
(138, 'DK', 'Europe/Copenhagen'),
(139, 'DM', 'America/Dominica'),
(140, 'DO', 'America/Santo_Domingo'),
(141, 'DZ', 'Africa/Algiers'),
(142, 'EC', 'America/Guayaquil'),
(143, 'EC', 'Pacific/Galapagos'),
(144, 'EE', 'Europe/Tallinn'),
(145, 'EG', 'Africa/Cairo'),
(146, 'EH', 'Africa/El_Aaiun'),
(147, 'ER', 'Africa/Asmara'),
(148, 'ES', 'Europe/Madrid'),
(149, 'ES', 'Africa/Ceuta'),
(150, 'ES', 'Atlantic/Canary'),
(151, 'ET', 'Africa/Addis_Ababa'),
(152, 'FI', 'Europe/Helsinki'),
(153, 'FJ', 'Pacific/Fiji'),
(154, 'FK', 'Atlantic/Stanley'),
(155, 'FM', 'Pacific/Chuuk'),
(156, 'FM', 'Pacific/Pohnpei'),
(157, 'FM', 'Pacific/Kosrae'),
(158, 'FO', 'Atlantic/Faroe'),
(159, 'FR', 'Europe/Paris'),
(160, 'GA', 'Africa/Libreville'),
(161, 'GB', 'Europe/London'),
(162, 'GD', 'America/Grenada'),
(163, 'GE', 'Asia/Tbilisi'),
(164, 'GF', 'America/Cayenne'),
(165, 'GG', 'Europe/Guernsey'),
(166, 'GH', 'Africa/Accra'),
(167, 'GI', 'Europe/Gibraltar'),
(168, 'GL', 'America/Godthab'),
(169, 'GL', 'America/Danmarkshavn'),
(170, 'GL', 'America/Scoresbysund'),
(171, 'GL', 'America/Thule'),
(172, 'GM', 'Africa/Banjul'),
(173, 'GN', 'Africa/Conakry'),
(174, 'GP', 'America/Guadeloupe'),
(175, 'GQ', 'Africa/Malabo'),
(176, 'GR', 'Europe/Athens'),
(177, 'GS', 'Atlantic/South_Georgia'),
(178, 'GT', 'America/Guatemala'),
(179, 'GU', 'Pacific/Guam'),
(180, 'GW', 'Africa/Bissau'),
(181, 'GY', 'America/Guyana'),
(182, 'HK', 'Asia/Hong_Kong'),
(183, 'HN', 'America/Tegucigalpa'),
(184, 'HR', 'Europe/Zagreb'),
(185, 'HT', 'America/Port-au-Prince'),
(186, 'HU', 'Europe/Budapest'),
(187, 'ID', 'Asia/Jakarta'),
(188, 'ID', 'Asia/Pontianak'),
(189, 'ID', 'Asia/Makassar'),
(190, 'ID', 'Asia/Jayapura'),
(191, 'IE', 'Europe/Dublin'),
(192, 'IL', 'Asia/Jerusalem'),
(193, 'IM', 'Europe/Isle_of_Man'),
(194, 'IN', 'Asia/Kolkata'),
(195, 'IO', 'Indian/Chagos'),
(196, 'IQ', 'Asia/Baghdad'),
(197, 'IR', 'Asia/Tehran'),
(198, 'IS', 'Atlantic/Reykjavik'),
(199, 'IT', 'Europe/Rome'),
(200, 'JE', 'Europe/Jersey'),
(201, 'JM', 'America/Jamaica'),
(202, 'JO', 'Asia/Amman'),
(203, 'JP', 'Asia/Tokyo'),
(204, 'KE', 'Africa/Nairobi'),
(205, 'KG', 'Asia/Bishkek'),
(206, 'KH', 'Asia/Phnom_Penh'),
(207, 'KI', 'Pacific/Tarawa'),
(208, 'KI', 'Pacific/Enderbury'),
(209, 'KI', 'Pacific/Kiritimati'),
(210, 'KM', 'Indian/Comoro'),
(211, 'KN', 'America/St_Kitts'),
(212, 'KP', 'Asia/Pyongyang'),
(213, 'KR', 'Asia/Seoul'),
(214, 'KW', 'Asia/Kuwait'),
(215, 'KY', 'America/Cayman'),
(216, 'KZ', 'Asia/Almaty'),
(217, 'KZ', 'Asia/Qyzylorda'),
(218, 'KZ', 'Asia/Aqtobe'),
(219, 'KZ', 'Asia/Aqtau'),
(220, 'KZ', 'Asia/Atyrau'),
(221, 'KZ', 'Asia/Oral'),
(222, 'LA', 'Asia/Vientiane'),
(223, 'LB', 'Asia/Beirut'),
(224, 'LC', 'America/St_Lucia'),
(225, 'LI', 'Europe/Vaduz'),
(226, 'LK', 'Asia/Colombo'),
(227, 'LR', 'Africa/Monrovia'),
(228, 'LS', 'Africa/Maseru'),
(229, 'LT', 'Europe/Vilnius'),
(230, 'LU', 'Europe/Luxembourg'),
(231, 'LV', 'Europe/Riga'),
(232, 'LY', 'Africa/Tripoli'),
(233, 'MA', 'Africa/Casablanca'),
(234, 'MC', 'Europe/Monaco'),
(235, 'MD', 'Europe/Chisinau'),
(236, 'ME', 'Europe/Podgorica'),
(237, 'MF', 'America/Marigot'),
(238, 'MG', 'Indian/Antananarivo'),
(239, 'MH', 'Pacific/Majuro'),
(240, 'MH', 'Pacific/Kwajalein'),
(241, 'MK', 'Europe/Skopje'),
(242, 'ML', 'Africa/Bamako'),
(243, 'MM', 'Asia/Yangon'),
(244, 'MN', 'Asia/Ulaanbaatar'),
(245, 'MN', 'Asia/Hovd'),
(246, 'MN', 'Asia/Choibalsan'),
(247, 'MO', 'Asia/Macau'),
(248, 'MP', 'Pacific/Saipan'),
(249, 'MQ', 'America/Martinique'),
(250, 'MR', 'Africa/Nouakchott'),
(251, 'MS', 'America/Montserrat'),
(252, 'MT', 'Europe/Malta'),
(253, 'MU', 'Indian/Mauritius'),
(254, 'MV', 'Indian/Maldives'),
(255, 'MW', 'Africa/Blantyre'),
(256, 'MX', 'America/Mexico_City'),
(257, 'MX', 'America/Cancun'),
(258, 'MX', 'America/Merida'),
(259, 'MX', 'America/Monterrey'),
(260, 'MX', 'America/Matamoros'),
(261, 'MX', 'America/Mazatlan'),
(262, 'MX', 'America/Chihuahua'),
(263, 'MX', 'America/Ojinaga'),
(264, 'MX', 'America/Hermosillo'),
(265, 'MX', 'America/Tijuana'),
(266, 'MX', 'America/Bahia_Banderas'),
(267, 'MY', 'Asia/Kuala_Lumpur'),
(268, 'MY', 'Asia/Kuching'),
(269, 'MZ', 'Africa/Maputo'),
(270, 'NA', 'Africa/Windhoek'),
(271, 'NC', 'Pacific/Noumea'),
(272, 'NE', 'Africa/Niamey'),
(273, 'NF', 'Pacific/Norfolk'),
(274, 'NG', 'Africa/Lagos'),
(275, 'NI', 'America/Managua'),
(276, 'NL', 'Europe/Amsterdam'),
(277, 'NO', 'Europe/Oslo'),
(278, 'NP', 'Asia/Kathmandu'),
(279, 'NR', 'Pacific/Nauru'),
(280, 'NU', 'Pacific/Niue'),
(281, 'NZ', 'Pacific/Auckland'),
(282, 'NZ', 'Pacific/Chatham'),
(283, 'OM', 'Asia/Muscat'),
(284, 'PA', 'America/Panama'),
(285, 'PE', 'America/Lima'),
(286, 'PF', 'Pacific/Tahiti'),
(287, 'PF', 'Pacific/Marquesas'),
(288, 'PF', 'Pacific/Gambier'),
(289, 'PG', 'Pacific/Port_Moresby'),
(290, 'PG', 'Pacific/Bougainville'),
(291, 'PH', 'Asia/Manila'),
(292, 'PK', 'Asia/Karachi'),
(293, 'PL', 'Europe/Warsaw'),
(294, 'PM', 'America/Miquelon'),
(295, 'PN', 'Pacific/Pitcairn'),
(296, 'PR', 'America/Puerto_Rico'),
(297, 'PS', 'Asia/Gaza'),
(298, 'PS', 'Asia/Hebron'),
(299, 'PT', 'Europe/Lisbon'),
(300, 'PT', 'Atlantic/Madeira'),
(301, 'PT', 'Atlantic/Azores'),
(302, 'PW', 'Pacific/Palau'),
(303, 'PY', 'America/Asuncion'),
(304, 'QA', 'Asia/Qatar'),
(305, 'RE', 'Indian/Reunion'),
(306, 'RO', 'Europe/Bucharest'),
(307, 'RS', 'Europe/Belgrade'),
(308, 'RU', 'Europe/Kaliningrad'),
(309, 'RU', 'Europe/Moscow'),
(310, 'RU', 'Europe/Simferopol'),
(311, 'RU', 'Europe/Volgograd'),
(312, 'RU', 'Europe/Kirov'),
(313, 'RU', 'Europe/Astrakhan'),
(314, 'RU', 'Europe/Saratov'),
(315, 'RU', 'Europe/Ulyanovsk'),
(316, 'RU', 'Europe/Samara'),
(317, 'RU', 'Asia/Yekaterinburg'),
(318, 'RU', 'Asia/Omsk'),
(319, 'RU', 'Asia/Novosibirsk'),
(320, 'RU', 'Asia/Barnaul'),
(321, 'RU', 'Asia/Tomsk'),
(322, 'RU', 'Asia/Novokuznetsk'),
(323, 'RU', 'Asia/Krasnoyarsk'),
(324, 'RU', 'Asia/Irkutsk'),
(325, 'RU', 'Asia/Chita'),
(326, 'RU', 'Asia/Yakutsk'),
(327, 'RU', 'Asia/Khandyga'),
(328, 'RU', 'Asia/Vladivostok'),
(329, 'RU', 'Asia/Ust-Nera'),
(330, 'RU', 'Asia/Magadan'),
(331, 'RU', 'Asia/Sakhalin'),
(332, 'RU', 'Asia/Srednekolymsk'),
(333, 'RU', 'Asia/Kamchatka'),
(334, 'RU', 'Asia/Anadyr'),
(335, 'RW', 'Africa/Kigali'),
(336, 'SA', 'Asia/Riyadh'),
(337, 'SB', 'Pacific/Guadalcanal'),
(338, 'SC', 'Indian/Mahe'),
(339, 'SD', 'Africa/Khartoum'),
(340, 'SE', 'Europe/Stockholm'),
(341, 'SG', 'Asia/Singapore'),
(342, 'SH', 'Atlantic/St_Helena'),
(343, 'SI', 'Europe/Ljubljana'),
(344, 'SJ', 'Arctic/Longyearbyen'),
(345, 'SK', 'Europe/Bratislava'),
(346, 'SL', 'Africa/Freetown'),
(347, 'SM', 'Europe/San_Marino'),
(348, 'SN', 'Africa/Dakar'),
(349, 'SO', 'Africa/Mogadishu'),
(350, 'SR', 'America/Paramaribo'),
(351, 'SS', 'Africa/Juba'),
(352, 'ST', 'Africa/Sao_Tome'),
(353, 'SV', 'America/El_Salvador'),
(354, 'SX', 'America/Lower_Princes'),
(355, 'SY', 'Asia/Damascus'),
(356, 'SZ', 'Africa/Mbabane'),
(357, 'TC', 'America/Grand_Turk'),
(358, 'TD', 'Africa/Ndjamena'),
(359, 'TF', 'Indian/Kerguelen'),
(360, 'TG', 'Africa/Lome'),
(361, 'TH', 'Asia/Bangkok'),
(362, 'TJ', 'Asia/Dushanbe'),
(363, 'TK', 'Pacific/Fakaofo'),
(364, 'TL', 'Asia/Dili'),
(365, 'TM', 'Asia/Ashgabat'),
(366, 'TN', 'Africa/Tunis'),
(367, 'TO', 'Pacific/Tongatapu'),
(368, 'TR', 'Europe/Istanbul'),
(369, 'TT', 'America/Port_of_Spain'),
(370, 'TV', 'Pacific/Funafuti'),
(371, 'TW', 'Asia/Taipei'),
(372, 'TZ', 'Africa/Dar_es_Salaam'),
(373, 'UA', 'Europe/Kiev'),
(374, 'UA', 'Europe/Uzhgorod'),
(375, 'UA', 'Europe/Zaporozhye'),
(376, 'UG', 'Africa/Kampala'),
(377, 'UM', 'Pacific/Midway'),
(378, 'UM', 'Pacific/Wake'),
(379, 'US', 'America/New_York'),
(380, 'US', 'America/Detroit'),
(381, 'US', 'America/Kentucky/Louisville'),
(382, 'US', 'America/Kentucky/Monticello'),
(383, 'US', 'America/Indiana/Indianapolis'),
(384, 'US', 'America/Indiana/Vincennes'),
(385, 'US', 'America/Indiana/Winamac'),
(386, 'US', 'America/Indiana/Marengo'),
(387, 'US', 'America/Indiana/Petersburg'),
(388, 'US', 'America/Indiana/Vevay'),
(389, 'US', 'America/Chicago'),
(390, 'US', 'America/Indiana/Tell_City'),
(391, 'US', 'America/Indiana/Knox'),
(392, 'US', 'America/Menominee'),
(393, 'US', 'America/North_Dakota/Center'),
(394, 'US', 'America/North_Dakota/New_Salem'),
(395, 'US', 'America/North_Dakota/Beulah'),
(396, 'US', 'America/Denver'),
(397, 'US', 'America/Boise'),
(398, 'US', 'America/Phoenix'),
(399, 'US', 'America/Los_Angeles'),
(400, 'US', 'America/Anchorage'),
(401, 'US', 'America/Juneau'),
(402, 'US', 'America/Sitka'),
(403, 'US', 'America/Metlakatla'),
(404, 'US', 'America/Yakutat'),
(405, 'US', 'America/Nome'),
(406, 'US', 'America/Adak'),
(407, 'US', 'Pacific/Honolulu'),
(408, 'UY', 'America/Montevideo'),
(409, 'UZ', 'Asia/Samarkand'),
(410, 'UZ', 'Asia/Tashkent'),
(411, 'VA', 'Europe/Vatican'),
(412, 'VC', 'America/St_Vincent'),
(413, 'VE', 'America/Caracas'),
(414, 'VG', 'America/Tortola'),
(415, 'VI', 'America/St_Thomas'),
(416, 'VN', 'Asia/Ho_Chi_Minh'),
(417, 'VU', 'Pacific/Efate'),
(418, 'WF', 'Pacific/Wallis'),
(419, 'WS', 'Pacific/Apia'),
(420, 'YE', 'Asia/Aden'),
(421, 'YT', 'Indian/Mayotte'),
(422, 'ZA', 'Africa/Johannesburg'),
(423, 'ZM', 'Africa/Lusaka'),
(424, 'ZW', 'Africa/Harare');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apps_user`
--
ALTER TABLE `apps_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quizzes`
--
ALTER TABLE `quizzes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sgt_contact`
--
ALTER TABLE `sgt_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stw_contact`
--
ALTER TABLE `stw_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stw_content`
--
ALTER TABLE `stw_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stw_item`
--
ALTER TABLE `stw_item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `person` (`id`,`created_at`);

--
-- Indexes for table `stw_quote`
--
ALTER TABLE `stw_quote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stw_zonal_data`
--
ALTER TABLE `stw_zonal_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`created_at`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`zone_id`),
  ADD KEY `idx_country_code` (`country_code`),
  ADD KEY `idx_zone_name` (`zone_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `apps_user`
--
ALTER TABLE `apps_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `quizzes`
--
ALTER TABLE `quizzes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sgt_contact`
--
ALTER TABLE `sgt_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stw_contact`
--
ALTER TABLE `stw_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `stw_content`
--
ALTER TABLE `stw_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `stw_item`
--
ALTER TABLE `stw_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stw_quote`
--
ALTER TABLE `stw_quote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `stw_zonal_data`
--
ALTER TABLE `stw_zonal_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `zone_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=425;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
