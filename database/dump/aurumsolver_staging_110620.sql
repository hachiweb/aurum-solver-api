-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 09, 2021 at 02:37 PM
-- Server version: 5.5.68-MariaDB-cll-lve
-- PHP Version: 7.3.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aurumsolver`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `tittle` text NOT NULL,
  `about_us` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `tittle`, `about_us`, `created_at`, `updated_at`) VALUES
(1, 'Hello First', '<p><img alt=\"\" src=\"https://image.shutterstock.com/image-photo/white-transparent-leaf-on-mirror-260nw-1029171697.jpg\" style=\"height:280px; width:381px\" /></p>\r\n\r\n<p><em>dfdfadafad</em></p>', '2020-06-19 09:07:17', '2020-06-19 09:07:17'),
(2, 'Second FirstLorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium, consectetur eos perspiciatis, harum eius quis illum nostrum ratione enim sequi veniam voluptatibus iste vel commodi porro a obcaecati soluta odio?', '<p><strong>Lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet&nbsp;consectetur&nbsp;adipisicing&nbsp;elit.&nbsp;Accusantium,&nbsp;consectetur&nbsp;eos&nbsp;perspiciatis,&nbsp;harum&nbsp;eius&nbsp;quis&nbsp;illum&nbsp;nostrum&nbsp;ratione&nbsp;enim&nbsp;sequi&nbsp;veniam&nbsp;voluptatibus&nbsp;iste&nbsp;vel&nbsp;commodi&nbsp;porro&nbsp;a&nbsp;obcaecati&nbsp;soluta&nbsp;odio?</strong></p>', '2020-06-19 09:16:14', '2020-06-19 09:16:14');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `questioner_id` int(11) NOT NULL,
  `answerer_id` varchar(255) DEFAULT NULL,
  `questioner_fee` varchar(25) DEFAULT NULL,
  `answerer_fee` varchar(25) DEFAULT NULL,
  `admin_fee` varchar(25) DEFAULT NULL,
  `category` text,
  `question` text,
  `title` text,
  `ans_attach` text,
  `answer` text NOT NULL,
  `desired_price` varchar(200) DEFAULT NULL,
  `post` date DEFAULT NULL,
  `purchase` tinyint(1) NOT NULL DEFAULT '0',
  `duedate` date DEFAULT NULL,
  `delivery_accepted` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `questioner_id`, `answerer_id`, `questioner_fee`, `answerer_fee`, `admin_fee`, `category`, `question`, `title`, `ans_attach`, `answer`, `desired_price`, `post`, `purchase`, `duedate`, `delivery_accepted`, `created_at`, `updated_at`) VALUES
(1, 36, 75, '84', NULL, NULL, NULL, NULL, 'What is AI?', NULL, NULL, 'Al  is Artificial Intellegence also known robotics.', NULL, '2020-04-13', 1, NULL, NULL, '2020-04-12 23:57:43', '2020-04-12 23:57:43'),
(2, NULL, 61, '84', NULL, NULL, NULL, NULL, 'wahts is management Information?', NULL, NULL, 'Information Management System', NULL, '2020-04-14', 0, NULL, NULL, '2020-04-13 23:34:46', '2020-04-13 23:34:46'),
(17, 36, 75, '84', NULL, NULL, NULL, NULL, 'What is AI?', NULL, NULL, 'Al  is Artificial Intellegence also known robotics.', NULL, '2020-04-14', 1, NULL, NULL, '2020-04-13 23:50:33', '2020-05-23 10:10:25'),
(18, 52, 86, '84', NULL, NULL, NULL, NULL, 'What is AI?', NULL, NULL, 'Al  is Artificial Intellegence also known robotics.', NULL, '2020-04-15', 0, NULL, NULL, '2020-04-15 04:46:07', '2020-04-15 04:46:07'),
(19, 52, 86, '84', NULL, NULL, NULL, NULL, 'What is AI?', NULL, NULL, 'Al  is Artificial Intellegence also known robotics.', NULL, '2020-04-15', 0, NULL, NULL, '2020-04-15 04:47:38', '2020-04-15 04:47:38'),
(20, NULL, 84, '75', NULL, NULL, NULL, NULL, 'wahts is management Information?', NULL, NULL, 'Information Management System', NULL, '2020-04-15', 0, NULL, NULL, '2020-04-15 04:49:48', '2020-04-15 04:49:48'),
(21, 52, 75, '84', NULL, NULL, NULL, NULL, 'What is AI?', NULL, NULL, 'Al  is Artificial Intellegence also known robotics.', NULL, '2020-04-15', 0, NULL, NULL, '2020-04-15 05:10:10', '2020-04-15 05:10:10'),
(22, 58, 84, '85', NULL, NULL, NULL, '0', 'What is AI?', NULL, NULL, 'Al  is Artificial Intellegence also known robotics.', NULL, '2020-05-06', 0, NULL, NULL, '2020-05-06 05:30:30', '2020-05-06 05:30:30'),
(23, 77, 102, '101', NULL, NULL, NULL, 'Education', NULL, 'TESTING', 'https://aurumsolver.com/storage/assignment_attachments/anwers/user-id-101/answer-1589907017.Screenshot_20200517-194822.jpg,https://aurumsolver.com/storage/assignment_attachments/anwers/user-id-101/answer-1589907017.Screenshot_20200517-172539.jpg', '[{\"insert\":\"Question\\nThis is a very good thing to TESTING \\n\"}]', NULL, '2020-05-24', 1, NULL, NULL, '2020-05-19 16:50:17', '2020-05-24 12:45:00'),
(24, 77, 102, '101', NULL, NULL, NULL, 'Education', NULL, 'Second Submission', 'https://aurumsolver.com/storage/assignment_attachments/anwers/user-id-101/answer-1589922543.snapshots for Summer Term Registration_INR.pdf', '[{\"insert\":\"Question\\nProvide systems engineering, software engineering, technical consulting, and marketing services as a member of the Systems Integration Division of a software engineering consulting company.\\n\\nDesigned and managed the development of an enterprise-level client/server automated auditing application for a major financial management company migrating from mainframe computers, db2, and FOCUS to a workgroup oriented, client/server architecture involving Windows for Workgroups, Windows NT Advanced Server, Microsoft SQL Server, Oracle7, and UNIX.\\nDesigned an enterprise level, high performance, mission-critical, client/server database system incorporating symmetric multiprocessing computers (SMP), Oracle7’s Parallel Server, Tuxedo’s on-line transaction processing (OLTP) monitor, and redundant array of inexpensive disks (RAID) technology.\\nConducted extensive trade studies of a large number of vendors that offer leading-edge technologies; these studies identified proven (low-risk) implementations of SMP and RDBMS systems that met stringent performance and availability criteria.\\nSystems Analyst\\nBusiness Consultants, Inc., Washington, DC 1990-1993\\nProvided technical consulting services to the Smithsonian Institute’s Information Technology Services Group, Amnesty International, and internal research and development initiatives.\\n\\nConsolidated and documented the Smithsonian Laboratory\'s Testing, Demonstration, and Training databases onto a single server, maximizing the use of the laboratory\'s computing resources.\\n\"}]', NULL, '2020-05-19', 1, NULL, NULL, '2020-05-19 21:09:03', '2020-06-15 06:34:59'),
(25, 78, 102, '101', NULL, NULL, NULL, 'Education', NULL, 'This is an Title', 'https://aurumsolver.com/storage/assignment_attachments/anwers/user-id-101/answer-1590169335.snapshots for Summer Term Registration_INR.pdf,https://aurumsolver.com/storage/assignment_attachments/anwers/user-id-101/answer-1590169335.Pizigani_1367_Chart_10MB.jpg', '[{\"insert\":\"Question\\nHey this is a title \"},{\"insert\":\"\\n\",\"attributes\":{\"heading\":1}},{\"insert\":\"\\nLooks great isn\'t it ?\\nWow\"},{\"insert\":\"\\n\",\"attributes\":{\"block\":\"ul\"}},{\"insert\":\"Fantastic \"},{\"insert\":\"\\n\",\"attributes\":{\"block\":\"ul\"}},{\"insert\":\"Awesome\"},{\"insert\":\"\\n\",\"attributes\":{\"block\":\"ul\"}},{\"insert\":\"\\n\\n\"}]', NULL, '2020-05-22', 0, NULL, NULL, '2020-05-20 18:55:15', '2020-05-22 17:42:23'),
(26, 79, 102, '101', NULL, NULL, NULL, 'Education', NULL, 'this is my first question', '', '[{\"insert\":\"Question\\n\"}]', NULL, '2020-05-22', 0, NULL, NULL, '2020-05-22 20:20:21', '2020-05-22 20:49:42'),
(27, 80, 102, '101', NULL, NULL, NULL, 'Education', NULL, 'TESTING', '', '[{\"insert\":\"Question\\n\"}]', NULL, '2020-05-22', 0, NULL, NULL, '2020-05-22 21:20:35', '2020-05-22 21:20:35'),
(28, 72, 100, '101', NULL, NULL, NULL, 'Chemistry', 'How can I able to break the multiple/netsed for loops?', NULL, '', '[{\"insert\":\"You can break them by using return statement \\n\"}]', NULL, '2020-05-26', 1, '2020-05-20', NULL, '2020-05-26 22:37:05', '2020-05-28 06:17:15'),
(29, 60, 86, '101', NULL, NULL, NULL, 'Chemistry', 'What is difference between call by value and call by reference?', 'first answer', 'https://aurumsolver.com/storage/answer_attachments/anwers/answerer-id-101/answer-1590832574.Screenshot (88).png,https://aurumsolver.com/storage/answer_attachments/anwers/answerer-id-101/answer-1590832575.Screenshot (89).png,https://aurumsolver.com/storage/answer_attachments/anwers/answerer-id-101/answer-1590832575.Screenshot (90).png', 'Hello', NULL, '2020-05-30', 0, '2020-05-02', NULL, '2020-05-29 05:20:45', '2020-05-30 09:56:15'),
(30, 72, 100, '109', NULL, NULL, NULL, 'Chemistry', 'How can I able to break the multiple/netsed for loops?', NULL, '', '[{\"insert\":\"Answer\\n\"}]', NULL, '2020-05-29', 0, '2020-05-20', NULL, '2020-05-29 05:30:27', '2020-05-29 05:30:27'),
(31, 64, 101, '109', NULL, NULL, NULL, 'Chemistry', 'What is LHS AND RHS?', NULL, '', '[{\"insert\":\"Trigonometry answer.\\n\"}]', NULL, '2020-05-29', 1, '2020-05-02', NULL, '2020-05-29 05:34:11', '2020-06-08 14:12:20'),
(32, 61, 86, '101', NULL, NULL, NULL, 'Chemistry', 'What is difference between call by value and call by reference?', 'first answer', 'https://aurumsolver.com/storage/answer_attachments/anwers/answerer-id-101/answer-1590832637.Screenshot (88).png,https://aurumsolver.com/storage/answer_attachments/anwers/answerer-id-101/answer-1590832638.Screenshot (89).png,https://aurumsolver.com/storage/answer_attachments/anwers/answerer-id-101/answer-1590832638.Screenshot (90).png', 'Hello', NULL, '2020-05-30', 0, '2020-05-02', NULL, '2020-05-30 09:57:18', '2020-05-30 09:57:18'),
(33, 83, 109, '101', NULL, NULL, NULL, 'Computer Science', '[{\"insert\":\"How to call by reference\\n\"}]', NULL, '', '[{\"insert\":\"Description:\\n\"}]', NULL, '2020-05-30', 1, '2020-05-30', NULL, '2020-05-30 10:08:13', '2020-06-02 01:04:57'),
(34, 84, 107, '101', NULL, NULL, NULL, 'Engineering', '[{\"insert\":\"What is the role of ellonquet? \\n\"}]', 'TESTING is my favorite', '', '[{\"insert\":\"Description:\\nOh I have a lot of duty to work on the app \\n\"}]', '5.65', '2020-06-01', 1, '2020-05-31', NULL, '2020-05-31 13:58:08', '2020-06-02 01:05:19'),
(35, 88, 110, '101', NULL, NULL, NULL, 'Statistics', '[{\"insert\":\"Description: Just another test question. \\n\"}]', 'TESTING', 'https://aurumsolver.com/storage/answer_attachments/anwers/answerer-id-101/answer-1591155683.snapshots for Summer Term Registration_INR.pdf', '[{\"insert\":\"Description:\\nJzjd\\n\\n\"}]', '12.5', '2020-06-03', 1, '2020-06-29', NULL, '2020-06-03 03:41:24', '2020-06-03 03:44:27'),
(36, 85, 101, '112', NULL, NULL, NULL, 'Computer Science', '[{\"insert\":\"Need a detailed information on artisan commands like db:seed\\n\"}]', 'this was a great', 'https://aurumsolver.com/storage/answer_attachments/anwers/answerer-id-112/answer-1591156547.snapshots for Summer Term Registration_INR.pdf', '[{\"insert\":\"Description:\\nNdjdj\\n\\n\"}]', '12.65', '2020-06-03', 1, '2020-06-18', NULL, '2020-06-03 03:55:47', '2020-06-07 07:07:45'),
(37, 93, 110, '113', NULL, NULL, NULL, 'Literary Studies', '[{\"insert\":\"Specify the euler constant value. \\n\"}]', 'I know the answer', 'https://aurumsolver.com/storage/answer_attachments/anwers/answerer-id-113/answer-1591522275.IMG-20200603-WA0037.jpg', '[{\"insert\":\"Description: Please see the attachment. \\n\"}]', '46.15', '2020-06-07', 1, '2020-06-15', NULL, '2020-06-07 09:31:16', '2020-06-07 09:32:07'),
(38, 99, 119, '120', NULL, NULL, NULL, 'Computer Science', NULL, 'Answer', 'https://aurumsolver.com/storage/assignment_attachments/anwers/user-id-120/answer-1591681315.logial-reasoning-puzzle-question-for-kids.png', '[{\"insert\":\"Api\\n\"}]', NULL, '2020-06-09', 0, NULL, NULL, '2020-06-09 05:41:55', '2020-06-09 05:41:55'),
(39, 100, 119, '120', NULL, NULL, NULL, 'Computer Science', NULL, 'php answer', 'https://aurumsolver.com/storage/assignment_attachments/anwers/user-id-120/answer-1591683290.logial-reasoning-puzzle-question-for-kids.png', '[{\"insert\":\"Answer:Php data object.\\n\"}]', NULL, '2020-06-09', 0, NULL, NULL, '2020-06-09 06:14:50', '2020-06-09 06:14:50'),
(40, 101, 112, '121', NULL, NULL, NULL, 'Education', NULL, 'Assigned 2', '', '[{\"insert\":\"Question the question\\n\"}]', NULL, '2020-06-10', 0, NULL, NULL, '2020-06-10 00:53:29', '2020-06-10 01:43:45'),
(41, 102, 121, '112', NULL, NULL, NULL, 'Education', NULL, 'tt', '', '[{\"insert\":\"Question\\n\"}]', NULL, '2020-06-10', 0, NULL, NULL, '2020-06-10 01:49:13', '2020-06-10 01:49:13'),
(42, 103, 121, '112', NULL, NULL, NULL, 'Education', NULL, 'Assignment answer', '', '[{\"insert\":\"Question\\n44inches the wolf \\n\"}]', NULL, '2020-06-10', 0, NULL, NULL, '2020-06-10 02:01:25', '2020-06-10 02:01:25'),
(43, 107, 120, '113', NULL, NULL, NULL, 'Literary Studies', NULL, 'Pre final assignment', 'https://aurumsolver.com/storage/assignment_attachments/anwers/user-id-113/answer-1591788968.Screenshot_2020-06-10-16-58-07-055_com.app.aurum_solver.aurum_solver.jpg', '[{\"insert\":\"Question find attachment\\n\"}]', NULL, '2020-06-10', 0, NULL, NULL, '2020-06-10 11:36:09', '2020-06-10 11:36:09'),
(44, 94, 110, '113', NULL, NULL, NULL, 'Literary Studies', NULL, 'prefinal', 'https://aurumsolver.com/storage/assignment_attachments/anwers/user-id-113/answer-1591789022.Screenshot_2020-06-10-16-58-07-055_com.app.aurum_solver.aurum_solver.jpg', '[{\"insert\":\"Question here\'s the assignment\\n\"}]', NULL, '2020-06-10', 0, NULL, NULL, '2020-06-10 11:37:02', '2020-06-10 11:37:02'),
(45, 109, 121, '112', NULL, NULL, NULL, 'Education', NULL, 'yy', '', '[{\"insert\":\"Question\\n\"}]', NULL, '2020-06-10', 0, NULL, NULL, '2020-06-10 14:44:14', '2020-06-10 16:35:54'),
(46, 111, 121, '112', NULL, NULL, NULL, 'Education', NULL, 'TT1000', '', '[{\"insert\":\"Question\\n\"}]', NULL, '2020-06-10', 0, NULL, NULL, '2020-06-10 17:15:00', '2020-06-10 17:15:00'),
(47, 113, 113, '110', NULL, NULL, NULL, 'Ayurveda', NULL, 'resub04', '', '[{\"insert\":\"Question resub05\\n\"}]', NULL, '2020-06-11', 0, NULL, NULL, '2020-06-11 02:54:28', '2020-06-11 02:58:23'),
(48, 41, 84, '120', NULL, NULL, NULL, 'Chemistry', 'How to modify?', 'questionadfgadf', '', 'dfadfgadgfagaggga', '10', '2020-06-15', 0, NULL, NULL, '2020-06-15 06:55:16', '2020-06-15 06:55:16');

-- --------------------------------------------------------

--
-- Table structure for table `apps_user`
--

CREATE TABLE `apps_user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `whoami` text,
  `dob` date DEFAULT NULL,
  `user_email` varchar(100) NOT NULL,
  `password` varchar(200) DEFAULT NULL,
  `profile_pic` text,
  `role` text,
  `user_type` text,
  `phone` varchar(15) DEFAULT NULL,
  `timezone` text,
  `category_id` int(11) DEFAULT NULL,
  `subscribe_category` text,
  `subscribe_notification_interval` text,
  `paypalaccount` text,
  `registration_type` varchar(100) DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `registration_token` text,
  `verification_token` text,
  `last_login` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `apps_user`
--

INSERT INTO `apps_user` (`id`, `first_name`, `last_name`, `whoami`, `dob`, `user_email`, `password`, `profile_pic`, `role`, `user_type`, `phone`, `timezone`, `category_id`, `subscribe_category`, `subscribe_notification_interval`, `paypalaccount`, `registration_type`, `is_verified`, `is_active`, `registration_token`, `verification_token`, `last_login`, `updated_at`, `created_at`) VALUES
(61, 'Rajendra', 'yadav', 'I\'m rock star.', '1998-05-05', 'rajendra.kumar@hachiweb.com', '$2y$10$yV3qcrhVYeix4yKYjnUmm.ogDxiEX7PW6ksZabH6QMyWaEUrJ3fku', 'https://aurumsolver.com/storage/user-profile-pic/id-61/rajen.jpg', 'Student', 'Student', NULL, 'India', 5, 'Computer Science', '2 Week', 'test@hachiweb.com', 'email', 1, 1, 'oHrEoqHv6VQV6KL7puJsxgtd55R2', NULL, '2020-05-28 06:06:00', '2020-06-01 14:55:03', '2020-04-26 13:56:51'),
(100, 'Mohan', '-', NULL, '2014-06-16', 'mohansaimanthri03@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-100/_2020-06-02T06:19:49.379203', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', 6, 'Astronmy', 'Instant', NULL, 'twitter.com', 1, 1, 'L7dfrgbL9aU3Ldju6StJl7w556E3', NULL, '2020-06-14 23:44:09', '2020-06-14 23:44:09', '2020-05-03 14:11:22'),
(101, 'Mohan Sai Manthri', '-', 'This is my updated bio.', '1997-07-09', 'paboco2176@mailop7.com', '$2y$10$5s4E0SmX1vDUkChYrI78q.jUQvbSACxLoyPxhI6uHqd.b0rnhQ2fO', 'https://aurumsolver.com/storage/user-profile-pic/id-101/_2020-06-09T06:19:29.444480', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', 4, 'Education', '24 Hour Delay', NULL, 'email', 1, 1, 'NXFtrGHOrkRTsn6LmLwOUse89Nt2', '1fphcFrrWmdLhsJODb7GGYZ60hGfVpio3ITttNUIXPp62Bj8VzBL40IWdLDXX5uLc2rXkT', '2020-06-15 15:09:07', '2020-06-15 15:09:07', '2020-05-04 01:21:44'),
(102, 'App Tester', '-', 'I was created to test and reports the bugs, Also help creator to make the app stable', '1906-01-27', 'jogal90382@inbov03.com', '$2y$10$nYlohD7runCcLLRogQoI5uIrYNXHJwgrdK8muRgpEOeCOl62X1.Bm', 'https://aurumsolver.com/storage/user-profile-pic/id-102/App Tester_2020-05-19T01:08:45.775979', NULL, 'User', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Education', 'Instant', NULL, 'email', 1, 1, 'd5HnkEonUJOp0UbyirXc59GcJAV2', '1rPrurmyNa0jvsbcSQrXpWYQQjudp775ZYu8LQWhjHvNFFrELB2KA58kQD5XtTTbUmcDpK', '2020-05-26 22:49:32', '2020-05-26 22:49:32', '2020-05-18 19:30:36'),
(106, 'Priya A', '-', 'I am a medical student', '1998-11-14', 'asthana0priya@gmail.com', '$2y$10$ffBX7M5Zg.Rf8rr9t0QwAOC3Dcfu9l9VEOOiNOvcULGWnhT1ZbqVu', 'https://aurumsolver.com/storage/user-profile-pic/id-106/_2020-05-30T21:24:31.002011', NULL, 'Both', NULL, '(UTC-09:00) Alaska', NULL, 'Biiology', '6 Hour Delay', NULL, 'email', 1, 1, 'JKDOkepgknNxIYQJ41HYLWuABU93', '1BLDf0KMSSlqGtJQxu5zpim25Y5pRe3e6Ac9iwYnf2NP2FLNoK3iU2Q7YdHgBbNqbDTOQr', '2020-05-30 15:54:37', '2020-05-30 15:54:37', '2020-05-28 04:12:30'),
(107, 'Alpha Tester', '-', 'I am alpha tester', '1995-03-29', 'dev.prateek.asthana@gmail.com', '$2y$10$YdaT9wKUB6moQIa4SKAW8uNvwnb.g59zgRTR6frvR7lrI8bvhw8La', 'https://aurumsolver.com/storage/user-profile-pic/id-107/_2020-05-30T20:54:27.430989', NULL, 'Both', NULL, '(UTC-06:00) Central America', NULL, 'Ayurveda', '1 Hour Delay', NULL, 'email', 1, 1, '6MoD67K1CEPgyCB0vn3E3tzk5pD3', 'oQQcF9fukdSJ9xRABSoXwGimmyixwwXggvPQJLHK3MZGyKIIR80yYPgR7AZxxM0aXS4mVm', '2020-06-07 10:20:49', '2020-06-07 10:20:49', '2020-05-28 04:31:15'),
(110, 'Beta Tester', '-', 'I am a beta tester', '1976-01-10', 'hachiwebapp@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-110/_2020-06-02T23:19:10.354811', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Ayurveda', '12 Hour Delay', NULL, 'google.com', 1, 1, 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjgyMmM1NDk4YTcwYjc0MjQ5NzI2ZDhmYjYxODlkZWI3NGMzNWM4MGEiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiUHJhdGVlayBBc3RoYW5hIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BT2gxNEdoMFpKcnJkbzdvSlM2eGFEeno2SWtqMWZ4em0xQ01yOXhxNk44OD1zOTYtYyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9hdXJ1bS1zb2x2ZXIiLCJhdWQiOiJhdXJ1bS1zb2x2ZXIiLCJhdXRoX3RpbWUiOjE1OTA5MTk2ODEsInVzZXJfaWQiOiJMdFpYWTBCSGxmWWxEYWNyUGdRYmFhcDFDeHUxIiwic3ViIjoiTHRaWFkwQkhsZllsRGFjclBnUWJhYXAxQ3h1MSIsImlhdCI6MTU5MDkxOTY4MSwiZXhwIjoxNTkwOTIzMjgxLCJlbWFpbCI6ImhhY2hpd2ViYXBwQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7Imdvb2dsZS5jb20iOlsiMTAxODEwNjE1NzcxOTk4MTYwNzg3Il0sImVtYWlsIjpbImhhY2hpd2ViYXBwQGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6Imdvb2dsZS5jb20ifX0.ZMXg13LD0WKngLBaXnlcfaovyRoCTbLwKX9dfm80twi-nUj6EZ2psU2ev67hqnO-QxfEboOzndL4ilSge-KCfByDphsTaJLzP5sDn_FpJZzKuIbnUFpuhp867V_5W8hrwxpEotvfyVXZT2Nq4dpcA-P64H7MhSoln78bMwD8R24JwHiemT_A5fsEqTlFkQkcLsz4CxhlCC951PlT9LG54ykVG-EFZgJuGaNubCW7ITx7eng_4NugLlnj6waYcbWyFPN49aScymy_Sdu8KZl_SAxtPiGIy6lmBS4NYpRifLD0qT1BuJ0GK_OohVOjSBSMS2bYHy6xvH5xEiz-D5W4-Q', NULL, '2020-06-11 18:38:18', '2020-06-11 18:38:18', '2020-05-31 10:08:12'),
(111, 'Pratima Asthana', '-', 'I am an Advocate', NULL, 'asthana0pratima@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-111/_2020-05-31T23:30:36.515943', NULL, 'null', NULL, NULL, NULL, NULL, 'Select', NULL, 'google.com', 1, 1, 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjgyMmM1NDk4YTcwYjc0MjQ5NzI2ZDhmYjYxODlkZWI3NGMzNWM4MGEiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiUHJhdGltYSBBc3RoYW5hIiwicGljdHVyZSI6Imh0dHBzOi8vbGg1Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tWVVvZnhVZ2gxTGcvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQUEvQU1adXVjbnl2ZjdmVWtlVnU5QWc5a3JmeUUzNEdBWHRiQS9zOTYtYy9waG90by5qcGciLCJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYXVydW0tc29sdmVyIiwiYXVkIjoiYXVydW0tc29sdmVyIiwiYXV0aF90aW1lIjoxNTkwOTQ3NTE4LCJ1c2VyX2lkIjoiSktET2tlcGdrbk54SVlRSjQxSFlMV3VBQlU5MyIsInN1YiI6IkpLRE9rZXBna25OeElZUUo0MUhZTFd1QUJVOTMiLCJpYXQiOjE1OTA5NDc1MTgsImV4cCI6MTU5MDk1MTExOCwiZW1haWwiOiJhc3RoYW5hMHByYXRpbWFAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZ29vZ2xlLmNvbSI6WyIxMTYzMTM1MjIzNTg4MzY4Mzc0NDciXSwiZW1haWwiOlsiYXN0aGFuYTBwcmF0aW1hQGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6Imdvb2dsZS5jb20ifX0.qe-af9qSmMXGbXeSXmYK2fItfMTAvvNwKxEiVts9p7Ei_Y_Y7Cb-h6Go7PKRofs-9J76TSlIgCn257IsUVyT8aDR-K367XRdfu-c-suszFBdULj1vYA588CvbrIFJLJoVnNLTlhLEMvkUZ9snuEeP8zZVC1Qj0ANyotY0oj7zOSNxmcW97ckDBAlusPI_iz4rOZabB1IzV5CLlMcb2G1ZBaftr1gC5t9FaIxS74AhndB-X_xWAlR3nKHgNq0JZfnCpuutNQYRZHOiNGn7jH5zaWQGz4OR8ESkc-dXiyGVhtNPL-Jr92galXymya6K2tzLUYXCjqYU_OwczgW1975nQ', NULL, '2020-05-31 18:00:00', '2020-05-31 18:02:05', '2020-05-31 17:52:03'),
(112, 'Mohan Sai', '-', 'Bio 6', '1980-01-16', 'mohansai.developer@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-112/_2020-06-16T12:48:23.951767', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Education', '3 Hour Delay', NULL, 'google.com', 1, 1, 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjgyMmM1NDk4YTcwYjc0MjQ5NzI2ZDhmYjYxODlkZWI3NGMzNWM4MGEiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiRmx1dHRlciBEZXZlbG9wZXIiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1uYUF2MFVYVWJtay9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BTVp1dWNrUkp5MC1WS29yT0lLak1KbWJUWEg1TS1tMG53L3M5Ni1jL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9hdXJ1bS1zb2x2ZXIiLCJhdWQiOiJhdXJ1bS1zb2x2ZXIiLCJhdXRoX3RpbWUiOjE1OTA5NDc4OTYsInVzZXJfaWQiOiJSYjVCVmE4UUJ4ZzNBMlY1bGFrYlJtckFiTUIyIiwic3ViIjoiUmI1QlZhOFFCeGczQTJWNWxha2JSbXJBYk1CMiIsImlhdCI6MTU5MDk0Nzg5NiwiZXhwIjoxNTkwOTUxNDk2LCJlbWFpbCI6Im1vaGFuc2FpLmRldmVsb3BlckBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJnb29nbGUuY29tIjpbIjEwNzI5Mjc3MjI0OTA2NDM4MTE4OCJdLCJlbWFpbCI6WyJtb2hhbnNhaS5kZXZlbG9wZXJAZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoiZ29vZ2xlLmNvbSJ9fQ.G8qMOUTeh-4CbifN1yANkHqWYfaNewCbIKMtmd_JZ23KbO63i0YtV1VejuY6RngfxLQ1vzdepcy0HBdBR0T0Yo91vHNrv7I1VW-HQXfkKwsOaSPyXEp-oDU4tl23gndriVdHr5fQ3dnfNYxl7DqIaZzohi108Tvle4_v68PJWjLs_DG_cPC-Wg-r2GQuHj1X7iDEXZ1bAW10ruR1oNNGeLMpgqfxHdGMVJ71BxcLofrv9hz5fkGzY2FHCDiqEtJf2XA1wAd0uwXkFR7UeAjP9A3U88_c4cU44PI3-N28ouoKZ5_vQs9Lhb_7gH_1CF3efsih0Lk4PLaLVzoZ0Lp0Jw', NULL, '2020-06-16 07:18:33', '2020-06-16 07:18:33', '2020-05-31 17:58:26'),
(113, 'Prateek', '-', 'I am a beta tester', '1977-01-21', 'test@hachiweb.com', '$2y$10$myaF.NYVQHvB5K2fjd/7..tudMckWQPqwvR3IvaB2/z4JF12uXCv2', 'https://aurumsolver.com/storage/user-profile-pic/id-113/_2020-06-07T14:52:51.155435', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Ayurveda', '24 Hour Delay', NULL, 'email', 1, 1, 'SHS7XdwLBhXBTWeaCwhsBBL1Fbs1', 'VycfAM2thzjdua9h3rADcV9q8SFcSNnPnrWW5xxhrFwhBm5DGkC159jpfQ9glRTmgRMru2', '2020-06-11 02:58:55', '2020-06-11 17:08:14', '2020-05-31 18:14:23'),
(114, 'Testing Acc', '-', NULL, '1994-02-12', 'testingdigimantralabs12@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-114/_2020-06-02T09:07:51.094248', NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL, 'google.com', 1, 1, '7LZEzIPJfYO8lm3dhqVcOcmJJy73', NULL, '2020-06-07 10:11:36', '2020-06-07 10:11:36', '2020-06-01 16:39:57'),
(115, 'Dimitri', '-', 'I Love math and I love to code.', '1991-12-02', 'eagleunique17@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-115/_2020-06-03T01:13:27.310375', NULL, 'Both', NULL, '(UTC-05:00) Eastern Time (US & Canada)', NULL, 'Mathematics', 'Instant', NULL, 'google.com', 1, 1, 'sjYIm63eh3ThCW6v7fpoUbLyX6Y2', NULL, '2020-06-08 01:15:03', '2020-06-08 01:15:03', '2020-06-03 05:09:58'),
(116, 'Dimitri', '-', 'I am a Programmer', '1991-12-02', 'dimitrinvestment@yahoo.com', '$2y$10$AiVBt3t22ge1YNbDLZywD.PxfxMZrwc3Cf/ac6B3gbymX0Lj3wJuW', 'https://aurumsolver.com/storage/user-profile-pic/id-116/Dimitri_2020-06-07T21:32:14.887783', NULL, 'Both', NULL, '(UTC-05:00) Eastern Time (US & Canada)', NULL, 'Engineering', 'Instant', NULL, 'email', 1, 1, 'mVhU1ZT8cbgeQUQUQxyf3HgsqR13', 'Nm7Gv74ldM8L0tc89Xce3SKFUp3vy1ym68sSCsw8910GdFbUtUZY75DI4LZQIMNRtvxw4Y', '2020-06-08 10:41:27', '2020-06-08 10:41:27', '2020-06-08 01:04:11'),
(117, 'bridget', '-', 'ready to teach and learn', '1995-09-17', 'bridgetkyei@gmail.com', '$2y$10$hdr3hXW4wwJwiMQSmPPY/.qFwmugJ89r9U1xyo9CECfVDQc/M0lQa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 1, 1, 'Wcl3OJMrgFPkZjKTqQ8pLCqdawT2', 'qjEHD7gOkgFk14x9OPlFHcIH0p0on9XYJ1TkaKyO1CkxK6pVgYrbfIh7XFckJKshHwg9So', NULL, '2020-06-08 01:10:00', '2020-06-08 01:07:00'),
(118, 'Kwadwo', '-', 'hrrrrrr', '1991-01-02', 'aurumsolver@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'twitter.com', 1, 1, 'cJvUb3knjXTCYzXhQP7YeBW28Cg1', NULL, NULL, '2020-06-08 01:18:48', '2020-06-08 01:17:20'),
(119, 'shaukeen', '-', 'gsgdhhd', '1970-01-01', 'shoukeenrao@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-119/_2020-06-09T10:23:09.119328', NULL, 'User', NULL, '(UTC+13:00) Samoa', NULL, 'Computer Science', '3 Hour Delay', 'test@hachiweb.com', 'google.com', 1, 1, 'eCM0P1KWSvQnhwfrMVGDjtgLjiI2', NULL, '2020-06-15 06:42:17', '2020-06-15 06:42:17', '2020-06-09 04:45:43'),
(120, 'Rajendra', '-', 'Developer', '1997-01-02', 'rajendraakmr@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-120/_2020-06-09T10:35:37.368850', NULL, 'User', NULL, '(UTC-05:00) Indiana (East)', NULL, 'Computer Science', '3 Hour Delay', NULL, 'google.com', 1, 1, 'FnNV3zF1EwVX4ZAMw1pZG1cHMPR2', NULL, '2020-06-10 11:10:20', '2020-06-10 11:10:20', '2020-06-09 05:04:26'),
(121, 'Hari Chandra', '-', 'Game Developer', '1996-07-24', 'p.harichandra2407@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-121/Hari Chandra_2020-06-10T05:59:12.942583', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Education', '1 Hour Delay', NULL, 'google.com', 1, 1, '75hR3EZpnTg9DhyLMK4821f3sVR2', NULL, '2020-06-16 04:02:01', '2020-06-16 04:02:01', '2020-06-10 00:13:01'),
(122, NULL, NULL, NULL, NULL, 'p.harichandra2401@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'google.com', 1, 1, 'NdSEm4SQxkgCEbiYW9z00p3yU5g1', NULL, '2020-06-10 00:27:00', '2020-06-10 00:27:00', '2020-06-10 00:24:29'),
(123, 'Mark Davis', '-', 'brrrrrrrrrrrrrrrrreeeeeeeeeee', '1991-12-02', 'kwadwobeng@yahoo.com', '$2y$10$i6z7iZduLZvzD8.EFZlCtuL79vK/jHnvwMy3PIUABYsJ7ilGp/E0m', NULL, NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL, 'email', 1, 1, 'dZf5IP8cvafNJBYxPV4lXpuNbx72', 'vmq6Axb5irUU3dh6dX5uYljWhXLTexePAMXxWf6ZuEHXgbIr1CVfPmtmXVP1tTkklxZWAN', '2020-06-11 19:16:09', '2020-06-11 19:16:09', '2020-06-11 16:26:51'),
(124, 'Adv', '-', 'Contributor', '1980-01-30', 'care@thelawstack.online', '$2y$10$/ITSbVZnfoIpQhxROABI3.EiwNyIsE6qPESlusSF1U4ZThvDmIl26', NULL, NULL, 'Select', NULL, '(UTC-07:00) Pacific Time (US & Canada)', NULL, 'Select', 'Select', NULL, 'email', 1, 1, 'yVa5czo8LDh4Ys2FADByCv1S8ns1', '5yFLMKxuUP22SxfoEp4SxmLpdcx7z3VG1PyTmNlvyhfO5E3pHM9LxPFdIRCWq1IcJZ2WyN', '2020-06-11 18:35:59', '2020-06-11 18:35:59', '2020-06-11 16:35:55'),
(125, 'Mohan Sai', '-', 'bio 4', '1997-01-09', 'lebon27758@bewedfv.com', '$2y$10$HsMjOVEN8Pjb8pN7F4/JzuVAKB7OPWJRu7sKGQNPqQVYKkIxkcveq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 1, 1, 'dAciCpiaARUwFFdzpWJKypzvJQB3', 'Vjh8niwHp0knwetv61yuIf2uJ2plmQutKHaEJql2WSxoxoFxWOqaJLBp8Yb88zJiqx6oGH', '2020-06-12 02:33:37', '2020-06-12 02:33:37', '2020-06-11 16:49:02'),
(126, NULL, '-', NULL, NULL, 'gorico1586@bewedfv.com', '$2y$10$V1qw23Db1svyLqMrnzBIyOVhEVAjfHtnzP4q60Ebj0kUH2szOLkT6', 'https://aurumsolver.com/storage/user-profile-pic/id-126/_2020-06-11T23:01:11.404027', NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL, 'email', 1, 1, 'RIGP2gMAALRlisedntyauod6M6I2', 'A6XPDsXinOJy0eFa7gZA6Z3CXJdW4L7cdC7BaNYyjlWycBQKDQnQIGeBqLvHYfq62EYmpO', '2020-06-11 17:31:19', '2020-06-11 17:31:19', '2020-06-11 17:20:40'),
(127, NULL, '-', NULL, NULL, 'kagiv83025@htwern.com', '$2y$10$cCTqbf9RuE5AuP59dL1iJ.ch8ud4UuyaLBorEKQeN9UqJl3YO7E.G', 'https://aurumsolver.com/storage/user-profile-pic/id-127/_2020-06-11T23:05:43.509451', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 1, 1, 'pD7bDwXeZhebLlU8Yraq0krGQ682', '2RiAsojTlurD02ePPI7dYfvJLW6sjNISwx1PbFHLiy99ztBiSCF4bIBChf6QRwhF0yleNE', '2020-06-11 17:39:12', '2020-06-11 17:39:12', '2020-06-11 17:35:28'),
(128, NULL, NULL, NULL, NULL, 'saniha2585@lercjy.com', '$2y$10$zFl/DfXLBQkzfV7jzSABMORuQT.t/3R3j6WPVsPAGVKGEt7JzZS5C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 1, 1, 'PX733g4N7XMKUwQxOO5iSfqA5nR2', 'RzJ35fzW8pIxkhQdhQhnZOOfmEHxzMAgVb1DkmDg0Uw86S0OpWxdysqG7KW5BUIksYPot0', NULL, '2020-06-11 17:41:45', '2020-06-11 17:41:45'),
(129, NULL, '-', NULL, NULL, 'rijiki6923@lefaqr5.com', '$2y$10$c.Wpk5S86z0Fl78sujT0xOLLgkjQdqiFpxqTYxzuWk./2uonte8x.', 'https://aurumsolver.com/storage/user-profile-pic/id-129/_2020-06-11T23:23:14.946179', NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL, 'email', 1, 1, 'NA', 'FvpgzEZYFF2P3duVLhcCJWSHL5382WDmzg2YQiqSjxJLfZRxavqG2EwbTsKJ8ht1E7kDdT', '2020-06-11 17:54:10', '2020-06-11 17:54:10', '2020-06-11 17:51:38'),
(130, NULL, NULL, NULL, NULL, 'behim37022@lercjy.com', '$2y$10$JOObr5/chJOGQpFpRu4gAub.s2r3V99BUqlEKn2ir0vFc5g7nC6qS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 1, 1, 'l525mlaov3WVgq689cO1vnJy35j2', 'Yq3yb5PZzs0KLDHV36lAIzlxX5AwWmMWDJuXFUEQAIN9ki6l1C0K1lX3XTUkY5cisHOrGD', NULL, '2020-06-11 18:00:03', '2020-06-11 18:00:03'),
(131, NULL, '-', NULL, NULL, 'jipelay433@kewrg.com', '$2y$10$CL7gDXNstY22V6C10SObHOGmgssLuJuO7myYsvZjRjcu1mKUymM6m', 'https://aurumsolver.com/storage/user-profile-pic/id-131/_2020-06-11T23:33:51.813313', NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL, 'email', 1, 1, 'hRYtH4GTwnVql2jJKCQxnKyKklw1', '5SIKzxmWDai8cGsxNNUaQpF2AVywiaUqkDXPFuXUhliY7spxid1d6kZty9o40jbYzkKUHL', '2020-06-16 06:19:20', '2020-06-16 06:19:20', '2020-06-11 18:03:14'),
(132, 'Scott Broverly', '-', 'A Scientist.', '1990-01-21', 'scientistdimma@gmail.com', '$2y$10$J3VOu4l/bEOg.n9etf/EnuA2ei10Nz38oRMQLmv42ix3uISoNfSmu', 'https://aurumsolver.com/storage/user-profile-pic/id-132/_2020-06-11T21:06:16.437420', NULL, 'Tutor', NULL, '(UTC-05:00) Eastern Time (US & Canada)', NULL, 'Chemistry', 'Instant', NULL, 'email', 1, 1, 'fW9bgk658gUhLkoERYia6ebX1iM2', '5LWK9lotYcxHyXwGEuY58bi4HGVae4QkjLr8ao1FacKfXRXnG5IdKqjPREUlfeNG7nvmAr', '2020-06-12 01:06:32', '2020-06-12 01:06:32', '2020-06-11 19:32:21'),
(133, NULL, NULL, NULL, NULL, 'gregorylarbi96@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'google.com', 1, 1, '8emVC8g4sCRhOouNx1rX4eQ2Ihv2', NULL, '2020-06-12 22:54:04', '2020-06-12 22:54:04', '2020-06-12 22:53:13'),
(134, 'BeHive', '-', 'Passionate Traveler', '1997-05-09', 'bihove5780@agilekz.com', '$2y$10$kkR6YQQw/WGW8fdhfPMaqOe3kz9irmmYuUzbksHQ25TPi3r8tJWuW', 'https://aurumsolver.com/storage/user-profile-pic/id-134/BeHive_2020-06-15T20:44:19.573463', NULL, 'Select', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Select', 'Select', NULL, 'email', 1, 1, 'ek5A4S54fBcCeQvludWUmU7xXj42', 'ysWXR7TQRtz6m5729qs5ZZnWyVczrBEGLqeJ7jgW0rc8Avn86X2DPkdW5AyYLE0sJLYzgU', '2020-06-15 15:14:44', '2020-06-15 15:14:44', '2020-06-15 15:13:01'),
(135, 'Thaliva', '-', 'Super Star', '1980-01-18', 'piwih67536@htwern.com', '$2y$10$liE0E96.GHOAs6gTZRd.X.X06lM58l.duHuqcfYrIQDDhku3Ne92q', NULL, NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Education', '1 Hour Delay', NULL, 'email', 1, 1, 'ja9Z0F8U4HNX9N2a2JW7we5U8hu1', 'bwfEwcHQCj0c1pA6GVvryVDfOJlMHLHf6Z0dHabaiV1eYvoLYzs8GJImzwcubCSQGJYRpi', '2020-06-16 12:00:28', '2020-06-16 12:00:28', '2020-06-16 06:22:40');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `subjects` text NOT NULL,
  `duration` float NOT NULL,
  `min_percent` float NOT NULL DEFAULT '80',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `subjects`, `duration`, `min_percent`, `created_at`, `updated_at`) VALUES
(1, 'Accounting', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(2, 'Architecture', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(3, 'Art', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(4, 'Asian Studies', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(5, 'Astronmy', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(6, 'Biiology', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(7, 'Bussiness', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(8, 'Chemistry', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(9, 'Communications', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(10, 'Computer Science', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(11, 'Economics', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(12, 'Education', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(13, 'Engineering', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(14, 'English', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(15, 'Finance', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(16, 'Foreign Languages', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(17, 'Gendr Studies', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(18, 'General Studies', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(19, 'Gendr Questions', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(20, 'Geography', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(21, 'Geology', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(22, 'Health Care', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(23, 'History', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(24, 'Law', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(25, 'Linguistics', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(26, 'Literary', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(27, 'Mathematics', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(28, 'Literary Studies', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(29, 'Music', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(30, 'Other', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(31, 'Philosophy', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(32, 'Physics', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(33, 'Political Science', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(34, 'Religious Studies', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(35, 'Sociology', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(36, 'Statistics', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(37, 'Urban Planning and Policy', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `from_message` text NOT NULL,
  `to_message` text NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `from_message`, `to_message`, `message`, `created_at`, `updated_at`) VALUES
(1, '84', '86', 'Hello,This Rajesh.', '2020-04-16 06:33:01', '2020-04-16 06:33:01');

-- --------------------------------------------------------

--
-- Table structure for table `faq_support`
--

CREATE TABLE `faq_support` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_support`
--

INSERT INTO `faq_support` (`id`, `question`, `answer`) VALUES
(1, 'At accusantium repreLorem, ipsum dolor sit amet consectetur adip', 'At accusantium repreAt accusantium repreLorem, ipsum dolor sit amet consectetur adipisicing elit. Veritatis earum totam nisi aliquid officiis quidem incidunt, impedit debitis! Nostrum praesentium optio maxime quo, excepturi alias expedita rerum iusto quae exercitationem.'),
(2, 'At accusantium repreLorem, ipsum dolor sit amet consectetur adipAt accusantium repreLorem, ipsum dolor sit amet consectetur adip.?', 'At accusantium repreLorem, ipsum dolor sit amet consectetur adipisicing elit. Veritatis earum totam nisi aliquid officiis quidem incidunt, impedit debitis! Nostrum praesentium optio maxime quo, excepturi alias expedita rerum iusto quae exercitationem. answerAt accusantium repreLorem, ipsum dolor sit amet consectetur adipisicing elit. Veritatis earum totam nisi aliquid officiis quidem incidunt, impedit debitis! Nostrum praesentium optio maxime quo, excepturi alias expedita rerum iusto quae exercitationem.'),
(5, 'Sunt nostrud at sunt', 'Vel perferendis non'),
(6, 'Sint aliquam eligend', 'Tempor nostrum dolor'),
(7, 'Non libero id illum', 'Consequatur nulla u'),
(8, 'Asperiores repudiand', 'Ut exercitationem su'),
(9, 'Rerum modi earum ea', 'Non et dolorem cillu');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` int(11) NOT NULL,
  `rating` float NOT NULL,
  `review` text,
  `rated_to` int(11) NOT NULL,
  `rated_by` int(11) NOT NULL,
  `rated_for` text NOT NULL,
  `rated_for_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `rating`, `review`, `rated_to`, `rated_by`, `rated_for`, `rated_for_id`, `created_at`, `updated_at`) VALUES
(1, 5, 'He is very good at his knowledge and knows what and how exactly can resolve the issue, Great to work with him.', 101, 102, 'Assignment', 24, '2020-05-23 09:18:12', '2020-05-23 14:48:12'),
(2, 5, 'great', 101, 100, 'Question', 28, '2020-05-27 19:26:30', '2020-05-28 00:56:30'),
(3, 5, 'Awesome', 101, 100, 'Question', 28, '2020-05-27 19:30:03', '2020-05-28 01:00:03'),
(4, 5, 'Great job', 101, 107, 'Question', 34, '2020-06-01 21:59:15', '2020-06-02 03:29:15'),
(5, 5, 'Test', 101, 110, 'Question', 35, '2020-06-02 22:14:59', '2020-06-03 03:44:59'),
(6, 3, 'new feedback', 101, 110, 'Question', 35, '2020-06-02 22:15:39', '2020-06-03 03:45:39'),
(7, 5, NULL, 101, 110, 'Question', 35, '2020-06-07 01:27:59', '2020-06-07 06:57:59'),
(8, 5, NULL, 101, 110, 'Question', 35, '2020-06-07 01:37:17', '2020-06-07 07:07:17'),
(9, 5, NULL, 112, 101, 'Question', 36, '2020-06-07 01:37:52', '2020-06-07 07:07:52'),
(10, 5, 'Good Job @Dev', 113, 110, 'Question', 37, '2020-06-07 04:02:44', '2020-06-07 09:32:44'),
(11, 5, 'Great', 101, 110, 'Question', 35, '2020-06-08 08:47:44', '2020-06-08 14:17:44'),
(12, 5, NULL, 112, 121, 'Assignment', 42, '2020-06-10 11:37:41', '2020-06-10 17:07:41'),
(13, 5, 'good job', 113, 110, 'Assignment', 44, '2020-06-11 05:39:43', '2020-06-11 11:09:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 2),
(3, 'App\\User', 3);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'dashboard', 'web', '2020-06-09 22:29:27', '2020-06-09 22:29:27');

-- --------------------------------------------------------

--
-- Table structure for table `prepayments`
--

CREATE TABLE `prepayments` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `from_request` int(11) DEFAULT NULL,
  `to_request` int(11) DEFAULT NULL,
  `budget_amount` float DEFAULT NULL,
  `requested_amount` float DEFAULT NULL,
  `approve_status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prepayments`
--

INSERT INTO `prepayments` (`id`, `question_id`, `from_request`, `to_request`, `budget_amount`, `requested_amount`, `approve_status`, `created_at`, `updated_at`) VALUES
(1, 76, 101, 100, 14, 12.6, NULL, '2020-05-18 18:59:30', '2020-05-18 18:59:30'),
(2, 76, 101, 100, 14, 12.6, NULL, '2020-05-18 18:59:44', '2020-05-18 18:59:44'),
(3, 76, 101, 100, 14, 12.6, NULL, '2020-05-18 18:59:57', '2020-05-18 18:59:57'),
(4, 76, 101, 100, 14, 5, NULL, '2020-05-18 19:32:37', '2020-05-18 19:32:37'),
(5, 74, 101, 100, 15, 7, NULL, '2020-05-18 19:57:24', '2020-05-18 19:57:24'),
(6, 77, 101, 102, 36, 3.3, 0, '2020-05-18 20:24:49', '2020-05-19 19:04:33'),
(7, 77, 101, 102, 36, 3.3, 1, '2020-05-18 20:27:05', '2020-05-19 19:53:42'),
(8, 77, 101, 102, 36, 4.4, NULL, '2020-05-18 20:33:17', '2020-05-18 20:33:17'),
(9, 77, 101, 102, 36, 4.4, NULL, '2020-05-18 20:33:17', '2020-05-18 20:33:17'),
(10, 78, 101, 102, 3, 2, NULL, '2020-05-20 20:37:17', '2020-05-20 20:37:17'),
(11, 79, 101, 102, 10, 6.7, NULL, '2020-05-22 20:19:12', '2020-05-22 20:19:12'),
(12, 94, 113, 110, 85.12, 42.5, NULL, '2020-06-09 04:57:23', '2020-06-09 04:57:23'),
(13, 99, 120, 119, 55, 3, 1, '2020-06-09 05:39:39', '2020-06-09 05:40:24'),
(14, 101, 121, 112, 6.95, 2, 1, '2020-06-10 00:35:05', '2020-06-10 00:48:49'),
(15, 101, 121, 112, 6.95, 2, 1, '2020-06-10 00:39:48', '2020-06-10 00:51:13'),
(16, 103, 112, 121, 2, 1, 1, '2020-06-10 01:56:49', '2020-06-10 02:00:56'),
(17, 105, 119, 120, 25, 7, NULL, '2020-06-10 10:46:25', '2020-06-10 10:46:25'),
(18, 94, 113, 110, 85.12, 6.8, 0, '2020-06-10 11:12:30', '2020-06-10 11:16:08'),
(19, 106, 119, 120, 30, 4, 0, '2020-06-10 11:20:32', '2020-06-10 11:21:05'),
(20, 94, 113, 110, 85.12, 2, 1, '2020-06-10 11:24:42', '2020-06-10 11:26:00'),
(21, 107, 113, 120, 25, 18, 1, '2020-06-10 11:29:43', '2020-06-10 11:30:16'),
(22, 107, 113, 120, 25, 1, 0, '2020-06-10 11:32:48', '2020-06-10 11:36:06'),
(23, 107, 113, 120, 25, 1, NULL, '2020-06-10 11:33:15', '2020-06-10 11:33:15'),
(24, 107, 113, 120, 25, 2, NULL, '2020-06-10 11:33:22', '2020-06-10 11:33:22'),
(25, 113, 110, 113, 39, 28, 1, '2020-06-11 02:51:28', '2020-06-11 02:53:25');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tutor_id` varchar(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` text,
  `question` text,
  `is_assigned` tinyint(1) NOT NULL DEFAULT '0',
  `attachment` text,
  `willing_to_pay` varchar(255) DEFAULT NULL,
  `private_question` tinyint(1) DEFAULT '0',
  `prepayment` tinyint(4) NOT NULL DEFAULT '0',
  `questioner_fee` varchar(25) DEFAULT NULL,
  `answerer_fee` varchar(25) DEFAULT NULL,
  `admin_fee` varchar(25) DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `request_accept` varchar(1) DEFAULT NULL,
  `delivery_accept` varchar(1) DEFAULT NULL,
  `is_paid` tinyint(1) DEFAULT NULL,
  `is_cancelled` tinyint(1) NOT NULL DEFAULT '0',
  `cancellation_reason` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_approved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `quizzes`
--

CREATE TABLE `quizzes` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `questions` text NOT NULL,
  `opt1` text NOT NULL,
  `opt2` text NOT NULL,
  `opt3` text NOT NULL,
  `opt4` text NOT NULL,
  `correct` varchar(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quizzes`
--

INSERT INTO `quizzes` (`id`, `category_id`, `questions`, `opt1`, `opt2`, `opt3`, `opt4`, `correct`, `created_at`, `updated_at`) VALUES
(1, 5, 'what is latest version of php?', '3.4', '6.8', '8', '7.4', 'opt1', '2020-04-09 01:38:12', '2020-04-09 01:38:12'),
(2, 5, 'What is microsoft additions', '5', '5.5', '8', '9', 'opt4', '2020-04-11 04:10:12', '2020-04-11 04:10:12'),
(3, 10, 'What is latest bootsrtap versions.', '4.3', '5.5', '3', '3.5', 'opt3', '2020-04-11 04:14:41', '2020-04-11 04:14:41'),
(4, 10, 'What is latest bootsrtap versions.', '4.3', '5.5', '3', '3.5', 'opt2', '2020-04-14 05:40:07', '2020-04-14 05:40:07'),
(5, 5, 'Micorsogt?', '4.3', '5.5', '3', '3.5', 'opt1', '2020-05-16 04:00:51', '2020-05-16 04:00:51'),
(6, 1, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:09:31', '2020-05-28 07:09:31'),
(7, 1, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:09:36', '2020-05-28 07:09:36'),
(8, 1, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:03', '2020-05-28 07:10:03'),
(9, 1, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:15', '2020-05-28 07:10:15'),
(10, 2, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:22', '2020-05-28 07:10:22'),
(11, 2, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:27', '2020-05-28 07:10:27'),
(12, 2, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:40', '2020-05-28 07:10:40'),
(13, 2, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:50', '2020-05-28 07:10:50'),
(14, 2, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:09', '2020-05-28 07:11:09'),
(15, 3, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:20', '2020-05-28 07:11:20'),
(16, 3, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(17, 3, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(18, 3, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(19, 3, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(20, 6, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(21, 6, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(22, 6, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(23, 4, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(24, 4, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(25, 5, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(26, 6, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(27, 7, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(28, 7, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(29, 7, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(30, 7, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(31, 7, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(32, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(33, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(34, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(35, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(36, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(37, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(38, 9, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(39, 9, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(40, 9, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(41, 9, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(42, 9, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(43, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(44, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(45, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(46, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(47, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(48, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(49, 12, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(50, 12, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(51, 12, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(52, 12, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(53, 13, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(54, 13, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(55, 13, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(56, 13, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(57, 13, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(58, 14, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(59, 14, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(60, 14, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(61, 14, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(62, 14, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(63, 15, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(64, 15, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(65, 15, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(66, 15, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(67, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(68, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(69, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:19:43', '2020-05-28 07:19:43'),
(70, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:19:43', '2020-05-28 07:19:43'),
(71, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:19:43', '2020-05-28 07:19:43'),
(72, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:19:43', '2020-05-28 07:19:43'),
(73, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:19:43', '2020-05-28 07:19:43'),
(74, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(75, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(76, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(77, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(78, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(79, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(80, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(81, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(82, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(83, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(84, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(85, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(86, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(87, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(88, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(89, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(90, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(91, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(92, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(93, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(94, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(95, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(96, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(97, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(98, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(99, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(100, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(101, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(102, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(103, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(104, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(105, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(106, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(107, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(108, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(109, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(110, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(111, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(112, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(113, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(114, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(115, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(116, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:08', '2020-05-28 07:23:08'),
(117, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:08', '2020-05-28 07:23:08'),
(118, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:08', '2020-05-28 07:23:08'),
(119, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:09', '2020-05-28 07:23:09'),
(120, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:09', '2020-05-28 07:23:09'),
(121, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:09', '2020-05-28 07:23:09'),
(122, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(123, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(124, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(125, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(126, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(127, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(128, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(129, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(130, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(131, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(132, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(133, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(134, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(135, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(136, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(137, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(138, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(139, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(140, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(141, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(142, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:05', '2020-05-28 07:25:05'),
(143, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(144, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(145, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(146, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(147, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(148, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(149, 28, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:20', '2020-05-28 07:25:20'),
(150, 28, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:21', '2020-05-28 07:25:21'),
(151, 28, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:21', '2020-05-28 07:25:21'),
(152, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(153, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(154, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(155, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(156, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(157, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(158, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(159, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(160, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(161, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(162, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(163, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(164, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(165, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(166, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(167, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answer`
--

CREATE TABLE `quiz_answer` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `correct_answer` int(11) NOT NULL,
  `percent` text NOT NULL,
  `total_attempt` int(11) NOT NULL,
  `is_passed` tinyint(1) NOT NULL DEFAULT '0',
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quiz_answer`
--

INSERT INTO `quiz_answer` (`id`, `category_id`, `user_id`, `correct_answer`, `percent`, `total_attempt`, `is_passed`, `is_paid`, `created_at`, `updated_at`) VALUES
(12, 1, '101', 20, '100', 1, 1, 1, '2020-05-19 17:59:11', '2020-05-19 23:28:28'),
(13, 2, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(14, 3, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(15, 4, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(16, 5, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(17, 6, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(18, 7, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(19, 8, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(20, 9, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(21, 10, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(22, 11, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(23, 12, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(24, 13, '101', 20, '100', 1, 1, 1, '2020-05-19 18:01:10', '2020-05-19 23:28:28'),
(25, 14, '101', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(26, 15, '101', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(27, 10, '120', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(28, 17, '101', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(29, 8, '120', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(30, 19, '101', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(31, 20, '101', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(32, 12, '120', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(33, 22, '101', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(34, 23, '101', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(35, 24, '101', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(36, 25, '101', 20, '100', 1, 1, 1, '2020-05-19 18:02:06', '2020-05-19 23:28:28'),
(37, 26, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:54', '2020-05-19 23:28:28'),
(38, 27, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(39, 28, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(40, 29, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(41, 30, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(42, 31, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(43, 32, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(44, 33, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(45, 34, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(46, 35, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(47, 36, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(48, 37, '101', 20, '100', 1, 1, 1, '2020-05-19 18:03:55', '2020-05-19 23:28:28'),
(49, 8, '109', 20, '90', 1, 1, 1, '2020-05-29 05:28:34', NULL),
(50, 27, '109', 20, '90', 1, 1, 1, '2020-05-29 05:29:20', NULL),
(51, 1, '107', 2, '50', 1, 0, 0, '2020-05-31 08:32:43', '2020-05-31 08:32:43'),
(52, 30, '107', 0, '0', 1, 0, 0, '2020-05-31 08:35:14', '2020-05-31 08:35:14'),
(53, 28, '107', 0, '0', 1, 0, 0, '2020-05-31 08:45:29', '2020-05-31 08:45:29'),
(54, 27, '107', 0, '0', 1, 0, 0, '2020-05-31 08:56:00', '2020-05-31 08:56:00'),
(55, 2, '107', 2, '40', 1, 0, 0, '2020-05-31 10:29:42', '2020-05-31 10:29:42'),
(56, 3, '107', 0, '0', 1, 0, 0, '2020-05-31 11:33:02', '2020-05-31 11:33:02'),
(57, 38, '113', 0, '0', 1, 0, 0, '2020-06-02 03:42:02', '2020-06-02 03:42:02'),
(58, 38, '112', 0, '0', 1, 0, 0, '2020-06-02 15:40:04', '2020-06-02 15:40:04'),
(59, 38, '110', 6, '100', 1, 1, 0, '2020-06-02 17:43:32', '2020-06-02 17:43:32'),
(60, 6, '112', 4, '100', 1, 1, 0, '2020-06-03 03:53:37', '2020-06-03 03:53:37'),
(61, 10, '112', 2, '100', 1, 1, 0, '2020-06-03 03:54:53', '2020-06-03 03:54:53'),
(62, 14, '115', 0, '0', 1, 0, 0, '2020-06-03 05:16:15', '2020-06-03 05:16:15'),
(63, 27, '115', 1, '14.29', 1, 0, 0, '2020-06-03 05:29:19', '2020-06-03 05:29:19'),
(64, 1, '115', 0, '0', 1, 0, 0, '2020-06-03 05:30:28', '2020-06-03 05:30:28'),
(65, 28, '113', 3, '100', 1, 1, 0, '2020-06-07 09:26:35', '2020-06-07 09:26:35'),
(66, 27, '113', 0, '0', 1, 0, 0, '2020-06-07 09:58:56', '2020-06-07 09:58:56'),
(67, 1, '113', 0, '0', 1, 0, 0, '2020-06-07 09:59:13', '2020-06-07 09:59:13'),
(68, 2, '113', 0, '0', 1, 0, 0, '2020-06-07 13:02:56', '2020-06-07 13:02:56'),
(69, 3, '113', 0, '0', 1, 0, 0, '2020-06-07 16:36:12', '2020-06-07 16:36:12'),
(70, 10, '106', 5, '250', 1, 1, 0, '2020-06-09 06:37:05', '2020-06-09 06:37:05'),
(71, 3, '112', 5, '100', 1, 1, 0, '2020-06-10 00:02:12', '2020-06-10 00:02:12'),
(72, 2, '112', 4, '80', 2, 1, 0, '2020-06-10 00:05:52', '2020-06-10 00:06:18'),
(73, 2, '120', 0, '0', 1, 0, 0, '2020-06-10 10:32:14', '2020-06-10 10:32:14'),
(74, 6, '120', 0, '0', 1, 0, 0, '2020-06-10 10:33:01', '2020-06-10 10:33:01'),
(75, 1, '110', 0, '0', 3, 0, 0, '2020-06-11 11:40:31', '2020-06-11 11:41:22'),
(76, 5, '110', 0, '0', 1, 0, 0, '2020-06-11 11:44:00', '2020-06-11 11:44:00'),
(77, 8, '112', 1, '16.67', 3, 0, 0, '2020-06-11 14:48:25', '2020-06-11 14:49:10');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-06-09 22:29:27', '2020-06-09 22:29:27'),
(2, 'moderator', 'web', '2020-06-09 22:30:08', '2020-06-09 22:30:08'),
(3, 'user', 'web', '2020-06-09 22:30:52', '2020-06-09 22:30:52');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `from` text NOT NULL,
  `to` text NOT NULL,
  `amount` text NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `status` text NOT NULL,
  `withdrawal` tinyint(4) NOT NULL DEFAULT '0',
  `is_gateway` tinyint(1) NOT NULL DEFAULT '0',
  `transaction_id` text NOT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `question_id`, `answer_id`, `from`, `to`, `amount`, `currency`, `status`, `withdrawal`, `is_gateway`, `transaction_id`, `description`, `created_at`, `updated_at`) VALUES
(1, NULL, 17, '75', '84', '35.5', NULL, 'paid', 1, 0, 'ADFAGRTRGHGHDASF', NULL, '2020-04-14 02:12:32', '2020-04-16 00:40:22'),
(6, 29, NULL, '61', '61', '20', NULL, 'paid', 0, 0, 'SFGGSGTYFGBSHT22', NULL, '2020-04-15 01:21:18', '2020-04-16 00:40:22'),
(7, NULL, NULL, '101', '101', '10000', NULL, 'paid', 0, 0, 'GSFGSFG6545F4SFG4RTHG', NULL, '2020-04-15 02:03:38', '2020-04-15 02:03:38'),
(8, NULL, 17, '75', '84', '35.5', NULL, 'paid', 1, 0, 'ADFAGRTRGHGHDASF', NULL, '2020-04-15 05:34:29', '2020-04-16 00:40:22'),
(9, 52, NULL, '86', '84', '20', NULL, 'paid', 1, 0, 'SFGGSGTYFGBSHT22', NULL, '2020-04-15 05:44:43', '2020-04-16 00:40:22'),
(10, NULL, NULL, '101', '102', '5000', NULL, 'paid', 0, 0, 'trgaw35rA#T%re4@43wrfsd', NULL, '2020-05-19 19:16:45', '2020-05-19 19:16:45'),
(11, 77, NULL, '102', '101', '3.30', NULL, 'paid', 0, 0, 'TESTING__PAID', NULL, '2020-05-19 19:53:42', '2020-05-19 19:53:42'),
(12, 77, NULL, '102', '101', '3.30', NULL, 'paid', 0, 0, 'TESTING__PAID', NULL, '2020-05-19 19:59:26', '2020-05-19 19:59:26'),
(13, 78, NULL, '102', '101', '3.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-05-20 18:54:30', '2020-05-20 18:54:30'),
(14, 79, NULL, '102', '102', '10.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-05-22 20:17:58', '2020-05-22 20:17:58'),
(15, 79, NULL, '102', '101', '10', NULL, 'paid', 0, 0, 'AS_TXN_1300017791', NULL, '2020-05-22 20:50:49', '2020-05-22 20:50:49'),
(16, 80, NULL, '102', '102', '10.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-05-22 21:20:08', '2020-05-22 21:20:08'),
(17, 80, NULL, '101', '102', '10.00', NULL, 'paid', 0, 0, 'T_ACTION_AS_EJKJAGJKLQ', NULL, '2020-05-22 21:29:39', '2020-05-22 21:29:39'),
(18, 80, NULL, '102', '102', '10.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-05-22 21:47:56', '2020-05-22 21:47:56'),
(19, 80, NULL, '101', '102', '10.00', NULL, 'paid', 0, 0, 'AS_TXN_685783953', NULL, '2020-05-22 21:50:43', '2020-05-22 21:50:43'),
(20, 80, NULL, '101', '102', '10.00', NULL, 'paid', 0, 0, 'AS_TXN_143267067', NULL, '2020-05-22 21:52:59', '2020-05-22 21:52:59'),
(21, 80, NULL, '101', '102', '10.00', NULL, 'paid', 0, 0, 'AS_TXN_890678306', NULL, '2020-05-22 21:53:34', '2020-05-22 21:53:34'),
(22, 80, NULL, '101', '102', '10.00', NULL, 'paid', 0, 0, 'AS_TXN_452192117', NULL, '2020-05-22 21:54:13', '2020-05-22 21:54:13'),
(25, 77, 23, '101', '101', '36.00', NULL, 'paid', 0, 0, 'RANDOM_TRANSACTION_ID', NULL, '2020-05-23 14:26:46', '2020-05-23 14:26:46'),
(26, 77, 23, '101', '101', '36.00', NULL, 'paid', 0, 0, 'RANDOM_TRANSACTION_ID', NULL, '2020-05-23 14:26:54', '2020-05-23 14:26:54'),
(27, 77, 24, '101', '101', '36.00', NULL, 'paid', 0, 0, 'RANDOM_TRANSACTION_ID', NULL, '2020-05-23 14:27:55', '2020-05-23 14:27:55'),
(28, 72, 28, '101', '101', '10.00', NULL, 'paid', 0, 0, 'RANDOM_TRANSACTION_ID', NULL, '2020-05-28 06:17:15', '2020-05-28 06:17:15'),
(29, 83, 33, '101', '101', '10.00', NULL, 'paid', 0, 0, 'RANDOM_TRANSACTION_ID', NULL, '2020-06-02 01:04:57', '2020-06-02 01:04:57'),
(30, 84, 34, '101', '101', '10.40', NULL, 'paid', 0, 0, 'RANDOM_TRANSACTION_ID', NULL, '2020-06-02 01:05:19', '2020-06-02 01:05:19'),
(31, 88, 35, '110', '101', '12.96', NULL, 'paid', 0, 0, 'RANDOM_TRANSACTION_ID', NULL, '2020-06-03 03:44:27', '2020-06-03 03:44:27'),
(32, 85, 36, '112', '112', '23.65', NULL, 'paid', 0, 0, 'RANDOM_TRANSACTION_ID', NULL, '2020-06-07 07:07:45', '2020-06-07 07:07:45'),
(33, 93, 37, '110', '113', '62.42', NULL, 'paid', 0, 0, 'RANDOM_TRANSACTION_ID', NULL, '2020-06-07 09:32:07', '2020-06-07 09:32:07'),
(34, 94, NULL, '110', '110', '85.12', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-07 10:34:18', '2020-06-07 10:34:18'),
(35, 64, 31, '101', '109', '20.00', NULL, 'paid', 0, 0, 'RANDOM_TRANSACTION_ID', NULL, '2020-06-08 14:12:20', '2020-06-08 14:12:20'),
(36, 97, NULL, '120', '120', '5.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-09 05:32:58', '2020-06-09 05:32:58'),
(37, 99, NULL, '119', '119', '55.00', NULL, 'paid', 0, 1, 'SOME_RANDOM_STRING', NULL, '2020-06-09 05:38:41', '2020-06-09 05:38:41'),
(38, 99, NULL, '119', '120', '3.00', NULL, 'paid', 0, 0, 'T_ACTION_AS_EJKJAGJKLQ', NULL, '2020-06-09 05:40:24', '2020-06-09 05:40:24'),
(39, 99, NULL, '120', '119', '55.00', NULL, 'paid', 0, 0, 'AS_TXN_210101018', NULL, '2020-06-09 05:56:32', '2020-06-09 05:56:32'),
(40, 100, NULL, '119', '119', '15.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-09 06:13:13', '2020-06-09 06:13:13'),
(41, 101, NULL, '112', '112', '6.95', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-10 00:34:46', '2020-06-10 00:34:46'),
(42, 101, NULL, '112', '121', '2.00', NULL, 'paid', 0, 0, 'T_ACTION_AS_EJKJAGJKLQ', NULL, '2020-06-10 00:48:49', '2020-06-10 00:48:49'),
(43, 101, NULL, '112', '121', '2.00', NULL, 'paid', 0, 0, 'T_ACTION_AS_EJKJAGJKLQ', NULL, '2020-06-10 00:51:13', '2020-06-10 00:51:13'),
(44, 102, NULL, '121', '121', '1.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-10 01:46:37', '2020-06-10 01:46:37'),
(45, 102, NULL, '121', '112', '1', NULL, 'paid', 0, 0, 'AS_TXN_6731677231', NULL, '2020-06-10 01:49:32', '2020-06-10 01:49:32'),
(46, 103, NULL, '121', '121', '2.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-10 01:54:25', '2020-06-10 01:54:25'),
(47, 103, NULL, '121', '112', '1.00', NULL, 'paid', 0, 0, 'T_ACTION_AS_EJKJAGJKLQ', NULL, '2020-06-10 02:00:56', '2020-06-10 02:00:56'),
(48, 103, NULL, '121', '112', '2', NULL, 'paid', 0, 0, 'AS_TXN_6914294354', NULL, '2020-06-10 02:04:19', '2020-06-10 02:04:19'),
(49, 105, NULL, '120', '120', '25.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-10 10:44:38', '2020-06-10 10:44:38'),
(50, 106, NULL, '120', '120', '30.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-10 11:19:59', '2020-06-10 11:19:59'),
(51, 94, NULL, '110', '113', '2.00', NULL, 'paid', 0, 0, 'T_ACTION_AS_EJKJAGJKLQ', NULL, '2020-06-10 11:26:00', '2020-06-10 11:26:00'),
(52, 107, NULL, '120', '120', '25.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-10 11:28:54', '2020-06-10 11:28:54'),
(53, 107, NULL, '120', '113', '18.00', NULL, 'paid', 0, 0, 'T_ACTION_AS_EJKJAGJKLQ', NULL, '2020-06-10 11:30:16', '2020-06-10 11:30:16'),
(54, 109, NULL, '121', '121', '36.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-10 14:43:42', '2020-06-10 14:43:42'),
(55, 110, NULL, '112', '112', '1.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-10 16:35:30', '2020-06-10 16:35:30'),
(56, 109, NULL, '112', '121', '36.00', NULL, 'paid', 0, 1, 'AS_TXN_532089016', NULL, '2020-06-10 16:58:49', '2020-06-10 16:58:49'),
(57, 111, NULL, '121', '121', '1.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-10 17:14:40', '2020-06-10 17:14:40'),
(58, 113, NULL, '113', '113', '39.00', NULL, 'paid', 0, 0, 'SOME_RANDOM_STRING', NULL, '2020-06-11 02:50:10', '2020-06-11 02:50:10'),
(59, 113, NULL, '113', '110', '28.00', NULL, 'paid', 0, 0, 'T_ACTION_AS_EJKJAGJKLQ', NULL, '2020-06-11 02:53:25', '2020-06-11 02:53:25'),
(60, 113, NULL, '113', '110', '39', NULL, 'paid', 0, 0, 'AS_TXN_4376253837', NULL, '2020-06-11 02:59:20', '2020-06-11 02:59:20'),
(61, NULL, NULL, '61', '61', '500', NULL, 'paid', 0, 0, 'AS_TXN_5026971910', NULL, '2020-06-11 11:09:27', '2020-06-11 11:09:27'),
(62, 77, 24, '61', '101', '15', NULL, 'paid', 0, 0, 'AS-TXN-1200653.', NULL, '2020-06-15 06:34:59', '2020-06-15 06:34:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `access_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin.test@hachiweb.com', NULL, '$2y$10$vpEUkJwwJkdjUdNbLpx7o.EFPG32G2xk98NI3f30Rt7nm.e729afO', 'X1KdMOljufg8DQdJ3nFbklloDHBxjmMM6NOlh8c67IT4TPkpuRsBW6mBg2cK', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXVydW1zb2x2ZXIuY29tXC9nZXQtdG9rZW4iLCJpYXQiOjE1OTIzMTcwNTYsImV4cCI6MTU5MjMyMDY1NiwibmJmIjoxNTkyMzE3MDU2LCJqdGkiOiJOQnBlY1FyWmswMkI2MExkIiwic3ViIjpudWxsfQ.6v1hEdm6DTBvk3k2q7p1-rRPtBRnmL-68NsFydKVyOs', NULL, '2020-06-16 14:17:36'),
(2, 'rajendra', 'test@hachiweb.com', NULL, '$2y$10$vpEUkJwwJkdjUdNbLpx7o.EFPG32G2xk98NI3f30Rt7nm.e729afO', 'R5O9Omd4suUv3raxglNUBSWsRfFaogZo0IyShhXMBBrrv3WLmGSBBslCoWAe', NULL, '2020-05-22 00:07:19', '2020-05-22 00:07:19'),
(3, 'Ferris Barrett', 'tofon@mailinator.com', NULL, '$2y$10$vpEUkJwwJkdjUdNbLpx7o.EFPG32G2xk98NI3f30Rt7nm.e729afO', NULL, NULL, '2020-06-10 23:50:23', '2020-06-10 23:50:23'),
(4, 'Sybill Sexton', 'wilutaki@mailinator.net', NULL, '$2y$10$vpEUkJwwJkdjUdNbLpx7o.EFPG32G2xk98NI3f30Rt7nm.e729afO', NULL, NULL, '2020-06-10 23:51:17', '2020-06-10 23:51:17'),
(5, 'Ora Ingram', 'zuwazin@mailinator.net', NULL, '$2y$10$tueZZ.4ZfLfiqNCVHfkXEuQAop5gj.kwGKghRoESWjx8CvN30HUh6', NULL, NULL, '2020-06-11 10:35:39', '2020-06-11 10:35:39');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawals`
--

CREATE TABLE `withdrawals` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `amount` text NOT NULL,
  `description` text,
  `is_approved` varchar(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `withdrawals`
--

INSERT INTO `withdrawals` (`id`, `user_id`, `amount`, `description`, `is_approved`, `created_at`, `updated_at`) VALUES
(3, '61', '10', 'Insufficient balance!', '0', '2020-04-16 00:38:37', '2020-06-16 11:00:34'),
(4, '61', '10', 'Request rejected', '0', '2020-04-16 00:38:58', '2020-07-11 22:19:39'),
(5, '119', '10', 'Request rejected', '0', '2020-04-16 00:40:22', '2020-07-11 22:19:50');

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `zone_id` int(10) NOT NULL,
  `country_code` char(2) COLLATE utf8_bin NOT NULL,
  `zone_name` varchar(35) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`zone_id`, `country_code`, `zone_name`) VALUES
(1, 'AD', 'Europe/Andorra'),
(2, 'AE', 'Asia/Dubai'),
(3, 'AF', 'Asia/Kabul'),
(4, 'AG', 'America/Antigua'),
(5, 'AI', 'America/Anguilla'),
(6, 'AL', 'Europe/Tirane'),
(7, 'AM', 'Asia/Yerevan'),
(8, 'AO', 'Africa/Luanda'),
(9, 'AQ', 'Antarctica/McMurdo'),
(10, 'AQ', 'Antarctica/Casey'),
(11, 'AQ', 'Antarctica/Davis'),
(12, 'AQ', 'Antarctica/DumontDUrville'),
(13, 'AQ', 'Antarctica/Mawson'),
(14, 'AQ', 'Antarctica/Palmer'),
(15, 'AQ', 'Antarctica/Rothera'),
(16, 'AQ', 'Antarctica/Syowa'),
(17, 'AQ', 'Antarctica/Troll'),
(18, 'AQ', 'Antarctica/Vostok'),
(19, 'AR', 'America/Argentina/Buenos_Aires'),
(20, 'AR', 'America/Argentina/Cordoba'),
(21, 'AR', 'America/Argentina/Salta'),
(22, 'AR', 'America/Argentina/Jujuy'),
(23, 'AR', 'America/Argentina/Tucuman'),
(24, 'AR', 'America/Argentina/Catamarca'),
(25, 'AR', 'America/Argentina/La_Rioja'),
(26, 'AR', 'America/Argentina/San_Juan'),
(27, 'AR', 'America/Argentina/Mendoza'),
(28, 'AR', 'America/Argentina/San_Luis'),
(29, 'AR', 'America/Argentina/Rio_Gallegos'),
(30, 'AR', 'America/Argentina/Ushuaia'),
(31, 'AS', 'Pacific/Pago_Pago'),
(32, 'AT', 'Europe/Vienna'),
(33, 'AU', 'Australia/Lord_Howe'),
(34, 'AU', 'Antarctica/Macquarie'),
(35, 'AU', 'Australia/Hobart'),
(36, 'AU', 'Australia/Currie'),
(37, 'AU', 'Australia/Melbourne'),
(38, 'AU', 'Australia/Sydney'),
(39, 'AU', 'Australia/Broken_Hill'),
(40, 'AU', 'Australia/Brisbane'),
(41, 'AU', 'Australia/Lindeman'),
(42, 'AU', 'Australia/Adelaide'),
(43, 'AU', 'Australia/Darwin'),
(44, 'AU', 'Australia/Perth'),
(45, 'AU', 'Australia/Eucla'),
(46, 'AW', 'America/Aruba'),
(47, 'AX', 'Europe/Mariehamn'),
(48, 'AZ', 'Asia/Baku'),
(49, 'BA', 'Europe/Sarajevo'),
(50, 'BB', 'America/Barbados'),
(51, 'BD', 'Asia/Dhaka'),
(52, 'BE', 'Europe/Brussels'),
(53, 'BF', 'Africa/Ouagadougou'),
(54, 'BG', 'Europe/Sofia'),
(55, 'BH', 'Asia/Bahrain'),
(56, 'BI', 'Africa/Bujumbura'),
(57, 'BJ', 'Africa/Porto-Novo'),
(58, 'BL', 'America/St_Barthelemy'),
(59, 'BM', 'Atlantic/Bermuda'),
(60, 'BN', 'Asia/Brunei'),
(61, 'BO', 'America/La_Paz'),
(62, 'BQ', 'America/Kralendijk'),
(63, 'BR', 'America/Noronha'),
(64, 'BR', 'America/Belem'),
(65, 'BR', 'America/Fortaleza'),
(66, 'BR', 'America/Recife'),
(67, 'BR', 'America/Araguaina'),
(68, 'BR', 'America/Maceio'),
(69, 'BR', 'America/Bahia'),
(70, 'BR', 'America/Sao_Paulo'),
(71, 'BR', 'America/Campo_Grande'),
(72, 'BR', 'America/Cuiaba'),
(73, 'BR', 'America/Santarem'),
(74, 'BR', 'America/Porto_Velho'),
(75, 'BR', 'America/Boa_Vista'),
(76, 'BR', 'America/Manaus'),
(77, 'BR', 'America/Eirunepe'),
(78, 'BR', 'America/Rio_Branco'),
(79, 'BS', 'America/Nassau'),
(80, 'BT', 'Asia/Thimphu'),
(81, 'BW', 'Africa/Gaborone'),
(82, 'BY', 'Europe/Minsk'),
(83, 'BZ', 'America/Belize'),
(84, 'CA', 'America/St_Johns'),
(85, 'CA', 'America/Halifax'),
(86, 'CA', 'America/Glace_Bay'),
(87, 'CA', 'America/Moncton'),
(88, 'CA', 'America/Goose_Bay'),
(89, 'CA', 'America/Blanc-Sablon'),
(90, 'CA', 'America/Toronto'),
(91, 'CA', 'America/Nipigon'),
(92, 'CA', 'America/Thunder_Bay'),
(93, 'CA', 'America/Iqaluit'),
(94, 'CA', 'America/Pangnirtung'),
(95, 'CA', 'America/Atikokan'),
(96, 'CA', 'America/Winnipeg'),
(97, 'CA', 'America/Rainy_River'),
(98, 'CA', 'America/Resolute'),
(99, 'CA', 'America/Rankin_Inlet'),
(100, 'CA', 'America/Regina'),
(101, 'CA', 'America/Swift_Current'),
(102, 'CA', 'America/Edmonton'),
(103, 'CA', 'America/Cambridge_Bay'),
(104, 'CA', 'America/Yellowknife'),
(105, 'CA', 'America/Inuvik'),
(106, 'CA', 'America/Creston'),
(107, 'CA', 'America/Dawson_Creek'),
(108, 'CA', 'America/Fort_Nelson'),
(109, 'CA', 'America/Vancouver'),
(110, 'CA', 'America/Whitehorse'),
(111, 'CA', 'America/Dawson'),
(112, 'CC', 'Indian/Cocos'),
(113, 'CD', 'Africa/Kinshasa'),
(114, 'CD', 'Africa/Lubumbashi'),
(115, 'CF', 'Africa/Bangui'),
(116, 'CG', 'Africa/Brazzaville'),
(117, 'CH', 'Europe/Zurich'),
(118, 'CI', 'Africa/Abidjan'),
(119, 'CK', 'Pacific/Rarotonga'),
(120, 'CL', 'America/Santiago'),
(121, 'CL', 'America/Punta_Arenas'),
(122, 'CL', 'Pacific/Easter'),
(123, 'CM', 'Africa/Douala'),
(124, 'CN', 'Asia/Shanghai'),
(125, 'CN', 'Asia/Urumqi'),
(126, 'CO', 'America/Bogota'),
(127, 'CR', 'America/Costa_Rica'),
(128, 'CU', 'America/Havana'),
(129, 'CV', 'Atlantic/Cape_Verde'),
(130, 'CW', 'America/Curacao'),
(131, 'CX', 'Indian/Christmas'),
(132, 'CY', 'Asia/Nicosia'),
(133, 'CY', 'Asia/Famagusta'),
(134, 'CZ', 'Europe/Prague'),
(135, 'DE', 'Europe/Berlin'),
(136, 'DE', 'Europe/Busingen'),
(137, 'DJ', 'Africa/Djibouti'),
(138, 'DK', 'Europe/Copenhagen'),
(139, 'DM', 'America/Dominica'),
(140, 'DO', 'America/Santo_Domingo'),
(141, 'DZ', 'Africa/Algiers'),
(142, 'EC', 'America/Guayaquil'),
(143, 'EC', 'Pacific/Galapagos'),
(144, 'EE', 'Europe/Tallinn'),
(145, 'EG', 'Africa/Cairo'),
(146, 'EH', 'Africa/El_Aaiun'),
(147, 'ER', 'Africa/Asmara'),
(148, 'ES', 'Europe/Madrid'),
(149, 'ES', 'Africa/Ceuta'),
(150, 'ES', 'Atlantic/Canary'),
(151, 'ET', 'Africa/Addis_Ababa'),
(152, 'FI', 'Europe/Helsinki'),
(153, 'FJ', 'Pacific/Fiji'),
(154, 'FK', 'Atlantic/Stanley'),
(155, 'FM', 'Pacific/Chuuk'),
(156, 'FM', 'Pacific/Pohnpei'),
(157, 'FM', 'Pacific/Kosrae'),
(158, 'FO', 'Atlantic/Faroe'),
(159, 'FR', 'Europe/Paris'),
(160, 'GA', 'Africa/Libreville'),
(161, 'GB', 'Europe/London'),
(162, 'GD', 'America/Grenada'),
(163, 'GE', 'Asia/Tbilisi'),
(164, 'GF', 'America/Cayenne'),
(165, 'GG', 'Europe/Guernsey'),
(166, 'GH', 'Africa/Accra'),
(167, 'GI', 'Europe/Gibraltar'),
(168, 'GL', 'America/Godthab'),
(169, 'GL', 'America/Danmarkshavn'),
(170, 'GL', 'America/Scoresbysund'),
(171, 'GL', 'America/Thule'),
(172, 'GM', 'Africa/Banjul'),
(173, 'GN', 'Africa/Conakry'),
(174, 'GP', 'America/Guadeloupe'),
(175, 'GQ', 'Africa/Malabo'),
(176, 'GR', 'Europe/Athens'),
(177, 'GS', 'Atlantic/South_Georgia'),
(178, 'GT', 'America/Guatemala'),
(179, 'GU', 'Pacific/Guam'),
(180, 'GW', 'Africa/Bissau'),
(181, 'GY', 'America/Guyana'),
(182, 'HK', 'Asia/Hong_Kong'),
(183, 'HN', 'America/Tegucigalpa'),
(184, 'HR', 'Europe/Zagreb'),
(185, 'HT', 'America/Port-au-Prince'),
(186, 'HU', 'Europe/Budapest'),
(187, 'ID', 'Asia/Jakarta'),
(188, 'ID', 'Asia/Pontianak'),
(189, 'ID', 'Asia/Makassar'),
(190, 'ID', 'Asia/Jayapura'),
(191, 'IE', 'Europe/Dublin'),
(192, 'IL', 'Asia/Jerusalem'),
(193, 'IM', 'Europe/Isle_of_Man'),
(194, 'IN', 'Asia/Kolkata'),
(195, 'IO', 'Indian/Chagos'),
(196, 'IQ', 'Asia/Baghdad'),
(197, 'IR', 'Asia/Tehran'),
(198, 'IS', 'Atlantic/Reykjavik'),
(199, 'IT', 'Europe/Rome'),
(200, 'JE', 'Europe/Jersey'),
(201, 'JM', 'America/Jamaica'),
(202, 'JO', 'Asia/Amman'),
(203, 'JP', 'Asia/Tokyo'),
(204, 'KE', 'Africa/Nairobi'),
(205, 'KG', 'Asia/Bishkek'),
(206, 'KH', 'Asia/Phnom_Penh'),
(207, 'KI', 'Pacific/Tarawa'),
(208, 'KI', 'Pacific/Enderbury'),
(209, 'KI', 'Pacific/Kiritimati'),
(210, 'KM', 'Indian/Comoro'),
(211, 'KN', 'America/St_Kitts'),
(212, 'KP', 'Asia/Pyongyang'),
(213, 'KR', 'Asia/Seoul'),
(214, 'KW', 'Asia/Kuwait'),
(215, 'KY', 'America/Cayman'),
(216, 'KZ', 'Asia/Almaty'),
(217, 'KZ', 'Asia/Qyzylorda'),
(218, 'KZ', 'Asia/Aqtobe'),
(219, 'KZ', 'Asia/Aqtau'),
(220, 'KZ', 'Asia/Atyrau'),
(221, 'KZ', 'Asia/Oral'),
(222, 'LA', 'Asia/Vientiane'),
(223, 'LB', 'Asia/Beirut'),
(224, 'LC', 'America/St_Lucia'),
(225, 'LI', 'Europe/Vaduz'),
(226, 'LK', 'Asia/Colombo'),
(227, 'LR', 'Africa/Monrovia'),
(228, 'LS', 'Africa/Maseru'),
(229, 'LT', 'Europe/Vilnius'),
(230, 'LU', 'Europe/Luxembourg'),
(231, 'LV', 'Europe/Riga'),
(232, 'LY', 'Africa/Tripoli'),
(233, 'MA', 'Africa/Casablanca'),
(234, 'MC', 'Europe/Monaco'),
(235, 'MD', 'Europe/Chisinau'),
(236, 'ME', 'Europe/Podgorica'),
(237, 'MF', 'America/Marigot'),
(238, 'MG', 'Indian/Antananarivo'),
(239, 'MH', 'Pacific/Majuro'),
(240, 'MH', 'Pacific/Kwajalein'),
(241, 'MK', 'Europe/Skopje'),
(242, 'ML', 'Africa/Bamako'),
(243, 'MM', 'Asia/Yangon'),
(244, 'MN', 'Asia/Ulaanbaatar'),
(245, 'MN', 'Asia/Hovd'),
(246, 'MN', 'Asia/Choibalsan'),
(247, 'MO', 'Asia/Macau'),
(248, 'MP', 'Pacific/Saipan'),
(249, 'MQ', 'America/Martinique'),
(250, 'MR', 'Africa/Nouakchott'),
(251, 'MS', 'America/Montserrat'),
(252, 'MT', 'Europe/Malta'),
(253, 'MU', 'Indian/Mauritius'),
(254, 'MV', 'Indian/Maldives'),
(255, 'MW', 'Africa/Blantyre'),
(256, 'MX', 'America/Mexico_City'),
(257, 'MX', 'America/Cancun'),
(258, 'MX', 'America/Merida'),
(259, 'MX', 'America/Monterrey'),
(260, 'MX', 'America/Matamoros'),
(261, 'MX', 'America/Mazatlan'),
(262, 'MX', 'America/Chihuahua'),
(263, 'MX', 'America/Ojinaga'),
(264, 'MX', 'America/Hermosillo'),
(265, 'MX', 'America/Tijuana'),
(266, 'MX', 'America/Bahia_Banderas'),
(267, 'MY', 'Asia/Kuala_Lumpur'),
(268, 'MY', 'Asia/Kuching'),
(269, 'MZ', 'Africa/Maputo'),
(270, 'NA', 'Africa/Windhoek'),
(271, 'NC', 'Pacific/Noumea'),
(272, 'NE', 'Africa/Niamey'),
(273, 'NF', 'Pacific/Norfolk'),
(274, 'NG', 'Africa/Lagos'),
(275, 'NI', 'America/Managua'),
(276, 'NL', 'Europe/Amsterdam'),
(277, 'NO', 'Europe/Oslo'),
(278, 'NP', 'Asia/Kathmandu'),
(279, 'NR', 'Pacific/Nauru'),
(280, 'NU', 'Pacific/Niue'),
(281, 'NZ', 'Pacific/Auckland'),
(282, 'NZ', 'Pacific/Chatham'),
(283, 'OM', 'Asia/Muscat'),
(284, 'PA', 'America/Panama'),
(285, 'PE', 'America/Lima'),
(286, 'PF', 'Pacific/Tahiti'),
(287, 'PF', 'Pacific/Marquesas'),
(288, 'PF', 'Pacific/Gambier'),
(289, 'PG', 'Pacific/Port_Moresby'),
(290, 'PG', 'Pacific/Bougainville'),
(291, 'PH', 'Asia/Manila'),
(292, 'PK', 'Asia/Karachi'),
(293, 'PL', 'Europe/Warsaw'),
(294, 'PM', 'America/Miquelon'),
(295, 'PN', 'Pacific/Pitcairn'),
(296, 'PR', 'America/Puerto_Rico'),
(297, 'PS', 'Asia/Gaza'),
(298, 'PS', 'Asia/Hebron'),
(299, 'PT', 'Europe/Lisbon'),
(300, 'PT', 'Atlantic/Madeira'),
(301, 'PT', 'Atlantic/Azores'),
(302, 'PW', 'Pacific/Palau'),
(303, 'PY', 'America/Asuncion'),
(304, 'QA', 'Asia/Qatar'),
(305, 'RE', 'Indian/Reunion'),
(306, 'RO', 'Europe/Bucharest'),
(307, 'RS', 'Europe/Belgrade'),
(308, 'RU', 'Europe/Kaliningrad'),
(309, 'RU', 'Europe/Moscow'),
(310, 'RU', 'Europe/Simferopol'),
(311, 'RU', 'Europe/Volgograd'),
(312, 'RU', 'Europe/Kirov'),
(313, 'RU', 'Europe/Astrakhan'),
(314, 'RU', 'Europe/Saratov'),
(315, 'RU', 'Europe/Ulyanovsk'),
(316, 'RU', 'Europe/Samara'),
(317, 'RU', 'Asia/Yekaterinburg'),
(318, 'RU', 'Asia/Omsk'),
(319, 'RU', 'Asia/Novosibirsk'),
(320, 'RU', 'Asia/Barnaul'),
(321, 'RU', 'Asia/Tomsk'),
(322, 'RU', 'Asia/Novokuznetsk'),
(323, 'RU', 'Asia/Krasnoyarsk'),
(324, 'RU', 'Asia/Irkutsk'),
(325, 'RU', 'Asia/Chita'),
(326, 'RU', 'Asia/Yakutsk'),
(327, 'RU', 'Asia/Khandyga'),
(328, 'RU', 'Asia/Vladivostok'),
(329, 'RU', 'Asia/Ust-Nera'),
(330, 'RU', 'Asia/Magadan'),
(331, 'RU', 'Asia/Sakhalin'),
(332, 'RU', 'Asia/Srednekolymsk'),
(333, 'RU', 'Asia/Kamchatka'),
(334, 'RU', 'Asia/Anadyr'),
(335, 'RW', 'Africa/Kigali'),
(336, 'SA', 'Asia/Riyadh'),
(337, 'SB', 'Pacific/Guadalcanal'),
(338, 'SC', 'Indian/Mahe'),
(339, 'SD', 'Africa/Khartoum'),
(340, 'SE', 'Europe/Stockholm'),
(341, 'SG', 'Asia/Singapore'),
(342, 'SH', 'Atlantic/St_Helena'),
(343, 'SI', 'Europe/Ljubljana'),
(344, 'SJ', 'Arctic/Longyearbyen'),
(345, 'SK', 'Europe/Bratislava'),
(346, 'SL', 'Africa/Freetown'),
(347, 'SM', 'Europe/San_Marino'),
(348, 'SN', 'Africa/Dakar'),
(349, 'SO', 'Africa/Mogadishu'),
(350, 'SR', 'America/Paramaribo'),
(351, 'SS', 'Africa/Juba'),
(352, 'ST', 'Africa/Sao_Tome'),
(353, 'SV', 'America/El_Salvador'),
(354, 'SX', 'America/Lower_Princes'),
(355, 'SY', 'Asia/Damascus'),
(356, 'SZ', 'Africa/Mbabane'),
(357, 'TC', 'America/Grand_Turk'),
(358, 'TD', 'Africa/Ndjamena'),
(359, 'TF', 'Indian/Kerguelen'),
(360, 'TG', 'Africa/Lome'),
(361, 'TH', 'Asia/Bangkok'),
(362, 'TJ', 'Asia/Dushanbe'),
(363, 'TK', 'Pacific/Fakaofo'),
(364, 'TL', 'Asia/Dili'),
(365, 'TM', 'Asia/Ashgabat'),
(366, 'TN', 'Africa/Tunis'),
(367, 'TO', 'Pacific/Tongatapu'),
(368, 'TR', 'Europe/Istanbul'),
(369, 'TT', 'America/Port_of_Spain'),
(370, 'TV', 'Pacific/Funafuti'),
(371, 'TW', 'Asia/Taipei'),
(372, 'TZ', 'Africa/Dar_es_Salaam'),
(373, 'UA', 'Europe/Kiev'),
(374, 'UA', 'Europe/Uzhgorod'),
(375, 'UA', 'Europe/Zaporozhye'),
(376, 'UG', 'Africa/Kampala'),
(377, 'UM', 'Pacific/Midway'),
(378, 'UM', 'Pacific/Wake'),
(379, 'US', 'America/New_York'),
(380, 'US', 'America/Detroit'),
(381, 'US', 'America/Kentucky/Louisville'),
(382, 'US', 'America/Kentucky/Monticello'),
(383, 'US', 'America/Indiana/Indianapolis'),
(384, 'US', 'America/Indiana/Vincennes'),
(385, 'US', 'America/Indiana/Winamac'),
(386, 'US', 'America/Indiana/Marengo'),
(387, 'US', 'America/Indiana/Petersburg'),
(388, 'US', 'America/Indiana/Vevay'),
(389, 'US', 'America/Chicago'),
(390, 'US', 'America/Indiana/Tell_City'),
(391, 'US', 'America/Indiana/Knox'),
(392, 'US', 'America/Menominee'),
(393, 'US', 'America/North_Dakota/Center'),
(394, 'US', 'America/North_Dakota/New_Salem'),
(395, 'US', 'America/North_Dakota/Beulah'),
(396, 'US', 'America/Denver'),
(397, 'US', 'America/Boise'),
(398, 'US', 'America/Phoenix'),
(399, 'US', 'America/Los_Angeles'),
(400, 'US', 'America/Anchorage'),
(401, 'US', 'America/Juneau'),
(402, 'US', 'America/Sitka'),
(403, 'US', 'America/Metlakatla'),
(404, 'US', 'America/Yakutat'),
(405, 'US', 'America/Nome'),
(406, 'US', 'America/Adak'),
(407, 'US', 'Pacific/Honolulu'),
(408, 'UY', 'America/Montevideo'),
(409, 'UZ', 'Asia/Samarkand'),
(410, 'UZ', 'Asia/Tashkent'),
(411, 'VA', 'Europe/Vatican'),
(412, 'VC', 'America/St_Vincent'),
(413, 'VE', 'America/Caracas'),
(414, 'VG', 'America/Tortola'),
(415, 'VI', 'America/St_Thomas'),
(416, 'VN', 'Asia/Ho_Chi_Minh'),
(417, 'VU', 'Pacific/Efate'),
(418, 'WF', 'Pacific/Wallis'),
(419, 'WS', 'Pacific/Apia'),
(420, 'YE', 'Asia/Aden'),
(421, 'YT', 'Indian/Mayotte'),
(422, 'ZA', 'Africa/Johannesburg'),
(423, 'ZM', 'Africa/Lusaka'),
(424, 'ZW', 'Africa/Harare');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apps_user`
--
ALTER TABLE `apps_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_support`
--
ALTER TABLE `faq_support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prepayments`
--
ALTER TABLE `prepayments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdrawals`
--
ALTER TABLE `withdrawals`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `faq_support`
--
ALTER TABLE `faq_support`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `prepayments`
--
ALTER TABLE `prepayments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `withdrawals`
--
ALTER TABLE `withdrawals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
