-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 21, 2021 at 02:56 PM
-- Server version: 10.4.14-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u750534813_aurum`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `tittle` text NOT NULL,
  `about_us` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `tittle`, `about_us`, `created_at`, `updated_at`) VALUES
(6, 'Aurum Solver: Tutoring made simple/accessible/ affordable. All mobile…', '<div>&nbsp;Ask any question, there are people to answer.<br>\r\nThis app has people all over the world who are ready to help with your homework/assignments.<br>\r\nTutoring or homework help shouldn\'t cost an arm and a leg.You can sign up as a Tutor or User or Both. (We\'re that flexible!)</div><div><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures;\"><b>Amazing Features!!!</b></span><br></div>', '2020-07-10 07:15:40', '2020-07-10 07:15:40'),
(7, 'For Users:', '<div><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\">You can ask questions or even better take a picture of the question and upload.</span><br style=\"box-sizing: inherit; color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\"><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\">You bargain and determine what you\'ll pay for your question to be answered.</span><br style=\"box-sizing: inherit; color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\"><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\">You\'ll receive step by step answers that will help you solve other subsequent questions yourself.</span><br style=\"box-sizing: inherit; color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\"><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\">There\'s on demand video tutoring available for the best rates out there.</span><br></div>', '2020-07-10 07:17:04', '2020-07-10 07:17:04'),
(8, 'For Tutors:', '<div><br style=\"box-sizing: inherit; color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\"><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\">You get to determine your price when solving a question.</span><br style=\"box-sizing: inherit; color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\"><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\">You get to set your rate for on demand tutoring.</span><br style=\"box-sizing: inherit; color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\"><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\">There isn\'t a limit to your earning.</span><br style=\"box-sizing: inherit; color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\"><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\">You get to help people, learn, and earn.</span><span class=\"c-mrkdwn__br\" data-stringify-type=\"paragraph-break\" style=\"box-sizing: inherit; display: block; height: 8px; color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\"></span><span class=\"c-mrkdwn__br\" data-stringify-type=\"paragraph-break\" style=\"box-sizing: inherit; display: block; height: 8px; color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\"></span><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-variant-ligatures: common-ligatures; background-color: rgb(248, 248, 248);\">You can also choose to assign your questions directly to a specific tutor based on their ratings or your personal preference.</span><br></div>', '2020-07-10 07:17:37', '2020-07-10 07:17:37');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `questioner_id` int(11) NOT NULL,
  `answerer_id` varchar(255) DEFAULT NULL,
  `questioner_fee` varchar(25) DEFAULT NULL,
  `answerer_fee` varchar(25) DEFAULT NULL,
  `admin_fee` varchar(25) DEFAULT NULL,
  `category` text DEFAULT NULL,
  `question` text DEFAULT NULL,
  `title` text DEFAULT NULL,
  `ans_attach` text DEFAULT NULL,
  `answer` text NOT NULL,
  `desired_price` varchar(200) DEFAULT NULL,
  `post` date DEFAULT NULL,
  `purchase` tinyint(1) NOT NULL DEFAULT 0,
  `duedate` date DEFAULT NULL,
  `delivery_accepted` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `questioner_id`, `answerer_id`, `questioner_fee`, `answerer_fee`, `admin_fee`, `category`, `question`, `title`, `ans_attach`, `answer`, `desired_price`, `post`, `purchase`, `duedate`, `delivery_accepted`, `created_at`, `updated_at`) VALUES
(1, 8, 40, '36', NULL, NULL, NULL, 'Accounting', '[{\"insert\":\"Description: from g\\n\"}]', 'ofglg by', '', '[{\"insert\":\"Descriptiondigkg:\\n\"}]', '10', '2020-12-15', 0, '2020-12-15', NULL, '2020-12-15 11:34:21', '2020-12-15 11:34:21'),
(2, 9, 41, '40', NULL, NULL, NULL, 'Accounting', '[{\"insert\":\"Description:chkdigo\\n\"}]', 'vhii', '', '[{\"insert\":\"Description:gjjn\\n\"}]', '80', '2020-12-18', 0, '2020-12-18', NULL, '2020-12-18 04:52:50', '2020-12-18 04:52:50');

-- --------------------------------------------------------

--
-- Table structure for table `apps_user`
--

CREATE TABLE `apps_user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `whoami` text DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `user_email` varchar(100) NOT NULL,
  `password` varchar(200) DEFAULT NULL,
  `profile_pic` text DEFAULT NULL,
  `role` text DEFAULT NULL,
  `user_type` text DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `timezone` text DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subscribe_category` text DEFAULT NULL,
  `subscribe_notification_interval` text DEFAULT NULL,
  `paypalaccount` text DEFAULT NULL,
  `stripe_account` varchar(50) DEFAULT NULL,
  `default_payment` enum('PAYPAL','CARD') DEFAULT 'PAYPAL',
  `hourly_rate` float NOT NULL DEFAULT 20,
  `registration_type` varchar(100) DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `registration_token` text DEFAULT NULL,
  `verification_token` text DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `apps_user`
--

INSERT INTO `apps_user` (`id`, `first_name`, `last_name`, `whoami`, `dob`, `user_email`, `password`, `profile_pic`, `role`, `user_type`, `phone`, `timezone`, `category_id`, `subscribe_category`, `subscribe_notification_interval`, `paypalaccount`, `stripe_account`, `default_payment`, `hourly_rate`, `registration_type`, `is_verified`, `is_active`, `registration_token`, `verification_token`, `last_login`, `updated_at`, `created_at`) VALUES
(3, 'Mohan Sai Manthri', '-', 'Developer', '1997-07-09', 'mohansai.developer@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-3/_2020-06-20T13:24:47.560670', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Education', '1 Week Delay', 'sb-xpnx72279648@business.example.com', NULL, 'PAYPAL', 35, 'google.com', 1, 1, 'SrkWabQBcBaAVmG2xHcU4i8PpY12', NULL, '2020-07-22 12:19:20', '2020-07-22 12:19:20', '2020-06-20 07:54:12'),
(4, 'Hari© houdhary -', '-', 'Gamer', '1975-01-17', 'p.harichandra2407@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-4/Hari© houdhary -_2020-06-20T14:12:55.020145', NULL, 'Both', NULL, '(UTC+02:00) E. Europe', NULL, 'Education', 'Instant', NULL, NULL, 'PAYPAL', 20, 'twitter.com', 1, 1, 'ByIHdzwWrVMkfH3OXtEWoXJ3d0B2', NULL, '2020-06-30 03:22:01', '2020-06-30 03:22:01', '2020-06-20 08:41:21'),
(5, 'Alpha Tester', '-', 'I am alpha tester', '1995-01-04', 'test@hachiweb.com', '$2y$10$kbUBTRKRvYKG0DqkfL44o.w1Dpa5HOmlxNXjNPnUO29GKYegxruVa', 'https://aurumsolver.com/storage/user-profile-pic/id-5/_2021-02-20T20:37:50.930192', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Computer Science', '24 Hour Delay', 'sb-kvhyv1939756@personal.example.com', NULL, 'PAYPAL', 20, 'email', 1, 1, 'yq8YXxsFFTYSYVVPleLx9g9aMfx2', 'bP8MSmZfcYgFiWutAuXwkPFW59EefnuLJNcJL4un1TzTSgIjqlRpF0gG3biqrGTG0QD4wK', '2021-02-20 16:37:54', '2021-02-20 16:37:54', '2020-06-20 14:40:53'),
(6, 'Testing D', '-', 'Tester', '1998-01-15', 'testingdigimantralabs12@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-6/ Testing D_2020-06-23T04:50:16.821501', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Education', 'Select', 'sb-xpnx72279648@business.example.com', NULL, 'PAYPAL', 20, 'google.com', 1, 1, '6FW6nX8FYKWSxurhEtVyPV8qnD92', NULL, '2020-07-11 15:35:50', '2020-07-11 15:35:50', '2020-06-22 23:18:00'),
(7, 'Kwadwo Gyampo', '-', 'Software Engineer', '1991-12-02', 'dimitrinvestment@yahoo.com', '$2y$10$SZpEpwtvZ8bwgF61DI9L/uXrEoyZqkpJsOKsRSzy3iGd.yfNHxOny', 'https://aurumsolver.com/storage/user-profile-pic/id-7/_2020-06-28T10:26:18.973288', NULL, 'Tutor', NULL, '(UTC-05:00) Eastern Time (US & Canada)', NULL, 'Mathematics', '3 Hour Delay', 'eagleunique@live.com', NULL, 'PAYPAL', 30, 'email', 1, 1, 'hAyMrNWmXLbH39HM7KZmSRqyKOv1', 'lChGotoUWAJ5GPy4ezhQXswfMEjjbxOvQXoYO43NfCBfC7mHNJenLMJdyIEMk1T0jhiLBG', '2020-07-12 10:07:06', '2020-07-12 10:07:06', '2020-06-28 06:17:16'),
(8, 'Tivati', '-', 'test bio 3', '1978-01-28', 'tivati2361@frost2d.net', '$2y$10$Qk2RVG7kk2jT2DwNeI9A.ez33dHoz9OUE7Rb1IJCkiSbvUwS5vDtq', 'https://aurumsolver.com/storage/user-profile-pic/id-8/Tivati_2020-07-01T01:09:24.707843', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Engineering', '1 Week Delay', NULL, NULL, 'PAYPAL', 25, 'email', 1, 1, 'tofO2Qij9QSU2mqBXccWwqWmGk72', '83VW1N3jrtyResssHPna973PcDJ1DzsKErktzlJ5SaFjDCIvx2ZWAH2tPxkqilE8fTvhGC', '2020-07-07 18:16:21', '2020-07-07 18:16:21', '2020-06-30 19:37:41'),
(9, NULL, NULL, NULL, NULL, 'davediverson1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'QgYgDp1qpoafWSgk4y7VHyE7ccJ2', NULL, '2020-07-02 20:47:30', '2020-07-02 20:47:30', '2020-07-02 20:47:30'),
(10, 'Bridget', '-', 'Aspiring Data analyst, ready to learn/teach', '1995-09-17', 'Bridgetkyei@gmail.com', '$2y$10$Q/1sRtJrkav5Myc3TsLJpOlU.yKpx.T2ofV1xf9nG1lxQEz95vgg.', 'https://aurumsolver.com/storage/user-profile-pic/id-10/_2020-07-03T01:03:07.025560', NULL, 'Both', NULL, '(UTC-05:00) Eastern Time (US & Canada)', NULL, 'Geography', 'Select', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'uwtP7a1KKxOB4BkskVcsIbK2SCH3', 'NdAraVEjkgKtFULwFpWneImKl6zU7ixJ4URUD79diL7zrb3MSODa0X7G0AwjqQIZ2IKEPg', '2020-10-21 22:14:47', '2020-10-21 22:14:47', '2020-07-03 05:02:38'),
(11, 'Desmond Berchie', '-', 'Be yourself', '1989-08-06', 'desmondberchie@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-11/_2020-07-03T14:34:34.910654', NULL, 'Both', NULL, '(UTC-05:00) Eastern Time (US & Canada)', NULL, 'Religious Studies', '1 Hour Delay', NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'rpFvovfK4xMXd7jroLi6z7g1Tf42', NULL, '2020-07-04 13:11:11', '2020-07-04 13:11:11', '2020-07-03 17:37:39'),
(12, NULL, NULL, NULL, NULL, 'eagleunique17@gmail.com', '$2y$10$vE4WfG9tNWrjoeGXRfEelu4pRpQSRiPllDLEBkTwHUz6xkgqXiNsS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, '2XNnzCaACycStFGk6AXpMyq65cV2', 'i5JgURwyqBV6ZhDR8MKhtJFzDc9Eobh2OMbKZygB7d49WoHIky52tBV4UDUCp1tS6QYhyz', '2020-07-09 09:08:47', '2020-07-09 09:08:47', '2020-07-09 09:08:46'),
(13, 'Tester', '-', 'I\'m a Cyber Security Engineer', '1970-01-01', 'eagleunique@live.com', '$2y$10$o57ncDYKlrSMuDWo8ISK6ejsQGlwhv06GUzvAzEtbPBWVUXO9zhzm', NULL, NULL, 'Select', NULL, '(UTC-07:00) Pacific Time (US & Canada)', NULL, 'Select', 'Select', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'yhuVBFNU1VWTxSQ4L1I5tJgzCU53', 'ljtO7jcfMnfZMz5XMd9h4c4CQOrcHuSIY78xYsDuGZfSbJHujlTRejjCmbBZsNYhQ3t0Ek', '2020-08-19 09:30:32', '2020-08-19 09:30:32', '2020-07-09 09:10:34'),
(14, '##', '-', '@@', '1998-01-15', 'rajendraakmr@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-14/_2020-07-10T12:54:59.155081', NULL, 'User', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Computer Science', '12 Hour Delay', NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'USmRU7lyL8bnKduZA9X4GMBUFSb2', NULL, '2020-07-10 07:26:25', '2020-07-10 07:26:25', '2020-07-09 11:52:18'),
(15, 'Test Jul 11', '-', 'App Developer', '1997-07-09', 'yokofos788@ageokfc.com', '$2y$10$2fq0Z31udcvYgszTW8cAF.VHPjYc0gfj25NXUQuNCTx7aN0aM9wJa', 'https://aurumsolver.com/storage/user-profile-pic/id-15/_2020-08-23T12:08:40.900429', NULL, 'Select', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Education', '3 Hour Delay', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, '1nOugmUrM2bY75375GTNusxfQ1h2', 'arAWTUKEImBHySGLxI7k19Jd7acMJnhVEBRB0g6cf4d3vkK6cRYeAgiHacrjkGPp4z0mus', '2020-08-23 06:38:45', '2020-08-23 06:38:45', '2020-07-11 09:04:45'),
(19, 'Test', '-', 'Developer', '1981-01-30', 'mohansaimanthri03@gmail.com', NULL, NULL, NULL, 'User', NULL, '(UTC-11:00) Coordinated Universal Time-11', NULL, 'Accounting', 'Select', NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'maV4vJmhu1YI6JrYpTiAjbZKNwg1', NULL, '2020-12-15 11:57:01', '2020-12-15 11:57:01', '2020-07-13 17:25:58'),
(20, 'Samuel Daniels', '-', 'History', '1970-01-01', 'ddk33xykiw@privaterelay.appleid.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-20/_2020-09-09T03:25:34.490589', NULL, 'Both', NULL, '(UTC-05:00) Eastern Time (US & Canada)', NULL, 'History', 'Instant', 'eagleunique@live.com', NULL, 'PAYPAL', 25, 'apple.com', 1, 1, 'qq1joJ7S51XQqnR5jDXQBPa04qw1', NULL, '2020-09-09 07:40:51', '2020-09-09 07:40:51', '2020-07-15 06:08:37'),
(21, 'Rhudii Quaye', '-', 'MSE in Robotics. Strong math background.', '1990-01-10', 'rhudidish44@hotmail.com', '$2y$10$WkhgZJNFHIeLtGllv2AjYunznuEOooKzcVSMc17m8BSWRNKuNR33u', NULL, NULL, 'Both', NULL, '(UTC-05:00) Eastern Time (US & Canada)', NULL, 'Mathematics', '1 Hour Delay', NULL, NULL, 'PAYPAL', 35, 'email', 1, 1, 'GdsE2iTawUZWcAQPKuAegs37alf1', '9leo74uOO2E3aNEJI2dWl5HiSDqFMlnhvrKmaTcTP33WNQTD4btiaujI8b9btTdTbcmeBC', '2020-07-15 18:59:24', '2020-07-15 18:59:24', '2020-07-15 18:55:26'),
(22, 'hi', '-', 'hihhhj', '2002-06-20', 'kanzah.jabeen2002@gmail.com', NULL, NULL, NULL, 'User', NULL, '(UTC-08:00) Pacific Time (US & Canada)', NULL, 'Mathematics', 'Instant', NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'ozz6kw5fI0NZbl6qgmJI4p62cMD2', NULL, '2020-07-16 18:42:22', '2020-07-16 18:42:22', '2020-07-16 18:40:19'),
(23, NULL, '-', NULL, NULL, '8x57mbxi95@privaterelay.appleid.com', NULL, NULL, NULL, 'Both', NULL, 'Select', NULL, 'Select', 'Select', NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'IYNsFEvpquepDxmml8uWzvCJSGh1', NULL, '2020-07-17 18:06:56', '2020-07-17 18:06:56', '2020-07-17 18:01:14'),
(24, 'almighty', '-', 'yoo', '1998-05-20', 'smithoctavious3@icloud.com', NULL, NULL, NULL, 'Select', NULL, '(UTC-05:00) Eastern Time (US & Canada)', NULL, 'Select', 'Select', NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'fFvvNqm8zRchEta4SVUyhVDIf4e2', NULL, '2020-07-18 00:55:38', '2020-07-18 00:55:38', '2020-07-18 00:53:14'),
(25, NULL, '-', NULL, NULL, 'princessprettycakes12@gmail.com', NULL, NULL, NULL, 'Select', NULL, 'Select', NULL, 'Select', 'Select', NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'IFwFIyeLfYTGTsR0sZM3Pbiveyz2', NULL, '2020-07-24 22:10:25', '2020-07-24 22:10:25', '2020-07-24 22:09:01'),
(26, NULL, '-', NULL, NULL, 'fendypierre@gmail.com', NULL, NULL, NULL, 'User', NULL, '(UTC-11:00) Coordinated Universal Time-11', NULL, 'Architecture', '1 Hour Delay', NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'sVxV3llD2qWuR9WmFCr6lHr6h682', NULL, '2020-07-25 04:20:43', '2020-07-25 04:20:43', '2020-07-25 04:19:14'),
(27, NULL, NULL, NULL, NULL, 'mbisht727@gmail.com', '$2y$10$dE5BXu1PHdQVR2FMEZ1pRu6B28S7EDNzPinpNLfmCffdLpzYMr2wK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'U1vZwRit9HOVnsZ047UIZdR88Ir1', 'AQ9zN6Foed8U0dY4CHEprD9ko7SFuq0ccEGhK6tWWMntw6JQe12KXABXrjv92RALcU0Cuv', '2020-07-25 05:13:06', '2020-07-25 05:13:06', '2020-07-25 05:13:01'),
(28, 'izhar', '-', 'This is izhar, Software Engineer and Freelancer', '1996-01-27', 'izharkhokhar00@gmail.com', '$2y$10$YLg.KIabhvWYzpg1Sqm7veafI2S0v9vkWgWpvkaggvzaDLYbc5cg.', NULL, NULL, 'Both', NULL, 'Select', NULL, 'Select', 'Select', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'T6tmgin1KQQRkOa65TUjEtKN1tE3', 'M8nTCymm5ECiiJjsDWA2QLw9FOVti0mDefgRp4cpcOvvOu3rm7zs7LvHBdwra6HhUYUPpM', '2020-08-11 11:20:37', '2020-08-11 11:20:37', '2020-08-11 11:08:37'),
(29, 'nm', '-', 'n', '1991-05-04', 'poochandan65@gmail.com', NULL, NULL, NULL, 'User', NULL, 'Select', NULL, 'Engineering', 'Instant', NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'mi8SlhVbyWZUF6AUcncGerpShtr1', NULL, '2020-08-18 14:05:27', '2020-08-18 14:05:27', '2020-08-18 14:00:42'),
(30, 'Jessica El-Amin', '-', 'I need help with my homework', '1995-09-14', 'jelawash95@gmail.com', '$2y$10$NqycuyLaGhqBenGVdex6A.tlP9OgVXWd1RJ4YJW84wBzmp9affN3q', NULL, NULL, 'User', NULL, '(UTC-05:00) Eastern Time (US & Canada)', NULL, 'English', 'Instant', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'avknN0kfVnevmknHjZLlfJZjSGa2', '2UpCAus6iG5wd0XomA6NFDAIYjz5CrakaucP6SHCF8MEfc1XH7fYqFly5DrxCtb8xbtKvk', '2020-09-02 08:40:36', '2020-09-02 08:40:36', '2020-09-02 08:37:46'),
(31, NULL, NULL, NULL, NULL, 'jeremiahaddo25@gmail.com', '$2y$10$H6H/FEPq4B7Z3Xz6q0uMOuxMm0/jPBecbQi6rAUUPEjaq.u/AEewm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'tEShzB4CRsd1T0W4IJ1qH0QabTD2', 'ejx22ieGnzyxWW5f2qvXNAOTqqop3JiLCxp8o9UjqfKADhfY16nPDtCkZRTa9q9nUhGoIv', '2020-09-20 18:11:07', '2020-09-20 18:11:07', '2020-09-20 18:11:05'),
(32, 'Nishant raj', '-', 'students', '1999-05-08', 'nishantraj96617@gmail.com', '$2y$10$NSM/P2iwuTblyB799Ri.3.ApxeqLfkqz51XN1mrE0N3gefxgP7tmq', NULL, NULL, 'User', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Computer Science', '1 Hour Delay', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'P7nrneXfyfR6e0UnznWrzXh9Wbf2', 'wdjIbMV5962ra6nHdN7WBXwllM5nLwMfpGbTOFiGJAYhl95Mmm9Zi5UVjVWObQwM5q4xE7', '2020-10-04 02:30:59', '2020-10-04 02:30:59', '2020-09-23 06:34:13'),
(33, 'Daniella', '-', 'hi', '1995-01-02', 'danielaramon480@gmail.com', NULL, NULL, NULL, 'Both', NULL, '(UTC-07:00) Pacific Time (US & Canada)', NULL, 'Education', 'Instant', 'danielaramon480@gmail.com', NULL, 'PAYPAL', 40, 'google.com', 1, 1, 'OMyoH32zBZQtXE8v3MJdaPThSL22', NULL, '2020-10-07 02:25:20', '2020-10-07 02:25:20', '2020-10-07 02:20:48'),
(34, 'Testee', '-', 'Testing', '1974-01-18', 'nayeh41391@abbuzz.com', '$2y$10$xumIb0w.yMFxoakjulug9Od268xxB/phn.3kR9AjuYU0B.ZGssymS', NULL, NULL, 'Both', NULL, '(UTC-10:00) Hawaii', NULL, 'Architecture', '1 Hour Delay', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, '0URe0EWWLVYLqqTpx6ZBYi5nzQ83', '8mACRncKIddaGB0C53P5e2rTtbe3D2hdO7bqeuZwPSC1Wc7IbJyVphLm26th5OyHD0QmTe', '2020-10-18 11:31:54', '2020-10-18 11:31:54', '2020-10-18 11:31:04'),
(35, NULL, '-', NULL, NULL, 'rolandwaplo17@gmail.com', NULL, NULL, NULL, 'Select', NULL, 'Select', NULL, 'Bussiness', 'Select', NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, '2bYKmpFBBQee84booqDLrguqcWo1', NULL, '2020-10-23 02:56:44', '2020-10-23 02:56:44', '2020-10-23 02:12:34'),
(36, 'Sumit Dabral', '-', 'intern flutter developer', '1999-05-17', 'sumitdabral987@gmail.com', '$2y$10$4St2MD7dvkNmS.lpl9IqN./a4lrJI074G14VIQj9njBEeid1RLfJq', NULL, NULL, 'Select', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Architecture', 'Select', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, '86zEqFNwkGh2D2TrvAxPbqs6Dal2', '4SBDTGKi2st59drvz6Ecdf1Ji4pZsDUtHwNQWv4nzz8wAyQogE40hbSMgHleW9WtO3dlyt', '2020-12-16 06:37:47', '2020-12-16 06:37:47', '2020-10-27 11:08:54'),
(37, 'Tee', '-', 'Mechanical Engineering Student', '2000-11-13', 'zj6cw56s29@privaterelay.appleid.com', NULL, NULL, NULL, 'Select', NULL, '(UTC+03:00) Kuwait, Riyadh', NULL, 'Select', 'Select', NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'Pr8pZTJyU9fEGbZvA28Gyadocdi2', NULL, '2020-11-16 17:00:27', '2020-11-16 17:00:27', '2020-11-16 16:58:09'),
(38, 'farhan', '-', 'fahim', '1991-01-02', 'fe8r2mu6t9@privaterelay.appleid.com', NULL, NULL, NULL, 'User', NULL, 'Select', NULL, 'Select', 'Select', NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, '9RoNGKRNtSYLvJxFxTJfTxVY3493', NULL, '2020-11-18 21:26:26', '2020-11-18 21:26:26', '2020-11-18 21:24:20'),
(39, NULL, NULL, NULL, NULL, 'sangeewane@icloud.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'i1ZT5Qw633TGeOG7sD9Ad1MoHK82', NULL, '2020-11-27 06:23:17', '2020-11-27 06:23:17', '2020-11-27 06:23:16'),
(40, 'sumit', '-', 'yxf', '1980-01-17', 'sumit.kumar@hachiweb.com', '$2y$10$tRLrn8jFsslhGvmEer6QbeFEpN4n74pt1VN0p6HDSQ7NOM.EuMEi2', NULL, NULL, 'User', NULL, 'Select', NULL, 'Select', 'Select', 'sb-xpnx72279648@business.example.com', NULL, 'PAYPAL', 20, 'email', 1, 1, 'PserZOa6MtUEQeu0nzzrQwlju2a2', 'ZEFtjEZiDIA3ewbeMCRkOYMdQTrJaF0WbdFliECXQXJnxQDdoTkSV9hc29m5deTkPhwZ6X', '2020-12-15 11:26:27', '2020-12-15 11:26:27', '2020-12-15 11:15:26'),
(41, 'Kiran Badoni', '-', 'I am a certified Ayurveda expert', '1996-08-09', 'kiran.kumar@hachiweb.com', '$2y$10$jpX4AXswmUVYXcC/f64PhuKivb29ItJSS2ECD5f3hKcx2xw9JQBje', 'https://aurumsolver.com/storage/user-profile-pic/id-41/_2020-12-17T16:22:48.658415', NULL, 'Both', NULL, '(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi', NULL, 'Ayurveda', 'Select', NULL, NULL, 'PAYPAL', 100, 'email', 1, 1, 'gf7iJmU15ROh0p68fNE9kcYitcm2', 'lgnIMzAnzwAzJSSkuySYImsjMAIrP9uXkxRT7YQEE690uLIeZKryyTbA6Ft4f3Q5N6fUCM', '2020-12-17 10:53:31', '2020-12-17 10:53:31', '2020-12-16 08:46:03'),
(42, 'f', '-', 'jcjc', '1970-01-01', 'rahul.rawat@hachiweb.com', '$2y$10$SXGG73NGq4QZTcVgAvMPMef85IDnnPUQ4RLRIjX1bgaK1qtI5MCCO', NULL, NULL, 'Both', NULL, '(UTC-03:00) Buenos Aires', NULL, 'Economics', '1 Week Delay', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'lax0SWNYByZ2EtGvxVSa6W6PaA82', 'iq78W7BJxj1oPTxrpfZuFiF1GbW6blJxr53X0JzWa1RKGHcnLp46CLnFDa0bqlDZfHkiQF', '2020-12-18 09:51:49', '2020-12-18 09:51:49', '2020-12-18 07:06:25'),
(43, NULL, '-', NULL, NULL, 'mohamedaly369@yahoo.com', '$2y$10$FodlkNlpAtdDlzH8CU5eT.ImV.Tm1EM.quFsFpb0wGBxi70eiMwdG', NULL, NULL, 'User', NULL, '(UTC+09:00) Seoul', NULL, 'Accounting', '1 Hour Delay', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, '6JWsi2cFAfNGFhfnCynZfIBH4qw2', 'yHB9zQ4DKm8V43lWv5fyvudIARsg0gKGGToSVnIYs6d2xLFuYa3EpFQxKQPhJSjCspMsI4', '2020-12-19 07:40:42', '2020-12-19 07:40:42', '2020-12-19 07:39:41'),
(44, NULL, NULL, NULL, NULL, 'gregorylarbi96@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'EsmQ7qhbMlNRNWCVrkyXoLmJd8w2', NULL, '2020-12-19 21:22:41', '2020-12-19 21:22:41', '2020-12-19 21:22:41'),
(45, 'Anna', '-', 'helpp', '2001-01-11', '2r36eawab5@privaterelay.appleid.com', NULL, NULL, NULL, 'User', NULL, '(UTC+08:00) Kuala Lumpur, Singapore', NULL, 'Chemistry', 'Select', NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'qoZ5rEFLf3XegPfzPMi8FkLbKfG3', NULL, '2021-01-08 13:01:31', '2021-01-08 13:01:31', '2021-01-08 12:58:53'),
(46, 'UniqueL.', '-', 'Unique', '2002-04-12', 'patiencelawson369@gmail.com', NULL, NULL, NULL, 'Both', NULL, 'Select', NULL, 'Select', 'Instant', 'Patiencelawson369@gmail.com', NULL, 'PAYPAL', 30, 'google.com', 1, 1, 'AGqKtKXJNZV60Bsi1ysPPE71l7k1', NULL, '2021-01-11 15:36:08', '2021-01-11 15:36:08', '2021-01-09 02:47:52'),
(47, 'Hans', '-', 'Disciplinarian', '1990-03-06', 'q54ix6rk5h@privaterelay.appleid.com', NULL, NULL, NULL, 'User', NULL, '(UTC) Edinburgh, London', NULL, 'Mathematics', '1 Hour Delay', NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'NUpwpKU5nOg4K4iIKvadQe8fmk22', NULL, '2021-02-01 16:50:33', '2021-02-01 16:50:33', '2021-01-21 19:40:59'),
(48, NULL, NULL, NULL, NULL, 'warattha222@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'GaOoStjEYgRhi4ofOdGVZEUZDrE2', NULL, '2021-01-21 23:31:06', '2021-01-21 23:31:06', '2021-01-21 23:31:05'),
(49, NULL, NULL, NULL, NULL, 'p.parnward.p@gmail.com', '$2y$10$RsW5/es.o9uA0NDb48o.9OCAppPyTRmWTCHdmLrXu6XiJ2BHYoMCm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'c5GZUGZeHxbO8eDufWrF3EUkF2S2', 'NL5a6fDzFbXjlM75bMORidTbmAmJKg4M1vQQYSgBy6BzBMgQY4X2rJnMVaRC8P3eaRgc7D', '2021-01-28 04:35:20', '2021-01-28 04:35:20', '2021-01-28 04:35:17'),
(50, NULL, NULL, NULL, NULL, 'sisth99@gmail.com', '$2y$10$YlLFWS7q5PEsTAHDfMiax.XFK.XJkChZyFTyJq.sHTiWOk23AZYvG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'YFQuX7IKaXWZc5OgpuEba4ZbhjU2', 'HUCFu6zybIXHpBUqaVQIWG71RUB87fbh20U1UEPIMWTXTEeHd0rjjAKybk6FHe7rAFuCaJ', '2021-01-28 05:07:58', '2021-01-28 05:07:58', '2021-01-28 05:07:56'),
(51, NULL, NULL, NULL, NULL, 'beachserm@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'iDrY47TpP7fFhRoxotDmhJ3dvrb2', NULL, '2021-01-28 09:03:10', '2021-01-28 09:03:10', '2021-01-28 09:03:09'),
(52, NULL, NULL, NULL, NULL, 'sirindarongsakul@gmail.com', '$2y$10$C9QTnxnHwzqSdbwFhTxLxOmkywQMwao/OssyCGb.AlL8iMlAknUrO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'bhu2C9tnuQUTaqGo0pphQ0URTH23', 'U0mOpBu9Ok0LJ6VA4Z5viHyruHfqS42JaxjglW9pg9PHfp4vZwKI0ZMR35p2ZJOcPB4MHj', '2021-01-28 09:40:32', '2021-01-28 09:40:32', '2021-01-28 09:40:30'),
(53, NULL, NULL, NULL, NULL, 'eva.arponsiriroj@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'EdX4ojJCIRcCaR2uBJ4NwbSk8Bv2', NULL, '2021-01-29 09:20:08', '2021-01-29 09:20:08', '2021-01-29 09:20:06'),
(54, NULL, NULL, NULL, NULL, 'z2d4bt43fc@privaterelay.appleid.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'Z2onqlLdi3SEncQZZVH1RONKGnq2', NULL, '2021-01-29 15:52:01', '2021-01-29 15:52:01', '2021-01-29 15:52:00'),
(55, NULL, '-', NULL, NULL, 'fangfai4346@hotmail.com', '$2y$10$1cQfqJzBl4AWQFD7MR0A0umNZLmvZYBlGQgEGuLUzuqz9L6L.IELy', 'https://aurumsolver.com/storage/user-profile-pic/id-55/_2021-01-30T11:24:46.979019', NULL, 'User', NULL, '(UTC+07:00) Bangkok, Hanoi, Jakarta', NULL, 'Chemistry', 'Instant', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'oF10KERPvkcAe3Vpti5kkhK8C3V2', 'jEePF04tim0tD2Ndafr8jCeM54A5b1kLYnArZoat52DhVSg8JWIETuSsgqaX7FdCik0zsr', '2021-01-30 04:27:18', '2021-01-30 04:27:18', '2021-01-30 04:24:19'),
(56, NULL, '-', NULL, NULL, 'kamollux2003@gmail.com', '$2y$10$QfX45u5EcElnExDKy6.BT.v2pURmM.Uh1/AMfXM9rlHKkPQs.bjWm', NULL, NULL, 'User', NULL, 'Select', NULL, 'Select', 'Select', NULL, NULL, 'PAYPAL', 20, 'email', 1, 1, 'UNvu0g5EvGYbCfuL3KET13wzS2P2', 'YIbJ6ux5V1PpOC1r1IRVci7tmbYdqpEsNYkHhpFx2qsSQSf4Bo6n0ii2uaEZDUywMh7V5t', '2021-01-30 11:39:55', '2021-01-30 11:39:55', '2021-01-30 11:39:19'),
(57, NULL, NULL, NULL, NULL, 'kantheera.krabi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'mG3UqlsOktYqhA6RMOwbL5bMFRn1', NULL, '2021-01-30 12:17:29', '2021-01-30 12:17:29', '2021-01-30 12:17:28'),
(58, NULL, NULL, NULL, NULL, 'panraksa0707@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'OKBPOf9vNVe9Gidd4jbNthjkIYh1', NULL, '2021-01-30 16:37:59', '2021-01-30 16:37:59', '2021-01-30 16:37:59'),
(59, NULL, NULL, NULL, NULL, '7afjyd5mzs@privaterelay.appleid.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'dptBd9TODdbB1dhZVcEnUqiousi1', NULL, '2021-01-31 12:44:36', '2021-01-31 12:44:36', '2021-01-31 12:44:35'),
(60, NULL, NULL, NULL, NULL, 'pegrqzmbc3@privaterelay.appleid.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'BqY9XUzZ9BPOss3xH3HaTrh71hc2', NULL, '2021-02-02 13:24:38', '2021-02-02 13:24:38', '2021-02-02 13:24:37'),
(61, NULL, NULL, NULL, NULL, 'xfnz9fvwx5@privaterelay.appleid.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'B0pxGTVYT1fxEJHjrNwiQj1JeZB3', NULL, '2021-02-06 10:53:36', '2021-02-06 10:53:36', '2021-02-06 10:53:35'),
(62, NULL, NULL, NULL, NULL, 'dkwbnuiz87@privaterelay.appleid.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'apple.com', 1, 1, 'xuEy6WiJLxSg33NZv5bwBqrqv7h2', NULL, '2021-02-08 15:21:22', '2021-02-08 15:21:22', '2021-02-08 15:21:20'),
(63, NULL, NULL, NULL, NULL, 'ppppp.chompoo@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'Fu5m2vM7BAPE2t7mokoXMyoLWcy1', NULL, '2021-02-12 14:31:36', '2021-02-12 14:31:36', '2021-02-12 14:31:35'),
(64, NULL, NULL, NULL, NULL, 'nalsumizo@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'DFSOPuLUGXdVYn7UrqGxQXTi6tH2', NULL, '2021-02-13 06:51:06', '2021-02-13 06:51:06', '2021-02-13 06:51:05'),
(65, NULL, '-', NULL, NULL, 'benyapabambam@gmail.com', NULL, NULL, NULL, 'Select', NULL, 'Select', NULL, 'Select', 'Select', 'benyapabambam@gmail.com', NULL, 'PAYPAL', 20, 'google.com', 1, 1, 'LsGexATGv9bkOey8dzI5FLAbnbr1', NULL, '2021-02-13 16:40:27', '2021-02-13 16:40:27', '2021-02-13 16:39:42'),
(66, 'frazer', '-', 'student', '1999-01-08', 'frazergob95@gmail.com', NULL, 'https://aurumsolver.com/storage/user-profile-pic/id-66/_2021-02-18T03:17:53.894460', NULL, 'User', NULL, '(UTC+04:00) Abu Dhabi, Muscat', NULL, 'Engineering', '3 Hour Delay', 'frazergob95@gmail.com', NULL, 'PAYPAL', 20, 'google.com', 1, 1, '8hzVpItjFhetS271MaogoxqL5mP2', NULL, '2021-02-17 23:20:32', '2021-02-17 23:20:32', '2021-02-17 23:16:06');

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_four` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `subjects` text NOT NULL,
  `duration` float NOT NULL,
  `min_percent` float NOT NULL DEFAULT 80,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `subjects`, `duration`, `min_percent`, `created_at`, `updated_at`) VALUES
(1, 'Accounting', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(2, 'Architecture', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(3, 'Art', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(4, 'Asian Studies', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(5, 'Astronmy', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(6, 'Biiology', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(7, 'Bussiness', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(8, 'Chemistry', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(9, 'Communications', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(10, 'Computer Science', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(11, 'Economics', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(12, 'Education', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(13, 'Engineering', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(14, 'English', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(15, 'Finance', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(16, 'Foreign Languages', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(17, 'Gendr Studies', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(18, 'General Studies', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(19, 'Gendr Questions', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(20, 'Geography', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(21, 'Geology', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(22, 'Health Care', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(23, 'History', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(24, 'Law', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(25, 'Linguistics', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(26, 'Literary', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(27, 'Mathematics', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(28, 'Literary Studies', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(29, 'Music', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(30, 'Other', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(31, 'Philosophy', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(32, 'Physics', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(33, 'Political Science', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(34, 'Religious Studies', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(35, 'Sociology', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(36, 'Statistics', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(37, 'Urban Planning and Policy', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10'),
(38, 'Ayurveda', 45, 80, '2020-05-28 07:35:10', '2020-05-28 07:35:10');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `from_message` text NOT NULL,
  `to_message` text NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `from_message`, `to_message`, `message`, `created_at`, `updated_at`) VALUES
(1, '84', '86', 'Hello,This Rajesh.', '2020-04-16 06:33:01', '2020-04-16 06:33:01');

-- --------------------------------------------------------

--
-- Table structure for table `faq_support`
--

CREATE TABLE `faq_support` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_support`
--

INSERT INTO `faq_support` (`id`, `question`, `answer`) VALUES
(1, 'At accusantium repreLorem, ipsum dolor sit amet consectetur adip', 'At accusantium repreAt accusantium repreLorem, ipsum dolor sit amet consectetur adipisicing elit. Veritatis earum totam nisi aliquid officiis quidem incidunt, impedit debitis! Nostrum praesentium optio maxime quo, excepturi alias expedita rerum iusto quae exercitationem.'),
(2, 'At accusantium repreLorem, ipsum dolor sit amet consectetur adipAt accusantium repreLorem, ipsum dolor sit amet consectetur adip.?', 'At accusantium repreLorem, ipsum dolor sit amet consectetur adipisicing elit. Veritatis earum totam nisi aliquid officiis quidem incidunt, impedit debitis! Nostrum praesentium optio maxime quo, excepturi alias expedita rerum iusto quae exercitationem. answerAt accusantium repreLorem, ipsum dolor sit amet consectetur adipisicing elit. Veritatis earum totam nisi aliquid officiis quidem incidunt, impedit debitis! Nostrum praesentium optio maxime quo, excepturi alias expedita rerum iusto quae exercitationem.'),
(5, 'Sunt nostrud at sunt', 'Vel perferendis non'),
(6, 'Sint aliquam eligend', 'Tempor nostrum dolor'),
(7, 'Non libero id illum', 'Consequatur nulla u'),
(8, 'Asperiores repudiand', 'Ut exercitationem su'),
(9, 'Rerum modi earum ea', 'Non et dolorem cillu');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` int(11) NOT NULL,
  `rating` float NOT NULL,
  `review` text DEFAULT NULL,
  `rated_to` int(11) NOT NULL,
  `rated_by` int(11) NOT NULL,
  `rated_for` text NOT NULL,
  `rated_for_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 2),
(3, 'App\\User', 3);

-- --------------------------------------------------------

--
-- Table structure for table `ondemand_tutoring`
--

CREATE TABLE `ondemand_tutoring` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `tutor_id` bigint(20) NOT NULL,
  `select_duration` float NOT NULL,
  `effective_duration` float DEFAULT NULL,
  `total_cost` float NOT NULL,
  `effective_cost` float DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ondemand_tutoring`
--

INSERT INTO `ondemand_tutoring` (`id`, `user_id`, `tutor_id`, `select_duration`, `effective_duration`, `total_cost`, `effective_cost`, `created_at`, `updated_at`) VALUES
(1, 10, 11, 5, NULL, 1, NULL, '2020-07-07 03:23:25', '2020-07-07 03:23:25'),
(2, 10, 11, 5, NULL, 1, NULL, '2020-07-07 03:28:00', '2020-07-07 03:28:00'),
(3, 10, 11, 10, NULL, 2.5, NULL, '2020-07-07 03:40:14', '2020-07-07 03:40:14'),
(4, 8, 8, 1, NULL, 0.31, NULL, '2020-07-07 04:02:29', '2020-07-07 04:02:29'),
(5, 8, 8, 1, 2, 0.31, 0.63, '2020-07-07 14:59:07', '2020-07-07 15:25:41'),
(6, 3, 8, 5, NULL, 1.56, NULL, '2020-07-07 15:43:37', '2020-07-07 15:43:37'),
(7, 3, 8, 5, 4.8, 1.56, 1.5, '2020-07-07 15:47:23', '2020-07-07 15:47:32'),
(8, 3, 8, 5, NULL, 1.56, NULL, '2020-07-07 16:58:45', '2020-07-07 16:58:45'),
(9, 15, 15, 5, 4.7, 1.25, 1.18, '2020-07-11 09:08:04', '2020-07-11 09:08:23'),
(10, 8, 15, 5, 4.73333, 1.25, 1.18, '2020-07-11 09:08:04', '2020-07-11 09:08:21'),
(11, 15, 15, 5, 4.23333, 1.25, 1.06, '2020-07-11 14:11:14', '2020-07-11 14:11:59'),
(12, 6, 15, 5, 4.28333, 1.25, 1.07, '2020-07-11 14:11:14', '2020-07-11 14:11:56'),
(13, 15, 15, 5, 4.75, 1.25, 1.19, '2020-07-11 14:12:19', '2020-07-11 14:12:33'),
(14, 6, 15, 5, 4.78333, 1.25, 1.2, '2020-07-11 14:12:20', '2020-07-11 14:12:31'),
(15, 6, 15, 5, 4.61667, 1.25, 1.15, '2020-07-11 15:01:27', '2020-07-11 15:01:52'),
(16, 15, 15, 5, 4.68333, 1.25, 1.17, '2020-07-11 15:01:28', '2020-07-11 15:01:49');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'dashboard', 'web', '2020-06-09 22:29:27', '2020-06-09 22:29:27');

-- --------------------------------------------------------

--
-- Table structure for table `prepayments`
--

CREATE TABLE `prepayments` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `from_request` int(11) DEFAULT NULL,
  `to_request` int(11) DEFAULT NULL,
  `budget_amount` float DEFAULT NULL,
  `requested_amount` float DEFAULT NULL,
  `approve_status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tutor_id` varchar(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `question` text DEFAULT NULL,
  `is_assigned` tinyint(1) NOT NULL DEFAULT 0,
  `attachment` text DEFAULT NULL,
  `willing_to_pay` varchar(255) DEFAULT NULL,
  `private_question` tinyint(1) DEFAULT 0,
  `prepayment` tinyint(4) NOT NULL DEFAULT 0,
  `questioner_fee` varchar(25) DEFAULT NULL,
  `answerer_fee` varchar(25) DEFAULT NULL,
  `admin_fee` varchar(25) DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `request_accept` varchar(1) DEFAULT NULL,
  `delivery_accept` varchar(1) DEFAULT NULL,
  `is_paid` tinyint(1) DEFAULT NULL,
  `is_cancelled` tinyint(1) NOT NULL DEFAULT 0,
  `cancellation_reason` text DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_approved` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `user_id`, `tutor_id`, `category_id`, `title`, `question`, `is_assigned`, `attachment`, `willing_to_pay`, `private_question`, `prepayment`, `questioner_fee`, `answerer_fee`, `admin_fee`, `post_date`, `due_date`, `request_accept`, `delivery_accept`, `is_paid`, `is_cancelled`, `cancellation_reason`, `is_active`, `is_approved`, `created_at`, `updated_at`) VALUES
(3, 7, '', 20, 'The Challenge', '[{\"insert\":\"Description:\\n2x + 1= 17\\nSolve for (x)\"},{\"insert\":\"\\n\",\"attributes\":{\"block\":\"ol\"}},{\"insert\":\"Write the first five multiples of the value of (x)\"},{\"insert\":\"\\n\",\"attributes\":{\"block\":\"ol\"}},{\"insert\":\"What are the factors of (x)\"},{\"insert\":\"\\n\",\"attributes\":{\"block\":\"ol\"}}]', 0, '', '1.00', 0, 0, '1.05', '0.80', '0.25', '2020-07-05', '2020-07-05', NULL, NULL, NULL, 0, NULL, 1, 0, '2020-07-05 23:40:17', '2020-07-05 23:40:17'),
(4, 22, '', 27, 'calculus work', '[{\"insert\":\"Description:\\n\"}]', 0, 'https://aurumsolver.com/storage/attachments/questions/user-id-22/questions-1594948910.image_picker_790B0904-C137-4C3C-9500-08012360FDC5-7733-0000053F3FD08150.jpg', '1.00', 0, 0, '1.05', '0.80', '0.25', '2020-07-17', '2020-07-16', NULL, NULL, NULL, 0, NULL, 1, 0, '2020-07-17 01:21:56', '2020-07-17 01:21:56'),
(5, 28, '', 10, 'what programming language is used for websites?', '[{\"insert\":\"I want to learn web development, Can anyone please suggest me which \"},{\"insert\":\"programming\",\"attributes\":{\"b\":true}},{\"insert\":\" language is used for website creation??? \\n\"}]', 0, '', '20.00', 0, 0, '21.00', '16.00', '5.00', '2020-08-11', '2020-08-11', NULL, NULL, NULL, 0, NULL, 1, 0, '2020-08-11 11:13:38', '2020-08-11 11:13:38'),
(6, 28, '7', 27, 'Assign Questions to this user.', '[{\"insert\":\"Description:\\n\"}]', 1, '', '10.00', 0, 0, '10.50', '8.00', '2.50', '2020-08-11', '2020-08-15', NULL, NULL, NULL, 0, NULL, 1, 0, '2020-08-11 11:22:29', '2020-08-11 11:22:29'),
(7, 40, '', 2, 'for c', '[{\"insert\":\"Description: ehssn idid\\n\"}]', 0, '', '10.00', 0, 0, '10.50', '8.00', '2.50', '2020-12-15', '2020-12-15', NULL, NULL, NULL, 0, NULL, 1, 0, '2020-12-15 11:29:00', '2020-12-15 11:29:00'),
(8, 40, '', 1, '😭🐶😒😡', '[{\"insert\":\"Description: from g\\n\"}]', 0, '', '5.00', 0, 0, '5.25', '4.00', '1.25', '2020-12-15', '2020-12-15', NULL, NULL, NULL, 0, NULL, 1, 0, '2020-12-15 11:33:37', '2020-12-15 11:33:37'),
(9, 41, '', 1, 'dyczui', '[{\"insert\":\"Description:chkdigo\\n\"}]', 0, '', '10.00', 0, 0, '10.50', '8.00', '2.50', '2020-12-18', '2020-12-18', NULL, NULL, NULL, 0, NULL, 1, 0, '2020-12-18 04:49:59', '2020-12-18 04:49:59'),
(10, 46, '', 32, 'Projectile Motion', '[{\"insert\":\"Description:\\n\"}]', 0, 'https://aurumsolver.com/storage/attachments/questions/user-id-46/questions-1610161220.image_picker_9614995C-3B3C-4995-A171-25932184DBC1-9891-000005886DED592A.png,https://aurumsolver.com/storage/attachments/questions/user-id-46/questions-1610161220.image_picker_55E26395-F994-46E8-A517-65DF87FA46B1-9891-00000588789571DF.png', '1.00', 1, 0, '1.05', '0.80', '0.25', '2021-01-09', '2021-01-08', NULL, NULL, NULL, 0, NULL, 1, 0, '2021-01-09 03:00:25', '2021-01-09 03:00:25'),
(11, 66, '', 13, 'Individual investigation and report on Electronic Flight Instrument Systems.', '[{\"insert\":\"Description:\\nStudents are required to submit a 1000 word investigative report to discuss the following:\\n1. Explore and explain how Electronic Flight Instruments have evolved from analogue instruments.\\n2. Determine how Electronic Flight Instruments and digital systems have played a role in the ergonomics and automation within the cockpit.\\n3. Through critical analysis discuss how the growth in digital avionics has had an impact on human factors and flight safety.\\n4. As a case study investigate an example of confusion arising from mismanagement of automation which had serious or potentially serious consequences for a serviceable aeroplane.  Discuss what improvements could be made to avoid this.\\n\\n\"}]', 0, '', '10.00', 1, 0, '10.50', '8.00', '2.50', '2021-02-17', '2021-03-01', NULL, NULL, NULL, 0, NULL, 1, 0, '2021-02-17 23:24:50', '2021-02-17 23:24:50');

-- --------------------------------------------------------

--
-- Table structure for table `quizzes`
--

CREATE TABLE `quizzes` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `questions` text NOT NULL,
  `opt1` text NOT NULL,
  `opt2` text NOT NULL,
  `opt3` text NOT NULL,
  `opt4` text NOT NULL,
  `correct` varchar(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quizzes`
--

INSERT INTO `quizzes` (`id`, `category_id`, `questions`, `opt1`, `opt2`, `opt3`, `opt4`, `correct`, `created_at`, `updated_at`) VALUES
(1, 5, 'what is latest version of php?', '3.4', '6.8', '8', '7.4', 'opt1', '2020-04-09 01:38:12', '2020-04-09 01:38:12'),
(2, 5, 'What is microsoft additions', '5', '5.5', '8', '9', 'opt4', '2020-04-11 04:10:12', '2020-04-11 04:10:12'),
(3, 10, 'What is latest bootsrtap versions.', '4.3', '5.5', '3', '3.5', 'opt3', '2020-04-11 04:14:41', '2020-04-11 04:14:41'),
(4, 10, 'What is latest bootsrtap versions.', '4.3', '5.5', '3', '3.5', 'opt2', '2020-04-14 05:40:07', '2020-04-14 05:40:07'),
(5, 5, 'Micorsogt?', '4.3', '5.5', '3', '3.5', 'opt1', '2020-05-16 04:00:51', '2020-05-16 04:00:51'),
(6, 1, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:09:31', '2020-05-28 07:09:31'),
(7, 1, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:09:36', '2020-05-28 07:09:36'),
(8, 1, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:03', '2020-05-28 07:10:03'),
(9, 1, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:15', '2020-05-28 07:10:15'),
(10, 2, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:22', '2020-05-28 07:10:22'),
(11, 2, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:27', '2020-05-28 07:10:27'),
(12, 2, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:40', '2020-05-28 07:10:40'),
(13, 2, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:10:50', '2020-05-28 07:10:50'),
(14, 2, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:09', '2020-05-28 07:11:09'),
(15, 3, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:20', '2020-05-28 07:11:20'),
(16, 3, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(17, 3, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(18, 3, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(19, 3, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(20, 6, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(21, 6, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(22, 6, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(23, 4, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(24, 4, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(25, 5, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(26, 6, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(27, 7, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(28, 7, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(29, 7, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(30, 7, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(31, 7, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(32, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:11:53', '2020-05-28 07:11:53'),
(33, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(34, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(35, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(36, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(37, 8, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(38, 9, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:14:29', '2020-05-28 07:14:29'),
(39, 9, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(40, 9, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(41, 9, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(42, 9, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(43, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(44, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(45, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(46, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(47, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(48, 11, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(49, 12, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(50, 12, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(51, 12, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(52, 12, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(53, 13, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(54, 13, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(55, 13, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(56, 13, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(57, 13, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(58, 14, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(59, 14, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(60, 14, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(61, 14, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(62, 14, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(63, 15, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(64, 15, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(65, 15, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(66, 15, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(67, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(68, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:15:14', '2020-05-28 07:15:14'),
(69, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:19:43', '2020-05-28 07:19:43'),
(70, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:19:43', '2020-05-28 07:19:43'),
(71, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:19:43', '2020-05-28 07:19:43'),
(72, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:19:43', '2020-05-28 07:19:43'),
(73, 16, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:19:43', '2020-05-28 07:19:43'),
(74, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(75, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(76, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(77, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(78, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(79, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(80, 17, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:20:25', '2020-05-28 07:20:25'),
(81, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(82, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(83, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(84, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(85, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(86, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(87, 18, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:20:53', '2020-05-28 07:20:53'),
(88, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(89, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(90, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(91, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(92, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(93, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(94, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(95, 19, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:24', '2020-05-28 07:21:24'),
(96, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(97, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(98, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(99, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(100, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(101, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(102, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(103, 20, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:21:50', '2020-05-28 07:21:50'),
(104, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(105, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(106, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(107, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(108, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(109, 21, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt3', '2020-05-28 07:22:26', '2020-05-28 07:22:26'),
(110, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(111, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(112, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(113, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(114, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(115, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:22:51', '2020-05-28 07:22:51'),
(116, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:08', '2020-05-28 07:23:08'),
(117, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:08', '2020-05-28 07:23:08'),
(118, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:08', '2020-05-28 07:23:08'),
(119, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:09', '2020-05-28 07:23:09'),
(120, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:09', '2020-05-28 07:23:09'),
(121, 23, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt2', '2020-05-28 07:23:09', '2020-05-28 07:23:09'),
(122, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(123, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(124, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(125, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(126, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(127, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(128, 24, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:34', '2020-05-28 07:23:34'),
(129, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(130, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(131, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(132, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(133, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(134, 25, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt1', '2020-05-28 07:23:52', '2020-05-28 07:23:52'),
(135, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(136, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(137, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(138, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(139, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(140, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(141, 26, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:24:38', '2020-05-28 07:24:38'),
(142, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:05', '2020-05-28 07:25:05'),
(143, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(144, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(145, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(146, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(147, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(148, 27, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:06', '2020-05-28 07:25:06'),
(149, 28, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:20', '2020-05-28 07:25:20'),
(150, 28, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:21', '2020-05-28 07:25:21'),
(151, 28, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:21', '2020-05-28 07:25:21'),
(152, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(153, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(154, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(155, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(156, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(157, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(158, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(159, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:25:42', '2020-05-28 07:25:42'),
(160, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(161, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(162, 29, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(163, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(164, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(165, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(166, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05'),
(167, 38, 'What do you mean by question?', 'phrase, or word that asks for information or is u', 'None', 'phrase, or word that asks for information or is u', 'phrase, or word that asks for information or is u', 'opt4', '2020-05-28 07:26:05', '2020-05-28 07:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answer`
--

CREATE TABLE `quiz_answer` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `correct_answer` int(11) NOT NULL,
  `percent` text NOT NULL,
  `total_attempt` int(11) NOT NULL,
  `is_passed` tinyint(1) NOT NULL DEFAULT 0,
  `is_paid` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quiz_answer`
--

INSERT INTO `quiz_answer` (`id`, `category_id`, `user_id`, `correct_answer`, `percent`, `total_attempt`, `is_passed`, `is_paid`, `created_at`, `updated_at`) VALUES
(1, 27, '21', 0, '0', 1, 0, 0, '2020-07-15 19:08:07', '2020-07-15 19:08:07'),
(2, 1, '21', 0, '0', 1, 0, 0, '2020-07-15 19:08:26', '2020-07-15 19:08:26'),
(3, 7, '32', 1, '20', 1, 0, 0, '2020-10-04 02:32:28', '2020-10-04 02:32:28'),
(4, 8, '36', 0, '0', 3, 0, 0, '2020-10-27 11:14:24', '2020-10-27 11:15:57'),
(5, 10, '36', 0, '0', 1, 0, 0, '2020-10-27 11:16:36', '2020-10-27 11:16:36'),
(6, 6, '36', 0, '0', 1, 0, 0, '2020-10-27 11:17:09', '2020-10-27 11:17:09'),
(7, 2, '36', 0, '0', 3, 0, 0, '2020-12-15 11:30:59', '2020-12-15 11:31:50'),
(8, 1, '36', 4, '100', 1, 1, 0, '2020-12-15 11:32:49', '2020-12-15 11:32:49'),
(9, 1, '40', 4, '100', 1, 1, 0, '2020-12-18 04:52:14', '2020-12-18 04:52:14'),
(10, 1, '42', 0, '0', 3, 0, 0, '2020-12-18 09:02:23', '2020-12-18 09:04:04'),
(11, 27, '46', 0, '0', 3, 0, 0, '2021-01-09 03:10:43', '2021-01-09 03:18:24'),
(12, 25, '46', 0, '0', 1, 0, 0, '2021-01-09 03:12:39', '2021-01-09 03:12:39'),
(13, 8, '46', 3, '50', 2, 0, 0, '2021-01-09 03:13:11', '2021-01-09 03:14:39'),
(14, 5, '46', 0, '0', 1, 0, 0, '2021-01-09 03:14:05', '2021-01-09 03:14:05'),
(15, 3, '46', 0, '0', 1, 0, 0, '2021-01-09 03:15:02', '2021-01-09 03:15:02'),
(16, 12, '46', 1, '25', 1, 0, 0, '2021-01-09 03:16:40', '2021-01-09 03:16:40'),
(17, 6, '46', 4, '100', 1, 1, 0, '2021-01-09 03:17:19', '2021-01-09 03:17:19'),
(18, 1, '46', 4, '100', 1, 1, 0, '2021-01-09 03:18:44', '2021-01-09 03:18:44'),
(19, 1, '5', 1, '25', 1, 0, 0, '2021-02-20 16:27:49', '2021-02-20 16:27:49');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-06-09 22:29:27', '2020-06-09 22:29:27'),
(2, 'moderator', 'web', '2020-06-09 22:30:08', '2020-06-09 22:30:08'),
(3, 'user', 'web', '2020-06-09 22:30:52', '2020-06-09 22:30:52');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `from` text NOT NULL,
  `to` text NOT NULL,
  `currency` text DEFAULT NULL,
  `amount` text NOT NULL,
  `status` text NOT NULL,
  `withdrawal` tinyint(4) NOT NULL DEFAULT 0,
  `is_gateway` tinyint(1) NOT NULL DEFAULT 0,
  `transaction_id` text NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `question_id`, `answer_id`, `from`, `to`, `currency`, `amount`, `status`, `withdrawal`, `is_gateway`, `transaction_id`, `description`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, '5', '5', NULL, '61.9', 'paid', 0, 1, 'PAYID-L3XRREY52670323XV290463B', NULL, '2020-06-21 08:24:37', '2020-06-21 08:24:37'),
(2, NULL, NULL, '3', '3', NULL, '40.0', 'paid', 0, 1, 'PAYID-L37CHLI8YH42110GY7787024', NULL, '2020-07-02 18:16:12', '2020-07-02 18:16:12'),
(3, NULL, NULL, '10', '11', 'USD', '1', 'paid', 0, 0, 'AS_TXN_651067921', 'On-demand tutoring', '2020-07-07 03:28:00', '2020-07-07 03:28:00'),
(4, NULL, NULL, '10', '11', 'USD', '2.50', 'paid', 0, 0, 'AS_TXN_117226670', 'On-demand tutoring', '2020-07-07 03:40:14', '2020-07-07 03:40:14'),
(5, NULL, NULL, '8', '8', 'USD', '0.31', 'paid', 0, 0, 'AS_TXN_838666967', 'On-demand tutoring', '2020-07-07 04:02:29', '2020-07-07 04:02:29'),
(6, NULL, NULL, '8', '8', 'USD', '0.31', 'paid', 0, 0, 'AS_TXN_651002533', 'On-demand tutoring', '2020-07-07 14:59:07', '2020-07-07 14:59:07'),
(7, NULL, NULL, '8', '8', 'USD', '-0.32', 'paid', 0, 0, 'AS_TXN_314247255', 'On-demand tutoring refund', '2020-07-07 15:25:42', '2020-07-07 15:25:42'),
(8, NULL, NULL, '3', '8', 'USD', '1.56', 'paid', 0, 0, 'AS_TXN_177475745', 'On-demand tutoring', '2020-07-07 15:43:37', '2020-07-07 15:43:37'),
(9, NULL, NULL, '3', '8', 'USD', '1.56', 'paid', 0, 0, 'AS_TXN_194215421', 'On-demand tutoring', '2020-07-07 15:47:23', '2020-07-07 15:47:23'),
(10, NULL, NULL, '8', '3', 'USD', '0.06000000000000005', 'paid', 0, 0, 'AS_TXN_494048918', 'On-demand tutoring refund', '2020-07-07 15:47:32', '2020-07-07 15:47:32'),
(11, NULL, NULL, '3', '8', 'USD', '1.56', 'paid', 0, 0, 'AS_TXN_712984256', 'On-demand tutoring', '2020-07-07 16:58:45', '2020-07-07 16:58:45'),
(12, NULL, NULL, '15', '15', 'USD', '1.25', 'paid', 0, 0, 'AS_TXN_124855978', 'On-demand tutoring', '2020-07-11 09:08:04', '2020-07-11 09:08:04'),
(13, NULL, NULL, '8', '15', 'USD', '1.25', 'paid', 0, 0, 'AS_TXN_901715996', 'On-demand tutoring', '2020-07-11 09:08:04', '2020-07-11 09:08:04'),
(14, NULL, NULL, '15', '8', 'USD', '0.07000000000000006', 'paid', 0, 0, 'AS_TXN_971872474', 'On-demand tutoring refund', '2020-07-11 09:08:21', '2020-07-11 09:08:21'),
(15, NULL, NULL, '15', '15', 'USD', '0.07000000000000006', 'paid', 0, 0, 'AS_TXN_336366067', 'On-demand tutoring refund', '2020-07-11 09:08:23', '2020-07-11 09:08:23'),
(16, NULL, NULL, '6', '6', NULL, '100.0', 'paid', 0, 1, 'PAYID-L4E4JBA1F31667849860593M', NULL, '2020-07-11 13:56:26', '2020-07-11 13:56:26'),
(17, NULL, NULL, '6', '6', NULL, '100.0', 'paid', 0, 1, 'PAYID-L4E4JBA1F31667849860593M', NULL, '2020-07-11 13:56:27', '2020-07-11 13:56:27'),
(18, NULL, NULL, '6', '6', NULL, '50.0', 'paid', 0, 1, 'PAYID-L4E4KNI5G9279521S997120C', NULL, '2020-07-11 13:57:37', '2020-07-11 13:57:37'),
(19, NULL, NULL, '15', '15', 'USD', '1.25', 'paid', 0, 0, 'AS_TXN_414554204', 'On-demand tutoring', '2020-07-11 14:11:14', '2020-07-11 14:11:14'),
(20, NULL, NULL, '6', '15', 'USD', '1.25', 'paid', 0, 0, 'AS_TXN_733427695', 'On-demand tutoring', '2020-07-11 14:11:14', '2020-07-11 14:11:14'),
(21, NULL, NULL, '15', '6', 'USD', '0.17999999999999994', 'paid', 0, 0, 'AS_TXN_162990543', 'On-demand tutoring refund', '2020-07-11 14:11:56', '2020-07-11 14:11:56'),
(22, NULL, NULL, '15', '15', 'USD', '0.18999999999999995', 'paid', 0, 0, 'AS_TXN_134689882', 'On-demand tutoring refund', '2020-07-11 14:11:59', '2020-07-11 14:11:59'),
(23, NULL, NULL, '15', '15', 'USD', '1.25', 'paid', 0, 0, 'AS_TXN_283480202', 'On-demand tutoring', '2020-07-11 14:12:19', '2020-07-11 14:12:19'),
(24, NULL, NULL, '6', '15', 'USD', '1.25', 'paid', 0, 0, 'AS_TXN_520834446', 'On-demand tutoring', '2020-07-11 14:12:21', '2020-07-11 14:12:21'),
(25, NULL, NULL, '15', '6', 'USD', '0.050000000000000044', 'paid', 0, 0, 'AS_TXN_408216855', 'On-demand tutoring refund', '2020-07-11 14:12:31', '2020-07-11 14:12:31'),
(26, NULL, NULL, '15', '15', 'USD', '0.06000000000000005', 'paid', 0, 0, 'AS_TXN_426770385', 'On-demand tutoring refund', '2020-07-11 14:12:33', '2020-07-11 14:12:33'),
(27, NULL, NULL, '6', '15', 'USD', '1.25', 'paid', 0, 0, 'AS_TXN_342405555', 'On-demand tutoring', '2020-07-11 15:01:27', '2020-07-11 15:01:27'),
(28, NULL, NULL, '15', '15', 'USD', '1.25', 'paid', 0, 0, 'AS_TXN_856752694', 'On-demand tutoring', '2020-07-11 15:01:28', '2020-07-11 15:01:28'),
(29, NULL, NULL, '15', '15', 'USD', '0.08000000000000007', 'paid', 0, 0, 'AS_TXN_905315319', 'On-demand tutoring refund', '2020-07-11 15:01:49', '2020-07-11 15:01:49'),
(30, NULL, NULL, '15', '6', 'USD', '0.10000000000000009', 'paid', 0, 0, 'AS_TXN_146420566', 'On-demand tutoring refund', '2020-07-11 15:01:52', '2020-07-11 15:01:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `access_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin.test@hachiweb.com', NULL, '$2y$10$vpEUkJwwJkdjUdNbLpx7o.EFPG32G2xk98NI3f30Rt7nm.e729afO', 'SCdB3BPn5bSrFaZaivhu1haW4zSky0gSyw4DHeyE0sANFUk8DISHJV2hi9pK', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXVydW1zb2x2ZXIuY29tXC9nZXQtdG9rZW4iLCJpYXQiOjE2MTM5MDk5ODksImV4cCI6MTYxMzkxMzU4OSwibmJmIjoxNjEzOTA5OTg5LCJqdGkiOiJEdldHWTBIeFV3a1dWV0IwIiwic3ViIjpudWxsfQ.fwMUpJSYvZEIKmxpY5rGCVGyloLeki9QRAmZZAaRkUE', NULL, '2021-02-21 12:19:51');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawals`
--

CREATE TABLE `withdrawals` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `amount` text NOT NULL,
  `description` text DEFAULT NULL,
  `is_approved` varchar(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `zone_id` int(10) NOT NULL,
  `country_code` char(2) COLLATE utf8_bin NOT NULL,
  `zone_name` varchar(35) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`zone_id`, `country_code`, `zone_name`) VALUES
(1, 'AD', 'Europe/Andorra'),
(2, 'AE', 'Asia/Dubai'),
(3, 'AF', 'Asia/Kabul'),
(4, 'AG', 'America/Antigua'),
(5, 'AI', 'America/Anguilla'),
(6, 'AL', 'Europe/Tirane'),
(7, 'AM', 'Asia/Yerevan'),
(8, 'AO', 'Africa/Luanda'),
(9, 'AQ', 'Antarctica/McMurdo'),
(10, 'AQ', 'Antarctica/Casey'),
(11, 'AQ', 'Antarctica/Davis'),
(12, 'AQ', 'Antarctica/DumontDUrville'),
(13, 'AQ', 'Antarctica/Mawson'),
(14, 'AQ', 'Antarctica/Palmer'),
(15, 'AQ', 'Antarctica/Rothera'),
(16, 'AQ', 'Antarctica/Syowa'),
(17, 'AQ', 'Antarctica/Troll'),
(18, 'AQ', 'Antarctica/Vostok'),
(19, 'AR', 'America/Argentina/Buenos_Aires'),
(20, 'AR', 'America/Argentina/Cordoba'),
(21, 'AR', 'America/Argentina/Salta'),
(22, 'AR', 'America/Argentina/Jujuy'),
(23, 'AR', 'America/Argentina/Tucuman'),
(24, 'AR', 'America/Argentina/Catamarca'),
(25, 'AR', 'America/Argentina/La_Rioja'),
(26, 'AR', 'America/Argentina/San_Juan'),
(27, 'AR', 'America/Argentina/Mendoza'),
(28, 'AR', 'America/Argentina/San_Luis'),
(29, 'AR', 'America/Argentina/Rio_Gallegos'),
(30, 'AR', 'America/Argentina/Ushuaia'),
(31, 'AS', 'Pacific/Pago_Pago'),
(32, 'AT', 'Europe/Vienna'),
(33, 'AU', 'Australia/Lord_Howe'),
(34, 'AU', 'Antarctica/Macquarie'),
(35, 'AU', 'Australia/Hobart'),
(36, 'AU', 'Australia/Currie'),
(37, 'AU', 'Australia/Melbourne'),
(38, 'AU', 'Australia/Sydney'),
(39, 'AU', 'Australia/Broken_Hill'),
(40, 'AU', 'Australia/Brisbane'),
(41, 'AU', 'Australia/Lindeman'),
(42, 'AU', 'Australia/Adelaide'),
(43, 'AU', 'Australia/Darwin'),
(44, 'AU', 'Australia/Perth'),
(45, 'AU', 'Australia/Eucla'),
(46, 'AW', 'America/Aruba'),
(47, 'AX', 'Europe/Mariehamn'),
(48, 'AZ', 'Asia/Baku'),
(49, 'BA', 'Europe/Sarajevo'),
(50, 'BB', 'America/Barbados'),
(51, 'BD', 'Asia/Dhaka'),
(52, 'BE', 'Europe/Brussels'),
(53, 'BF', 'Africa/Ouagadougou'),
(54, 'BG', 'Europe/Sofia'),
(55, 'BH', 'Asia/Bahrain'),
(56, 'BI', 'Africa/Bujumbura'),
(57, 'BJ', 'Africa/Porto-Novo'),
(58, 'BL', 'America/St_Barthelemy'),
(59, 'BM', 'Atlantic/Bermuda'),
(60, 'BN', 'Asia/Brunei'),
(61, 'BO', 'America/La_Paz'),
(62, 'BQ', 'America/Kralendijk'),
(63, 'BR', 'America/Noronha'),
(64, 'BR', 'America/Belem'),
(65, 'BR', 'America/Fortaleza'),
(66, 'BR', 'America/Recife'),
(67, 'BR', 'America/Araguaina'),
(68, 'BR', 'America/Maceio'),
(69, 'BR', 'America/Bahia'),
(70, 'BR', 'America/Sao_Paulo'),
(71, 'BR', 'America/Campo_Grande'),
(72, 'BR', 'America/Cuiaba'),
(73, 'BR', 'America/Santarem'),
(74, 'BR', 'America/Porto_Velho'),
(75, 'BR', 'America/Boa_Vista'),
(76, 'BR', 'America/Manaus'),
(77, 'BR', 'America/Eirunepe'),
(78, 'BR', 'America/Rio_Branco'),
(79, 'BS', 'America/Nassau'),
(80, 'BT', 'Asia/Thimphu'),
(81, 'BW', 'Africa/Gaborone'),
(82, 'BY', 'Europe/Minsk'),
(83, 'BZ', 'America/Belize'),
(84, 'CA', 'America/St_Johns'),
(85, 'CA', 'America/Halifax'),
(86, 'CA', 'America/Glace_Bay'),
(87, 'CA', 'America/Moncton'),
(88, 'CA', 'America/Goose_Bay'),
(89, 'CA', 'America/Blanc-Sablon'),
(90, 'CA', 'America/Toronto'),
(91, 'CA', 'America/Nipigon'),
(92, 'CA', 'America/Thunder_Bay'),
(93, 'CA', 'America/Iqaluit'),
(94, 'CA', 'America/Pangnirtung'),
(95, 'CA', 'America/Atikokan'),
(96, 'CA', 'America/Winnipeg'),
(97, 'CA', 'America/Rainy_River'),
(98, 'CA', 'America/Resolute'),
(99, 'CA', 'America/Rankin_Inlet'),
(100, 'CA', 'America/Regina'),
(101, 'CA', 'America/Swift_Current'),
(102, 'CA', 'America/Edmonton'),
(103, 'CA', 'America/Cambridge_Bay'),
(104, 'CA', 'America/Yellowknife'),
(105, 'CA', 'America/Inuvik'),
(106, 'CA', 'America/Creston'),
(107, 'CA', 'America/Dawson_Creek'),
(108, 'CA', 'America/Fort_Nelson'),
(109, 'CA', 'America/Vancouver'),
(110, 'CA', 'America/Whitehorse'),
(111, 'CA', 'America/Dawson'),
(112, 'CC', 'Indian/Cocos'),
(113, 'CD', 'Africa/Kinshasa'),
(114, 'CD', 'Africa/Lubumbashi'),
(115, 'CF', 'Africa/Bangui'),
(116, 'CG', 'Africa/Brazzaville'),
(117, 'CH', 'Europe/Zurich'),
(118, 'CI', 'Africa/Abidjan'),
(119, 'CK', 'Pacific/Rarotonga'),
(120, 'CL', 'America/Santiago'),
(121, 'CL', 'America/Punta_Arenas'),
(122, 'CL', 'Pacific/Easter'),
(123, 'CM', 'Africa/Douala'),
(124, 'CN', 'Asia/Shanghai'),
(125, 'CN', 'Asia/Urumqi'),
(126, 'CO', 'America/Bogota'),
(127, 'CR', 'America/Costa_Rica'),
(128, 'CU', 'America/Havana'),
(129, 'CV', 'Atlantic/Cape_Verde'),
(130, 'CW', 'America/Curacao'),
(131, 'CX', 'Indian/Christmas'),
(132, 'CY', 'Asia/Nicosia'),
(133, 'CY', 'Asia/Famagusta'),
(134, 'CZ', 'Europe/Prague'),
(135, 'DE', 'Europe/Berlin'),
(136, 'DE', 'Europe/Busingen'),
(137, 'DJ', 'Africa/Djibouti'),
(138, 'DK', 'Europe/Copenhagen'),
(139, 'DM', 'America/Dominica'),
(140, 'DO', 'America/Santo_Domingo'),
(141, 'DZ', 'Africa/Algiers'),
(142, 'EC', 'America/Guayaquil'),
(143, 'EC', 'Pacific/Galapagos'),
(144, 'EE', 'Europe/Tallinn'),
(145, 'EG', 'Africa/Cairo'),
(146, 'EH', 'Africa/El_Aaiun'),
(147, 'ER', 'Africa/Asmara'),
(148, 'ES', 'Europe/Madrid'),
(149, 'ES', 'Africa/Ceuta'),
(150, 'ES', 'Atlantic/Canary'),
(151, 'ET', 'Africa/Addis_Ababa'),
(152, 'FI', 'Europe/Helsinki'),
(153, 'FJ', 'Pacific/Fiji'),
(154, 'FK', 'Atlantic/Stanley'),
(155, 'FM', 'Pacific/Chuuk'),
(156, 'FM', 'Pacific/Pohnpei'),
(157, 'FM', 'Pacific/Kosrae'),
(158, 'FO', 'Atlantic/Faroe'),
(159, 'FR', 'Europe/Paris'),
(160, 'GA', 'Africa/Libreville'),
(161, 'GB', 'Europe/London'),
(162, 'GD', 'America/Grenada'),
(163, 'GE', 'Asia/Tbilisi'),
(164, 'GF', 'America/Cayenne'),
(165, 'GG', 'Europe/Guernsey'),
(166, 'GH', 'Africa/Accra'),
(167, 'GI', 'Europe/Gibraltar'),
(168, 'GL', 'America/Godthab'),
(169, 'GL', 'America/Danmarkshavn'),
(170, 'GL', 'America/Scoresbysund'),
(171, 'GL', 'America/Thule'),
(172, 'GM', 'Africa/Banjul'),
(173, 'GN', 'Africa/Conakry'),
(174, 'GP', 'America/Guadeloupe'),
(175, 'GQ', 'Africa/Malabo'),
(176, 'GR', 'Europe/Athens'),
(177, 'GS', 'Atlantic/South_Georgia'),
(178, 'GT', 'America/Guatemala'),
(179, 'GU', 'Pacific/Guam'),
(180, 'GW', 'Africa/Bissau'),
(181, 'GY', 'America/Guyana'),
(182, 'HK', 'Asia/Hong_Kong'),
(183, 'HN', 'America/Tegucigalpa'),
(184, 'HR', 'Europe/Zagreb'),
(185, 'HT', 'America/Port-au-Prince'),
(186, 'HU', 'Europe/Budapest'),
(187, 'ID', 'Asia/Jakarta'),
(188, 'ID', 'Asia/Pontianak'),
(189, 'ID', 'Asia/Makassar'),
(190, 'ID', 'Asia/Jayapura'),
(191, 'IE', 'Europe/Dublin'),
(192, 'IL', 'Asia/Jerusalem'),
(193, 'IM', 'Europe/Isle_of_Man'),
(194, 'IN', 'Asia/Kolkata'),
(195, 'IO', 'Indian/Chagos'),
(196, 'IQ', 'Asia/Baghdad'),
(197, 'IR', 'Asia/Tehran'),
(198, 'IS', 'Atlantic/Reykjavik'),
(199, 'IT', 'Europe/Rome'),
(200, 'JE', 'Europe/Jersey'),
(201, 'JM', 'America/Jamaica'),
(202, 'JO', 'Asia/Amman'),
(203, 'JP', 'Asia/Tokyo'),
(204, 'KE', 'Africa/Nairobi'),
(205, 'KG', 'Asia/Bishkek'),
(206, 'KH', 'Asia/Phnom_Penh'),
(207, 'KI', 'Pacific/Tarawa'),
(208, 'KI', 'Pacific/Enderbury'),
(209, 'KI', 'Pacific/Kiritimati'),
(210, 'KM', 'Indian/Comoro'),
(211, 'KN', 'America/St_Kitts'),
(212, 'KP', 'Asia/Pyongyang'),
(213, 'KR', 'Asia/Seoul'),
(214, 'KW', 'Asia/Kuwait'),
(215, 'KY', 'America/Cayman'),
(216, 'KZ', 'Asia/Almaty'),
(217, 'KZ', 'Asia/Qyzylorda'),
(218, 'KZ', 'Asia/Aqtobe'),
(219, 'KZ', 'Asia/Aqtau'),
(220, 'KZ', 'Asia/Atyrau'),
(221, 'KZ', 'Asia/Oral'),
(222, 'LA', 'Asia/Vientiane'),
(223, 'LB', 'Asia/Beirut'),
(224, 'LC', 'America/St_Lucia'),
(225, 'LI', 'Europe/Vaduz'),
(226, 'LK', 'Asia/Colombo'),
(227, 'LR', 'Africa/Monrovia'),
(228, 'LS', 'Africa/Maseru'),
(229, 'LT', 'Europe/Vilnius'),
(230, 'LU', 'Europe/Luxembourg'),
(231, 'LV', 'Europe/Riga'),
(232, 'LY', 'Africa/Tripoli'),
(233, 'MA', 'Africa/Casablanca'),
(234, 'MC', 'Europe/Monaco'),
(235, 'MD', 'Europe/Chisinau'),
(236, 'ME', 'Europe/Podgorica'),
(237, 'MF', 'America/Marigot'),
(238, 'MG', 'Indian/Antananarivo'),
(239, 'MH', 'Pacific/Majuro'),
(240, 'MH', 'Pacific/Kwajalein'),
(241, 'MK', 'Europe/Skopje'),
(242, 'ML', 'Africa/Bamako'),
(243, 'MM', 'Asia/Yangon'),
(244, 'MN', 'Asia/Ulaanbaatar'),
(245, 'MN', 'Asia/Hovd'),
(246, 'MN', 'Asia/Choibalsan'),
(247, 'MO', 'Asia/Macau'),
(248, 'MP', 'Pacific/Saipan'),
(249, 'MQ', 'America/Martinique'),
(250, 'MR', 'Africa/Nouakchott'),
(251, 'MS', 'America/Montserrat'),
(252, 'MT', 'Europe/Malta'),
(253, 'MU', 'Indian/Mauritius'),
(254, 'MV', 'Indian/Maldives'),
(255, 'MW', 'Africa/Blantyre'),
(256, 'MX', 'America/Mexico_City'),
(257, 'MX', 'America/Cancun'),
(258, 'MX', 'America/Merida'),
(259, 'MX', 'America/Monterrey'),
(260, 'MX', 'America/Matamoros'),
(261, 'MX', 'America/Mazatlan'),
(262, 'MX', 'America/Chihuahua'),
(263, 'MX', 'America/Ojinaga'),
(264, 'MX', 'America/Hermosillo'),
(265, 'MX', 'America/Tijuana'),
(266, 'MX', 'America/Bahia_Banderas'),
(267, 'MY', 'Asia/Kuala_Lumpur'),
(268, 'MY', 'Asia/Kuching'),
(269, 'MZ', 'Africa/Maputo'),
(270, 'NA', 'Africa/Windhoek'),
(271, 'NC', 'Pacific/Noumea'),
(272, 'NE', 'Africa/Niamey'),
(273, 'NF', 'Pacific/Norfolk'),
(274, 'NG', 'Africa/Lagos'),
(275, 'NI', 'America/Managua'),
(276, 'NL', 'Europe/Amsterdam'),
(277, 'NO', 'Europe/Oslo'),
(278, 'NP', 'Asia/Kathmandu'),
(279, 'NR', 'Pacific/Nauru'),
(280, 'NU', 'Pacific/Niue'),
(281, 'NZ', 'Pacific/Auckland'),
(282, 'NZ', 'Pacific/Chatham'),
(283, 'OM', 'Asia/Muscat'),
(284, 'PA', 'America/Panama'),
(285, 'PE', 'America/Lima'),
(286, 'PF', 'Pacific/Tahiti'),
(287, 'PF', 'Pacific/Marquesas'),
(288, 'PF', 'Pacific/Gambier'),
(289, 'PG', 'Pacific/Port_Moresby'),
(290, 'PG', 'Pacific/Bougainville'),
(291, 'PH', 'Asia/Manila'),
(292, 'PK', 'Asia/Karachi'),
(293, 'PL', 'Europe/Warsaw'),
(294, 'PM', 'America/Miquelon'),
(295, 'PN', 'Pacific/Pitcairn'),
(296, 'PR', 'America/Puerto_Rico'),
(297, 'PS', 'Asia/Gaza'),
(298, 'PS', 'Asia/Hebron'),
(299, 'PT', 'Europe/Lisbon'),
(300, 'PT', 'Atlantic/Madeira'),
(301, 'PT', 'Atlantic/Azores'),
(302, 'PW', 'Pacific/Palau'),
(303, 'PY', 'America/Asuncion'),
(304, 'QA', 'Asia/Qatar'),
(305, 'RE', 'Indian/Reunion'),
(306, 'RO', 'Europe/Bucharest'),
(307, 'RS', 'Europe/Belgrade'),
(308, 'RU', 'Europe/Kaliningrad'),
(309, 'RU', 'Europe/Moscow'),
(310, 'RU', 'Europe/Simferopol'),
(311, 'RU', 'Europe/Volgograd'),
(312, 'RU', 'Europe/Kirov'),
(313, 'RU', 'Europe/Astrakhan'),
(314, 'RU', 'Europe/Saratov'),
(315, 'RU', 'Europe/Ulyanovsk'),
(316, 'RU', 'Europe/Samara'),
(317, 'RU', 'Asia/Yekaterinburg'),
(318, 'RU', 'Asia/Omsk'),
(319, 'RU', 'Asia/Novosibirsk'),
(320, 'RU', 'Asia/Barnaul'),
(321, 'RU', 'Asia/Tomsk'),
(322, 'RU', 'Asia/Novokuznetsk'),
(323, 'RU', 'Asia/Krasnoyarsk'),
(324, 'RU', 'Asia/Irkutsk'),
(325, 'RU', 'Asia/Chita'),
(326, 'RU', 'Asia/Yakutsk'),
(327, 'RU', 'Asia/Khandyga'),
(328, 'RU', 'Asia/Vladivostok'),
(329, 'RU', 'Asia/Ust-Nera'),
(330, 'RU', 'Asia/Magadan'),
(331, 'RU', 'Asia/Sakhalin'),
(332, 'RU', 'Asia/Srednekolymsk'),
(333, 'RU', 'Asia/Kamchatka'),
(334, 'RU', 'Asia/Anadyr'),
(335, 'RW', 'Africa/Kigali'),
(336, 'SA', 'Asia/Riyadh'),
(337, 'SB', 'Pacific/Guadalcanal'),
(338, 'SC', 'Indian/Mahe'),
(339, 'SD', 'Africa/Khartoum'),
(340, 'SE', 'Europe/Stockholm'),
(341, 'SG', 'Asia/Singapore'),
(342, 'SH', 'Atlantic/St_Helena'),
(343, 'SI', 'Europe/Ljubljana'),
(344, 'SJ', 'Arctic/Longyearbyen'),
(345, 'SK', 'Europe/Bratislava'),
(346, 'SL', 'Africa/Freetown'),
(347, 'SM', 'Europe/San_Marino'),
(348, 'SN', 'Africa/Dakar'),
(349, 'SO', 'Africa/Mogadishu'),
(350, 'SR', 'America/Paramaribo'),
(351, 'SS', 'Africa/Juba'),
(352, 'ST', 'Africa/Sao_Tome'),
(353, 'SV', 'America/El_Salvador'),
(354, 'SX', 'America/Lower_Princes'),
(355, 'SY', 'Asia/Damascus'),
(356, 'SZ', 'Africa/Mbabane'),
(357, 'TC', 'America/Grand_Turk'),
(358, 'TD', 'Africa/Ndjamena'),
(359, 'TF', 'Indian/Kerguelen'),
(360, 'TG', 'Africa/Lome'),
(361, 'TH', 'Asia/Bangkok'),
(362, 'TJ', 'Asia/Dushanbe'),
(363, 'TK', 'Pacific/Fakaofo'),
(364, 'TL', 'Asia/Dili'),
(365, 'TM', 'Asia/Ashgabat'),
(366, 'TN', 'Africa/Tunis'),
(367, 'TO', 'Pacific/Tongatapu'),
(368, 'TR', 'Europe/Istanbul'),
(369, 'TT', 'America/Port_of_Spain'),
(370, 'TV', 'Pacific/Funafuti'),
(371, 'TW', 'Asia/Taipei'),
(372, 'TZ', 'Africa/Dar_es_Salaam'),
(373, 'UA', 'Europe/Kiev'),
(374, 'UA', 'Europe/Uzhgorod'),
(375, 'UA', 'Europe/Zaporozhye'),
(376, 'UG', 'Africa/Kampala'),
(377, 'UM', 'Pacific/Midway'),
(378, 'UM', 'Pacific/Wake'),
(379, 'US', 'America/New_York'),
(380, 'US', 'America/Detroit'),
(381, 'US', 'America/Kentucky/Louisville'),
(382, 'US', 'America/Kentucky/Monticello'),
(383, 'US', 'America/Indiana/Indianapolis'),
(384, 'US', 'America/Indiana/Vincennes'),
(385, 'US', 'America/Indiana/Winamac'),
(386, 'US', 'America/Indiana/Marengo'),
(387, 'US', 'America/Indiana/Petersburg'),
(388, 'US', 'America/Indiana/Vevay'),
(389, 'US', 'America/Chicago'),
(390, 'US', 'America/Indiana/Tell_City'),
(391, 'US', 'America/Indiana/Knox'),
(392, 'US', 'America/Menominee'),
(393, 'US', 'America/North_Dakota/Center'),
(394, 'US', 'America/North_Dakota/New_Salem'),
(395, 'US', 'America/North_Dakota/Beulah'),
(396, 'US', 'America/Denver'),
(397, 'US', 'America/Boise'),
(398, 'US', 'America/Phoenix'),
(399, 'US', 'America/Los_Angeles'),
(400, 'US', 'America/Anchorage'),
(401, 'US', 'America/Juneau'),
(402, 'US', 'America/Sitka'),
(403, 'US', 'America/Metlakatla'),
(404, 'US', 'America/Yakutat'),
(405, 'US', 'America/Nome'),
(406, 'US', 'America/Adak'),
(407, 'US', 'Pacific/Honolulu'),
(408, 'UY', 'America/Montevideo'),
(409, 'UZ', 'Asia/Samarkand'),
(410, 'UZ', 'Asia/Tashkent'),
(411, 'VA', 'Europe/Vatican'),
(412, 'VC', 'America/St_Vincent'),
(413, 'VE', 'America/Caracas'),
(414, 'VG', 'America/Tortola'),
(415, 'VI', 'America/St_Thomas'),
(416, 'VN', 'Asia/Ho_Chi_Minh'),
(417, 'VU', 'Pacific/Efate'),
(418, 'WF', 'Pacific/Wallis'),
(419, 'WS', 'Pacific/Apia'),
(420, 'YE', 'Asia/Aden'),
(421, 'YT', 'Indian/Mayotte'),
(422, 'ZA', 'Africa/Johannesburg'),
(423, 'ZM', 'Africa/Lusaka'),
(424, 'ZW', 'Africa/Harare');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apps_user`
--
ALTER TABLE `apps_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_support`
--
ALTER TABLE `faq_support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `ondemand_tutoring`
--
ALTER TABLE `ondemand_tutoring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prepayments`
--
ALTER TABLE `prepayments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quizzes`
--
ALTER TABLE `quizzes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `withdrawals`
--
ALTER TABLE `withdrawals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`zone_id`),
  ADD KEY `idx_country_code` (`country_code`),
  ADD KEY `idx_zone_name` (`zone_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `apps_user`
--
ALTER TABLE `apps_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `faq_support`
--
ALTER TABLE `faq_support`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ondemand_tutoring`
--
ALTER TABLE `ondemand_tutoring`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prepayments`
--
ALTER TABLE `prepayments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `quizzes`
--
ALTER TABLE `quizzes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `withdrawals`
--
ALTER TABLE `withdrawals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `zone_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=425;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
