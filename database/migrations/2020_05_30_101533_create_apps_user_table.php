<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps_user', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->text('whoami')->nullable();
            $table->date('dob')->nullable();
            $table->string('user_email');
            $table->string('password')->nullable();
            $table->text('profile_pic')->nullable();
            $table->text('role')->nullable();
            $table->text('user_type')->nullable();
            $table->bigInteger('phone')->nullable();
            $table->text('timezone')->nullable();
            $table->integer('category_id')->nullable();
            $table->text('subscribe_category')->nullable();
            $table->text('subscribe_notification_interval')->nullable();
            $table->bigInteger('paypalaccount')->nullable();
            $table->text('registration_type')->nullable();
            $table->tinyInteger('is_verified')->default(1);
            $table->tinyInteger('is_active')->default(1);
            $table->text('registration_token')->nullable();
            $table->text('verification_token')->nullable();
            $table->timestamp('last_login')->useCurrent()->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apps_user');
    }
}
