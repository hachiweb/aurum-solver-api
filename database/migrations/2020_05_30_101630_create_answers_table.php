<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('question_id')->nullable();
            $table->integer('questioner_id')->nullable();
            $table->integer('answerer_id')->nullable();
            $table->text('category')->nullable();
            $table->text('question')->nullable();
            $table->text('title')->nullable();
            $table->text('ans_attach')->nullable();
            $table->text('answer')->nullable();
            $table->float('desired_price')->nullable();
            $table->dateTime('post')->nullable();
            $table->tinyInteger('purchase')->length(1)->default(0);
            $table->dateTime('duedate')->nullable();
            $table->tinyInteger('delivery_accepted')->length(1)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
