<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('tutor_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->text('title')->nullable();
            $table->text('question')->nullable();
            $table->tinyInteger('is_assigned')->length(1)->default(0);
            $table->text('attachment')->nullable();
            $table->float('willing_to_pay')->nullable();
            $table->tinyInteger('private_question')->length(1)->default(0);
            $table->tinyInteger('prepayment')->length(1)->default(0);
            $table->float('questioner_fee')->nullable();
            $table->float('answerer_fee')->nullable();
            $table->float('admin_fee')->nullable();
            $table->date('post_date')->nullable();
            $table->date('due_date')->nullable();
            $table->string('request_accept',1)->nullable();
            $table->string('delivery_accept',1)->nullable();
            $table->tinyInteger('is_paid')->length(1)->default(0);
            $table->tinyInteger('is_cancelled')->length(1)->nullable();
            $table->text('cancellation_reason')->nullable();
            $table->tinyInteger('is_active')->length(1)->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
