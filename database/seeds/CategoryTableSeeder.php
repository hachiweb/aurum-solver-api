<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create category
        DB::table('category')->insert([
            ['subjects' =>"Accounting",'duration' => 30,'min_percent' => 70,],
            ['subjects' =>"Architecture",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Art", 'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Asian Studies",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Astronmy", 'duration' => 45, 'min_percent' =>80,],
            ['subjects' =>"Biiology",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Bussiness",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Chemistry", 'duration' => 45, 'min_percent' =>80,],  
            ['subjects' =>"Computer Science",'duration' => 45,'min_percent' =>80],
            ['subjects' =>"Economics",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Education",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Engineering",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"English",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Finance",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Foreign Languages",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Gender Studies",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"General Studies",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Gendr Questions",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Geography",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Geology",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Health Care",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"History",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Law",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Linguistics",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Literary",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Mathematics",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Literary Studies",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Music",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Other",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Philosophy",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Physics",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Political Science",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Religious Studies",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Sociology",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Statistics",'duration' => 45,'min_percent' =>80,],
            ['subjects' =>"Urban Planning and Policy",'duration' => 45,'min_percent' =>80,],
             
          ]);
    }
}
