-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 30, 2019 at 11:24 AM
-- Server version: 10.2.27-MariaDB
-- PHP Version: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u565879516_web_services`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sgt_contact`
--

CREATE TABLE `sgt_contact` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `contact` int(10) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stw_contact`
--

CREATE TABLE `stw_contact` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `contact` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stw_contact`
--

INSERT INTO `stw_contact` (`id`, `name`, `email`, `contact`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Holmes Lowery', 'xoheb@mailinator.net', 9865321475, 'Aut est quis doloribus libero optio ut illum impedit soluta id nostrud voluptates occaecat maiores', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(2, 'Daquan Harmon', 'guqu@mailinator.net', 9632512436, 'Incidunt aliquid in qui at ducimus soluta sed', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(3, 'Daquan Harmon', 'guqu@mailinator.net', 9632512436, 'Incidunt aliquid in qui at ducimus soluta sed', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(4, 'name', 'sfytgfd@gmail.com', 9852369874, 'site', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(5, 'ContactForm', 'raphaesiftfriest@gmail.com', 375627528, 'Ciao!  shivatubewells.com \r\n \r\nWe suggesting \r\n \r\nSending your commercial proposal through the feedback form which can be found on the sites in the Communication partition. Feedback forms are filled in by our software and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This technique improve the chances that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nWhatsApp - +44 7598 509161 \r\nEmail - FeedbackForm@make-success.com', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(6, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'Look at a bonzer  wanting to thrive approach as a ripen to winning. shivatubewells.com \r\nhttp://bit.ly/2NJ0P2k', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(7, 'mohan', 'mohan@gmail.com', 7814016006, 'test message\n', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(8, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'There is a tangibles  value purchase from as a medication seeking your team. shivatubewells.com \r\nhttp://bit.ly/2NJwS2d', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(9, 'JimmieJax', 'spv529163@gmail.com', 354476655, 'Opportunism in business expansion FINANCIAL capital for NGOs entrepreneurship.FDI seeks to invest through equity, loans or guarantees, and through allocations of public funds to specific projects.', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(10, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'Take notice is  an important  visuals as a recondition to winning. shivatubewells.com \r\nhttp://bit.ly/2NJ0vAE', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(11, 'EddieAmock', 'support@monkeydigital.co', 117343455, 'What will we do to increase your DA? \r\n \r\nOur Plan is very simple yet very effective. We have researched all available sources and gained access to thousands of High DA domains that are directly indexed and re-crawled by Moz Robots on each update. So, we will build and index: \r\n \r\n- 5000 DA30+ backlinks \r\n- 2500 dofollow from high PA pages \r\n- 1000 unique domains links with high DA scores \r\n \r\nstart boosting ranks and SEO metrics with our plan today: \r\nhttp://monkeydigital.co/product/moz-da-seo-plan/ \r\n \r\nBest regards \r\nMike \r\nmonkeydigital.co@gmail.com', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(12, 'shivatubewells.com', 'micgyhaelBus@gmail.com', 217448616, 'Pacify note an astounding  value pass on instead of you. shivatubewells.com \r\nhttp://bit.ly/2NUV7L4', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(13, 'Williamdet', 'raphaesiftfriest@gmail.com', 277241778, 'Hi!  shivatubewells.com \r\n \r\nWe suggesting \r\n \r\nSending your commercial offer through the feedback form which can be found on the sites in the Communication partition. Contact form are filled in by our application and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This method improve the chances that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(14, 'YourBusinessFundingNow', 'noreply@your-business-funding-now.info', 89146411334, 'Hi, letting you know that http://Your-Business-Funding-Now.info can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://Your-Business-Funding-Now.info \r\n \r\nMinimum requirements include your company being established for at least a year and with current gross revenue of at least 120K. Eligibility and funding can be completed in as fast as 48hrs. Terms are personalized for each business so I suggest applying to find out exactly how much you can get on various terms. \r\n \r\nThis is a free service from a qualified lender and the approval will be based on the annual revenue of your business. These funds are Non-Restrictive, allowing you to spend the full amount in any way you require including business debt consolidation, hiring, marketing, or Absolutely Any Other expense. \r\n \r\nIf you need fast and easy business funding take a look at these programs now as there is limited availability: \r\n \r\nhttp://Your-Business-Funding-Now.info \r\n \r\nHave a great day, \r\nThe Your Business Funding Now Team \r\n \r\nunsubscribe/remove - http://your-business-funding-now.info/r.php?url=shivatubewells.com&id=e164', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(15, 'XaPEjVbDwGzv', 'ab2077230@gmail.com', 3943799398, '', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(16, 'Michaelwen', 'mircgyhaelBus@gmail.com', 84638323512, 'Look at unblended  inspiriting in rations of victory. shivatubewells.com  http://anuphepma.gq/gez6', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(17, 'HarryNUATE', 'edwardm@lioncourtcrypto.net', 81735995832, 'We buy all crypto currencies at good rate, with reasonable commission between sellers and the mandates. \r\n \r\nContact us with the informations below if you are or have a potential seller. \r\n \r\nTel: +353 1 4378345 \r\nFax: +353 1 6865354 \r\nEmail: edwardm@lioncourtcrypto.com \r\n \r\nThank you, \r\nCrypto currencies Purchase Department, \r\nEdward Molina.', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(18, 'Holliscessy', 'contacts@businesswebconnection.com', 85759784251, 'Are you using the internet to its greatest potential?  We are professional web developers that can help you get the most benefit from the web and make your site interactive with your users and clients, making your business more efficient and powerful than your competition. \r\nIf you are just looking to upgrade your web site to current technology or give your business a boost with data integration, we can guide you to success. \r\nhttps://www.businesswebconnection.com/ \r\ncontacts@businesswebconnection.com', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(19, 'http://v.ht/GaZiZA', 'forever_ever212@hotmail.com', 84488829599, 'http://go-4.net/fn5u', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(20, 'Thomasdat', 'noreplymonkeydigital@gmail.com', 89821934961, 'Having a better Alexa for your website will increase sales and visibility \r\n \r\nOur service is intended to improve the Global Alexa traffic rank of a website. It usually takes seven days to see the primary change and one month to achieve your desired three-month average Alexa Rank. The three-month average Alexa traffic rank is the one that Alexa.com shows on the Alexa’s toolbar. \r\n \r\nFor more information visit our website \r\nhttps://monkeydigital.co/product/alexa-rank-service/ \r\n \r\nthanks and regards \r\nMike \r\nmonkeydigital.co@gmail.com', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(21, 'Steviesnoma', 'guqing@chinagiftware.com', 89295984671, 'Wholesale Christmas Decorative Hanging  Ornaments. Online store: http://chinagiftware.com \r\nReindeer Shatterproof Ornament gold or silver Foil Finish + Glitter Finish, \r\nUnicorn Shatterproof Ornament gold or silver Foil Finish + Glitter Finish \r\n \r\nHandmade oil paintings reproductions, Museum quality oil painting reproductions for artist Pino Daeni, Henry Asencio and all Top Artists. A Custom Portrait painting From Photo Online store  http://www.canvaspainting.net/ \r\nWhatsApp: +86-13328758688', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(22, 'Sandrapsype', '', 85578192798, ' Hy there,  Look what we acquire in place of you! a kinddonation  http://esindiser.tk/3v2f', '2019-11-25 11:05:14', '0000-00-00 00:00:00'),
(23, 'Prateek', 'prateek.asthana@hachiweb.com', 8837842284, 'Test', '2019-11-28 17:58:23', '2019-11-28 17:58:23'),
(24, 'Prateek', 'prateek.asthana@hachiweb.com', 8837842284, 'Test', '2019-11-29 04:02:16', '2019-11-29 04:02:16'),
(25, 'Prateek', 'prateek.asthana@hachiweb.com', 8837842284, 'Test', '2019-11-29 04:07:49', '2019-11-29 04:07:49'),
(26, 'Mohan', 'p.harichandra2407@gmail.com', 6434163461, 'Testing', '2019-11-30 07:15:26', '2019-11-30 07:15:26'),
(27, 'Mohan', 'p.harichandra2407@gmail.com', 6434163461, 'Testing', '2019-11-30 07:15:43', '2019-11-30 07:15:43'),
(28, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Hey, this is just for testing..!', '2019-11-30 07:16:51', '2019-11-30 07:16:51'),
(29, 'Hari', 'p.harichandra2407@gmail.com', 6464646646, 'Testing', '2019-11-30 07:34:44', '2019-11-30 07:34:44'),
(30, 'Prateek', 'infohachiweb@gmail.com', 8837842284, 'Test', '2019-11-30 09:34:18', '2019-11-30 09:34:18');

-- --------------------------------------------------------

--
-- Table structure for table `stw_content`
--

CREATE TABLE `stw_content` (
  `id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `value` text NOT NULL,
  `source` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stw_content`
--

INSERT INTO `stw_content` (`id`, `type`, `value`, `source`, `created_at`, `updated_at`) VALUES
(1, 0, 'We are renowned bore-well developer in the state. We deal with 5\", 7\" & 8\" Odex drilling. We employ ODEX Percussion Down-The-Hole Hammer drilling method. It is an adaptation of the air-operated down-the-hole hammer. It uses a swing-out eccentric bit to ream the bottom of the casing. The percussion bit is a two-piece bit consisting of a concentric pilot bit behind which is an eccentric second bit that swings out to enlarge the hole diameter. Immediately above the eccentric bit is a drive sub that engages a special internal shouldered drive shoe on the bottom of the ODEX casing. The ODEX is thus pulled down by the drill stem as the hole is advanced. Cuttings blow up through the drive sub and casing annuls to a swivel conducting them to a sample collector or onto the ground.\r\n\r\nBesides drilling, we are also authorized distributor of renowned submersible pumps in the state.We have been providing drilling services in the state for last 11 year. Services offered to commercial/non commercial and government projects. We have constructed at least one borewell in every part of the state.\r\nOur team of highly experienced drillers and skilled workers discharge their duties with the aim to deliver satisfactory result. For any assistance or query regarding the bore-well drilling feel free to contact us any time.\r\nTo contact us please navigate to our contact page\r\n\r\nshiva tubewells dehradun\r\nWe own two DTH hydraulic drill with accessories truck mounted for large dia LG compressor with capacity 1000/275 cfm/psi & 900/200 cfm/psi respectively.\r\n\r\n\r\nPlease note that we also hire other rig machines in case of over booking.', 'Home', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(2, 1, 'https://shivatubewells.com/images/slide3.jpg', 'Home', '2019-11-30 09:56:34', '0000-00-00 00:00:00'),
(3, 1, 'https://shivatubewells.com/images/slide6.jpg', 'Home', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(4, 1, 'https://shivatubewells.com/images/slide4.jpg', 'Home', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(5, 1, 'https://shivatubewells.com/images/slide1.jpg', 'Home', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(6, 1, 'https://shivatubewells.com/images/slide5.jpg', 'Home', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(7, 1, 'https://shivatubewells.com/images/slide2.jpg', 'Home', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(8, 1, 'https://shivatubewells.com/images/banner1.jpg', 'Home', '2019-11-30 09:55:51', '0000-00-00 00:00:00'),
(10, 0, 'We are Dehradun based govt. regd. leading water drilling firm. We are announced officially in the year 2004 but we\'ve been working much earlier.\r\nWe hold vast drilling experience. We have expertise in 5, 7, & 8\" Odex drilling.\r\nOur targeted customer belongs to the following categories:-\r\nFarmers-Agriculture\r\nIndustrialists-Plants and Machinery industry\r\nBuilders-Real estate (building construction)\r\nGovernment- Civil construction (like bridge, flyover, road, drainage construction or hand pump installations)\r\nCultivation- Nursery, farm irrigation\r\nCommon People- For domestic use etc.\r\nWe provide complete boring/drilling service by involving our own associated materials like submersible motor pumps, reqd. pipes (like MS, GI, PVC or column), panel (starter), wires etc.\r\n\r\nBeing a responsible and trusted firm we offer free maintenance service for 1 yr after the project and 3 yrs of warranty on products.\r\n*T&C Apply.\r\nFor more detail contact us.\r\n\r\n\r\nIntroduction To ODEX Casing System\r\n\r\nThe diagrams below show a ODEX Casing system in both the drilling mode as well as the retract mode. Also shown is a ODEX Sampling system that would be used for Placer Sampling of overburden. We have in stock 5\", 7\", and 8\" ODEX systems. Other sizes are available on request. In our water exploration drilling we always set ODEX casing at the top of the hole until bedrock is encountered. This helps reduce caving of the hole and helps direct the sample into the center of the drill pipe where it belongs by the use of a Sealed Diverter between the casing and the drill rod.', 'About', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(11, 1, 'https://shivatubewells.com/images/slide1.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(12, 1, 'https://shivatubewells.com/images/slide2.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(13, 1, 'https://shivatubewells.com/images/slide3.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(14, 1, 'https://shivatubewells.com/images/slide4.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(15, 1, 'https://shivatubewells.com/images/slide5.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(16, 1, 'https://shivatubewells.com/images/slide6.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(17, 1, 'https://shivatubewells.com/images/img1.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(18, 1, 'https://shivatubewells.com/images/odex1.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(19, 1, 'https://shivatubewells.com/images/odex2.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(20, 1, 'https://shivatubewells.com/images/img2.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(21, 1, 'https://shivatubewells.com/images/img4.jpg', 'About', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(22, 0, 'Our long term vision and commited service makes us stand out of the crowd. Being a responsible firm we understand your requirement and thats why there is a maintenance department in our firm who assures your after salses service.', 'Services', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(23, 1, 'https://shivatubewells.com/images/slides/1.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(24, 1, 'https://shivatubewells.com/images/slides/2.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(25, 1, 'https://shivatubewells.com/images/slides/3.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(26, 1, 'https://shivatubewells.com/images/slides/4.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(27, 1, 'https://shivatubewells.com/images/slides/5.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(28, 1, 'https://shivatubewells.com/images/slides/6.jpg', 'Services', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(30, 0, 'WELCOME TO OUR PORTFOLIO PAGE. THE PAGE INCLUDES SOME OF OUR CLIENTS AND RECENT PROJECT SLIDES\r\n\r\n   \r\n    \r\nLARGEST BOREWELL DEVELOPER IN UTTARAKHAND\r\nMore than 1000 commercial,private/individual,government tubewells delivered since 2004\r\n\r\nOur recent borewell projects inlude Doiwala flyover bridge, multiple factories at Langha, multiple aashrams and hotels at Rishikesh, Hill View Apartment Sahastradhara, Mars Catering Mussoorie, Kalandi Medco, Poly houses at Herbertpur, Sahaspur and Badowala, Kukreja Inst, Corel Lab, Amber Interprises etc\r\n\r\nOUR CLIENTS\r\nAREA SERVED\r\n \r\nEvery region lying at the Himalayan belt are served. Instances below\r\n	       \r\nAjabpur         Koti		\r\n\r\nMahebawalla	Barautha\r\n\r\nHerbetpur 	Bariowganj\r\n\r\nVikasnagar 	Bhaniawala\r\n\r\nSelaqui 	Bhogpur	\r\n\r\nPrem Nagar 	Bhidoli\r\n\r\nBhauwala 	Chakrata \r\n\r\nRishikesh 	Chibroo\r\n\r\nRanipur 	Clement Town\r\n\r\nDoiwala 	Dakpathar\r\n\r\nKangri          Dehradun Cantt \r\n\r\nNepali Farm 	Dhakrani \r\n\r\nJolly grant 	Dehradun 		\r\n\r\nHathiBarkla         Balawala \r\n\r\nGhanghora	and many more...\r\n\r\nHarrawalla	\r\n\r\nHerbertpur	\r\n\r\nJharipani	\r\n\r\nKalsi	\r\n\r\nKolagarh	\r\n\r\n\r\n\r\n \r\nDEMONSTRATION\r\n \r\n\r\n\r\n\r\n \r\nGive us a call at\r\n+91 8869081529, +91 9412938365', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(31, 1, 'http://shivatubewells.com/css/images/1.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(32, 1, 'http://shivatubewells.com/css/images/2.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(33, 1, 'http://shivatubewells.com/css/images/2.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(34, 1, 'http://shivatubewells.com/css/images/3.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(35, 1, 'http://shivatubewells.com/css/images/4.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(36, 1, 'http://shivatubewells.com/css/images/5.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(37, 1, 'http://shivatubewells.com/css/images/6.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(39, 1, 'http://shivatubewells.com/css/images/7.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(40, 1, 'http://shivatubewells.com/css/images/8.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(41, 1, 'http://shivatubewells.com/css/images/9.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(42, 1, 'http://shivatubewells.com/css/images/10.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(43, 1, 'http://shivatubewells.com/css/images/11.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(44, 1, 'http://shivatubewells.com/css/images/12.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(45, 1, 'http://shivatubewells.com/css/images/13.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(46, 1, 'http://shivatubewells.com/css/images/14.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(47, 1, 'http://shivatubewells.com/css/images/15.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(48, 1, 'http://shivatubewells.com/css/images/16.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(49, 1, 'http://shivatubewells.com/css/images/17.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(50, 1, 'http://shivatubewells.com/css/images/18.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(51, 1, 'http://shivatubewells.com/css/images/19.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(52, 1, 'http://shivatubewells.com/css/images/20.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(53, 1, 'http://shivatubewells.com/css/images/21.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(54, 1, 'http://shivatubewells.com/css/images/22.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(55, 1, 'http://shivatubewells.com/css/images/23.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(56, 1, 'http://shivatubewells.com/css/images/24.jpg', 'Portfolio', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(57, 1, 'https://shivatubewells.com/images/slide1.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(58, 1, 'https://shivatubewells.com/images/slide2.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(59, 1, 'https://shivatubewells.com/images/slide3.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(60, 1, 'https://shivatubewells.com/images/slide4.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(61, 1, 'https://shivatubewells.com/images/slide5.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(62, 1, 'https://shivatubewells.com/images/slide6.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(63, 1, 'https://shivatubewells.com/images/slide6.jpg', 'Contacts', '2019-11-30 10:01:14', '0000-00-00 00:00:00'),
(64, 0, '\r\nHead Office : Prem Nagar\r\nBranch Office\r\n\r\nJhajra, Near Balaji Temple\r\nZip Code:248197\r\nCountry:India\r\nCity:Dehradun\r\nCell no:+91 8869081529\r\nCell no:+91 9412938365\r\nEmail:info@shivatubewells.com\r\n\r\nContact Us\r\nCall us or leave us a mail. We\'d be glad to assist', 'Contact', '2019-11-25 11:03:17', '0000-00-00 00:00:00'),
(65, 0, 'Contact Form\r\nFields marked with * are mandatory.\r\nFull Name *	\r\nEmail Address *	\r\nContact Number	\r\nYour Message *	\r\nTo help prevent automated spam, please answer this question\r\n\r\n* Using only numbers, what is 10 + 15?   \r\nEnter your answer here', 'Contact', '2019-11-25 11:03:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stw_item`
--

CREATE TABLE `stw_item` (
  `id` int(11) NOT NULL,
  `serial_number` varchar(25) DEFAULT NULL,
  `item_name` text CHARACTER SET latin1 NOT NULL,
  `unit_rate` int(11) NOT NULL,
  `particulars` text DEFAULT NULL,
  `unit` varchar(10) NOT NULL,
  `quantity` int(7) NOT NULL,
  `description` text NOT NULL,
  `cgst` tinyint(2) NOT NULL,
  `sgst` tinyint(2) NOT NULL,
  `igst` tinyint(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stw_item`
--

INSERT INTO `stw_item` (`id`, `serial_number`, `item_name`, `unit_rate`, `particulars`, `unit`, `quantity`, `description`, `cgst`, `sgst`, `igst`, `created_at`, `updated_at`) VALUES
(1, '1', 'name', 951, '1', '1', 1, 'description', 1, 1, 1, '2019-07-02 10:19:22', '0000-00-00 00:00:00'),
(2, NULL, 'name', 951, NULL, '1', 1, 'description', 0, 0, 0, '2019-07-02 10:31:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stw_quote`
--

CREATE TABLE `stw_quote` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `contact` bigint(20) NOT NULL,
  `site` text NOT NULL,
  `bore_dia` text NOT NULL,
  `yield` float DEFAULT NULL,
  `purpose` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stw_quote`
--

INSERT INTO `stw_quote` (`id`, `name`, `email`, `contact`, `site`, `bore_dia`, `yield`, `purpose`, `created_at`, `updated_at`) VALUES
(1, 'name', 'emewtgfergail@gmail.com', 9852369874, 'site', '5cm', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(2, 'name', 'emewtgfergail@gmail.com', 9852369874, '', '', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(3, 'name', 'sfytgfd@gmail.com', 9852369874, '', '', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(4, 'mohan', 'mohan@gmail.com', 7814016006, 'www.google.com', '53', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(5, '\"Mohan\"', 'mohan@gmail.com', 7814016006, '\"122\"', '\"422\"', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(6, '\"Mohan\"', 'mohan@gmail.com', 7814016006, '\"122\"', '\"422\"', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(7, '\"Mohan\"', 'mohan@gmail.com', 7814016006, '\"122\"', '\"422\"', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(8, 'name', 'email@email.com', 9475862134, 'Dehradun', '5', NULL, NULL, '2019-11-25 10:53:05', '0000-00-00 00:00:00'),
(9, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 06:51:37', '2019-11-30 06:51:37'),
(10, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 07:10:57', '2019-11-30 07:10:57'),
(11, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 07:12:56', '2019-11-30 07:12:56'),
(12, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 07:13:21', '2019-11-30 07:13:21'),
(13, 'mohan', 'mohansaimanthri03@gmail.com', 7814016006, 'Old School Ground', '50', 1.25, 'Hey, this is just for testing..!', '2019-11-30 07:18:39', '2019-11-30 07:18:39'),
(14, 'Hari', 'p.harichandra2407@gmail.com', 7816643191, 'School Ground', '36', 54, NULL, '2019-11-30 07:19:23', '2019-11-30 07:19:23'),
(15, 'Hari', 'p.harichandra2407@gmail.com', 7816643191, 'School Ground', '36', 54, NULL, '2019-11-30 07:19:29', '2019-11-30 07:19:29'),
(16, 'Prateek', 'prateek.asthana@hachiweb.com', 8837842284, 'Jhakra', '7', 3, NULL, '2019-11-30 09:33:40', '2019-11-30 09:33:40');

-- --------------------------------------------------------

--
-- Table structure for table `stw_zonal_data`
--

CREATE TABLE `stw_zonal_data` (
  `id` int(11) NOT NULL,
  `area` text NOT NULL,
  `bore_depth` int(6) NOT NULL,
  `rate` float NOT NULL,
  `unit` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `access_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@hachiweb.com', NULL, '$2y$10$gKdMZlfd1bFJUEWA0PpIG.9sSdLFnL6S1/DhSNCd1tO2v/RcGrNz.', 'BpA90FzFGgFpLW2GE00qCH52XJlkSxua3S1tx8tpWFBsc4valhMH4drZpppc', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sgt_contact`
--
ALTER TABLE `sgt_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stw_contact`
--
ALTER TABLE `stw_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stw_content`
--
ALTER TABLE `stw_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stw_item`
--
ALTER TABLE `stw_item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `person` (`id`,`created_at`);

--
-- Indexes for table `stw_quote`
--
ALTER TABLE `stw_quote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stw_zonal_data`
--
ALTER TABLE `stw_zonal_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`created_at`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sgt_contact`
--
ALTER TABLE `sgt_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stw_contact`
--
ALTER TABLE `stw_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `stw_content`
--
ALTER TABLE `stw_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `stw_item`
--
ALTER TABLE `stw_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stw_quote`
--
ALTER TABLE `stw_quote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `stw_zonal_data`
--
ALTER TABLE `stw_zonal_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
