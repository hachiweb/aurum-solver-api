<!DOCTYPE html>
<html lang="en">

<head>
    <title>Aurum Solver | Coming Soon</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{asset('images/icons/favicon.ico')}}" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/bootstrap/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/animate/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/select2/select2.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}?ver=2">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <!-- <link rel="stylesheet" href="{{asset('css/mm-vertical.css')}}"> -->
    <!--===============================================================================================-->
</head>

<body>
    <div class="bg-img1 size1 overlay1" style="background-image: url('images/bg01.jpg');">
        <div class="container">
            <div class="row py-4">
            <div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/1-Wecome.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>1. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
<div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>2. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/2-Sign-up.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/3-Dashboard.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>3. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>4. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/4-Search-Page.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/5-Ask-a-question.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>5. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>6. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/6-Ask-a-question-Copy.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/7-Answer-a-question-list.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>7. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>8. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/8-Answer-a-question-detailed.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/9-Answer-a-question-detailed-Copy.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>9. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>10. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/10-Answer-a-question.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-12 border-top-set"></div>
                <div class="col-sm-6 col-12 m-auto">
                    <div class="show-image">
                        <h4>11. Lorem Ipsum</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-12">
                    <div class="show-image">
                        <img src="{{asset('images/show-img/11-Profile.png')}}" alt="" class="">
                    </div>
                </div>
                <div class="col-12 border-top-set"></div>
            </div>
        </div>
    </div>





    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/jquery/jquery-3.2.1.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('vendorfile/bootstrap/js/bootstrap.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/select2/select2.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/countdowntime/moment.min.js')}}"></script>
    <script src="{{asset('vendorfile/countdowntime/moment-timezone.min.js')}}"></script>
    <script src="{{asset('vendorfile/countdowntime/moment-timezone-with-data.min.js')}}"></script>
    <script src="{{asset('vendorfile/countdowntime/countdowntime.js')}}"></script>

    <!--===============================================================================================-->
    <script src="js/main.js"></script>

</body>

</html>