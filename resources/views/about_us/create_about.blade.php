@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create About</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">About</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="{{route('dashboard.create_about')}}">
                                @csrf
                                <div class="form-group">
                                    <label for="title">Title:</label>
                                    <textarea class="form-control" name="title" id="title" cols="30"
                                        rows="2">
                                    </textarea>
                                </div>
                                <label for="description">Description:</label>
                                <textarea name="about" id="description" cols="30" rows="10"></textarea>
                                <div class="form-group">
                                    <button class="btn btn-success">submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->

<link rel="stylesheet" href="{{asset('js/richtext.min.css')}}">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.richtext.js')}}"></script>
<script>
$(document).ready(function() {});
$('#description').richText();
</script>

@endsection