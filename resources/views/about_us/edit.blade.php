@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create About</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">About</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="card">
                        <div class="card-body">

                            <form method="post" action="{{route('dashboard.update_about')}}">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}" id="">
                                <div class="form-group">
                                    <label for="title">Title:</label>
                                    <input type="text" class="form-control" value="{{$data->tittle}}" name="title"
                                        id="title">
                                </div>
                                <label for="description">Description:</label>
                                <textarea name="about" id="textarea" cols="30" rows="30">
                                    {{$data->about_us}}
                                 </textarea>
                                <div class="form-group">
                                    <button class="btn btn-success">update</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script> -->
<!-- <script> -->
<!-- // $('textarea').ckeditor(); -->
<!-- $('#textarea').ckeditor(); // if class is prefered. -->
<!-- </script> -->
<link rel="stylesheet" href="{{asset('js/richtext.min.css')}}">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.richtext.js')}}"></script>
<script>
$(document).ready(function() {

});
$('#textarea').richText();
</script>

@endsection