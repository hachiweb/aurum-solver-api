@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Manage About</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">About</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    @foreach($data as $about)
                    <div class="card">
                        <div class="card-body shadow-lg">
                            <h4 class="font-weight-bold"> {!!$about->tittle!!}</h4>
                            <div class="form-group">
                                {!!$about->about_us!!}
                            </div>
                            <div class="form-group shadow">
                                {{-- <a href="{{route('dashboard.del_about',['id'=>$about->id])}}" class="btn btn-danger"
                                    onclick="return confirm('Do you want to delete?')">delete
                                </a> --}}

                                <a href="" class="btn btn-danger" data-id="{{ $about->id }}">
                                    Delete
                                </a>

                                <a href="{{route('dashboard.edit_about',['id'=>$about->id])}}"
                                    class="btn btn-success">edit
                                </a>
                            </div>

                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#success-alert").hide(6000);
    $("#myWish").click(function showAlert() {
        $("#success-alert").alert();
        window.setTimeout(function() {
            $("#success-alert").alert('close');
        }, 2000);
    });
    $("#category").change(function() {
        $('.category_id').val($(this).val());
        // alert('Selected value: ' + );
    });
});

function deleteRows() {
    // event.target will be the input element.
    var td = event.target.parentNode;
    var tr = td.parentNode; // the row to be removed
    tr.parentNode.removeChild(tr);
}
$('tableId').DataTable();
</script>
{{-- delete about --}}
<script>
    $(document).ready(function(){
        $('.btn-danger').on('click', function(e) {
            e.preventDefault();
            var userId = $(this).data('id'); 
            swal({
                title: "Do you want to delete?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'GET',
                        url: '{{ route('dashboard.del_about', ':id') }}'.replace(':id', userId),
                        data: {
                            _token: '{{ csrf_token() }}',
                        },
                        success: function(response) {
                            if (response.success) {
                                swal("Success!", "Successfully deleted!", "success")
                                .then(function() {
                                    flasher.success('Successfully deleted!');
                                    setTimeout(() => {
                                        location.reload();
                                    }, 2000);
                                });
                            } else {
                                swal("Error!", "An error occurred while deleting the data.", "error");
                            }
                        },
                        error: function() {
                            swal("Error!", "An error occurred while deleting the data.", "error");
                        }
                    });
                }
            });
        });
    });
</script>
@endsection