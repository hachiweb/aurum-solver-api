<div id="loading" style="width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  position: fixed;
  display: block;
  opacity: 0.7;
  background-color: #fff;
  z-index: 99;
  text-align: center;">
  <img id="loading-image" src="{{asset('assets/loader.gif')}}" alt="Loading..." style="position: absolute;
  top: 40%;
  left: 40%;
  z-index: 100;"/>
  <h2 style="position: absolute;
  top: 45%;
  left: 45%;
  z-index: 100;">Processing...</h2>
</div>