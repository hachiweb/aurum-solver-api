<!--===============================================================================================-->
    <script src="{{asset('vendorfile/jquery/jquery-3.2.1.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('vendorfile/bootstrap/js/bootstrap.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/select2/select2.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/countdowntime/moment.min.js')}}"></script>
    <script src="{{asset('vendorfile/countdowntime/moment-timezone.min.js')}}"></script>
    <script src="{{asset('vendorfile/countdowntime/moment-timezone-with-data.min.js')}}"></script>
    <script src="{{asset('vendorfile/countdowntime/countdowntime.js')}}"></script>

    <!-- <script src="{{asset('js/mm-animate.js')}}"></script> -->
    <script>
        start = new Date();
        end = new Date("2020-05-10")
        diff = new Date(end - start);
        days = diff/1000/60/60/24;
    $('.cd100').countdown100({
        /*Set Endtime here*/
        /*Endtime must be > current time*/
        endtimeYear: 0,
        endtimeMonth: 0,
        endtimeDate: days,
        endtimeHours: 18,
        endtimeMinutes: 0,
        endtimeSeconds: 0,
        timeZone: ""
        // ex:  timeZone: "America/New_York"
        //go to " http://momentjs.com/timezone/ " to get timezone
    });
    </script>
    <!--===============================================================================================-->
    <script src="vendor/tilt/tilt.jquery.min.js"></script>
    <script>
    $('.js-tilt').tilt({
        scale: 1.1
    })
    </script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>

</body>

</html>