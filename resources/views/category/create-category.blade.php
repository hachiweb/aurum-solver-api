@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create Category</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Category</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

                    <div class="row centered-form justify-content-center">
                        <div class="col-xs-12 col-sm-8 col-md-6 ">
                            <div class="panel panel-default">
                                <div class="panel-heading">

                                    <h3 class="panel-title">Create New Category <span id="per_alert" class="text-danger"></span></h3>
                                </div>
                                <div class="jumbotron">
                                <div class="panel-body">
                                    <form action="{{route('dashboard.category.CreateCatogry')}}" id="form" role="form">
                                        {{@csrf_field()}}
                                        <div class="form-group">
                                            <label for="category_name">Category Name</label>
                                            <input autocomplete="off" id="category_name" name="category_name" type="text"
                                                class="form-control input-sm" placeholder="Category Name">
                                        </div>

                                        <div class="form-group">
                                            <label for="duration_time">Duration</label>
                                            <input autocomplete="off" type="number" id="duration_time" name="duration_time"
                                                class="form-control input-sm" placeholder="Duration">
                                        </div>

                                        <div class="form-group">
                                            <label for="duration_time">Duration</label>
                                            <input type="text" autocomplete="off" id="pass_percent" name="pass_percent"
                                                class="form-control input-sm" placeholder="Minimum Percentage">
                                        </div>
                                        <input type="submit" value="Create" class="btn btn-info btn-block">
                                    </form>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
@if(Session::has('message'))
var type = "{{ Session::get('alert-type', 'info') }}";
switch (type) {
    case 'info':
        flasher.info("{{ Session::get('message') }}");
        break;

    case 'warning':
        flasher.warning("{{ Session::get('message') }}");
        break;

    case 'success':
        flasher.success("{{ Session::get('message') }}");
        break;

    case 'error':
        flashser.error("{{ Session::get('message') }}");
        break;
}
@endif

$(document).ready(function() {
    let rowIndex = 0;
    $("#AddRow").click(function() {
        $('#maintable tr:last').after(`<tr>
        <td><input name="category_name${rowIndex++}" type="text" class="form-control"></td>
        <td><input type="text" name="duration_time${rowIndex++}" class="form-control"></td>
        <td><input type="text" name="pass_percent${rowIndex++}" class="form-control"></td>
        <td><button type="button"id="delete" onclick="deleteRows()"><i class="far fa-trash-alt"></i></button></td>
        </tr>`);
    });
});
$('#pass_percent').keyup(function() {
    if ($(this).val() > 100) {
        $('#per_alert').addClass("alert").text('Percentage not be more than 100!').show().fadeOut(
            3000);
        $(this).val('100');
    } else if ($(this).val() < 0) {
        $('#per_alert').addClass("alert").text('Percentage not be less than 0!').show().fadeOut(
            3000);
        $(this).val('100');
    }
});
$("#pass_percent").on("input", function(evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57)) 
     {
	   evt.preventDefault();
       $('#per_alert').addClass("alert").text('Only number is valid!').show().fadeOut(1000);
     }
 });
function deleteRows() {
    // event.target will be the input element.
    var td = event.target.parentNode;
    var tr = td.parentNode; 
    tr.parentNode.removeChild(tr);
}
</script>
@endsection