@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Manage Category</h1>
                    @if (session('update'))
                    <div class="alert alert-warning" role="alert">
                        <h1>{{session('update')}}</h1>
                    </div>
                    @endif
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Category</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="card-body table-responsive p-0">
                                    <table id="dtHorizontalExample" class="table table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>CATEGORY</th>
                                                <th>DURATION</th>
                                                <th>PASSING PERCENTAGE</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         
                                            @foreach($data as $category)

                                            <tr>
                                                <td>{{$category->id}}</td>
                                                <td>{{$category->subjects}}</td>
                                                <td>{{$category->duration}} min</td>
                                                <td>{{$category->min_percent}} %</td>
                                                <td>
                                                    <a href="{{route('dashboard.category.edit',['id'=>$category->id])}}"
                                                        class="btn btn-success"><i class="far fa-edit"></i>
                                                    </a>

                                                    {{-- <a href="{{route('dashboard.category.delcategory',['id'=>$category->id])}}"
                                                        onclick="return confirm('Are you sure ? to delete!')" class="btn btn-danger">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a> --}}

                                                    <a href="" class="btn btn-danger delete-btn" data-id="{{ $category->id }}">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- modal -->
    <div class="bs-example">
        <!-- Button HTML (to Trigger Modal) -->
        <!-- <input type="button" class="btn btn-lg btn-primary launch-modal" value="Add"> -->

        <!-- Modal HTML -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6>Update Category</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    </div>
                    <div class="modal-body">
                        <form action="" class="shadow-lg p-3">
                            <div class="form-group">
                                <label for="name_category">Category Name</label>
                                <input type="text" name="name_category" class="form-control" id="name_category">
                            </div>
                            <div class="form-group">
                                <label for="duration_time">Duration</label>
                                <input type="text" name="duration_time" class="form-control" id="duration_time">
                            </div>
                            <div class="form-group">
                                <label for="passing_marks">Passing Percentage</label>
                                <input type="text" name="passing_marks" class="form-control" id="passing_marks">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- modal end -->
    
    <script>
        $('#dtHorizontalExample').DataTable();
        $(document).ready(function() {
            // @if(Session::has('message'))
            // var type = "{{Session::get('alert-type','info')}}"
            // switch (type) {
            //     case 'info':
            //         toastr.info("{{ Session::get('message') }}");
            //         break;
            //     case 'success':
            //         toastr.success("{{ Session::get('message') }}");
            //         break;
            //     case 'warning':
            //         toastr.warning("{{ Session::get('message') }}");
            //         break;
            //     case 'error':
            //         toastr.error("{{ Session::get('message') }}");
            //         break;
            // }
            // @endif
        });
    </script>

    {{-- delete-category --}}
    <script>
        $(document).ready(function(){
            $('.btn-danger').on('click', function(e) {
                e.preventDefault();
                var userId = $(this).data('id'); 
                swal({
                    title: "Are you sure? to delete!",
                    text: "Once deleted, you will not be able to recover this data!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: 'GET',
                            url: '{{ route('dashboard.category.delcategory', ':id') }}'.replace(':id', userId),
                            data: {
                                _token: '{{ csrf_token() }}',
                            },
                            success: function(response) {
                                if (response.success) {
                                    swal("Success!", "Successfully deleted!", "success")
                                    .then(function() {
                                        flasher.success('Successfully deleted!');
                                        setTimeout(() => {
                                            location.reload();
                                        }, 2000);
                                    });
                                } else {
                                    swal("Error!", "An error occurred while deleting the category.", "error");
                                }
                            },
                            error: function() {
                                swal("Error!", "An error occurred while deleting the category.", "error");
                            }
                        });
                    }
                });
            });
        });
    </script>
    @endsection