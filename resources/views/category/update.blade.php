@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Update Category</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Update</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="row justify-content-center">
                                <div class="col-lg-6">

                                    <form action="{{route('dashboard.category.update',['id'=>$data->id])}}" class="shadow-lg p-3">
                                        <div class="form-group">
                                            <input type="hidden" name="id" class="form-control" value="{{$data->id}}"
                                                id="name_category">
                                            <label for="name_category">Category Name</label>
                                            <input type="text" name="name_category" class="form-control"
                                                value="{{$data->subjects}}" id="name_category">
                                        </div>
                                        <div class="form-group">
                                            <label for="duration_time">Duration <span id="duration"></span></label>
                                            <input type="text" value=" {{$data->duration}} " name="duration_time"
                                                class="form-control  " id="duration_time">
                                        </div>
                                        <div class="form-group">
                                            <label for="passing_marks">Passing Percentage <span class="alert_err"id="passing"></span></label>
                                            <input type="text" value="{{$data->min_percent}}" name="passing_marks"
                                                class="form-control numbervalidation" id="passing_marks">
                                        </div>
                                        <div class="modal-footer">
                                            <a href="{{url('/dashboard/category/manage')}}" class="btn btn-danger">
                                                Cancel
                                            </a>
                                            <button type="submit" name="submit" class="btn btn-primary">Save
                                                changes
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
@if(Session::has('message'))
var type = "{{Session::get('alert-type','info')}}"
switch (type) {
    case 'info':
        flasher.info("{{ Session::get('message') }}");
        break;
    case 'success':
        flasher.success("{{ Session::get('message') }}");
        break;
    case 'warning':
        flasher.warning("{{ Session::get('message') }}");
        break;
    case 'error':
        flasher.error("{{ Session::get('message') }}");
        break;
}
@endif

$('#passing_marks').keyup(function() {
    if ($(this).val() > 100) {
        $('#passing').addClass("alert").text('Percentage not be more than 100!').show().fadeOut(
            3000);
        $(this).val('100');
    } else if ($(this).val() < 0) {
        $('#passing').addClass("alert").text('Percentage not be less than 0!').show().fadeOut(
            3000);
        $(this).val('100');
    }

});
$(".numbervalidation").on("input", function(evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57)) 
     {
	   evt.preventDefault();
       $('.alert_err').addClass("alert").text('Only number is valid!').show().fadeOut(1000);
     }
 });
 $("#duration_time").on("input", function(evt) {
    var self = $(this);
    self.val(self.val().replace(/[^\d].+/, ""));
    if ((evt.which < 48 || evt.which > 57)) 
     {
	   evt.preventDefault();
       $('#duration').addClass("alert").text('Only number is valid!').show().fadeOut(1000);
     }
 });
</script>

@endsection