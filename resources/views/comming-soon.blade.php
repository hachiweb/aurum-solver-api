<!DOCTYPE html>
<html lang="en">

<head>
    <title>Aurum Solver | Coming Soon</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{asset('images/icons/favicon.ico')}}" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/bootstrap/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/animate/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/select2/select2.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}?ver=2">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <!-- <link rel="stylesheet" href="{{asset('css/mm-vertical.css')}}"> -->
    <!--===============================================================================================-->
</head>

<body>
    <div class="bg-img1 size1 overlay1" style="background-image: url('images/bg01.jpg');">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                    <li><a href="/about" class="nav-link text-white">About The Application</a></li> 
                    <li><a href="{{route('supports')}}" class="nav-link text-white">Support/FAQ</a></li> 
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <!-- @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif -->
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('dashboard') }}"
                                       >
                                        {{ __('dashboard') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>       
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            
                        @endguest
                        
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <!-- <a href="/about" class="btn btn-custom-link">About The Application</a>
    <a href="{{url('/login')}}" class="btn btn-danger">login</a> -->
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 my-auto">
                    <div class="text-center">
					<a href="https://play.google.com/store/apps/details?id=com.app.aurum_solver.aurum_solver&hl=en&gl=US" title="click here" target="_black">
                        <div class="mobile">
                            <img src="{{asset('images/mobile-removebg-preview.png')}}" alt="" class="mobi">
                            <img src="{{asset('images/logo-remove.png')}}" alt="" class="logo">
                        </div>
                    </a>
					</div>
                </div>
                <div class="col-lg-8 col-md-12 my-auto">
                    <div class="">
                        <p class="txt-center l1-txt1">
                            Our Mobile Application is <br><span class="l1-txt2">Under Development.</span><br>COMMING
                            SOON!
                        </p>
                    </div>
                    <div class="row cd100">
                        <div class="col-md-3 col-6">
                            <div class="flex-col-c-m size2 mx-auto bor1 m-b-20 m-l-10 m-r-10">
                                <span class="l1-txt3 p-b-9 days">35</span>
                                <span class="s1-txt2">Days</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="flex-col-c-m size2 mx-auto bor1 m-b-20 m-l-10 m-r-10">
                                <span class="l1-txt3 p-b-9 hours">17</span>
                                <span class="s1-txt2">Hours</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="flex-col-c-m size2 mx-auto bor1 m-b-20 m-l-10 m-r-10">
                                <span class="l1-txt3 p-b-9 minutes">50</span>
                                <span class="s1-txt2">Minutes</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="flex-col-c-m size2 mx-auto bor1 m-b-20 m-l-10 m-r-10">
                                <span class="l1-txt3 p-b-9 seconds">39</span>
                                <span class="s1-txt2">Seconds</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/jquery/jquery-3.2.1.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('vendorfile/bootstrap/js/bootstrap.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/select2/select2.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('vendorfile/countdowntime/moment.min.js')}}"></script>
    <script src="{{asset('vendorfile/countdowntime/moment-timezone.min.js')}}"></script>
    <script src="{{asset('vendorfile/countdowntime/moment-timezone-with-data.min.js')}}"></script>
    <script src="{{asset('vendorfile/countdowntime/countdowntime.js')}}"></script>

    <!-- <script src="{{asset('js/mm-animate.js')}}"></script> -->
    <script>
        start = new Date();
        end = new Date("2020-05-10")
        diff = new Date(end - start);
        days = diff/1000/60/60/24;
    $('.cd100').countdown100({
        /*Set Endtime here*/
        /*Endtime must be > current time*/
        endtimeYear: 0,
        endtimeMonth: 0,
        endtimeDate: days,
        endtimeHours: 18,
        endtimeMinutes: 0,
        endtimeSeconds: 0,
        timeZone: ""
        // ex:  timeZone: "America/New_York"
        //go to " http://momentjs.com/timezone/ " to get timezone
    });
    </script>
    <!--===============================================================================================-->
    <script src="vendor/tilt/tilt.jquery.min.js"></script>
    <script>
    $('.js-tilt').tilt({
        scale: 1.1
    })
    </script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>

</body>

</html>