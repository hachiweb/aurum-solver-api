<style>
    .text-hover:hover
    {
        color:black !important;
    }
</style>
<div class="container-fluid mt-4">
    <div class="row">
        <div class="col-md-12 col-sm-6">
            <!-- <div class="mt-5 row cd100"> -->
            <!-- <div class="page-content page-container" id="page-content"> -->
            <!-- <div class="padding"> -->
            <div class="row">
                <div class="col-lg-3 col">
                </div>
                <div class="col-lg-6 col">
                    <div class="row">
                        <div class="col-sm-6 text-right mt-3">
                            <a href=" https://apps.apple.com/in/app/aurum-solver/id1521390147" class="mr-3 btn btn-outline-dark btn-icon-text  text-white download-link"> <i class="fa fa-apple btn-icon-prepend mdi-36px app-store"></i> <span class="d-inline-block app-block"> <small class="font-weight-light d-block">  &nbsp;  &nbsp;&nbsp;Get
                                        it on the &nbsp; &nbsp; </small>App Store
                                </span>
                            </a>
                        </div>
                        <div class="col-sm-6 text-left mt-3">
                            <a href=" https://play.google.com/store/apps/details?id=com.app.aurum_solver.aurum_solver&hl=en&gl=US" class="btn btn-outline-dark btn-icon-text  text-white download-link" style="float:left;">
                                <!-- <i class=""></i> -->
                                <i class="fab fa-google-play btn-icon-prepend mdi-36px google-store"></i> <span class="d-inline-block  app-block"><small class="font-weight-light d-block">Available on
                                        the</small> Google Store</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class=" col-lg-3 col">
                </div>
            </div>
            <!-- </div> -->
            <!-- </div> -->
            <!-- </div> -->
        </div>
    </div>
</div>
<footer class="main-footer text-white" style="background: rgba(86, 61, 124,0.5);text-align:center;">
    <strong>Copyright &copy; 2020 - <?= date('Y'); ?>
        <a class="text-white text-hover" href="{{config('APP_URL')}}">{{env('APP_NAME','Think Pair')}}</a>.
    </strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">

    </div>
</footer>
<!--===============================================================================================-->
<script src="{{asset('vendorfile/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('vendorfile/bootstrap/js/popper.js')}}"></script>
<script src="{{asset('vendorfile/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('vendorfile/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('vendorfile/countdowntime/moment.min.js')}}"></script>
<script src="{{asset('vendorfile/countdowntime/moment-timezone.min.js')}}"></script>
<script src="{{asset('vendorfile/countdowntime/moment-timezone-with-data.min.js')}}"></script>
<script src="{{asset('vendorfile/countdowntime/countdowntime.js')}}"></script>

<!-- <script src="{{asset('js/mm-animate.js')}}"></script> -->
<script>
    start = new Date();
    end = new Date("2020-05-10")
    diff = new Date(end - start);
    days = diff / 1000 / 60 / 60 / 24;
    $('.cd100').countdown100({
        /*Set Endtime here*/
        /*Endtime must be > current time*/
        endtimeYear: 0,
        endtimeMonth: 0,
        endtimeDate: days,
        endtimeHours: 18,
        endtimeMinutes: 0,
        endtimeSeconds: 0,
        timeZone: ""
        // ex:  timeZone: "America/New_York"
        //go to " http://momentjs.com/timezone/ " to get timezone
    });
</script>
<!--===============================================================================================-->
<script src="vendor/tilt/tilt.jquery.min.js"></script>
<script>
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="js/main.js"></script>

<script>
    // window.onscroll = function() {myFunction()};

    // var header = document.getElementById("aurum-header");
    // var sticky = header.offsetTop;

    // function myFunction() {
    //   if (window.pageYOffset > sticky) {
    //     header.classList.add("sticky");
    //   } else {
    //     header.classList.remove("sticky");
    //   }
    // }
</script>

</body>

</html>