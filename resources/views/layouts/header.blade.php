<!DOCTYPE html>
<html lang="en">
<head>
    {{-- <title>Aurum Solver | Coming Soon</title> --}}
    <title>Think Pair | Coming Soon</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{asset('images/think-pair-2.png')}}" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/bootstrap/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/animate/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/select2/select2.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}?ver=2">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
    <!-- https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css			copycopy script -->
    <!-- Bootstrap JS	https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js			copycopy script -->
    <!-- Icons	https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">
    
    <link rel="stylesheet" href="{{asset('custom/style.css')}}">
    <!--===============================================================================================-->
    <style>
    .header 
    {
        padding: 10px 16px;
        color: #f1f1f1;
        background: rgba(86, 61, 124,0.5);
        /* background: linear-gradient(167deg, rgba(243,0,0,1) 0%, rgba(158,26,26,1) 35%);text-align:center;     */
    }
    .container-fluid 
    {
        padding: 16px;
    }
    .sticky 
    {
      position: fixed;
      top: 0;
      width: 100%;
    }
    .sticky + .container-fluid 
    {
      padding-top: 102px;
    }
    @media(max-width:540px)
    {
        .d-block-sm
        {
            display: none;
        }
    }
    </style>
</head>
<body>
    <div class="bg-img1 size1 overlay1" style="background-image: url('images/bg01.jpg');">
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel header sticky-top" id="aurum-header">
                <div class="container">
                    <a class="navbar-brand text-white" href="{{ url('/') }}">
                    <img src="{{asset('images/think-pair-2.png')}}" class="rounded" height="70" class="" alt="Think Pair Logo">
                      {{-- <span class="d-block-sm">{{ config('app.name', 'Aurum Solver') }}</span> --}}
                      <span class="d-block-sm">{{ config('', 'Think Pair') }}</span>
                    </a>
                    <button class="navbar-toggler bg-white" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            {{-- <li><a href="/about" class="nav-link text-white {{ Request::is('about*') ? 'active text-primary btn-lg' : '' }}">About The Application</a> --}}
                                <li><a href="/about" class="nav-link text-white">About The Application</a>
                            </li>
                            {{-- <li><a href="{{route('supports')}}" class="nav-link text-white {{ Request::is('support*') ? 'active text-primary btn-lg' : '' }}">Support/FAQ</a> --}}
                                <li><a href="{{route('supports')}}" class="nav-link text-white">Support/FAQ</a>
                            </li>
                            <!-- Authentication Links -->
                            @guest
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <!-- @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif -->
                            @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('dashboard') }}">
                                        {{ __('dashboard') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>