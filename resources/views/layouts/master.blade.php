 <!DOCTYPE html>
 <html lang="en">

 <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta http-equiv="x-ua-compatible" content="ie=edge">

     <title>Think Pair | Coming soon</title>
     {{-- @toastr_css --}}
     <link rel="icon" type="image/png" href="{{asset('images/icons/favicon.ico')}}" />
     <!-- Font Awesome Icons -->
     <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
     <!-- IonIcons -->
     <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
     <!-- Theme style -->
     <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
     <link rel="stylesheet" href="{{asset('custom/style.css')}}">
     <!-- Google Font: Source Sans Pro -->

     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
     <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
     {{-- @jquery
     @toastr_js --}}
     <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

     {{-- flahser, sweetalert and jquery cdn --}}
     <link rel="stylesheet" href="https://unpkg.com/@flasher/flasher/dist/flasher.css">
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.19/dist/sweetalert2.min.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/@flasher/flasher@1.2.4/dist/flasher.min.js"></script>

     <style>
     @media (min-width: 800px) 
     {
         .modal-dialog 
         {
             max-width: 900px !important;
         }
     }
     </style>
 </head>

 <body class="hold-transition sidebar-mini">
     <div class="wrapper">
         <!-- Navbar -->
         <nav class="main-header navbar navbar-expand navbar-white navbar-light">
             <!-- Left navbar links -->
             <ul class="navbar-nav">
                 <li class="nav-item">
                     <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                 </li>
                 <li class="nav-item d-none d-sm-inline-block">
                     <a href="/" class="nav-link">Home</a>
                 </li>
                 <!-- <li class="nav-item d-none d-sm-inline-block">
                     <a href="#" class="nav-link">Contact</a>
                 </li> -->
             </ul>

             <!-- SEARCH FORM -->
             <!-- <form class="form-inline ml-3">
                 <div class="input-group input-group-sm">
                     <input class="form-control form-control-navbar" type="search" placeholder="Search"
                         aria-label="Search">
                     <div class="input-group-append">
                         <button class="btn btn-navbar" type="submit">
                             <i class="fas fa-search"></i>
                         </button>
                     </div>
                 </div>
             </form> -->

             <!-- Right navbar links -->
             <ul class="navbar-nav ml-auto">
                 <!-- Messages Dropdown Menu -->
                 <?php /*
                 <li class="nav-item dropdown">
                     <a class="nav-link" data-toggle="dropdown" href="#">
                         <i class="far fa-comments"></i>
                         <span class="badge badge-danger navbar-badge">3</span>
                     </a>
                     <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                         <a href="#" class="dropdown-item">
                             <!-- Message Start -->
                             <div class="media">
                                 <img src="dist/img/user1-128x128.jpg" alt="User Avatar"
                                     class="img-size-50 mr-3 img-circle">
                                 <div class="media-body">
                                     <h3 class="dropdown-item-title">
                                         Brad Diesel
                                         <span class="float-right text-sm text-danger"><i
                                                 class="fas fa-star"></i></span>
                                     </h3>
                                     <p class="text-sm">Call me whenever you can...</p>
                                     <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                 </div>
                             </div>
                             <!-- Message End -->
                         </a>
                         <div class="dropdown-divider"></div>
                         <a href="#" class="dropdown-item">
                             <!-- Message Start -->
                             <div class="media">
                                 <img src="dist/img/user8-128x128.jpg" alt="User Avatar"
                                     class="img-size-50 img-circle mr-3">
                                 <div class="media-body">
                                     <h3 class="dropdown-item-title">
                                         John Pierce
                                         <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                                     </h3>
                                     <p class="text-sm">I got your message bro</p>
                                     <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                 </div>
                             </div>
                             <!-- Message End -->
                         </a>
                         <div class="dropdown-divider"></div>
                         <a href="#" class="dropdown-item">
                             <!-- Message Start -->
                             <div class="media">
                                 <img src="dist/img/user3-128x128.jpg" alt="User Avatar"
                                     class="img-size-50 img-circle mr-3">
                                 <div class="media-body">
                                     <h3 class="dropdown-item-title">
                                         Nora Silvester
                                         <span class="float-right text-sm text-warning"><i
                                                 class="fas fa-star"></i></span>
                                     </h3>
                                     <p class="text-sm">The subject goes here</p>
                                     <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                 </div>
                             </div>
                             <!-- Message End -->
                         </a>
                         <div class="dropdown-divider"></div>
                         <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                     </div>
                 </li>
                 */?>

                 <li class="nav-item dropdown">
                     <a id="navbarDropdown" class="nav-link dropdown-toggle font-weight-bold" href="#" role="button"
                         data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                         {{ Auth::user()->name }}
                     </a>

                     <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                         <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                             {{ __('Logout') }}
                         </a>
                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             @csrf
                         </form>
                     </div>
                 </li>
             </ul>
         </nav>
         <!-- Main Sidebar Container -->
         <aside class="main-sidebar sidebar-dark-primary elevation-4">
             <!-- Brand Logo -->
             <a href="" class="brand-link">
                 <img src="{{asset('images/icons/favicon.ico')}}" alt="Think | Pair"
                     class="brand-image img-circle elevation-3" style="opacity: .8; height: auto; width: 2.1rem;">
                 {{-- <span class="brand-text font-weight-bold">Aurum | Solver</span> --}}
                 <span class="brand-text font-weight-bold">Think | Pair</span>
             </a>

             <!-- Sidebar -->
             <div class="sidebar">
                 <!-- Sidebar user panel (optional) -->
                 <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                     <div class="image">
                         <img src="{{asset('images/icons/favicon.ico')}}" class="img-circle elevation-2"
                             alt="User Image">
                     </div>
                     <div class="info">
                         @role('admin')
                         <a href="#" class="d-block font-weight-bold"> {{ Auth::user()->name }}</a>
                         @endrole
                         @role('moderator')
                         <a href="#" class="d-block font-weight-bold"> {{ Auth::user()->name }}</a>
                         @endrole
                         @role('user')
                         <a href="#" class="d-block font-weight-bold"> {{ Auth::user()->name }}</a>
                         @endrole
                     </div>
                 </div>

                 <!-- Sidebar Menu -->
                 <nav class="mt-2">
                     <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                         data-accordion="false">
                         <li class="nav-item has-treeview menu-open">
                             <a href="/dashboard" class="nav-link {{ Route::is('dashboard') ? 'active' : '' }}">
                                 <i class="nav-icon fas fa-tachometer-alt" style="font-size:16px"></i>
                                 <p class="ml-2 font-weight-bold">
                                     Dashboard
                                     <!-- <i class="right fas fa-angle-left"></i> -->
                                 </p>
                             </a>

                         </li>
                         <!-- <li class="nav-item">
                             <a href="{{route('dashboard.myprofile')}}" class="nav-link">
                                 <i class="nav-icon fa fa-user"></i>
                                 <p>Profile</p>
                             </a>
                         </li> -->
                         @role('admin|moderator')
                         <li class="nav-item has-treeview {{ Request::is('dashboard/create/new/user*','dashboard/users*') ? 'menu-open' : '' }}">
                             <a href="#" class="nav-link {{ Request::is('dashboard/create/new/user*','dashboard/users*') ? 'menu-open btn-primary' : '' }}">
                                 <i class="nav-icon fa fa-user-plus" aria-hidden="true" style="font-size:16px"></i>
                                 <p class="ml-2 font-weight-bold">
                                     User Management
                                     <i class="fas fa-angle-left right"></i>
                                 </p>
                             </a>
                             <ul class="nav nav-treeview">
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.add')}}" class="nav-link {{ Request::is('dashboard/create/new/user*') ? 'active' : '' }}">
                                         <i class="far fa-circle nav-icon"></i>

                                         <p>Create New User</p>
                                     </a>
                                 </li>
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.user.list')}}" class="nav-link {{ Request::is('dashboard/users*') ? 'active' : '' }}">
                                         <i class="far fa-circle nav-icon"></i>
                                         <p>Users</p>
                                     </a>
                                 </li>
                             </ul>
                         </li>
                         <li class="nav-item has-treeview {{ Request::is('dashboard/questions*','dashboard/assignment/questions*') ? 'menu-open' : '' }}">
                             <a href="#" class="nav-link">
                                 <i class="fas nav-icon fa-book " style="font-size:16px"></i>
                                 <p class="ml-2 font-weight-bold">
                                     Question Answer
                                     <i class="fas fa-angle-left right"></i>
                                 </p>
                             </a>
                             <ul class="nav nav-treeview">
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.all-question')}}" class="nav-link {{ Request::is('dashboard/questions*') ? 'active' : '' }}">
                                         <i class="far fa-circle nav-icon"></i>
                                         <p>All</p>
                                     </a>
                                 </li>
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.question')}}" class="nav-link {{ Request::is('dashboard/assignment/questions*') ? 'active' : '' }}">
                                         <i class="far fa-circle nav-icon"></i>
                                         <p>Assignment</p>
                                     </a>
                                 </li>

                             </ul>
                         </li>

                         <li class="nav-item has-treeview {{ Request::is('dashboard/category/create*','dashboard/category*') ? 'menu-open' : '' }}">
                             <a href="#" class="nav-link {{ Request::is('dashboard/category/create*','dashboard/category*') ? 'menu-open' : '' }}">
                             <i class="fas fa-code-branch nav-icon " style="font-size:16px"></i>
                                
                                 <p class="ml-2 font-weight-bold">
                                     Category
                                     <i class="fas fa-angle-left right"></i>
                                 </p>
                             </a>
                             <ul class="nav nav-treeview">
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.category.create')}}" class="nav-link {{ Request::is('dashboard/category/create*') ? 'active' : '' }}">
                                         <i class="far fa-circle nav-icon"></i>
                                         <p>Create Category</p>
                                     </a>
                                 </li>
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.category.manage')}}" class="nav-link {{ Request::is('dashboard/category/manage*') ? 'active' : '' }}">
                                         <i class="far fa-circle nav-icon"></i>
                                         <p>Manage Category</p>
                                     </a>
                                 </li>

                             </ul>
                         </li>
                         <li class="nav-item has-treeview {{ Request::is('dashboard/quiz/create*','dashboard/quiz/manage*') ? 'menu-open' : '' }}">
                             <a href="#" class="nav-link {{ Request::is('dashboard/quiz/create*','dashboard/quiz/manage*') ? 'menu-open' : '' }}">                          
                                 <i class="icon fas fa-brain" style="font-size:16px"></i>
                                
                                 <p class="ml-2 font-weight-bold">
                                     Quiz
                                     <i class="right fas fa-angle-left"></i>
                                 </p>
                             </a>
                             <ul class="nav nav-treeview">
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.quiz.create')}}" class="nav-link {{ Request::is('dashboard/quiz/create*') ? 'active' : '' }}">
                                         <i class="far fa-circle nav-icon"></i>
                                         <p>Create quiz</p>
                                     </a>
                                 </li>

                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.quiz.manage-quiz')}}" class="nav-link {{ Request::is('dashboard/quiz/manage*') ? 'active' : '' }}">
                                         <i class="far fa-circle nav-icon"></i>
                                         <p>Manage quiz</p>
                                     </a>
                                 </li>
                             </ul>
                         </li>

                         <li class="nav-item has-treeview {{ Request::is('dashboard/users/transaction*','dashboard/cancelled/assignment','dashboard/withdrawal/request*') ? 'menu-open' : '' }}">
                             <a href="#" class="nav-link {{ Request::is('dashboard/users/transaction*','dashboard/cancelled/assignment','dashboard/withdrawal/request*') ? 'menu-open' : '' }}">
                                 <i class="nav-icon fab fa-cc-paypal" style="font-size:16px"></i>

                                 <p class="ml-2 font-weight-bold">
                                     Payments
                                     <i class="fas fa-angle-left right"></i>
                                 </p>
                             </a>
                             <ul class="nav nav-treeview">
                                
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.user.transaction')}}" class="nav-link {{ Request::is('dashboard/users/transaction*') ? 'active' : '' }}">
                                         <i class="fas nav-icon fa-exchange-alt"></i>
                                         <p>Transaction</p>
                                     </a>
                                 </li>
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.cancel-assignment')}}" class="nav-link {{ Request::is('dashboard/cancelled/assignment*') ? 'active' : '' }}">
                                         <i class="nav-icon fas fa-money-bill-wave" aria-hidden="true"></i>
                                         <p>Refund Amount</p>
                                     </a>
                                 </li>
                                 <li class="nav-item">
                                     <a href="{{route('dashboard.withdrawal')}}" class="nav-link {{ Request::is('dashboard/withdrawal/request*') ? 'active' : '' }}">
                                         <i class="nav-icon fa fa-paypal" aria-hidden="true"></i>
                                         <p>Withdrawal Request</p>
                                     </a>
                                 </li>

                             </ul>
                         </li>
                         <!-- support and faq -->
                         <li class="nav-item has-treeview {{ Request::is('dashboard/create/faq*','dashboard/manage-support-faq') ? 'menu-open' : '' }} ">
                             <a href="#" class="nav-link {{ Request::is('dashboard/create/faq*','dashboard/manage-support-faq') ? 'menu-open' : '' }}">
                             <i class="fas nav-icon fa-question-circle" style="font-size:16px"></i>

                                 <p class="ml-2 font-weight-bold">
                                     FAQ
                                     <i class="fas fa-angle-left right"></i>
                                 </p>
                             </a>
                             <ul class="nav nav-treeview ">
                                 <li class="nav-item">
                                     <a href="{{route('dashboard.support-show')}}" class="nav-link {{ Request::is('dashboard/create/faq*') ? 'active' : '' }}">
                                     <i class="nav-icon far fa-circle text-danger"></i>
                                         <p>Create</p>
                                     </a>
                                 </li>
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.support-manage-fq')}}" class="nav-link {{ Request::is('dashboard/manage-support-faq*') ? 'active' : '' }}">
                                     <i class="nav-icon far fa-circle text-danger"></i>
                                         <p>Manage</p>
                                     </a>
                                 </li>
                             </ul>
                         </li>
                         <!-- About -->
                         <li class="nav-item has-treeview {{ Request::is('dashboard/create-about*','dashboard/manage-about') ? 'menu-open' : '' }} ">
                             <a href="#" class="nav-link {{ Request::is('dashboard/create-about*','dashboard/manage-about') ? 'menu-open' : '' }}">   
                                 <i class="nav-icon fas fa-address-card" style="font-size:16px"></i>
                                 <p class="ml-2 font-weight-bold">
                                     About
                                     <i class="fas fa-angle-left right"></i>
                                 </p>
                             </a>
                             <ul class="nav nav-treeview">

                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.show_about')}}" class="nav-link {{ Request::is('dashboard/create-about*') ? 'active' : '' }}">
                                     <i class="nav-icon far fa-circle text-danger"></i>
                                         <p>Create</p>
                                     </a>
                                 </li>
                                 <li class="nav-item ">
                                     <a href="{{route('dashboard.manage_about')}}" class="nav-link {{ Request::is('dashboard/manage-about*') ? 'active' : '' }}">
                                     <i class="nav-icon far fa-circle text-danger"></i>
                                         <p>Manage</p>
                                     </a>
                                 </li>
                                 
                             </ul>
                         </li>
                         @endrole
                         @role('user')
                         <!-- <li class="nav-item has-treeview"> -->
                         <li class="nav-item">
                             <a href="{{route('dashboard.mytransaction')}}" class="nav-link">
                                 <i class="fas nav-icon fa-exchange-alt"></i>
                                 <p>My Transaction</p>
                             </a>
                         </li>
                         @endrole
                         <!-- 
                         <li class="nav-header">LABELS</li>
                         <li class="nav-item">
                             <a href="{{url('/about')}}" class="nav-link">
                                 <i class="nav-icon far fa-circle text-danger"></i>
                                 <p class="text">About Us</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="#" class="nav-link">
                                 <i class="nav-icon far fa-circle text-danger"></i>
                                 <p class="text">Services</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="#" class="nav-link">
                                 <i class="nav-icon far fa-circle text-danger"></i>
                                 <p class="text">Contact Us</p>
                             </a>
                         </li> -->

                     </ul>
                 </nav>
             </div>
         </aside>

         @yield('content')

         <!-- Main Footer -->
         <div>
             <footer class="main-footer">
                <strong>Copyright &copy; 2020 - <?= date('Y'); ?>
                    <a class="text-muted" href="{{config('APP_URL')}}">{{env('APP_NAME','Think Pair')}}</a>.
                </strong>
                All rights reserved.
                 <div class="float-right d-none d-sm-inline-block">
                     <b>Version</b> 3.0.1
                 </div>
             </footer>
         </div>
         <!-- REQUIRED SCRIPTS -->
         <!-- jQuery -->
         {{-- @toastr_render --}}
         <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
         <script type="text/JavaScript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
         <!-- <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script> -->
         <!-- Bootstrap -->
         <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
         <!--  -->
         <script src="{{asset('dist/js/adminlte.js')}}"></script>
         <!-- OPTIONAL SCRIPTS -->
         <!-- <script src="{{asset('dist/js/pages/dashboard3.js')}}"></script> -->
 </body>
 </html>