<!DOCTYPE html>
<html lang="en">

<head>
    <title>Aurum Solver | Coming Soon</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{asset('images/icons/favicon.ico')}}" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/bootstrap/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/animate/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendorfile/select2/select2.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}?ver=2">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


    <!-- <link rel="stylesheet" href="{{asset('css/mm-vertical.css')}}"> -->
    <!--===============================================================================================-->
</head>

<body>
    @toastr_css
    @jquery
    @toastr_js
    @toastr_render
    <div class="bg-img1 size1 overlay1" style="background-image: url('images/bg01.jpg');">
        <div class="container">
            <div class="row justify-content-lg-center">
                <div class="col-md-6 mt-5">
                    <div class="justify-content-center">
                        <div class="card">
                            <h5 class="card-header">Set New Password</h5>
                            <div class="card-body">
                                <form action="{{route('setpassword')}}" method="get" id="form-submit"
                                    class="shadow-lg p-4">
                                    <!-- @csrf -->
                                    {{ csrf_field() }}
                                    <label>New Password</label>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="password" name="password"
                                            placeholder="New Password" required onkeyup="checkPassword()">
                                        <input type="hidden" name="oobCode" value="{{isset($oobCode)?$oobCode:''}}">
                                    </div>
                                    <label>Confirm New Password <span id="massege"></span></label>
                                    <div class="form-group ">
                                        <input type="password" class="form-control" id="confrimpassword"
                                            name="confrimpassword" placeholder="Confirm Password" required>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-4">
                                            <button class="btn btn-danger ">Cancel</button>
                                            <button class="btn btn-success">Save</button>
                                        </div>
                                        <div class="col-lg-8">
                                            <p id="checkerror"></p>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--===============================================================================================-->
        <script src="{{asset('vendorfile/jquery/jquery-3.2.1.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('vendorfile/bootstrap/js/popper.js')}}"></script>
        <script src="{{asset('vendorfile/bootstrap/js/bootstrap.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('vendorfile/select2/select2.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('vendorfile/countdowntime/moment.min.js')}}"></script>
        <script src="{{asset('vendorfile/countdowntime/moment-timezone.min.js')}}"></script>
        <script src="{{asset('vendorfile/countdowntime/moment-timezone-with-data.min.js')}}"></script>
        <script src="{{asset('vendorfile/countdowntime/countdowntime.js')}}"></script>

        <!--===============================================================================================-->
        <script src="js/main.js"></script>
        <script>
        $(document).ready(function() {
            // form submit 
            $('#form-submit').submit(function(e) {
                if ($('#password').val() != $('#confrimpassword').val()) {
                    $('#checkerror').html("Password does not match!").css('color', 'red');
                    return false;
                }

                $(this).submit();
            });
        });
        // Check Password length
        function checkPassword() {
            // if ($('#password').val().length < 8) {
            //         $('#checkerror').html("Weak (should be atleast 6 characters.)");
            // }
            var number = /([0-9])/;
            var alphabets = /([a-zA-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#password').val().length < 8) {
                $('#checkerror').removeClass();
                $('#checkerror').addClass('weak-password');
                $('#checkerror').html("Weak (should be atleast 6 characters.)").css('color', '#fc5e03');
            } else {
                if ($('#password').val().match(number) && $('#password').val().match(alphabets) && $('#password').val()
                    .match(special_characters)) {
                    $('#checkerror').removeClass();
                    $('#checkerror').addClass('strong-password');
                    $('#checkerror').html("Strong");
                } else {
                    $('#checkerror').removeClass();
                    $('#checkerror').addClass('medium-password');
                    $('#checkerror').html(
                        "Medium (should include alphabets, numbers and special characters.)").css('color', 'green');
                }
            }

        }
        // signup time password matching
        $('#password, #confirmpassword').on('keyup', function() {
            if ($('#password').val() == $('#confirmpassword').val()) {
                $('#massage').html('Matching').css('color', 'green');
            } else
                $('#massage').html('Not Matching').css('color', 'red');
        });
        </script>

</body>

</html>