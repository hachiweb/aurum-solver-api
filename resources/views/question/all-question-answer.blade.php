@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">All Post Question </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Question</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="card-body ">
                                    <table id="userTable" class="table table-striped table-bordered" cellspacing="0"
                                        width="100%">
                                        <thead class="bg-dark">
                                            <tr>
                                                <th>#ID</th>
                                                <th>Questioner</th>
                                                <th>Answerer</th>
                                                <th>Question</th>
                                                <th>Answer</th>
                                                <th>Budget</th>
                                                <th>Post Date</th>
                                                <th class="text-center" >Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="maintable">
                                            @foreach($data as $q)
                                            <tr>
                                                <td>{{$q->id}}</td>
                                                <td>{{isset($q->userDetail->first_name)?$q->userDetail->first_name:'N/A'}}
                                                </td>
                                                <td>{{isset($q->tutorDetail->first_name)?$q->tutorDetail->first_name:'N/A'}}
                                                </td>
                                                <td>{{$q->question}}</td>
                                                <td>{{isset($q->total_answers_count)?$q->total_answers_count:'N/A'}}
                                                <!-- <button>show</button> -->
                                                </td>
                                                <td>${{$q->willing_to_pay}}</td>
                                                <td>{{$q->post_date}}</td>
                                                <td class="d-flex">
                                                    <a href="{{route('dashboard.user.edit-post',['id'=>$q->id])}}"
                                                        class="btn btn-dark">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    {{-- <a href="{{route('dashboard.user.delete-post',['id'=>$q->id])}}" class="btn btn-danger"
                                                        onclick="return confirm('Are you sure ? to delete')">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </a> --}}
                                                    <a href="javascript:void(0);" 
                                                    class="btn btn-danger" 
                                                    id="delete_user_{{ $q->id }}" data-id="{{ $q->id }}">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $("#success-alert").hide(6000);
    $("#myWish").click(function showAlert() {
        $("#success-alert").alert();
        window.setTimeout(function() {
            $("#success-alert").alert('close');
        }, 2000);
    });
    // delete postQuestion
    $('.btn-danger').on('click', function(e) {
        e.preventDefault();
        var userId = $(this).data('id'); 
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'GET',
                    url: '{{ route('dashboard.user.delete-post', ':id') }}'.replace(':id', userId),
                    data: {
                        _token: '{{ csrf_token() }}',
                    },
                    success: function(response) {
                        if (response.success) {
                            swal("Success!", "Post question has been removed!", "success")
                            .then(function() {
                                flasher.success('Post question has been removed!');
                                setTimeout(() => {
                                    location.reload();
                                }, 2000);
                            });
                        } else {
                            swal("Error!", "An error occurred while deleting the Post question.", "error");
                        }
                    },
                    error: function() {
                        swal("Error!", "An error occurred while deleting the Post question.", "error");
                    }
                });
            }
        });
    });
});
$('#userTable').DataTable();
</script>
@endsection