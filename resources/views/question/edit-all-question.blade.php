@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Assignment</h1>

                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-header">Question</div>

                        <div class="card-body">
                            <div class="form-group"><label for="tittle_answer"><i>Post by
                                        {{isset($question->assignerDetail->first_name)?$question->assignerDetail->first_name:''}}</i>
                                        <br>
                                 <i>Due Date {{isset($question->due_date)?$question->due_date:'yy-mm-dd'}}</i>
                                 <br>
                                
                                <strong>${{isset($question->willing_to_pay)?$question->willing_to_pay:'0.00'}}</strong></label>
                            </div>
                            <form   action="{{route('dashboard.question-update')}}" class="p-5" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="queston_id"value="{{isset($question->id)?$question->id:''}}">
                                <div class="form-group">
                                    <label for="last_name">Subject</label>
                                    <input type="text" readonly autocomplete="off"
                                        value="{{isset($question->categoryDetail->subjects)?$question->categoryDetail->subjects:''}}"
                                        class="form-control" placeholder="Subject">
                                </div>
                                <div class="form-group">
                                    <label for="tittle">Title</label>
                                    <input type="text" autocomplete="off" name="title" class="form-control" placeholder="Title"
                                        value="{{isset($question->title)?$question->title:''}}">
                                </div>
                                <div class="form-group">
                                    <label for="question">Question</label>
                                    <textarea autocomplete="off" name="question" id="question" class="form-control"
                                        cols="30"
                                        rows="3">{{isset($question->question)?$question->question:''}}</textarea>
                                </div>

                               <div class="form-group"> <button type="submit" class="btn btn-success">Update</button></div>
                               </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-header bg-dark">All Answers</div>
                        <div class="card-body ">
                            <div class="card-body" style="height:430px; overflow-y:scroll; overflow:auto;">
                                @if($answers->isNotEmpty())
                                @foreach($answers as $ans)
                                <div class="shadow p-4">
                                    <div class="form-group bg-dark">
                                    <label for="tittle_answer" class="m-3"><i>Answered By
                                                <strong class="text-secondary">{{isset($ans->userDetail->first_name)?$ans->userDetail->first_name:'Unknown'}}</strong></i>
                                            <br>
                                            <i> {{isset($ans->post)?$ans->post:'N/A'}}</i>
                                            <br>
                                            
                                        </label>
                                        <label for=""><i class="text-primary">${{isset($ans->desired_price)?$ans->desired_price:$question->willing_to_pay}}</i></label>
                                    </div>
                                    <form action="{{route('dashboard.answer-update')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="answer_id" value="{{$ans->id}}">
                                        <div class="form-group">
                                            <label for="tittle_answer">Title</label>
                                            <input type="text" autocomplete="off" name="tittle_answer"
                                                class="form-control" placeholder="Title" value="{{$ans->title}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="about">Answer</label>
                                            <textarea autocomplete="off" name="answer"  value=" "
                                                class="form-control" cols="30" rows="5">{{$ans->answer}}</textarea>
                                        </div>
                                        <div class="form-gorup">
                                            <a href="{{route('dashboard.answer-remove',['id'=>$ans->id])}}"
                                                class="btn btn-danger" onclick="return confirm('Are you sure you want to deleter?')">delete</a>
                                            <button type="submit" class="btn btn-success">save</button>
                                        </div>
                                    </form>
                                </div>
                                <hr>
                                @endforeach
                                @else
                                <div class="jumbotron">
                                    No one answer yet!
                                </div>
                                @endif
                            </div>
                            <!-- <div class="card-footer">

                                <div class="col-md-3">
                                    <button class="form-control btn btn-success">Submit</button>
                                </div>
                                <div class="col-md-3">
                                    <a href=" " class="form-control btn btn-dark"> Back</a>
                                    <button class="form-control btn btn-dark">Back</button>
                                </div>
                            </div> -->

                        </div>
                    </div>
                </div>
            </div>
         
            <!-- /.row -->

            <!-- /.container-fluid -->
        </div>
    </div>
    <!-- /.content -->
</div>
<link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js">
</script>
<script>
@if(Session::has('message'))
var type = "{{Session::get('alert-type','info')}}"
switch (type) {
    case 'info':
        toastr.info("{{ Session::get('message') }}");
        break;
    case 'success':
        toastr.success("{{ Session::get('message') }}");
        break;
    case 'warning':
        toastr.warning("{{ Session::get('message') }}");
        break;
    case 'error':
        toastr.error("{{ Session::get('message') }}");
        break;
}
@endif
$("#date_of_birth").datepicker({

    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',

});
$(document).ready(function() {
    $(".ui-datepicker-title").css("background-color", "#green", "important");
    $('#password, #cpassword').on('keyup', function() {
        if ($('#password').val() == $('#cpassword').val()) {
            $('#massage').html('Matching').css('color', 'green');
        } else
            $('#massage').html('Not Matching').css('color', 'red');
        return false;
    });
    $('#formsubmit').submit(function(e) {

        if ($('#user_email').val() == '') {
            $('#email_required').html('required').css('color', 'red');
            return false;

        } else if ($('#password').val() == '') {
            $('.pass_required').html('required').css('color', 'red');
            return false;
        } else if ($('#password').val() != $('#cpassword').val()) {
            $('.pass_required').html('Not matching').css('color', 'red');
            return false;
        }
        $(this).submit();
    });

    // hide show password
    $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
});
</script>
@endsection