@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!-- @if (session('delete'))
                    <div class="alert alert-danger" id="success-alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>Success!</strong>
                        {{session('delete')}}
                    </div>
                    @endif
                    <h1 class="m-0 text-dark">Manage Quiz</h1> -->
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Quiz</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">

                        <div class="card-body">
                            <div class="d-flex">
                                <div class="card-body table-responsive p-0">
                                <table id="dtHorizontalExample" class="table table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#ID</th>
                                                <th>Category Name </th>
                                                <th>Total Questions</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="maintable">
                                            <!-- {{print_r($data)}} -->
                                            @foreach($data as $key=>$val)
                                            <tr>
                                                <td>{{$val->id}}</td>
                                                <td>{{$val->subjects}}</td>

                                                <td>{{$val->total_quiz_count}}</td>
                                                <td class="text-center">
                                                    <a href="{{route('dashboard.quiz.create',['id'=>$val->subjects])}}" data-toggle="tooltip"
                                                        title="Create quiz!" data-placement="bottom"
                                                        class="btn btn-primary"><i class="fas fa-plus"></i></a>
                                                    <a href="/dashboard/quiz/edit/{{$val->id}}" class="btn btn-success"
                                                        data-placement="bottom" data-toggle="tooltip"
                                                        title="Update quiz"><i class="far fa-edit"></i></a>
                                                    <!-- <a href="/dashboard/quiz/list/{{$val->id}} "
                                                        class="btn btn-secondary" data-placement="bottom"
                                                        data-toggle="tooltip" title="Quiz"><i
                                                            class="fas fa-list"></i></a> -->
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                            <!-- /.d-flex -->
                        </div>
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <script>
    //   tostr alert
    $('tableId1').DataTable();
    @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}"
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
    $('#dtHorizontalExample').DataTable();
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
      
        $("#success-alert").hide(6000);
        $("#myWish").click(function showAlert() {
            $("#success-alert").alert();
            window.setTimeout(function() {
                $("#success-alert").alert('close');
            }, 2000);
        });

    });
    </script>
    @endsection