@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        <h1>{{session('status')}}</h1>
                    </div>
                    @endif
                    @if (session('status_fail'))
                    <div class="alert alert-warning" role="alert">
                        <h1>{{session('status_fail')}}</h1>
                    </div>
                    @endif
                    <h1 class="m-0 text-dark">Update quiz</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Update</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title">Manage Quiz</h3>
                                <!-- <a href="javascript:void(0);">Add more</a> -->
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table " cellspacing="0" width="100%">
                                <tr>

                                    <td colspan="3">
                                        <input type="text" readonly class="form-control font-weight-bold"
                                            value="{{$category->subjects}}">
                                    </td>

                                    <td colspan="4">
                                    </td>
                                    <td colspan="">
                                        <span class="font-weight-bold"> Total Questions</span><span
                                            id="total_questions"></span>
                                            @foreach($data as $val)
                                         {{$val->categoryQuiz->category_quiz_count}} 
                                         @endforeach
                                    </td>
                                <tr>
                            </table>

                            <div class="d-flex">
                                <div class="card-body ">
                                    <form action="/update/quiz">
                                        <input type="hidden" name="category_id" value="{{$category->id}}">
                                        <table id="dtHorizontalExample" class="table table-striped table-bordered"
                                            cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>#ID</th>
                                                    <th>Questions </th>
                                                    <th>Option 1</th>
                                                    <th>Option 2</th>
                                                    <th>Option 3</th>
                                                    <th>Option 4</th>
                                                    <th style="width:120px;">Correct Option</th>
                                                </tr>
                                            </thead>
                                            <tbody id="maintable">

                                                @foreach($data as $val)
                                                <tr>
                                                    <td><input type="number" readonly value="{{$val->id}}" name="id[]"
                                                            class="form-control"></td>

                                                    <td><input type="text" value="{{$val->questions}}"
                                                            name="questions[]" class="form-control"></td>
                                                    <td>
                                                        <input type="text" value="{{$val->opt1}}" name="opt1[]"
                                                            class="form-control" id="answer_a[]">
                                                    </td>
                                                    <td>
                                                        <input type="text" value="{{$val->opt2}}" name="opt2[]"
                                                            class="form-control" id="answer_b[]">
                                                    </td>
                                                    <td>
                                                        <input type="text" value="{{$val->opt3}}" name="opt3[]"
                                                            class="form-control" id="answer_c[]">
                                                    </td>
                                                    <td>
                                                        <input type="text" value="{{$val->opt4}}" name="opt4[]"
                                                            class="form-control" id="answer_d[]">
                                                    </td>
                                                    <td>
                                                        <select name="correct[]" value="{{$val->correct}}"
                                                            class="form-control" id="correct_answer">
                                                            @if($val->correct=='opt1')
                                                            <option class="form-control" selected value="opt1">Option 1</option>
                                                            <option class="form-control"  value="opt2">Option 2</option>
                                                            <option class="form-control" value="opt3">Option 3</option>
                                                            <option class="form-control" value="opt4">Option 4</option>
                                                            @elseif($val->correct=='opt2')
                                                            <option class="form-control" value="opt1">Option 1</option>
                                                            <option class="form-control" selected value="opt2">Option 2</option>
                                                            <option class="form-control" value="opt3">Option 3</option>
                                                            <option class="form-control" value="opt4">Option 4</option>
                                                            @elseif($val->correct=='opt3')
                                                            <option class="form-control" value="opt1">Option 1</option>
                                                            <option class="form-control" value="opt2">Option 2</option>
                                                            <option class="form-control" selected value="opt3">Option 3</option>
                                                            <option class="form-control" value="opt4">Option 4</option>
                                                            @else
                                                            <option class="form-control" value="opt1">Option 1</option>
                                                            <option class="form-control" value="opt2">Option 2</option>
                                                            <option class="form-control" value="opt3">Option 3</option>
                                                            <option class="form-control" selected value="opt4">Option 4</option>

                                                            @endif

                                                        </select>
                                                    </td>
                                                </tr>

                                                @endforeach
                                                <td>
                                                    <a href="{{url('/dashboard/manage/quiz')}}"
                                                        class="btn btn-danger ">Cancel</a>
                                                </td>
                                                <td>

                                                    <button type="submit" class="btn btn-success">Update</button>
                                                </td>
                                            </tbody>
                                        </table>
                                    </form>

                                </div>
                            </div>
                            <!-- /.d-flex -->
                        </div>
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <script>
    @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}"
    switch (type) {
        case 'info':
            flasher.info("{{ Session::get('message') }}");
            break;
        case 'success':
            flasher.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            flasher.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            flasher.error("{{ Session::get('message') }}");
            break;
    }
    @endif
    </script>
    @endsection