@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Manage Quiz</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Quiz</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="card-body ">


                                    <table id="userTable" class="table table-striped table-bordered" cellspacing="0"
                                        width="100%">
                                        <thead class="bg-dark">
                                            <tr>
                                                <th># ID</th>
                                                <th>Category Name </th>
                                                <th>Total Quiz Available</th>

                                                <th class="text-center" style=" ">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="maintable">

                                            @foreach($data as $key=>$val)
                                            <td>{{$val->id}}</td>
                                            <td>{{$val->subjects}}</td>
                                            <td class="text-center">{{$val->total_quiz_count}}</td>

                                            <td class="text-center">
                                            <a href="{{route('dashboard.quiz.create')}}" class="btn btn-primary"><i
                                                        class="fa fa-plus"></i> add</a>
                                                @if($val->total_quiz_count > 0)
                                                <a href="{{route('dashboard.quiz.show',['id'=>$val->id])}}"
                                                    title="edit quiz" class="btn btn-dark" title="Show all quiz"><i
                                                        class="fa fa-eye"></i></a>
                                                <a href="/dashboard/quiz/edit/{{$val->id}}" title="edit quiz"
                                                    class="btn btn-success"><i class="far fa-edit"></i></a>
                                                <!-- <a href="/dashboard/delete/quiz/{{$val->id}}" class="btn btn-danger"
                                                        onclick="return confirm('Are you sure ? to delete')"><i
                                                            class="fas fa-trash"></i></a> -->
                                                @else
                                                <button disabled class="btn btn-danger"><i
                                                        class="fa fa-eye-slash"></i></button>
                                                <a href="" disabled class="btn btn-success"><i
                                                        class="far fa-edit"></i></a>
                                                
                                                <!-- <a href="/dashboard/delete/quiz/{{$val->id}}" class="btn btn-danger"
                                                    disabled onclick="return confirm('Are you sure ? to delete')"><i
                                                        class="fas fa-trash"></i></a> -->
                                                @endif

                                            </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                            <!-- /.d-flex -->
                        </div>
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <!-- <h4 class="modal-title">Modal Header</h4> -->
                    </div>
                    <div class="modal-body">
                        <table id="userTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Questions </th>
                                    <th>Option 1</th>
                                    <th>Option 2</th>
                                    <th>Option 3</th>
                                    <th>Option 4</th>
                                    <th style="width:120px;">Correct Option</th>
                                </tr>
                            </thead>
                        </table>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
<!-- Modal view quiz -->
<!-- Trigger the modal with a button -->

<script>
@if(Session::has('message'))
var type = "{{Session::get('alert-type','info')}}"
switch (type) {
    case 'info':
        toastr.info("{{ Session::get('message') }}");
        break;
    case 'success':
        toastr.success("{{ Session::get('message') }}");
        break;
    case 'warning':
        toastr.warning("{{ Session::get('message') }}");
        break;
    case 'error':
        toastr.error("{{ Session::get('message') }}");
        break;
}
@endif
$(document).ready(function() {
    // $('#veiwQuiz').click(function(){
    //     var id=$('#veiwQuiz').val();
    //     alert(id);
    // });
    // $("#category_id").change(function() {
    //     var cat_id = $(this).val();
    //     //    $('#fetch').attr('href').append('/quiz/list/'+cat_id)
    //     $.ajax({
    //         type: "GET",
    //         url: "/quiz/list/" + cat_id,
    //         dataType   :'json',
    //         success: function(data){
    //             $('#total_questions').text(data.data);
    //             $.each(data, function(index, row) {

    //             })

    //         }
    //     });
    // });
    // table pagination
    // //   table responsive
    // $('#dtHorizontalExample').DataTable({
    //     "scrollX": true
    // });
    // $('.dataTables_length').addClass('bs-select');
    // $('#example').DataTable();
    $("#success-alert").hide(6000);
    $("#myWish").click(function showAlert() {
        $("#success-alert").alert();
        window.setTimeout(function() {
            $("#success-alert").alert('close');
        }, 2000);
    });

});
$('#userTable').DataTable();
</script>
@endsection