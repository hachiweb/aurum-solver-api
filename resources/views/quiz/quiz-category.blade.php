@extends('layouts.master')
@section('content')
@jquery
@toastr_js
@toastr_render
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Quiz questions</h1>

                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">All Quiz</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                         
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="card-body table-responsive p-0">
                                    <table id="userTable" class="table table-striped table-bordered" cellspacing="0"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Category Name</th>
                                                <th>Option 1</th>
                                                <th>Option 2</th>
                                                <th>Option 3</th>
                                                <th>Option 4</th>
                                                <th>Correct Option</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach($data as $quiz)
                                            <tr>
                                                <td>{{$quiz->id}}</td>
                                                <td>{{isset($quiz->categoryQuiz->subjects)?$quiz->categoryQuiz->subjects:'N/A'}}
                                                </td>
                                                <td>{{$quiz->opt1}}</td>
                                                <td>{{$quiz->opt2}}</td>
                                                <td>{{$quiz->opt3}}</td>
                                                <td>{{$quiz->opt4}}</td>
                                                <td>{{$quiz->correct}}</td>
                                                <td>
                                                    {{-- <a href="{{route('dashboard.quiz.create',['id'=>$quiz->id])}}" class="btn btn-danger"
                                                        onclick="return confirm('Are you sure ? to delete')"><i
                                                        class="fas fa-trash"></i>
                                                    </a> --}}
                                                    <a href="" class="btn btn-danger" data-id="{{$quiz->id}}">
                                                        <i class="fas fa-trash"></i>
                                                    </a> 
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- <a href="" class="btn btn-danger"
                                            onclick="return confirm('Are you sure ? to delete')"><i
                                            class="fas fa-trash"></i>
                                        </a> -->
                                </div>

                            </div>
                            <!-- /.d-flex -->
                        </div>
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
</div>
<!-- /.content -->
<script>
$('#userTable').DataTable();
$(document).ready(function() {
    // $('#delteuser').click(function() {

    //        var deleteid = $(this).val();
    //     //    alert("deleteid");
    //        alert(deleteid);

    // });
});

// function ConfirmDelete(event) {
//     var x = confirm("Are you sure you want to delete?");
//     if (x) {
//         return true;
//     } else {
//         event.preventDefault();
//         return false;
//     }
// }
</script>

{{-- delete-quiz-question --}}
<script>
    $(document).ready(function(){
        $('.btn-danger').on('click', function(e) {
            e.preventDefault();
            var userId = $(this).data('id'); 
            swal({
                title: "Are you sure? to delete!",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'GET',
                        url: '{{ route('dashboard.quiz.del', ':id') }}'.replace(':id', userId),
                        data: {
                            _token: '{{ csrf_token() }}',
                        },
                        success: function(response) {
                            if (response.success) {
                                swal("Success!", "Successfully deleted!", "success")
                                .then(function() {
                                    flasher.success('Quiz delete successfully!');
                                    setTimeout(() => {
                                        location.reload();
                                    }, 2000);
                                });
                            } else {
                                swal("Error!", "An error occurred while deleting the quiz.", "error");
                            }
                        },
                        error: function() {
                            swal("Error!", "An error occurred while deleting the quiz.", "error");
                        }
                    });
                }
            });
        });
    });
</script>

@endsection