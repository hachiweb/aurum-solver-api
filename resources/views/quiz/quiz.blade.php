@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create Quiz</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Quiz</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <select name="category" class="form-control" id="category">
                                            <option value="" disabled selected>Select Category</option>
                                            @foreach($data as $category)
                                                <option value="{{$category->id}}">{{$category->subjects}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <button class="btn btn-success float-right" id="AddRow">Add New Row</button>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="card-body table-responsive p-0">
                                    <form action="/insert/quiz" mehod="get">
                                        {{@csrf_field()}}
                                        <table class="table table-striped table-valign-middle">
                                            <thead>
                                                <tr>
                                                    <th>Questions </th>
                                                    <th>Option 1</th>
                                                    <th>Option 2</th>
                                                    <th>Option 3</th>
                                                    <th>Option 4</th>
                                                    <th>Correct Option</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="maintable">
                                                <tr>
                                                    <input type="hidden" name="category_id[]"
                                                        class="form-control category_id" value="" id="">
                                                    <td>
                                                        <textarea autocomplete="off" type="text" name="questions[]"
                                                            class="form-control" cols="30" rows="2"></textarea>

                                                    </td>
                                                    <td>
                                                        <textarea autocomplete="off" type="text" name="answer_a[]"
                                                            class="form-control" id="answer_a" cols="30"
                                                            rows="2"></textarea>

                                                    </td>
                                                    <td>
                                                        <textarea autocomplete="off" type="text" name="answer_b[]"
                                                            class="form-control" id="answer_b" cols="30"
                                                            rows="2"></textarea>
                                                    </td>
                                                    <td>
                                                        <textarea autocomplete="off" type="text" name="answer_c[]"
                                                            class="form-control" id="answer_c" cols="30"
                                                            rows="2"></textarea>
                                                    </td>
                                                    <td>
                                                        <textarea autocomplete="off" type="text" name="answer_d[]"
                                                            class="form-control" id="answer_d" cols="30"
                                                            rows="2"></textarea>
                                                    </td>
                                                    <td>
                                                        <select autocomplete="off" cols="30" rows="3"
                                                            name="correct_answer[]" class="form-control"
                                                            id="correct_answer">
                                                            <option class="form-control" value="1">Select Options
                                                            </option>
                                                            <option class="form-control" value="opt1">Option 1</option>
                                                            <option class="form-control" value="opt2">Option 2</option>
                                                            <option class="form-control" value="opt3">Option 3</option>
                                                            <option class="form-control" value="opt4">Option 4</option>
                                                        </select>
                                                        <!-- <input type="text" name="correct_answer[]" class="form-control"
                                                            id="correct_answer"> -->
                                                    </td>
                                                    <td>
                                                        <button type="button" id="delete" disabled
                                                            onclick="deleteRows()" class="btn btn-danger"><i
                                                                class="far fa-trash-alt"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- <a href="{{url('/dashboard/quiz')}}" class="ml-0 btn btn-danger">Cancel</a> -->
                                        <button class="ml-0 btn btn-primary" type="submit">Create Quiz</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.d-flex -->
                        </div>
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
<script>
@if(Session::has('message'))
var type = "{{Session::get('alert-type','info')}}"
switch (type) {
    case 'info':
        flasher.info("{{ Session::get('message') }}");
        break;
    case 'success':
        flasher.success("{{ Session::get('message') }}");
        break;
    case 'warning':
        flasher.warning("{{ Session::get('message') }}");
        break;
    case 'error':
        flasher.error("{{ Session::get('message') }}");
        break;
}
@endif

$(document).ready(function() {
    let rowIndex = 0;
    $("#AddRow").click(function() {
        $('#maintable tr:last').after(`<tr>
            <input type="hidden" name="category_id[]" class="form-control category_id"
                                                        value=" " id="">
        <td><textarea autocomplete="off" type="text" name="questions[]"
                                                            class="form-control" cols="30"
                                                            rows="2"></textarea></td>
            <td>
            <textarea autocomplete="off" type="text" name="answer_a[]"
                                                            class="form-control" id="answer_a" cols="30"
                                                            rows="2"></textarea>
            </td>
            <td>
            <textarea autocomplete="off" type="text" name="answer_b[]"
                                                            class="form-control" id="answer_b" cols="30"
                                                            rows="2"></textarea>
            </td>
            <td>
            <textarea autocomplete="off" type="text" name="answer_c[]"
                                                            class="form-control" id="answer_c" cols="30"
                                                            rows="2"></textarea>
            </td>
            <td>
            <textarea autocomplete="off" type="text" name="answer_d[]"
                                                            class="form-control" id="answer_d" cols="30"
                                                            rows="2"></textarea>
            </td>
            <td>
                <select name="correct_answer[]" required class="form-control" id="correct_answer">
                <option value="1">Select Options</option>
                <option value="opt1">Option 1</option>
                <option value="opt2">Option 2</option>
                <option value="opt3">Option 3</option>
                <option value="opt4">Option 4</option>
                </select>
            </td>
            <td>
            <button  type="button"id="delete" onclick="deleteRows()" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
            </td>
        </tr>`);
    });

    $("#success-alert").hide(6000);
    $("#myWish").click(function showAlert() {
        $("#success-alert").alert();
        window.setTimeout(function() {
            $("#success-alert").alert('close');
        }, 2000);
    });
    $("#category").change(function() {
        $('.category_id').val($(this).val());
        // alert('Selected value: ' + );
    });
});

function deleteRows() {
    // event.target will be the input element.
    var td = event.target.parentNode;
    var tr = td.parentNode; // the row to be removed
    tr.parentNode.removeChild(tr);
}
$('tableId').DataTable();
</script>
@endsection