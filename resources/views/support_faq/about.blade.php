@include('layouts.header')
<div class="container-fluid border-top">
    <div class="row">
        <div class="col-md-3">
            <div class="text-center">
                <a href="https://play.google.com/store/apps/details?id=com.app.aurum_solver.aurum_solver&hl=en&gl=US" title="click here">
                    <div class="mobile">
                        <img src="{{asset('images/mobile-removebg-preview.png')}}" alt="" class="mobi">
                        <img src="{{asset('images/think-pair-2.png')}}" alt="" class="logo rounded">
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-9">
            <div class="mx-4">
                @if($data->isNotEmpty())
                    @foreach($data as $about)
                    <div class="mt-5 row cd100">
                        <div class="card card-body">
                            <h3 class="shadow bg-info p-4 text-light">{{$about->tittle}}</h3>
                            <div>
                                {!!$about->about_us!!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
