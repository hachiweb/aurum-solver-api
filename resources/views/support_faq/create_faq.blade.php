@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create FAQ</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">FAQ</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header border-0">

                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">

                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <button class="btn btn-success float-right" id="AddRow">Add New Row</button>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <div class="d-flex">
                                        <div class="card-body table-responsive p-0">
                                            <form action="{{route('dashboard.savefaq-support')}}" method="post"
                                                class="shadow-lg p-4">
                                                {{@csrf_field()}}
                                                <div id="maintable">
                                                    <div class="form-group">
                                                        <label for="question">FAQ Question</label>
                                                        <textarea required name="question[]" id="question"
                                                            class="form-control" cols="30" rows="2"></textarea>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="answer">FAQ Answer</label>
                                                        <textarea required name="answer[]" id="description"
                                                            class="form-control description" cols="30" rows="5">
                                                    </textarea>
                                                    </div>
                                                </div>
                                                <!-- <a href="{{url('/dashboard/quiz')}}" class="ml-0 btn btn-danger">Cancel</a> -->
                                                <button class="ml-0 btn btn-success" type="submit">Save</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.d-flex -->
                        </div>
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->

<link rel="stylesheet" href="{{asset('js/richtext.min.css')}}">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.richtext.js')}}"></script>
<script>
// $('tableId').DataTable();
$(document).ready(function() {
    let rowIndex = 0;
    $("#AddRow").click(function() {
        $('#maintable').after(`<div class="form-group">
            <label for="question">FAQ Question</label>
            <textarea name="question[]" required class="form-control"
                cols="30" rows="2">
                </textarea>

                </div>
                <div class="form-group">
                    <label for="answer">FAQ Answer</label>
                    <textarea name="answer[]" required class="form-control description"
                        cols="30" rows="5">
                </textarea>
            </div>`);
    });

    $("#success-alert").hide(6000);
    $("#myWish").click(function showAlert() {
        $("#success-alert").alert();
        window.setTimeout(function() {
            $("#success-alert").alert('close');
        }, 2000);
    });
    $("#category").change(function() {
        $('.category_id').val($(this).val());
        // alert('Selected value: ' + );
    });
});

function deleteRows() {
    // event.target will be the input element.
    var td = event.target.parentNode;
    var tr = td.parentNode; // the row to be removed
    tr.parentNode.removeChild(tr);
}
$('.description').richText();
</script>
@endsection