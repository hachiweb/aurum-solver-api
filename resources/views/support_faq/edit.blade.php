@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">FAQ Edit</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card" style="min-height:80vh;">
                        <div class="card-header">
                            <h1 class="m-0 text-dark">FAQ Edit</h1>
                        </div>
                        <div class="card-body bg-secondry">
                            <div class="row justify-content-center">
                                <div class="col-lg-10">
                                    <div class="d-flex">
                                        <div class="card-body table-responsive p-0"
                                            style="height:650px; overflow-y:scroll; overflow:auto;">

                                            @if($faq)
                                            {{-- @foreach($data as $faq) --}}
                                            <div class="shadow p-2">
                                                <form action="{{route('dashboard.update-support')}}" method="get"
                                                    class="shadow-lg p-4">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$faq->id}}" id="">
                                                    <div class="form-group">
                                                        <label for="type">
                                                            Types Question
                                                        </label>
                                                    </div>
                                                    <div id="maintable">
                                                        <div class="form-group">
                                                            <label for="question">Question</label>
                                                            <textarea name="question" class="form-control" cols="30" rows="2">{{$faq->question}} </textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="answer">Answer</label>
                                                            <textarea name="answer" class="textrich form-control"
                                                                cols="30" rows="5">{{$faq->answer}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        {{-- <a href="{{route('dashboard.support-remove',['id'=>$faq->id])}}"
                                                            class="btn btn-danger"
                                                            onclick="return confirm('Do you want to delete?')">delete
                                                        </a> --}}
                                                        <a href="/dashboard/manage-support-faq" class="btn btn-danger" data-id="{{ $faq->id }}">
                                                            Cancel
                                                        </a>
                                                        <button class="ml-0 btn btn-success" type="submit">Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                            {{-- @endforeach --}}
                                            @else
                                            <div class="jumbotron">
                                                No one answer yet!
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.d-flex -->
                        </div>
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->


<link rel="stylesheet" href="{{asset('js/richtext.min.css')}}">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.richtext.js')}}"></script>
<script>
$(document).ready(function() {

});
$('.textrich').richText();
</script>
@endsection