@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Manage FAQ</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">FAQ</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="card-body ">


                                    <table id="userTable" class="table table-striped table-bordered" cellspacing="0"
                                        width="100%">
                                        <thead class="">
                                            <tr>
                                                <th># ID</th>
                                                <th>Types</th>
                                                <th>FAQ Quetions</th>
                                                <th>FAQ Answers</th>
                                                <th class="">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $faqdata)
                                            <tr>
                                                <td>{{$faqdata->id}}</td>
                                                <td>{{$faqdata->id}}</td>
                                                <td>{{$faqdata->question}}</td>
                                                <td>{{$faqdata->answer}}</td>
                                                <td class="d-flex">
                                                    <form action="{{route('dashboard.support-faq')}}"
                                                        method="GET">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$faqdata->id}}" />
                                                        <input type="hidden" name="status" value="1" />
                                                        <button class="btn btn-success mr-1" type="submit"
                                                             ><i
                                                                class="fa fa-edit" aria-hidden="true"></i>
                                                        </button>
                                                    </form>

                                                    {{-- <form action="{{route('dashboard.support-faq')}}"
                                                        method="GET">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$faqdata->id}}" />
                                                        <input type="hidden" name="status" value="0" />
                                                        <button class="btn btn-danger ml-2" type="submit">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </button>
                                                    </form> --}}
                                                    <a href="" class="btn btn-danger" data-id="{{ $faqdata->id }}">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.d-flex -->
                        </div>
                    </div>
                    <!-- /.card -->
                    <!-- /.card -->
                </div>
            </div>
        </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
 

<script>
$('#userTable').DataTable();
</script>
{{-- delete faq --}}
<script>
    $(document).ready(function(){
        $('.btn-danger').on('click', function(e) {
            e.preventDefault();
            var userId = $(this).data('id'); 
            swal({
                title: "Do you want to delete?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'GET',
                        url: '{{ route('dashboard.support-remove', ':id') }}'.replace(':id', userId),
                        data: {
                            _token: '{{ csrf_token() }}',
                        },
                        success: function(response) {
                            if (response.success) {
                                swal("Success!", "Successfully deleted!", "success")
                                .then(function() {
                                    flasher.success('Successfully deleted!');
                                    setTimeout(() => {
                                        location.reload();
                                    }, 2000);
                                });
                            } else {
                                swal("Error!", "An error occurred while deleting the data.", "error");
                            }
                        },
                        error: function() {
                            swal("Error!", "An error occurred while deleting the data.", "error");
                        }
                    });
                }
            });
        });
    });
</script>
@endsection