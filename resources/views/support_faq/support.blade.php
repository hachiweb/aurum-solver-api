 @include('layouts.header')
 <div class="container-fluid">
     <div class="row">
         <div class="col-md-3">
             <div class="text-center">
                 <a href="/welcome" title="click here">
                     <div class="mobile">
                         <img src="{{asset('images/mobile-removebg-preview.png')}}" alt="" class="mobi">
                         <img src="{{asset('images/think-pair-2.png')}}" alt="" class="logo rounded">
                     </div>
                 </a>
             </div>
         </div>
         <div class="col-md-9 ">
             <div class="mx-4">
                 @if($data->isNotEmpty())
                 <div class="mt-5 row cd100">
                     <div class="card card-body">
                         <h3 class="font-weight-bold shadow bg-white">
                             FAQ
                         </h3>
                         <p class="font-weight-bold">Having problems, need help or have a suggestion?</p>
                         <a href="">Contact here..</a>
                         @foreach($data as $faqdata)
                         <ul>
                             <li>
                                <i class="fas fa-plus-circle"></i> 
                                <a class="font-weight-bold ml-2"
                                     style="color:black; font-size:18px;" data-toggle="collapse"
                                     href="#{{$faqdata->id}}" role="button" aria-expanded="false"
                                     aria-controls="{{$faqdata->id}}">{{$faqdata->question}}
                                </a>
                            </li>
                         </ul>
                         <div class="collapse" id="{{$faqdata->id}}">
                             <p class="ml-4" style="color:black; font-size:18px;">{{$faqdata->answer}}</p>
                         </div>
                         @endforeach
                     </div>
                 </div>
                 @endif
             </div>
         </div>
     </div>
 </div>
@include('layouts.footer')