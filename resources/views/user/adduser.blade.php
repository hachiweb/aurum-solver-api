@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create New User</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">User Add</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <div class="d-flex justify-content-between">
                                <div class="form-group">
                                    <label for="">Create new user</label>
                                    <select name="select_type_user" id="select_type_user" class="form-control">
                                        <option value="app">App User</option>
                                        <option value="dash">Dashboard User</option>
                                        <option value="both">Both</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-body ">
                            <div class="d-flex">
                                <div class="card-body table-responsive p-0">
                                    <div class="jumbotron">
                                        <div id="userHide">
                                            <div class="row justify-content-center">
                                                <div class="col-lg-8">
                                                    <form id="formsubmit"
                                                        action="{{route('dashboard.user.create-new-user')}}"
                                                        class="p-4 shadow-lg" method="post"
                                                        enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="usercreated_typ" id="">
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <label for="first_name">First Name</label>
                                                                <input type="text" autocomplete="off"
                                                                    class="form-control" id="first_name"
                                                                    placeholder="First Name" name="first_name">
                                                            </div>
                                                            <div class="col">
                                                                <label for="last_name">Last Name</label>
                                                                <input type="text" autocomplete="off"
                                                                    class="form-control" placeholder="Last Name"
                                                                    id="last_name" name="last_name">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <label for="user_email">Email Address <span
                                                                        class="text-danger font-weight-bold">*</span><span
                                                                        id="email_required"></span>
                                                                </label>
                                                                <input type="email" autocomplete="off"
                                                                    class="form-control" required name="user_email"
                                                                    placeholder="Enter email" id="user_email">
                                                            </div>
                                                            <div class="col">
                                                                <label for="phone_number">Phone Number</label>
                                                                <input type="number" pattern="\d{3}[\-]\d{3}[\-]\d{4}"
                                                                    autocomplete="off" class="form-control"
                                                                    placeholder="Phone Number" id="phone_number"
                                                                    name="phone_number">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="about">About</label>
                                                            <textarea autocomplete="off" name="about" id="about"
                                                                placeholder="About yourself in a few words"
                                                                class="form-control" cols="30" rows="3">
                                                            </textarea>
                                                            <div class="form-row">
                                                                <div class="col">
                                                                    <label for="date_of_birth">Date of Birth</label>
                                                                    <input type="text" autocomplete="off" value=""
                                                                        class="form-control  dob "
                                                                        placeholder="Date of Birth" id="date_of_birth"
                                                                        name="date_of_birth">
                                                                </div>
                                                                <div class="col">
                                                                    <label for="profile_pic">Profile Picture</label>
                                                                    <input type="file" autocomplete="off"
                                                                        class="form-control" id="profile_pic"
                                                                        placeholder="First Name" name="profile_pic">
                                                                </div>
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="col">
                                                                    <label for="timezone">Timezone</label>
                                                                    <select name="timezone" id="timezone"
                                                                        class="form-control">
                                                                        @foreach($zone as $zone_name)
                                                                        <option value="{{$zone_name->zone_name}}">
                                                                            {{$zone_name->zone_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="subscribe_category">Subscribe to
                                                                        category</label>
                                                                    <select name="subscribe_category"
                                                                        id="subscribe_category" class="form-control">
                                                                        @foreach($data as $cat)
                                                                        <option value="{{$cat->subjects}}">
                                                                            {{$cat->subjects}}
                                                                        </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="col">
                                                                    <label
                                                                        for="subscribe_notification_interval">Subscribed
                                                                        notification
                                                                        intervals</label>
                                                                    <select name="subscribe_notification_interval"
                                                                        id="subscribe_notification_interval"
                                                                        class="form-control">
                                                                        <option value="instant">Instant</option>
                                                                        <option value="1 Hour Delay">1 Hour Delay
                                                                        </option>
                                                                        <option value="3 Hour Delay">3 Hour Delay
                                                                        </option>
                                                                        <option value="6 Hour Delay">6 Hour Delay
                                                                        </option>
                                                                        <option value="12 Hour Delay">12 Hour Delay
                                                                        </option>
                                                                        <option value="24 Hour Delay">24 Hour Delay
                                                                        </option>
                                                                        <option value="Week Delay">1 Week Delay</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="user_type">User Type</label>
                                                                    <select name="user_type" id="user_type"
                                                                        class="form-control">
                                                                        <option value="student">Student</option>
                                                                        <option value="tutor">Tutor</option>
                                                                        <option value="both">Both</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="col">
                                                                    <label for="password">Password <span
                                                                            class="text-danger font-weight-bold">*</span></label>
                                                                    <input type="password" autocomplete="off"
                                                                        class="form-control" id="password"
                                                                        placeholder="Password" required value="" name="password">
                                                                    <span toggle="#password"
                                                                        class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                                </div>
                                                                <div class="col">
                                                                    <label for="cpassword">Confirm Password <span
                                                                            class="text-danger font-weight-bold">*</span><span
                                                                            class="pass_required"></span></label>
                                                                    <input type="password" value="" autocomplete="off"
                                                                        class="form-control"
                                                                        placeholder="Confirm Password" id="cpassword"
                                                                        name="cpassword">
                                                                    <p id="massage"></p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                    class="btn btn-primary mt-2">Submit
                                                                </button>
                                                            </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- *************************Dashboard User************** -->
                                    <div id="dashHide" style="display:none;">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-8">
                                                <form id="dformsubmit"
                                                    action="{{route('dashboard.user.dash-new-user')}}"
                                                    class="p-4 shadow-lg" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <label for="dfirst_name">First Name</label>
                                                        <input type="text" name="dfirst_name" class="form-control"
                                                            id="dfirst_name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="demail">Email Address</label>
                                                        <input type="text" required name="demail" class="form-control"
                                                            id="demail">
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <label for="dpassword">Password <span
                                                                    class="text-danger font-weight-bold">*</span></label>
                                                            <input type="dpassword" required autocomplete="off"
                                                                class="form-control" id="dpassword"
                                                                placeholder="Password" value="" name="dpassword">
                                                            <span toggle="#dpassword"
                                                                class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                        </div>
                                                        <div class="col">
                                                            <label for="dcpassword">Confirm Password <span
                                                            class="text-danger font-weight-bold">*</span><span
                                                                        class="dpass_required"></span></label>
                                                            <input type="password" value="" autocomplete="off"
                                                                class="form-control" placeholder="Confirm Password"
                                                                id="dcpassword" name="dcpassword">
                                                            <p id="dmassage"></p>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit"
                                                            class="btn btn-primary mt-2">Submit</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ***********************Both User*********** -->
                                    <div id="bothHide" style="display:none;">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-8">
                                                <form id="bformsubmit" class="shadow-lg p-4"
                                                    action="{{route('dashboard.user.both-user')}}" class="p-5"
                                                    method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <label for="bfirst_name">First Name</label>
                                                            <input type="text" autocomplete="off" class="form-control"
                                                                id="bfirst_name" placeholder="First Name"
                                                                name="bfirst_name">
                                                        </div>
                                                        <div class="col">
                                                            <label for="blast_name">Last Name</label>
                                                            <input type="text" autocomplete="off" class="form-control"
                                                                placeholder="Last Name" id="blast_name"
                                                                name="blast_name">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col">

                                                            <label for="buser_email">Email Address <span
                                                                    class="text-danger font-weight-bold">*</span><span
                                                                    id="bemail_required"></span></label>
                                                            <input type="email" required autocomplete="off" class="form-control"
                                                                name="buser_email" placeholder="Enter email"
                                                                id="buser_email">

                                                        </div>
                                                        <div class="col">
                                                            <label for="bphone_number">Phone Number</label>
                                                            <input type="number" pattern="\d{3}[\-]\d{3}[\-]\d{4}"
                                                                autocomplete="off" class="form-control"
                                                                placeholder="Phone Number" id="bphone_number"
                                                                name="bphone_number">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="babout">About</label>
                                                        <textarea autocomplete="off" name="babout" id="babout"
                                                            placeholder="About yourself in a few words"
                                                            class="form-control" cols="30" rows="3"></textarea>

                                                        <div class="form-row">
                                                            <div class="col">
                                                                <label for="bdate_of_birth">Date of Birth</label>
                                                                <input type="text" autocomplete="off" value=""
                                                                    class="form-control  dob "
                                                                    placeholder="Date of Birth" id="bdate_of_birth"
                                                                    name="bdate_of_birth">
                                                            </div>
                                                            <div class="col">
                                                                <label for="bprofile_pic">Profile Picture</label>
                                                                <input type="file" autocomplete="off"
                                                                    class="form-control" id="bprofile_pic"
                                                                    placeholder="First Name" name="bprofile_pic">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <label for="btimezone">Timezone</label>
                                                                <select name="btimezone" id="btimezone"
                                                                    class="form-control">
                                                                    @foreach($zone as $zone_name)
                                                                    <option value="{{$zone_name->zone_name}}">
                                                                        {{$zone_name->zone_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="bsubscribe_category">Subscribe to
                                                                    category</label>
                                                                <select name="bsubscribe_category"
                                                                    id="bsubscribe_category" class="form-control">
                                                                    @foreach($data as $cat)
                                                                    <option value="{{$cat->subjects}}">
                                                                        {{$cat->subjects}}
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <label for="bsubscribe_notification_interval">Subscribed
                                                                    notification
                                                                    intervals</label>
                                                                <select name="bsubscribe_notification_interval"
                                                                    id="bsubscribe_notification_interval"
                                                                    class="form-control">
                                                                    <option value="instant">Instant</option>
                                                                    <option value="1 Hour Delay">1 Hour Delay</option>
                                                                    <option value="3 Hour Delay">3 Hour Delay</option>
                                                                    <option value="6 Hour Delay">6 Hour Delay</option>
                                                                    <option value="12 Hour Delay">12 Hour Delay</option>
                                                                    <option value="24 Hour Delay">24 Hour Delay</option>
                                                                    <option value="Week Delay">1 Week Delay</option>
                                                                </select>
                                                            </div>
                                                            <div class="col">
                                                                <label for="buser_type">User Type</label>
                                                                <select name="buser_type" id="buser_type"
                                                                    class="form-control">
                                                                    <option value="student">Student</option>
                                                                    <option value="tutor">Tutor</option>
                                                                    <option value="both">Both</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col">
                                                                <label for="bpassword">Password <span
                                                                        class="text-danger font-weight-bold">*</span></label>
                                                                <input type="password" autocomplete="off"
                                                                    class="form-control" required id="bpassword"
                                                                    placeholder="Password" value="" name="bpassword">
                                                                <span toggle="#bpassword"
                                                                    class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                            </div>
                                                            <div class="col">
                                                                <label for="bcpassword">Confirm Password <span
                                                                        class="text-danger font-weight-bold">*</span><span
                                                                        class="bpass_required"></span></label>
                                                                <input type="password" value="" required autocomplete="off"
                                                                    class="form-control" placeholder="Confirm Password"
                                                                    id="bcpassword" name="bcpassword">
                                                                <p id="bmassage"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit"
                                                                class="btn btn-primary mt-2">Submit
                                                            </button>
                                                        </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js">
</script>
<script>

$(".dob").datepicker({

    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',

});
$(document).ready(function() 
{
    $(".ui-datepicker-title").css("background-color", "#green", "important");
    $('#password, #cpassword').on('keyup', function() {
        if ($('#password').val() == $('#cpassword').val()) {
            $('#massage').html('Matching').css('color', 'green');
        } else
            $('#massage').html('Not Matching').css('color', 'red');
        return false;
    });
    // Both
    $('#bpassword, #bcpassword').on('keyup', function() {
        if ($('#bpassword').val() == $('#bcpassword').val()) {
            $('#bmassage').html('Matching').css('color', 'green');
        } else
            $('#bmassage').html('Not Matching').css('color', 'red');
        return false;
    });
    // Dash
    $('#dpassword, #dcpassword').on('keyup', function() {
        if ($('#dpassword').val() == $('#dcpassword').val()) {
            $('#dmassage').html('Matching').css('color', 'green');
        } else
            $('#dmassage').html('Not Matching').css('color', 'red');
        return false;
    });
    $('#formsubmit').submit(function(e) {
        if ($('#user_email').val() == '') {
            $('#email_required').html('required').css('color', 'red');
            return false;

        } else if ($('#password').val() == '') {
            $('.pass_required').html('required').css('color', 'red');
            return false;
        } else if ($('#password').val() != $('#cpassword').val()) {
            $('.pass_required').html('Not matching').css('color', 'red');
            return false;
        }
        $(this).submit();
        $('#password, #cpassword').on('keyup', function() {
            if ($('#password').val() == $('#cpassword').val()) {
                $('#massage').html('Matching').css('color', 'green');
            } else
                $('#massage').html('Not Matching').css('color', 'red');
            return false;
        });
    });
    // Both user 
    $('#bformsubmit').submit(function(e) {
        if ($('#buser_email').val() == '') {
            $('#bemail_required').html('required').css('color', 'red');
            return false;

        } else if ($('#bpassword').val() == '') {
            $('.bpass_required').html('required').css('color', 'red');
            return false;
        } else if ($('#bpassword').val() != $('#bcpassword').val()) {
            $('.bpass_required').html('Not matching').css('color', 'red');
            return false;
        }
        $(this).submit();
        $('#bpassword, #bcpassword').on('keyup', function() {
            if ($('#bpassword').val() == $('#bcpassword').val()) {
                $('#bmassage').html('Matching').css('color', 'green');
            } else
                $('#bmassage').html('Not Matching').css('color', 'red');
            return false;
        });
    });
// dashboard
    $('#dformsubmit').submit(function(e) {
        if ($('#demail').val() == '') {
            $('#demail_required').html('required').css('color', 'red');
            return false;

        } else if ($('#dpassword').val() == '') {
            $('.dpass_required').html('required').css('color', 'red');
            return false;
        } else if ($('#dpassword').val() != $('#dcpassword').val()) {
            $('.dpass_required').html('Not matching').css('color', 'red');
            return false;
        }
        $(this).submit();
        $('#dpassword, #dcpassword').on('keyup', function() {
            if ($('#dpassword').val() == $('#dcpassword').val()) {
                $('#dmassage').html('Matching').css('color', 'green');
            } else
                $('#dmassage').html('Not Matching').css('color', 'red');
            return false;
        });
    });
    // hide show password
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
});

$('#select_type_user').on('change', function() {
    var user_type = this.value;
    if (user_type == 'dash') {
        $('#dashHide').css('display', 'block');
        $('#userHide').css('display', 'none');
        $('#bothHide').css('display', 'none');
    }
    if (user_type == 'both') {
        $('#bothHide').css('display', 'block');
        $('#userHide').css('display', 'none');
        $('#dashHide').css('display', 'none');
    }
    if (user_type == 'app') {
        $('#userHide').css('display', 'block');
        $('#bothHide').css('display', 'none');
        $('#dashHide').css('display', 'none');
    }
});
</script>
@endsection
