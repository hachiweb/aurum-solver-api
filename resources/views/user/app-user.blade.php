@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">User list</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="card-body table-responsive p-0">
                                    <table id="userTable" class="table table-striped" cellspacing="0"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>PROFILE</th>
                                                <th>EMAIL-ID</th>
                                                <th>FIRST NAME</th>
                                                <th>USER ROLE</th>
                                                <th>LAST LOGIN</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $val)
                                            <tr>
                                                <td>{{$val->id}}</td>
                                                <td>
                                                    <div class="image">
                                                        <img src="{{isset($val->profile_pic)?$val->profile_pic:''}}"
                                                            class="img-circle elevation-2"
                                                            style="width:70px; height:70px;" alt="pic">
                                                    </div>
                                                </td>
                                                <td>{{$val->user_email}}</td>
                                                <td>{{isset($val->first_name)?$val->first_name:'N/A'}}</td>
                                                <td>{{isset($val->user_type)?$val->user_type:'N/A'}}</td>
                                                <td>{{isset($val->last_login)?$val->  last_login:'N/A'}}</td>
                                                <td class="d-flex">
                                                    {{-- <a href="{{route('dashboard.user.delete',['id'=>$val->id])}}"
                                                        class="btn btn-danger mr-1"
                                                        onclick="return confirm('Are you sure ? to delete')">
                                                        Delete
                                                    </a> --}}
                                                    <a href="" 
                                                    class="btn btn-danger mr-1" 
                                                    id="delete_user_{{ $val->id }}" data-id="{{ $val->id }}">
                                                        Delete
                                                    </a>
                                                    @if($val->is_active==1)
                                                    {{-- <a href="{{route('dashboard.user.toblock',['id'=>$val->id])}}"
                                                        class="btn btn-primary"
                                                        onclick="return confirm('Are you sure you want to block this user?')">
                                                        Block</a> --}}
                                                        <a href="javascript:void(0);"
                                                            class="btn btn-primary btn-primary1"
                                                           id="block_user_{{ $val->id }}" data-id="{{ $val->id }}">
                                                            Block
                                                        </a>
                                                    @else
                                                    {{-- <a href="{{route('dashboard.user.unblock',['id'=>$val->id])}}"
                                                        class="btn btn-success"
                                                        onclick="return confirm('Are you sure you want to acivate this user?')">
                                                        Unblock</a> --}}
                                                        <a href="javascript:void(0);"
                                                            class="btn btn-success btn-success1"
                                                           id="unblock_user_{{ $val->id }}" data-id="{{ $val->id }}">
                                                            Unblock
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
   $('#userTable').DataTable({
        "order": [[ 0, "desc" ]]
    });
</script>
{{-- delete user and block & unblock--}}
<script>
    $(document).ready(function(){
        // delete user
        $('.btn-danger').on('click', function(e) {
            e.preventDefault();
            var userId = $(this).data('id'); 
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'GET',
                        url: '{{ route('dashboard.user.delete', ':id') }}'.replace(':id', userId),
                        data: {
                            _token: '{{ csrf_token() }}',
                        },
                        success: function(response) {
                            if (response.success) {
                                swal("Success!", "User has been deleted.", "success")
                                .then(function() {
                                    flasher.success('User deleted');
                                    setTimeout(() => {
                                        location.reload();
                                    }, 2000);
                                });
                            } else {
                                swal("Error!", "An error occurred while deleting the user.", "error");
                            }
                        },
                        error: function() {
                            swal("Error!", "An error occurred while deleting the user.", "error");
                        }
                    });
                }
            });
        });

        // user block & unblock
        $('.btn-primary1, .btn-success1').on('click', function(e) {
            e.preventDefault();
            var userId = $(this).data('id'); 
            var isBlocking = $(this).hasClass('btn-primary');
            swal({
                title: "Are you sure?",
                text: isBlocking ? "You want to block this user?" : "You want to activate this user?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willProceed) => {
                if (willProceed) {
                    $.ajax({
                        type: 'GET',
                        url: isBlocking
                            ? '{{ route('dashboard.user.toblock', ':id') }}'.replace(':id', userId)
                            : '{{ route('dashboard.user.unblock', ':id') }}'.replace(':id', userId),
                        data: {
                            _token: '{{ csrf_token() }}',
                        },
                        success: function(response) {
                            if (response.success) {
                                swal("Success!", isBlocking ? "User has been blocked!" : "User has been activated!", "success")
                                .then(function() {
                                    flasher.success(isBlocking ? 'User has been blocked!' : 'User has been activated!');
                                    setTimeout(() => {
                                        location.reload();
                                    }, 2000);
                                });
                            } else {
                                swal("Error!", "An error occurred while updating the user status.", "error");
                            }
                        },
                        error: function() {
                            swal("Error!", "An error occurred while updating the user status.", "error");
                        }
                    });
                }
            });
        });
    });
</script>
@endsection

 