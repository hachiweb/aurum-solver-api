@extends('layouts.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                    @if (session('update'))
                    <div class="alert alert-warning" role="alert">
                        <h1>{{session('update')}}</h1>
                    </div>
                    @endif
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">User list</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                         
                       
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="card-body table-responsive p-0">
                                    <table id="userTable" class="table table-striped table-bordered" cellspacing="0"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>PROFILE</th>
                                                <th>EMAIL-ID</th>
                                                <th>FIRST NAME</th>
                                                <th>USER ROLE</th>
                                                <th>LAST LOGIN</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $val)
                                            <tr>
                                                <td>{{$val->id}}</td>
                                                <td>
                                                    <div class="image">
                                                        <img src="{{isset($val->profile_pic)?$val->profile_pic:''}}"
                                                            class="img-circle elevation-2"
                                                            style="width:70px; height:70px;" alt="pic">
                                                    </div>
                                                </td>
                                                <td>{{$val->user_email}}</td>
                                                <td>{{isset($val->first_name)?$val->first_name:'N/A'}}</td>
                                                <td>{{isset($val->user_type)?$val->user_type:'N/A'}}</td>
                                                <td>{{isset($val->last_login)?$val->  last_login:'N/A'}}</td>
                                                <td>
                                                    <a href="{{route('dashboard.user.unblock',['id'=>$val->id])}}" class="btn btn-success"
                                                        onclick="return confirm('Are you sure you want to acivate this user?')">
                                                        Unblock</a>
                                                    <a href="{{route('dashboard.user.delete',['id'=>$val->id])}}" class="btn btn-danger"
                                                        onclick="return confirm('Are you sure ? to delete')">
                                                        Delete</a>
                                                   
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            <!-- /.d-flex -->
                        </div>
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->


    <script>
   @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}"
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
    
    $('#userTable').DataTable({
        "order": [[ 0, "desc" ]]
    });

    $(document).ready(function() {

    });
    </script>
    @endsection