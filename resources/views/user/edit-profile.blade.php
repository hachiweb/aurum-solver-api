@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Profile</h1>

                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-body">
                            <form id="formsubmit" action="{{route('dashboard.user.update')}}" class="p-5" method="get"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="col">
                                        <label for="first_name">First Name</label>
                                        <input type="text" name="first_name" autocomplete="off" class="form-control"
                                            placeholder="First Name" value="{{ auth()->user()->name }}">
                                    </div>
                                    <div class="col">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" name="last_name" autocomplete="off" class="form-control"
                                            placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email_address">Email Address</label>
                                    <input type="text" name="email_address" autocomplete="off" readonly
                                        class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="about">About</label>
                                    <textarea autocomplete="off" name="about" id="about"
                                        placeholder="About yourself in a few words" value="{{ auth()->user()->email }}"
                                        class="form-control" cols="30" rows="3"></textarea>

                                    <div class="form-row">
                                        <div class="col">
                                            <label for="date_of_birth">Date of Birth</label>
                                            <input type="text" name="date_of_birth" autocomplete="off"
                                                value="{{ auth()->user()->email }}" class="form-control"
                                                placeholder="Date of Birth">
                                        </div>
                                        <div class="col">
                                            <label for="phone_number">Phone Number</label>
                                            <input type="text" name="phone_number" autocomplete="off"
                                                value="{{ auth()->user()->email }}" class="form-control"
                                                placeholder="9182404785">
                                        </div>

                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="profile_pic">Profile Pic</label>
                                            <input type="file" name="profile_pic" autocomplete="off"
                                                value="{{ auth()->user()->email }}" class="form-control"
                                                placeholder="Select your image">
                                        </div>
                                        <div class="col">
                                            <label for="paypal_account">Paypal Account</label>
                                            <input type="text" name="paypal_account" autocomplete="off" value=" "
                                                class="form-control" placeholder="9182404785">
                                        </div>

                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="timezone">Timezone</label>
                                            <select name="timezone" id="timezone" class="form-control">

                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="subscribe_category">Subscribe to category</label>
                                            <select name="subscribe_category" id="subscribe_category"
                                                class="form-control">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="subscribe_notification_interval">Subscribed notification
                                                intervals</label>
                                            <select name="subscribe_notification_interval"
                                                id="subscribe_notification_interval" class="form-control">
                                                <option value="instant">Instant</option>
                                                <option value="1 Hour Delay">1 Hour Delay</option>
                                                <option value="3 Hour Delay">3 Hour Delay</option>
                                                <option value="6 Hour Delay">6 Hour Delay</option>
                                                <option value="12 Hour Delay">12 Hour Delay</option>
                                                <option value="24 Hour Delay">24 Hour Delay</option>
                                                <option value="Week Delay">1 Week Delay</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="user_type">User Type</label>
                                            <select name="user_type" id="user_type" class="form-control">
                                                <option value="student">Student</option>
                                                <option value="tutor">Tutor</option>
                                                <option value="both">Both</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="form-row">
                                        <div class="col">
                                            <label for="password">Password <span
                                                    class="text-danger font-weight-bold">*</span></label>
                                            <input type="password" autocomplete="off" class="form-control" id="password"
                                                placeholder="Password" value="" name="password">
                                            <span toggle="#password"
                                                class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        </div>
                                        <div class="col">
                                            <label for="cpassword">Confirm Password <span
                                                    class="text-danger font-weight-bold">*</span><span
                                                    class="pass_required"></span></label>
                                            <input type="password" value="" autocomplete="off" class="form-control"
                                                placeholder="Confirm Password" id="cpassword" name="cpassword">
                                            <p id="massage"></p>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <br>

                                        <button type="submit" value="1" class="btn btn-success">
                                            Save
                                        </button>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.content -->
</div>
<link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js">
</script>
<script>
$("#date_of_birth").datepicker({

    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',

});
$('#myModal').on('shown.bs.modal', function() {
    $('#myInput').trigger('focus')
})
$(document).ready(function() {
    $(".ui-datepicker-title").css("background-color", "#green", "important");
    $('#password, #cpassword').on('keyup', function() {
        if ($('#password').val() == $('#cpassword').val()) {
            $('#massage').html('Matching').css('color', 'green');
        } else
            $('#massage').html('Not Matching').css('color', 'red');
        return false;
    });
    $('#formsubmit').submit(function(e) {
        if ($('#password').val() == '') {
            $('.pass_required').html('required').css('color', 'red');
            return false;
        } else if ($('#password').val() != $('#cpassword').val()) {
            $('.pass_required').html('Not matching').css('color', 'red');
            return false;
        }
        $(this).submit();
    });

    // hide show password
    $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
});
</script>
@endsection