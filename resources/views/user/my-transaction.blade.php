@extends('layouts.master')
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users Transaction</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Payments</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="card-body table-responsive p-0">
                                    <table id="userTable" class="table table-striped table-bordered" cellspacing="0"
                                        width="100%">
                                        <thead class="bg-dark font-weight-bold">
                                            <tr>
                                                <th>#ID</th>
                                                <!-- <th>From (First Name) </th> -->
                                                <th>To (First Name)</th>
                                                <!-- <th>From (Role)</th> -->
                                                <th>To (Role)</th>
                                                <!-- <th>Transaction ID</th> -->
                                                <th>Amount</th>
                                                <th>Status</th>
                                                <!-- <th>Email Address</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $val)
                                            <tr>
                                                <td>{{$val->id}}</td>
                                                <td>{{isset($val->fromUserTransaction->first_name)?$val->fromUserTransaction->first_name:'N/A'}}
                                                </td>
                                                <td>{{isset($val->toUserTransaction->first_name)?$val->fromUserTransaction->first_name:'N/A'}}
                                                </td>
                                                <td>{{isset($val->toUserTransaction->user_type)?$val->fromUserTransaction->user_type:'N/A'}}
                                                </td>
                                                <td>{{isset($val->toUserTransaction->user_type)?$val->fromUserTransaction->user_type:'N/A'}}
                                                </td>
                                                <!-- <td>{{$val->transaction_id}}</td> -->
                                                <td>$<span class="font-weight-bold">{{$val->amount}}</span></td>
                                                <td>{{$val->status}}</td>
                                                <!-- <td>{{isset($val->toUserTransaction->user_email)?$val->fromUserTransaction->user_email:'N/A'}}
                                                </td> -->
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            <!-- /.d-flex -->
                        </div>
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->
<script>
$('#userTable').DataTable();
</script>
@endsection