@extends('layouts.master')
@section('content')
    
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">


                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">My Account</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="card">

                <div class="row">
                    <div class="col-lg-4">
                        <h4>Admin</h4>
                        <img class="card-img-top"
                            src="https://media.sproutsocial.com/uploads/2017/02/10x-featured-social-media-image-size.png"
                            alt="Card image cap">
                        <div class="card-body ">
                            <div class="form-group">
                                <label for="balance">Total Balance</label>
                                <input type="text" readonly class="form-control" value="${{$total_balance}}">
                            </div>

                            <div class="form-group">
                                <label for="balance">Email</label>
                                <input type="text" readonly class="form-control" value="{{ auth()->user()->email }}">
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card-body ">
                            <h3 class="mb-5">My Account</h3>
                            <form action="{{route('dashboard.savepaypl')}}" class="shadow-lg mb-4 p-4">
                                <div class="form-group mt-5 ">
                                    <label for="balance">Paypal Email</label>
                                    <input type="email" class="form-control" name="email_paypal" value=" ">
                                </div>
                                <div class="from-group">
                                    <button class="btn btn-success">Save</button>
                                </div>
                            </form>
                            <form action="">
                                <div class="form-row">
                                    <div class="from-group mt-5">
                                        <label for="total_withdrawal">Total Available Balance :<strong>
                                                ${{$total_balance}}</strong></label><br>
                                        <button class="btn btn-dark">
                                            Withdrawal
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>



            <!-- /.d-flex -->
        </div>
    </div>
    <!-- /.card -->
    <!-- /.card -->
</div>



<script>
@if(Session::has('message'))
var type = "{{Session::get('alert-type','info')}}"
switch (type) {
    case 'info':
        toastr.info("{{ Session::get('message') }}");
        break;
    case 'success':
        toastr.success("{{ Session::get('message') }}");
        break;
    case 'warning':
        toastr.warning("{{ Session::get('message') }}");
        break;
    case 'error':
        toastr.error("{{ Session::get('message') }}");
        break;
}
@endif
$('#userTable').DataTable();
$(document).ready(function() {

});
</script>
@endsection