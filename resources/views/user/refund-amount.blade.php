@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Cancelled Assignment</h1>

                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Cancelled Assignment</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-body table-responsive p-0">
                                <table id="userTable" class="table table-striped table-bordered" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Questioner</th>
                                            <th>Answerer</th>
                                            <th>Question</th>
                                            <th>Status</th>
                                            <th>Cancellation Reason</th>
                                            <th>Amount</th>
                                            <th style="width:110px; text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($assignment as $assign)
                                        <tr>
                                            <td>{{$assign->id}}</td>
                                            <td>{{isset($assign->userDetail->first_name)?$assign->userDetail->first_name:'n/l'}}
                                            </td>
                                            <td>{{isset($assign->tutorDetail->first_name)?$assign->tutorDetail->first_name:'n/l'}}
                                            </td>
                                            <td></td>
                                            <td class="text-center">
                                                @if($assign->is_approved==0)
                                                <p class="font-weight-bold text-warning">Refund Pending</p>

                                                @elseif($assign->is_approved==1)
                                                <p class="font-weight-bold text-success">Refund Approved</p>
                                                @else
                                                <p class="font-weight-bold text-danger">Refund Rejected</p>
                                                @endif
                                            </td>
                                            <td>{{$assign->cancellation_reason}}</td>
                                            <td>${{$assign->willing_to_pay}}</td>
                                            <td class="d-flex">
                                                @if($assign->is_approved==0)
                                                <form action="{{route('dashboard.refund-request.update')}}"
                                                    method="POST">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$assign->id}}" />
                                                    <input type="hidden" name="status" value="1" />
                                                    <button class="btn btn-success" type="submit"
                                                        onclick="return confirm('Are you sure you want to approve the request?')"><i
                                                            class="fa fa-check" aria-hidden="true"></i></button>

                                                </form>
                                                <form action="{{route('dashboard.refund-request.update')}}"
                                                    method="POST">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$assign->id}}" />
                                                    <input type="hidden" name="status" value="0" />
                                                    <button class="btn btn-danger" type="submit"
                                                        onclick="return confirm('Do you want to reject?')"><i
                                                            class="fa fa-ban" aria-hidden="true"></i></button>

                                                </form>

                                                @else
                                                <a disabled href="" class="btn btn-success">
                                                    <i class="fa fa-check" aria-hidden="true"></i></a>
                                                <a href="" disabled class="btn btn-danger">
                                                    <i class="fa fa-ban" aria-hidden="true"></i></a>


                                                @endif

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
</div>
<script>
$('#userTable').DataTable();
$(document).ready(function() {
    // $('#userTable').DataTable();

});
</script>
@endsection