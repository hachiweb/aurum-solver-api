@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Withdrawal Request</h1>

                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Withdrawal Request</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-heaer">
                            <h4 class="float-right mr-5"><strong>Total balance:&#36;{{$total_balance}}</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-body table-responsive p-0">
                                <table id="userTable" class="table table-striped table-bordered" cellspacing="0"
                                    width="100%">
                                    <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th>From Request</th>
                                            <th>Amount</th>
                                            <th class="text-center">Status</th>
                                            <th>Requested At</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($withdrawal as $with)
                                        <tr>
                                            <td>{{isset($with->requesterUser->id)?$with->requesterUser->id:'n/l'}}</td>
                                            <!-- <td>
                                                <div class="image">
                                                    <img src="{{isset($with->requesterUser->profile_pic)?$with->requesterUser->profile_pic:'n/l'}}"
                                                        class="img-circle elevation-2" style="width:70px; height:70px;"
                                                        alt="pic">
                                                </div>
                                            </td> -->
                                            <td>{{isset($with->requesterUser->first_name)?$with->requesterUser->first_name:'n/l'}}
                                            </td>
                                            <td><b>${{$with->amount}}</b></td>
                                            <td class="text-center">
                                                @if($with->is_approved=='0')
                                                <p class="font-weight-bold text-danger">Rejected</p>
                                                @elseif($with->is_approved==1)
                                                <p class="font-weight-bold text-success">Approved</p>
                                                @else
                                                <p class="font-weight-bold text-warning">Pending</p>
                                                @endif
                                            </td>

                                            <td><b>{{$with->created_at}}</b></td>
                                            <td class="d-flex">
                                                @if($with->is_approved=='0')
                                                <a href="" disabled class="btn btn-success">
                                                    <i class="fa fa-check" aria-hidden="true"></i></a>
                                                <a href="" disabled class="btn btn-danger">
                                                    <i class="fa fa-ban" aria-hidden="true"></i></a>

                                                @elseif($with->is_approved==1)
                                                <a href="" disabled class="btn btn-success">
                                                    <i class="fa fa-check" aria-hidden="true"></i></a>
                                                <a href="" disabled class="btn btn-danger">
                                                    <i class="fa fa-ban" aria-hidden="true"></i></a>
                                                @else
                                                <form action="{{route('dashboard.withdrawal-request.update')}}"
                                                    method="POST">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$with->id}}" />
                                                    <input type="hidden" name="status" value="1" />
                                                    <button class="btn btn-success" type="submit"
                                                        onclick="return confirm('Are you sure you want to approve the request?')"><i
                                                            class="fa fa-check" aria-hidden="true"></i></button>

                                                </form>
                                                <form action="{{route('dashboard.withdrawal-request.update')}}"
                                                    method="POST">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$with->id}}" />
                                                    <input type="hidden" name="status" value="0" />
                                                    <button class="btn btn-danger" type="submit"
                                                        onclick="return confirm('Do you want to reject?')"><i
                                                            class="fa fa-ban" aria-hidden="true"></i></button>

                                                </form>

                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.content -->
</div>


<link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js">
</script>
<script>
$("#date_of_birth").datepicker({

    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',

});
$('#myModal').on('shown.bs.modal', function() {
    $('#myInput').trigger('focus')
})
$(document).ready(function() {

    $(".ui-datepicker-title").css("background-color", "#green", "important");
    $('#password, #cpassword').on('keyup', function() {
        if ($('#password').val() == $('#cpassword').val()) {
            $('#massage').html('Matching').css('color', 'green');
        } else
            $('#massage').html('Not Matching').css('color', 'red');
        return false;
    });
    $('#formsubmit').submit(function(e) {

        if ($('#user_email').val() == '') {
            $('#email_required').html('required').css('color', 'red');
            return false;

        } else if ($('#password').val() == '') {
            $('.pass_required').html('required').css('color', 'red');
            return false;
        } else if ($('#password').val() != $('#cpassword').val()) {
            $('.pass_required').html('Not matching').css('color', 'red');
            return false;
        }
        $(this).submit();
    });

    // hide show password
    $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
});
</script>
@endsection