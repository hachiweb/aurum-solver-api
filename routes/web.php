<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Render home
Route::get('/', 'SupportAboutFaqController@aboutUs');
Route::get('/home', function () {
    return view('/');
});

// render welcome page
Route::get('/welcome', function () {
    return view('welcome');
});
// render about page
// Route::get('/about', function () {
//     return view('about-us');
// });
Route::get('/about', 'SupportAboutFaqController@aboutUs')->name('about');
Route::get('/support', 'SupportAboutFaqController@displayFaq')->name('supports');
// Get Token
Route::post('/get-token', 'DataController@generateToken');
// Destroy Token
Route::post('/destroy-token', 'DataController@destroyToken');
Route::get('/reset-password', 'FirebaseController@resetPassword');
Route::get('/save', 'FirebaseController@savePassword')->name('setpassword');
// Route::get('/verify/email/{token}', 'Apps_UserController@mailVerify');
// Protected routes
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('/apps/users/', 'Login_UserController@users');
    Route::post('/profile/update', 'profileUpdateController@userUpdate');
    Route::get('/user/{user_id}', 'profileUpdateController@userInfo');
    Route::post('/user/social/signup/','SocialController@socialSignup');
    Route::post('/user/login', 'Login_UserController@loginUser');
    Route::post('/user/register', 'Apps_UserController@appsUser');
    Route::get('/users/category/{category_id}', 'CategoryController@usersByCategory');
    Route::get('/category/{category_id}', 'CategoryController@category');
    Route::get('/user-profile/{id}', 'UsersProfilesController@userProfile');
   
    Route::post('/questions/ask/', 'questionContents@askQuestion');
    Route::post('/cancel/assignment/', 'assignmentPaymentController@cancelAssignment');
    Route::get('/questions/my/{user_id}', 'questionsController@askedQuestions');
    Route::get('/questions/all', 'questionsController@allQuestions');
    Route::get('/question/details/{id}', 'questionsController@getQuestion');
    Route::get('/questions/assigned/{user_id}', 'questionsController@assignedQuestion');
    Route::post('/questions/filter', 'filterAnswersController@filter');
    Route::post('/prepayments/to', 'transactionsController@prePayments');
    Route::get('/prepayments/my/{user_id}', 'PrepaymentRequestController@MyRequest');
    Route::post('/prepayments/request/update', 'PrepaymentRequestController@updateRequest');
    Route::get('/prepayments/requested/{user_id}', 'PrepaymentRequestController@PreRequested');
    Route::post('/assignment/pay', 'assignmentPaymentController@assignmentPayment');
    Route::post('/assignment/request/update', 'questionsController@assignmentUpdate');

    Route::get('/answer/all', 'ansQuestionsController@allAnswers');
    Route::post('/submit/assignment', 'ansQuestionsController@assignSubmit');
    Route::post('/submit/answer', 'ansQuestionsController@replyAns');
    Route::get('/answer/my/{user_id}', 'ansQuestionsController@myAnswers');                        
    Route::post('/answer/buy/', 'purchaseAnsController@purchaseAns');
    Route::get('/paid/answer/{user_id}', 'purchaseAnsController@MyBuyAnswer');
    Route::post('/delivery/update', 'assignmentPaymentController@deliveryUpdate');
    Route::get('/quizzes/ans', 'quizzesController@ansAtempts');
    Route::get('/user/quiz/{user_id}', 'quizzesController@ansAtempts');
    Route::get('/categories/all', 'CategoryController@categoryAll');
    Route::get('/category/all/{user_id}', 'CategoryController@usercategoryAll');
    Route::post('/category/quizzes', 'CategoryController@categoryQuizzes');
    Route::post('/answer/quiz', 'CategoryController@answerQuiz');
    Route::post('/quizzes/questions', 'quizzesController@quizAttempts');
    Route::post('/quizzes/attempt', 'replyQuizzeeController@quizAns');
    Route::post('/account/funds/add', 'addFundsController@addFunds'); 
    Route::post('/account/withdrawal', 'withdrawalAmountController@withdrawal');
    Route::get('/fetch/transaction/', 'transactionsController@fetch');
    Route::post('/chat/message', 'chatMessageController@chats');
    Route::get('/chat/reply', 'chatMessageController@preference');
    Route::get('/my/transaction/{id}', 'transactionsController@myTransaction');
    Route::get('/get/balance/{user_id}', 'transactionsController@balance');
    Route::post('/feedback/review', 'feedbacksController@feedback');
    Route::get('/search/{search_key}', 'filterAnswersController@search');
    Route::get('/feedback/my/{user_id}', 'feedbacksController@myFeedback');
    Route::post('/create/category', 'CategoryController@categoryCreate');
    Route::get('/app/user-profile/{registration_token}', 'Apps_UserController@getUser');
    Route::post('/ondemand/connect', 'AppUsersController@onDemandConnect');
    Route::post('/ondemand/disconnect', 'AppUsersController@onDemandDisconnect');
});    

Auth::routes();
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/logout', 'HomeController@logout')->name('home');
Route::group(['prefix'=>'dashboard','as'=>'dashboard.','middleware' => 'auth'], function(){
    Route::get('profile-update', 'AppUsersController@editProfile')->name('profile-update');
    Route::get('cancelled/assigment', 'AssignmentQuestionAnswerController@cancelledAssignment')->name('cancelled-assignment'); 
    Route::get('save', 'AppUsersController@savePaypal')->name('savepaypl'); 
    Route::get('assignment/questions', 'AssignmentQuestionAnswerController@assignment')->name('question');
    Route::get('refund-accept/{id}', 'AssigmentTransactionController@refundAccept')->name('refund-approve');
    Route::get('refund-reject/{id}', 'AssigmentTransactionController@refundReject')->name('refund-reject');
    Route::get('create/new/user', 'AppUsersController@createUser')->name('add');
    Route::get('cancelled/assignment', 'AssigmentTransactionController@cancellAssignmentList')->name('cancel-assignment');
    Route::post('refund/request/update', 'WithdrawalController@refundRequestUpdate')->name('refund-request.update');
    Route::post('withdrawal/request', 'WithdrawalController@withdrawalApprove')->name('withdrawal-request.update');
    Route::get('withdrawal/handle/{status}', 'WithdrawalController@withdrawalHandle')->name('withdrawal.handle');
    Route::get('refund/handle/{status}', 'WithdrawalController@refundHandle')->name('refund.handle');
    Route::get('withdrawal/request', 'AssigmentTransactionController@withdrawalRequest')->name('withdrawal');
    Route::get('profile', 'AppUsersController@myProfile')->name('myprofile');
    Route::get('my/transaction', 'AppUsersController@myTrasactionHistory')->name('mytransaction');

    Route::group(['prefix'=>'users','as'=>'user.'], function(){
        Route::get('update', 'AppUsersController@updateProfile')->name('update');
        Route::get('questions/edit/{id}', 'AssignmentQuestionAnswerController@editAllQuetion')->name('edit-post'); 
        Route::get('questions/delete/{id}', 'AssignmentQuestionAnswerController@deletePostQuestion')->name('delete-post'); 
        Route::get('delete/{id}', 'AppUsersController@deleteUser')->name('delete');
       
        Route::get('unblock/{id}', 'AppUsersController@unblock')->name('unblock');
        Route::get('block/{id}', 'AppUsersController@updateBlock')->name('toblock');
        Route::post('create', 'AppUsersController@createNewUser')->name('create-new-user');
        Route::post('create/dashboard', 'AppUsersController@createDasboardUser')->name('dash-new-user');
        Route::post('create/both-user', 'AppUsersController@createBothUser')->name('both-user');
        Route::get('/', 'AppUsersController@usersList')->name('list');
        Route::get('blocked', 'AppUsersController@blockUser')->name('block');
        Route::get('account', 'withdrawalAmountController@myAccount')->name('myaccount');
        Route::get('transaction', 'transactionsController@getTransactions')->name('transaction');
    });

    Route::group(['prefix'=>'category','as'=>'category.'], function(){
        Route::get('create',function(){
            return view('category.create-category');
        })->name('create');
        Route::get('edit/{id}', 'CreateCategoryController@edit')->name('edit');
        Route::get('create-category', 'CreateCategoryController@createCategory')->name('CreateCatogry');
        Route::get('/manage','CreateCategoryController@allCategory')->name('manage');
        Route::get('{id}', 'CreateCategoryController@delete')->name('delcategory');
        Route::get('update/{id}', 'CreateCategoryController@updateCategory')->name('update');        
    });

    Route::group(['prefix'=>'quiz','as'=>'quiz.'], function(){
        Route::get('/del/{id}','QuizCategoryController@delQuiz')->name('del');
        Route::get('/all/{id}','QuizCategoryController@allQuiz')->name('show');
        Route::get('/create','QuizCategoryController@quizzAllCreate')->name('create-quiz');
        Route::get('/manage','QuizCategoryController@allQuizzes')->name('manage-quiz');
        Route::get('create/', 'QuizCategoryController@createQuiz')->name('create');
    });

    Route::get('questions', 'AssignmentQuestionAnswerController@allQuestionAnswer')->name('all-question');
    
    Route::get('create/faq',function () {
        return view('support_faq.create_faq');
    })->name('support-show');
    
    Route::get('manage-support-faq', 'SupportAboutFaqController@manageFaqSupport')->name('support-manage-fq');
    
    Route::get('create-about',function () {
        return view('about_us.create_about');
    })->name('show_about'); 
    
    Route::get('edit-about/{id}', 'SupportAboutFaqController@editAbout')->name('edit_about'); 
    Route::get('delete-about/{id}', 'SupportAboutFaqController@delAbout')->name('del_about');   
    Route::get('manage-about', 'SupportAboutFaqController@manageAbout')->name('manage_about');
    Route::get('update-support', 'SupportAboutFaqController@updateSupportsQuestion')->name('update-support');
    Route::get('remove-support-faq/{id}', 'SupportAboutFaqController@removeSupportsQuestion')->name('support-remove');
    Route::post('create-support-faq', 'SupportAboutFaqController@createFaq')->name('savefaq-support');
    Route::get('manage-support', 'SupportAboutFaqController@updateFaqSupport')->name('support-faq');
    Route::post('create-about-us', 'SupportAboutFaqController@createAbout')->name('create_about');
    Route::post('update-about', 'SupportAboutFaqController@updateAbout')->name('update_about');
    Route::get('assignment/edit/{id}', 'AssignmentQuestionAnswerController@editAssignment')->name('edit');
    Route::get('answer/{id}','AssignmentQuestionAnswerController@removeAnswer')->name('answer-remove');
    Route::post('answer/update','AssignmentQuestionAnswerController@answerUpdate')->name('answer-update');
    Route::post('question/update','AssignmentQuestionAnswerController@quetionUpdate')->name('question-update');
    Route::get('question/{id}','AssignmentQuestionAnswerController@removeQuestion')->name('question-remove');
    Route::get('assignment/{id}','AssignmentQuestionAnswerController@cancelAssignment')->name('assignment-cancel');
    Route::resource('card', 'Resource\CardResource');
});

Route::get('/insert/quiz', 'QuizCategoryController@insertQuiz');
Route::get('/dashboard/manage/quiz', 'QuizCategoryController@allQuizzes');
Route::get('/dashboard/quiz/list/{id}', 'QuizCategoryController@selectQuizById');
Route::get('/dashboard/quiz/edit/{id}', 'QuizCategoryController@quizEdit');
Route::get('/update/quiz/', 'QuizCategoryController@updateQuiz');

//Public assets for app
Route::resource('asset', 'Resource\AssetResource');

//Public assets for app
Route::get('/loader', 'Resource\AssetResource@viewLoader');

//Policies
Route::get('/tnc', function() {
    // $file = base_path(). "/public/assets/others/policy/tnc.pdf";

    // $name = "tnc.pdf";

    // $headers = ['Content-Type: application/pdf'];

    // return response()->file($file,$headers);
    return view('policy.tnc');
});

Route::get('/privacy-policy', function() {
    // $file = base_path(). "/public/assets/others/policy/privacy-policy.pdf";

    // $name = "privacy-policy.pdf";

    // $headers = ['Content-Type: application/pdf'];

    // return response()->file($file,$headers);
    return view('policy.policy');
});

//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

// Storage link
Route::get('/storage-link', function () {
    Artisan::call('storage:link');
    return '<h1>Storage link created</h1>';
});

// Vendor publish
Route::get('/paypal-vendor-publish', function () {
    Artisan::call('vendor:publish --provider "Srmklive\PayPal\Providers\PayPalServiceProvider"');
    return '<h1>Paypal vendor published</h1>';
});
// Text formate publish
Route::get('/ckeditor-vendor-publish', function () {
    Artisan::call('vendor:publish --tag=ckeditor');
    return '<h1>Ckeditor vendor published</h1>';
});